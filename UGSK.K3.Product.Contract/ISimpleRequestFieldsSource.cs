﻿using System;
using System.Collections.Generic;
using System.Reflection;
namespace UGSK.K3.Product.Contract
{
    public interface ISimpleRequestFieldsSource<T> where T : IRequest
    {
        IEnumerable<PropertyInfo> GetFields();
        IEnumerable<string> GetQualifiedFieldNames();
        Func<T, object> GetPropertyGetter(string propertyName);
    }
}
