﻿using System.Collections.Generic;
using System.Linq;

namespace UGSK.K3.Product.Contract
{
    public class FirstHavingSelectionRule : ISelectionRule<IRequest>
    {
        public int Priority { get; set; }

        public bool NeedStateChanging
        {
            get { return false; }
        }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            return sourceCollection.ToDictionary(k => k, v => true); // по идее, вообще не должен быть вызван
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedValues)
        {
            return false; // потому что это правило ничего не может заблокировать
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            if (sourceCollection.Any())
            {
                return new[] { sourceCollection.First() };
            }
            return Enumerable.Empty<SimpleEntity>();
        }
    }
}