﻿using System;
using System.Collections.Generic;

namespace UGSK.K3.Product.Contract
{
    /// <summary>
    /// Правило выборки, не запрещающее ничего
    /// </summary>
    public abstract class UndenierSelectionRule : ISelectionRule<IRequest>
    {
        public bool NeedStateChanging { get; private set; }
        public int Priority { get; set; }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            throw new NotImplementedException();
        }

        public virtual bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> currentValue)
        {
            return false;
        }

        public abstract IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request);
    }
}