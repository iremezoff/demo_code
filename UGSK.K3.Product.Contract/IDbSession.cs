﻿namespace UGSK.K3.Product.Contract
{
    public interface IDbSession
    {
        string TariffVersion { get; }
        string TariffDate { get; }
    }
}