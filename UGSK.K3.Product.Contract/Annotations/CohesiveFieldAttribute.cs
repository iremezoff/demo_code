﻿using System;

namespace UGSK.K3.Product.Contract.Annotations
{
    /// <summary>
    /// Атрибут, описывающий, что поля сущности связаын внешней таблицей (HDBK)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
    public class CohesiveFieldAttribute : FieldAttributeBase
    {
        /// <summary>
        /// Таблица (HDBK), описывающая связь
        /// </summary>
        public Type CohesiveSourceType { get; private set; }

        public string CohesiveTableColumnName { get; set; } // todo Ремезов: не реализовано

        public string GrouperPropertyName { get; set; }

        public bool DoNotSort { get; set; }
        
        /// <summary>
        /// Имя поля в HDBK
        /// </summary>
        public string ValuePropertyName { get; set; }

        public int Order { get; set; }

        public CohesiveFieldAttribute(Type сohesiveSourceType)
        {
            CohesiveSourceType = сohesiveSourceType;
            Order = 1;
            AttributePriority = 9;
        }
    }
}