﻿using System;

namespace UGSK.K3.Product.Contract.Annotations
{
    public class FieldAttributeBase : Attribute
    {
        /// <summary>
        /// Определяет приоритет вызова ограничителей, связанных с атрибутами
        /// </summary>
        public int AttributePriority { get; protected set; } 
    }
}