﻿using System;

namespace UGSK.K3.Product.Contract.Annotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
    public class FlexibleFieldAttribute : FieldAttributeBase
    {
        public int Priority { get; set; }

        public Type SpecificationType { get; private set; }
        
        public string[] ObservedFields { get; set; }

        public bool Strict { get; set; }

        public FlexibleFieldAttribute(Type specificationType, int priority = 0)
        {
            SpecificationType = specificationType;
            Priority = priority;
            AttributePriority = 8;
            Strict = true;
        }
    }
}
