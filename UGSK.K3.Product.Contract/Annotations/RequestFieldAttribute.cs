﻿using System;

namespace UGSK.K3.Product.Contract.Annotations
{
    /// <summary>
    /// Атрибут, описывающий зависимое поле класса (чаще запроса).
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class RequestFieldAttribute : FieldAttributeBase
    {
        public string[] DependsOnFields { get; set; }

        public Type SourceType { get; private set; }

        public string ValuePropertyName { get; set; } // todo Ремезов: не реализовано

        public bool DoNotRequireGroup { get; set; }

        public bool DoNotDerive { get; set; }

        public Type Converter { get; set; }

        /// <summary>
        /// Данный атрибут работает только с репозиторными типами
        /// </summary>
        public string SortColumn { get; set; }

        public RequestFieldAttribute(Type sourceType, string valuePropertyName = "Name")
        {
            SourceType = sourceType;
            ValuePropertyName = valuePropertyName;
            AttributePriority = 10;
        }
    }


    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class PassToDependentIfNull : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class NullingOnParentChanged : Attribute
    {
    }


    
}