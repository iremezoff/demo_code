﻿using System;

namespace UGSK.K3.Product.Contract.Annotations
{
    /// <summary>
    /// Отвечает за задание правила выборки значения из списка допустимых
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
    public class FieldSelectorAttribute : Attribute
    {
        public Type RuleType { get; private set; }
        public int Priority { get; private set; }

        public FieldSelectorAttribute(Type ruleType, int priority)
        {
            RuleType = ruleType;
            Priority = priority;
        }
    }
}
