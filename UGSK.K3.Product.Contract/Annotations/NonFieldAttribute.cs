﻿using System;

namespace UGSK.K3.Product.Contract.Annotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class NonFieldAttribute : Attribute 
    { }
}
