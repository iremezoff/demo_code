﻿namespace UGSK.K3.Product.Contract
{
    /// <summary>
    /// Предназначен для фильтрации данных, уже расположенных в памяти приложения, а не во внешнем источнике, таком как БД. 
    /// Для формирования выражения с трансляцией в sql используется ISimpleSpecification
    /// </summary>
    /// <typeparam name="T">Тип данных</typeparam>
    public interface ISpecification<in T>
    {
        bool IsSatisfiedBy(T request);
    }
}