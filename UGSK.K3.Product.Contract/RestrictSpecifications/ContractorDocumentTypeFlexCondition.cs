﻿using System;
using System.Linq.Expressions;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.Contract.RestrictSpecifications
{
    public abstract class ContractorDocumentTypeFlexCondition<TRequest> : IFlexCondition<TRequest, GenericSimpleEntity<ContractorDocumentType>>
        where TRequest : IRequest
    {
        protected const int LegalPersonDocumentId = 62;
        protected const int RussianCitizenPasportId = 12;

        public abstract Expression<Func<GenericSimpleEntity<ContractorDocumentType>, bool>> IsSatisfiedBy(TRequest request);
    }
}
