﻿using System;
using System.Linq.Expressions;

namespace UGSK.K3.Product.Contract
{
    public class EverTrueCondition : IFlexCondition<IRequest, SimpleEntity>
    {
        public Expression<Func<SimpleEntity, bool>> IsSatisfiedBy(IRequest request)
        {
            return factor => true;
        }
    }
}