﻿using System;
using System.Linq.Expressions;

namespace UGSK.K3.Product.Contract
{
    public interface IFlexCondition<in TRequest, TEntity>
        where TRequest : IRequest
        where TEntity : SimpleEntity
    {
        Expression<Func<TEntity, bool>> IsSatisfiedBy(TRequest request);
    }
}
