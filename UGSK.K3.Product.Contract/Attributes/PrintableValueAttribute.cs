﻿using System;

namespace UGSK.K3.Product.Contract.Attributes
{
    /// <summary>
    /// Атрибут, обозначающий признак вывода на печать, вывода в чек.
    /// Предназначен для опознавания свойств, которые необходимо выводить на печать
    /// чека, листа расчёта, подробностей расчета
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PrintableValueAttribute : Attribute
    {
        public string Name { get; private set; }
        public string Abbr { get; private set; }
        public int SortOrder { get; private set; }
        public bool IsUnderwriter { get; private set; }
        public PrintableValueAttribute(string abbr, string name, int sortOrder = int.MaxValue, bool isUnderwriter = true)
        {
            Name = name;
            Abbr = abbr;
            SortOrder = sortOrder;
            IsUnderwriter = isUnderwriter;
        }
    }
}
