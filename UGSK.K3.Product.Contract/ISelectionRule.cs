﻿using System.Collections.Generic;

namespace UGSK.K3.Product.Contract
{
    public interface ISelectionRule<in T> where T : IRequest
    {
        /// <summary>
        /// Управляет необходимостью изменения флага доступности для каждого элемента путем вызова метода GetCollectionSourceWithAllowable
        /// </summary>
        bool NeedStateChanging { get; }
        int Priority { get; set; }

        IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, T request);
        bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> currentValue);
        IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, T request); // todo Ремезов: перевести на возврат делегата
    }
}
