﻿using System;

namespace UGSK.K3.Product.Contract
{
    public interface ICalculator
    {
        string ProductName { get; }
        IDbSession DbSession { get; }
        IResponse Calculate(IRequest request);
        Type RequestType { get; }
    }

    public interface ICalculator<in T> : ICalculator where T : IRequest
    {
        IResponse Calculate(T request);
    }
}