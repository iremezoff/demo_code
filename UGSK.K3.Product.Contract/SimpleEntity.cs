﻿using System;

namespace UGSK.K3.Product.Contract
{
    [Serializable]
    public abstract class SimpleEntity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }

    // todo Ремезов: убрать
    public class GenericSimpleEntity<T> : SimpleEntity
    {
        public T Value { get; set; }

        public GenericSimpleEntity() { }

        public GenericSimpleEntity(T value)
        {
            Value = value;            
        }
    }
}