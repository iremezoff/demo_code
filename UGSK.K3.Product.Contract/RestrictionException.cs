﻿using System;

namespace UGSK.K3.Product.Contract
{
    /// <summary>
    /// Исключение возникающее в библиотеке если не выполняется 
    /// проверка на ограничение (условное ограничение по коэффициентам или другим величинам)
    /// </summary>
    public class RestrictionException : ApplicationException
    {
        public RestrictionException()
            : base()
        {
        }
        public RestrictionException(string message)
            : base(message)
        {
        }

        public RestrictionException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
