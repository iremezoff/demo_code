﻿using System;
using UGSK.K3.Infrastructure;
namespace UGSK.K3.Product.Contract.Verification
{
    public sealed class FieldVerificationError
    {
        public FieldVerificationError(string name, Type fieldType, object originalValue, object newValue)
        {
            Name = name;
            FieldType = fieldType;
            OriginalValue = originalValue;
            NewValue = newValue;
        }

        public string Name { get; private set; }
        public Type FieldType { get; private set; }
        public object OriginalValue { get; private set; }
        public object NewValue { get; private set; }

        public override string ToString()
        {
            if (NewValue == null && OriginalValue == null)
                return string.Format("Field: {0}", Name);

            return string.Format("Field: {0}, Actual: {1}, Expected: {2}", Name, StringifyField(NewValue), StringifyField(OriginalValue));
        }

        private string StringifyField(object value)
        {
            if (value != null)
            {
                var actualFieldType = value.GetType();

                if (actualFieldType.GetMethod("ToString", new Type[] { }).DeclaringType == actualFieldType ||
                    NewValue is ValueEntity)
                {
                    return value.ToString();
                }
            }
            return "null";
        }
    }
}
