﻿using System;
using System.Linq;

namespace UGSK.K3.Product.Contract.Verification
{
    public class CalculationFieldsVerifier<T> : ISimpleRequestVerifier<IRequest> where T : IRequest
    {
        private readonly ISimpleRequestFieldsSource<T> _extractor;

        public CalculationFieldsVerifier(ISimpleRequestFieldsSource<T> extractor)
        {
            _extractor = extractor;
        }

        /// <summary>
        /// Определяет изменился ли SimpleRequest при продаже из статуса "Возврат на корректировку".
        /// </summary>
        /// <param name="actual">SimpleRequest, полученный от фронта, предположительно может содержать изменения</param>
        /// <param name="expected">текущий вариант SimpleRequest, сохраненный в БД</param>
        /// <returns>результат верификации</returns>
        public SimpleRequestVerificationState Verify(IRequest actual, IRequest expected)
        {
            if (actual == null || expected == null)
                throw new ArgumentNullException();

            return _extractor.GetFields().Aggregate(new SimpleRequestVerificationState(), (state, property) =>
            {
                var getter = _extractor.GetPropertyGetter(property.Name);

                object actualValue = getter((T)actual);
                object expectedValue = getter((T)expected);


                if (!object.Equals(actualValue, expectedValue))
                {
                    state.Add(property.Name, property.PropertyType, actualValue, expectedValue);
                }
                return state;
            });
        }
    }
}
