﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UGSK.K3.Product.Contract.Verification
{
    public sealed class SimpleRequestVerificationState : IEnumerable<FieldVerificationError>
    {
        private readonly List<FieldVerificationError> _errors = new List<FieldVerificationError>();

        public SimpleRequestVerificationState Add(string fieldName, Type fieldType, object originalValue, object newValue)
        {
            _errors.Add(new FieldVerificationError(fieldName, fieldType, originalValue, newValue));
            return this;
        }

        public bool IsValid
        {
            get
            {
                return !_errors.Any();
            }
        }

        public IEnumerator<FieldVerificationError> GetEnumerator()
        {
            return _errors.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_errors).GetEnumerator();
        }
    }
}
