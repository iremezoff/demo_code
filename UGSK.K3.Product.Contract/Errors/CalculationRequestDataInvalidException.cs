using System;
using System.Collections.Generic;
using FluentValidation.Results;

namespace UGSK.K3.Product.Contract.Errors
{
    public class InvalidCalculationRequestException : ApplicationException
    {
        public IList<ValidationResult> Errors { get; private set; }
        public IRequest Request { get; set; }

        public InvalidCalculationRequestException(string message, IList<ValidationResult> errors, IRequest request = null)
            : base(message)
        {
            Errors = errors;
            Request = request;
        }

        public InvalidCalculationRequestException(string message, Exception innerException)
            : base(message, innerException)
        {
        //Request = request;
            Errors=new List<ValidationResult>() {new ValidationResult(innerException.Message)};

        }
    }
}