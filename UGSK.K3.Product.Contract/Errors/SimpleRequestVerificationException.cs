﻿using System;
using UGSK.K3.Product.Contract.Verification;

namespace UGSK.K3.Product.Contract.Errors
{
    public class SimpleRequestVerificationException : ApplicationException
    {
        public SimpleRequestVerificationException(string message, SimpleRequestVerificationState state) : base(message)
        {
            State = state;
        }

        public SimpleRequestVerificationState State { get; private set; }
    }
}
