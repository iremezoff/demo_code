﻿using System;

namespace UGSK.K3.Product.Contract.Errors
{
    [Serializable]
    public class CreateDublicateContractException : ApplicationException
    {
        public string RedirectLocation { get; set; }

        public CreateDublicateContractException(string message, string redirectLocation) : base(message) { RedirectLocation = redirectLocation; }
    }
}
