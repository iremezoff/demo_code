﻿using UGSK.K3.Product.Contract.Verification;

namespace UGSK.K3.Product.Contract
{
    public interface ISimpleRequestVerifier<in T> where T : IRequest
    {
        SimpleRequestVerificationState Verify(T actual, T expected);
    }
}
