﻿using System;
using System.Collections.Generic;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.Contract.SelectionRules
{
    public abstract class BasePaymentIsCashSelectionRule : ISelectionRule<IRequest>
    {
        public bool NeedStateChanging { get { return false; } }

        public int Priority { get; set; }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            throw new NotImplementedException();
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> currentValue)
        {
            return false;
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection,
            IRequest request)
        {
            return new[] 
            { 
                new GenericSimpleEntity<bool>() 
                { 
                    Value = PaymentCode(request) == PaymentMethodCode.Cash
                } 
            };
        }

        protected abstract PaymentMethodCode PaymentCode(IRequest request);

    }

}
