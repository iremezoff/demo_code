﻿using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.Contract.SelectionRules
{
    public class ContractorDocumentTypeSelectionRule : ISelectionRule<IRequest>
    {
        private readonly int _defaultDocumentId = 12;        
        public bool NeedStateChanging
        {
            get { return false; }
        }

        public int Priority { get; set; }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            throw new NotImplementedException();
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> currentValue)
        {
            return false;
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {

            if (sourceCollection.Any())
            {
                var generic = sourceCollection.OfType<GenericSimpleEntity<ContractorDocumentType>>();
                if (generic == null)
                    return Enumerable.Empty<SimpleEntity>();
                
                var val = generic.SingleOrDefault(v => v.Value.Id == _defaultDocumentId);

                if (val == null)
                    return Enumerable.Empty<SimpleEntity>();
                
                return new[] { val };
            }
            return Enumerable.Empty<SimpleEntity>();
        }
    }
}
