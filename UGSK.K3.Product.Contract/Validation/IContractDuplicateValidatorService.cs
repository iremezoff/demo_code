﻿namespace UGSK.K3.Product.Contract.Validation
{
    public interface IContractDuplicationValidatorService<TContract>
    {
        bool HasSinedDuplication(TContract contract);
    }
}
