﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.Contract.Validation
{
    public static class ContractExistanceHelper
    {
        public static bool DoesNewContractDuplicateExisted(InsuranceContract contract, IQueryable<InsuranceContract> query)
        {
            if (contract.PolicySerial == null)
                return true;
            return !query.Any(v =>
                v.Id != contract.Id &&
                v.PolicyNumber == contract.PolicyNumber &&
                v.PolicySerial != null &&
                v.PolicySerial.Id == contract.PolicySerial.Id);
        }
    }
}
