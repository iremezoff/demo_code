using System;
using System.Collections.Generic;
using UGSK.K3.Infrastructure;

namespace UGSK.K3.Product.Contract
{
    public interface IFieldValueConverter
    {
        Type SourceType { get; }
        Type ExpectedType { get; }

        IEnumerable<KeyValuePair<object, bool>> Convert(IDictionary<SimpleEntity, bool> source, IRequest request);
        object Convert(Entity<int> entity);
    }
}