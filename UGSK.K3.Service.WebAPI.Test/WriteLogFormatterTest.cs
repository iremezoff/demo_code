﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace UGSK.K3.Service.WebAPI.Test
{
    [TestFixture]
    public class WriteLogFormatterTest
    {
        [Test]
        public void InvalidCalculationRequestExceptionFormatterTest()
        {
            string expected = @"AdditionalInfo: {""AdditionalInfo"":null,""InvalidCalculationErrors"":""\r\n{ ErrorMessage: Message1, MemberNames: MemberNames: { Member1-1, Member1-2 } }\r\n{ ErrorMessage: Message2, MemberNames: MemberNames: { Member2-1, Member2-2 } }\r\n""}";

            var target = new UGSK.K3.Service.WebAPI.Logging.WriteLogFormatters.InvalidCalculationRequestExceptionFormatter();
            var ex = new UGSK.K3.Product.Contract.Errors.InvalidCalculationRequestException("ErrorMessage",
                new List<System.ComponentModel.DataAnnotations.ValidationResult> 
                {
                    new System.ComponentModel.DataAnnotations.ValidationResult("Message1", new List<string>{"Member1-1, Member1-2"}),
                    new System.ComponentModel.DataAnnotations.ValidationResult("Message2", new List<string>{"Member2-1, Member2-2"})
                },
                null);
            var context = new HttpRequestContext();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/");
            request.Content = new StringContent("test string content");
            context.Principal = new System.Security.Claims.ClaimsPrincipal();
            var result = target.Format(typeof(UGSK.K3.Product.Contract.Errors.InvalidCalculationRequestException), ex, request, context, null, null);

            Assert.True(result.Contains(expected));
        }
    }
}
