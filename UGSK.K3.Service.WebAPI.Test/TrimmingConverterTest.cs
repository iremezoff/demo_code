﻿using Newtonsoft.Json;
using NUnit.Framework;
using System.Collections.Generic;
using UGSK.K3.Service.DTO;
using UGSK.K3.Service.WebAPI.Converting;

namespace UGSK.K3.Service.WebAPI.Test
{
    [TestFixture]
    class TrimmingConverterTest
    {
        [Test]
        public void TrimmingSuccess()
        {
            var contract = new ContractDTO
            {
               InsuredPersonFirstName = " test ",
               InsuredPersonLastName = "testov ",
               InsuredPersonMiddleName = " testovich",
               
            };

            var json = JsonConvert.SerializeObject(contract);

            var excludePropertiesFromTrimming = new List<string> { "InsuredPersonFirstName" };
            var actual = JsonConvert.DeserializeObject<ContractDTO>(json, new TrimmingConverter(excludePropertiesFromTrimming));

            var expectedFirstName = "test";
            var expectedLastName = "testov";
            var expectedMiddleName = "testovich";
            var expectedFranchiseAmount = " ";

            Assert.AreEqual(expectedFirstName, actual.InsuredPersonFirstName);
            Assert.AreEqual(expectedLastName, actual.InsuredPersonLastName);
            Assert.AreEqual(expectedMiddleName, actual.InsuredPersonMiddleName);
            Assert.AreEqual(expectedFranchiseAmount, actual.InsuredPersonFirstName);
        }
    }
}
