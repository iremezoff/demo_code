﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=301879
  -->
<configuration>
  <configSections>
    <section name="appConfig" type="UGSK.K3.Service.WebAPI.ConfigFile.AppConfigConfiguration, UGSK.K3.Service.WebAPI" />
    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog" />
    <section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
    <section name="bugsnagConfig" type="Bugsnag.ConfigurationStorage.ConfigSection, Bugsnag" />
    <section name="logRouter" type="UGSK.K3.Service.WebAPI.ConfigFile.LogRouterConfig.LogRouterConfiguration, UGSK.K3.Service.WebAPI, Version=2.13.1.754, Culture=neutral" />
    
  </configSections>
  <appSettings>
    <add key="webpages:Version" value="3.0.0.0" />
    <add key="webpages:Enabled" value="false" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
  </appSettings>
  <bugsnagConfig apiKey="87a40b8c919290e9fd08cba915effb29" />
  
  <connectionStrings>
  </connectionStrings>
  <system.web>
    <compilation debug="true" targetFramework="4.5" />
    <httpRuntime targetFramework="4.5" />
  </system.web>
  <nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" autoReload="true">
    <variable name="logDirectory" value="c:/Logs/integration" />
    <variable name="AppName" value="UGSK.K3.Service.WebAPI" />
    <variable name="GeneralLogFileName" value="${shortdate}.log" />
    <variable name="BlackListLogFileName" value="${shortdate}.log" />
    <variable name="UserSpaceLogFileName" value="${shortdate}_UserSpace.log" />
    <targets>
      <target name="GeneralLogFile" xsi:type="File" autoFlush="true" createDirs="true" fileName="${logDirectory}/${AppName}/${GeneralLogFileName}" encoding="UTF-8" keepFileOpen="true" />
      <target name="BlackListLogFile" xsi:type="File" archiveAboveSize="1000000" autoFlush="true" createDirs="true" fileName="${logDirectory}/${AppName}/BlackList/${BlackListLogFileName}" encoding="UTF-8" keepFileOpen="true" maxArchiveFiles="10" />
      <target name="WebApiUserSpaceLogFile" xsi:type="File" autoFlush="true" createDirs="true" fileName="${logDirectory}/${AppName}/${UserSpaceLogFileName}" encoding="UTF-8" keepFileOpen="true" />
      <target name="PVSLogFile" xsi:type="File" archiveAboveSize="1000000" autoFlush="true" createDirs="true" fileName="${logDirectory}/${AppName}/PVS/${GeneralLogFileName}" encoding="UTF-8" keepFileOpen="true" maxArchiveFiles="10" />
      <target name="MessageQueueLogFile" xsi:type="File" archiveAboveSize="1000000" autoFlush="true" createDirs="true" fileName="${logDirectory}/${AppName}/MessageQueue/${GeneralLogFileName}" encoding="UTF-8" keepFileOpen="true" maxArchiveFiles="10" />
      <target name="DbCommandsFile" xsi:type="File" autoFlush="true" createDirs="true" fileName="${logDirectory}/${AppName}/dbCommands.log" encoding="UTF-8" keepFileOpen="true" />
    </targets>
    <rules>
      <logger name="BlackList" minlevel="Info" writeTo="BlackListLogFile" final="true" />
      <logger name="NLogCommandInterceptor" final="true" writeTo="DbCommandsFile" />
      <logger name="WebApiUserSpaceLogPoint" minlevel="Info" writeTo="WebApiUserSpaceLogFile" final="true" />
      <logger name="PVS" minlevel="Info" writeTo="PVSLogFile" final="true" />
      <logger name="MessageQueue" minlevel="Info" writeTo="MessageQueueLogFile" final="true" />
      <logger name="*" minlevel="Trace" writeTo="GeneralLogFile" final="true" />
    </rules>
  </nlog>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-7.0.0.0" newVersion="7.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages.Razor" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http.Owin" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.2.0" newVersion="5.2.2.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http.WebHost" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.2.0" newVersion="5.2.2.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.5.2.14234" newVersion="1.5.2.14234" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.1.0.0" newVersion="2.1.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.OData.Edm" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-6.11.0.0" newVersion="6.11.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.OData.Core" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-6.11.0.0" newVersion="6.11.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Spatial" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-6.11.0.0" newVersion="6.11.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.2.0" newVersion="5.2.2.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.webServer>
    <modules runAllManagedModulesForAllRequests="true">
      <remove name="WebDAVModule" />
    </modules>
    <handlers>
      <remove name="OPTIONSVerbHandler" />
      <remove name="WebDAV" />
      <remove name="TRACEVerbHandler" />
      <remove name="ExtensionlessUrlHandler-Integrated-4.0" />
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*" verb="GET,HEAD,POST,PUT,DELETE" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" />
    </handlers>
    <validation validateIntegratedModeConfiguration="false" />
  </system.webServer>
  <entityFramework>
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.SqlConnectionFactory, EntityFramework" />
    <providers>
      <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer" />
    </providers>
  </entityFramework>
  <appConfig>
    <products>
      
    </products>
    <searchService host="devpro.ugsk.ru" port="9200">
      <types>
        <type name="indexbulkitem" indexName="fias" keyName="guid" resultType="UGSK.K3.Infrastructure.Search.SearchResult, UGSK.K3.Infrastructure.Search" />
        <type name="vehicle_class_item" indexName="vehicle_classifier" keyName="_id" resultType="UGSK.K3.AppServices.Tasks.Search.VehicleClassifier.VehicleClassifierSearchResult, UGSK.K3.AppServices.Tasks" />
      </types>
    </searchService>
    <logRouter>
      <routes>
        <route exceptionType="FluentValidation.ValidationException, FluentValidation" logPointName="WebApiUserSpaceLogPoint" />
        <route exceptionType="System.ApplicationException, mscorlib" logPointName="WebApiUserSpaceLogPoint" />
        <route exceptionType="System.Data.Entity.Validation.DbEntityValidationException, EntityFramework" logPointName="WebApiUserSpaceLogPoint" />
      </routes>
    </logRouter>
  </appConfig>
</configuration>
