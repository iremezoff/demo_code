﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO;
using UGSK.K3.Service.DTO.Shared;
using UGSK.K3.Service.WebAPI.Helpers.Tools;

namespace UGSK.K3.Service.WebAPI.Test
{
    [TestFixture]
    class FieldValueTypeBuilderTest
    {
        private FieldValueTypeBuilder _target;

        [SetUp]
        public void Init()
        {
            _target = new FieldValueTypeBuilder();
        }

        [Test]
        public void GetConvertedCollection_ListOfFieldValue_EmptyItemsOfFieldValue()
        {
            string fieldName = "Field";
            _target.RegisterType(typeof(FieldValueTestRequest).GetProperty(fieldName), typeof(FieldValueTestEntity), typeof(FieldValueTestEntity),
                                 (source, request) => source.Select(i => new KeyValuePair<object, bool>(i.Key, i.Value)));

            var dic = new Dictionary<SimpleEntity, bool> { { new FieldValueTestEntity { Name = "1" }, false } };

            var actual = _target.GetConvertedCollection(typeof(FieldValueTestRequest).GetProperty(fieldName), dic, new SimpleRequest());

            Assert.AreEqual(dic.Count, actual.Count());

            var actualItem = actual.First();

            CollectionAssert.IsEmpty(actualItem.CollectionSource);
        }

        [Test]
        public void GetConvertedCollection_ListOfFieldValue_ItemsOfFieldValueForSimpleEntity()
        {
            _target.RegisterType(typeof(FieldValueTestRequest).GetProperty("Field"), typeof(FieldValueTestEntity), typeof(FieldValueTestEntity),
                                 (source, request) => source.Select(i => new KeyValuePair<object, bool>(i.Key, i.Value)));
            _target.RegisterProperty(typeof(FieldValueTestRequest).GetProperty("Field"), "Name", "Field");

            var dic = new Dictionary<SimpleEntity, bool> { { new FieldValueTestEntity { Name = "1" }, false }, { new FieldValueTestEntity { Name = "2" }, false } };

            var actual = _target.GetConvertedCollection(typeof(FieldValueTestRequest).GetProperty("Field"), dic, new FieldValueTestRequest());

            Assert.AreEqual(dic.Count, actual.Count());

            CollectionAssert.AreEqual(dic.Values, actual.Select(i => i.State != FieldState.Disabled)); // false - значит Disabled
            CollectionAssert.AreEqual(dic.Select(i => "Field"), actual.Select(i => i.CollectionSource[0].FieldName));

            foreach (var value in actual)
            {
                Assert.AreEqual(1, value.CollectionSource.Count());
                var valFromDic = dic.Where(i => i.Key.Name == value.CollectionSource[0].Value);
                Assert.IsNotNull(valFromDic);
            }
        }

        [Test]
        public void GetConvertedCollection_ListOfFieldValue_ItemsOfFieldValueForNonSimpleEntity()
        {
            var dic = new Dictionary<SimpleEntity, bool> { { new FieldValueTestEntity { Name = "1" }, false }, { new FieldValueTestEntity { Name = "2" }, false } };

            var otherDic = dic.Select(i => new KeyValuePair<object, bool>(new OtherType { OtherField = i.Key.Name }, i.Value));

            _target.RegisterType(typeof(FieldValueTestRequest).GetProperty("Field"), typeof(FieldValueTestEntity), typeof(OtherType),
                                 (source, request) => otherDic);
            _target.RegisterProperty(typeof(FieldValueTestRequest).GetProperty("Field"), "OtherField", "Field");

            var actual = _target.GetConvertedCollection(typeof(FieldValueTestRequest).GetProperty("Field"), dic, new FieldValueTestRequest());

            Assert.AreEqual(dic.Count, actual.Count());

            CollectionAssert.AreEqual(dic.Values, actual.Select(i => i.State != FieldState.Disabled)); // false - значит Disabled
            CollectionAssert.AreEqual(dic.Select(i => "Field"), actual.Select(i => i.CollectionSource[0].FieldName));

            foreach (var value in actual)
            {
                Assert.AreEqual(1, value.CollectionSource.Count());
                var valFromDic = dic.Where(i => i.Key.Name == value.CollectionSource[0].Value);
                Assert.IsNotNull(valFromDic);
            }
        }

        [Test]
        public void GetConvertedCollection_ListOfFieldValue_ManyItemsOfFieldValueForNonSimpleEntity() // создание нескольких ValueItem в FieldValue со значением в свойстве FieldName, соответствующего вновь определенному
        {
            var dic = new Dictionary<SimpleEntity, bool> { { new FieldValueTestEntity { Name = "1" }, false }, { new FieldValueTestEntity { Name = "2" }, true } };

            var otherDic = dic.Select(i => new KeyValuePair<object, bool>(new OtherType { OtherField = i.Key.Name, Abrakadabra = i.Key.Name + "+" + i.Key.Name }, i.Value));

            _target.RegisterType(typeof(FieldValueTestRequest).GetProperty("Field"), typeof(FieldValueTestEntity), typeof(OtherType),
                                 (source, request) => otherDic);
            _target.RegisterProperty(typeof(FieldValueTestRequest).GetProperty("Field"), "OtherField", "Field");
            _target.RegisterProperty(typeof(FieldValueTestRequest).GetProperty("Field"), "Abrakadabra", "NonField");

            var actual = _target.GetConvertedCollection(typeof(FieldValueTestRequest).GetProperty("Field"), dic, new FieldValueTestRequest());

            Assert.AreEqual(dic.Count, actual.Count());

            CollectionAssert.AreEqual(dic.Values, actual.Select(i => i.State != FieldState.Disabled));  // false - значит Disabled
            CollectionAssert.AreEqual(dic.Select(i => "Field"), actual.Select(i => i.CollectionSource[0].FieldName));
            CollectionAssert.AreEqual(dic.Select(i => "NonField"), actual.Select(i => i.CollectionSource[1].FieldName));

            foreach (var value in actual)
            {
                Assert.AreEqual(2, value.CollectionSource.Count());
                var valFromDic = dic.Where(i => i.Key.Name == value.CollectionSource[0].Value);
                var valFromDic2 = dic.Where(i => i.Key.Name + "+" + i.Key.Name == value.CollectionSource[1].Value);
                Assert.IsNotNull(valFromDic);
                Assert.IsNotNull(valFromDic2);
            }
        }
    }


    public class FieldValueTestEntity : SimpleEntity
    {
    }

    public class FieldValueTestRequest : IRequest
    {
        public string Field { get; set; }
    }

    public class OtherType
    {
        public string OtherField { get; set; }
        public string Abrakadabra { get; set; }
    }
}
