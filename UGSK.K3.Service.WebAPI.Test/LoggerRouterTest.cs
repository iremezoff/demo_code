﻿using NLog;
using NLog.Config;
using NLog.Targets;
using NUnit.Framework;
using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using UGSK.K3.Product.Contract;
using UGSK.K3.Service.WebAPI.Logging;

namespace UGSK.K3.Service.WebAPI.Test
{
    [TestFixture]
    public class LoggerRouterTest
    {
        LoggerRouter logRouter;
        DebugTarget debugTargetUserSpace;
        DebugTarget debugTargetDefault;

        [SetUp]
        public void Init()
        {
            var config = new LoggingConfiguration();

            debugTargetUserSpace = new DebugTarget();
            config.AddTarget("UserSpaceLogTarget", debugTargetUserSpace);

            debugTargetDefault = new DebugTarget();
            config.AddTarget("DefaultTarget", debugTargetDefault);

            var ruleForUserSpace = new LoggingRule("WebApiUserSpaceLogPoint", LogLevel.Info, debugTargetUserSpace);
            ruleForUserSpace.Final = true;
            config.LoggingRules.Add(ruleForUserSpace);

            var ruleForDefaultLogPoint = new LoggingRule("*", LogLevel.Info, debugTargetDefault);
            ruleForUserSpace.Final = true;
            config.LoggingRules.Add(ruleForDefaultLogPoint);

            LogManager.Configuration = config;

            logRouter = LoggerRouter.Instance;
        }

        [TearDown]
        public void Clean()
        {
        }

        [TestCase(typeof(Exception), new object[] { "" }, TestName = "Exception_SaveToSystemLog")]
        [TestCase(typeof(OutOfMemoryException), new object[] { "" }, TestName = "OutOfMemoryException_SaveToSystemLog")]
        [TestCase(typeof(NullReferenceException), new object[] { "" }, TestName = "NullReferenceException_SaveToSystemLog")]
        public void TestDoWriteToSystemLogPoint(Type exceptionType, object[] exceptionArgs)
        {
            Exception exceptionInstance = (Exception)Activator.CreateInstance(exceptionType, exceptionArgs);

            string incidentId = "050915123456789";
            logRouter.Log(GetType(), exceptionInstance, new HttpRequestMessage(),
                    new HttpRequestContext(), new HttpResponseMessage(), incidentId, new { TestData1 = "TestData1" });

            Assert.AreEqual(1, debugTargetDefault.Counter);
            Assert.IsNotNullOrEmpty(debugTargetDefault.LastMessage);
            Assert.AreEqual(0, debugTargetUserSpace.Counter);
            Assert.IsNullOrEmpty(debugTargetUserSpace.LastMessage);
        }

        [TestCase(typeof(ApplicationException), new object[] { "" }, TestName = "EnrichException_SaveToUserSpaceLog")]
        [TestCase(typeof(UGSK.K3.Product.Contract.Errors.InvalidCalculationRequestException), new object[] { "", null,null}, TestName = "InvalidCalculationRequestException_SaveToUserSpaceLog")]
        [TestCase(typeof(UGSK.K3.Service.WebAPI.Converting.Formatters.InsufficientDataException), new object[] { "" }, TestName = "InsufficientDataException_SaveToUserSpaceLog")]
        [TestCase(typeof(RestrictionException), new object[] { "" }, TestName = "RestrictionException_SavToUserSpaceLog")]
        [TestCase(typeof(ApplicationException), new object[] { "" }, TestName = "ApplicationException_SaveToUserSpaceLog")]
        [TestCase(typeof(UGSK.K3.AppServices.DataDeriver.Errors.RequestFieldNotFoundException), new object[] { "" }, TestName = "RequestFieldNotFoundException_SaveToUserSpaceLog")]
        [TestCase(typeof(UGSK.K3.Service.WebAPI.Security.ImpersonateForbiddenException), new object[] { "", "", "" }, TestName = "ImpersonateForbiddenException_SaveToUserSpaceLog")]
        public void TestDoWriteToUserSpaceLogPoint(Type exceptionType, object[] exceptionArgs)
        {
            Exception exceptionInstance = (Exception)Activator.CreateInstance(exceptionType, exceptionArgs);

            string incidentId = "050915123456789";                                                                       
            logRouter.Log(GetType(), exceptionInstance, new HttpRequestMessage(),
                    new HttpRequestContext(), new HttpResponseMessage(), incidentId, new { TestData1 = "TestData1" });

            Assert.AreEqual(0, debugTargetDefault.Counter);
            Assert.IsNullOrEmpty(debugTargetDefault.LastMessage);
            Assert.AreEqual(1, debugTargetUserSpace.Counter);
            Assert.IsNotNullOrEmpty(debugTargetUserSpace.LastMessage);
        }
    }
}
