﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using UGSK.K3.USTC.Contract;
using UGSK.K3.USTC.Contract.Annotations;

namespace UGSK.K3.USTC.Model.Test
{
    [TestFixture]
    class RequestFieldBatlerTest
    {
        private RequestFieldBatler _target;
        private Mock<IFieldSourceRestrictor<FieldAttributeBase>> _firstRestrictor;
        private Mock<IFieldSourceRestrictor<FieldAttributeBase>> _secondRestrictor;
        private List<BatlerTestEntity> _mockColl;
        private PropertyInfo _property;
        private BatlerTestRequest _request;

        [SetUp]
        public void Init()
        {
            _mockColl = new List<BatlerTestEntity>();
            _request = new BatlerTestRequest();
            _firstRestrictor = new Mock<IFieldSourceRestrictor<FieldAttributeBase>>();
            _secondRestrictor = new Mock<IFieldSourceRestrictor<FieldAttributeBase>>();

            _property = typeof(BatlerTestRequest).GetProperty("Field");
            _target = new RequestFieldBatler(_property, _mockColl, new List<IFieldSourceRestrictor<FieldAttributeBase>>());
        }

        [Test]
        public void SimpleTest()
        {
            Assert.AreEqual(_property, _target.FieldProperty);
            Assert.AreEqual(_mockColl, _target.CollectionSource);

            CollectionAssert.AreEquivalent(_mockColl, _target.CollectionSource);
        }

        [Test]
        public void GetSourceCollection_SourceCollection_ReturnOriginalOnRestrictorsAreEmpty()
        {
            var actual = _target.GetSourceCollection(_request);

            CollectionAssert.AreEquivalent(_mockColl, actual);
        }

        [Test]
        public void GetSourceCollection_SourceCollection_ReturnRestrcitedOnSingleRestrictor()
        {
            _mockColl.AddRange(new List<BatlerTestEntity>() { new BatlerTestEntity(), new BatlerTestEntity(), new BatlerTestEntity() });

            var expected = new List<SimpleEntity>() { _mockColl[0], _mockColl[1] };

            _firstRestrictor.Setup(m => m.GetRestrictedSource(_mockColl, _request)).Returns(expected);

            _target = new RequestFieldBatler(_property, _mockColl, new List<IFieldSourceRestrictor<FieldAttributeBase>>() { _firstRestrictor.Object });
            var actual = _target.GetSourceCollection(_request);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.IsSubsetOf(actual, expected);
        }

        [Test]
        public void GetSourceCollection_SourceCollection_ReturnOnTwiceRestrictor()
        {
            _mockColl.AddRange(new List<BatlerTestEntity>() { new BatlerTestEntity(), new BatlerTestEntity(), new BatlerTestEntity() });

            var restrictedAfterFirst = new List<SimpleEntity>() { _mockColl[0], _mockColl[1] };

            var expected = new List<SimpleEntity>() { _mockColl[1] };

            _firstRestrictor.Setup(m => m.GetRestrictedSource(_mockColl, _request)).Returns(restrictedAfterFirst);

            _secondRestrictor.Setup(m => m.GetRestrictedSource(restrictedAfterFirst, _request)).Returns(expected);

            _target = new RequestFieldBatler(_property, _mockColl, new List<IFieldSourceRestrictor<FieldAttributeBase>>() { _firstRestrictor.Object, _secondRestrictor.Object });
            var actual = _target.GetSourceCollection(_request);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.IsSubsetOf(actual, expected);
        }
    }

    public class BatlerTestRequest:IRequest
    {
        [RequestField(typeof(BatlerTestEntity))]
        public string Field { get; set; }
    }

    public class BatlerTestEntity : SimpleEntity
    {
        public override string Name { get; set; }
    }


}
