﻿using System;
using System.Linq.Expressions;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Test.Mocks.Originals
{
    public class TestRequest2 : IRequest
    {
        public string Field2_1 { get; set; }

        public string Field2_2 { get; set; }

        public string Field2_3 { get; set; }

        public string Field2_4 { get; set; }
    }

    public class TestEntity2_1 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity2_2 : IdEntity
    {
        public string Field1 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity2_3 : IdEntity
    {
        public string Field2 { get; set; }
        public string Field4 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity2_4 : IdEntity
    {
        public override string Name { get; set; }
    }


    public class TestRequest : IRequest
    {
        public string Field1 { get; set; }

        public string Field2 { get; set; }

        public string Field3 { get; set; }

        public string Field4 { get; set; }

        public string Field5 { get; set; }

        public string Field6 { get; set; }

        public string Field7 { get; set; }

        public string Field8 { get; set; }

        public string Field9 { get; set; }

        public string Field10 { get; set; }

        public string Field11 { get; set; }

        public string Field12 { get; set; }

        public string Field13 { get; set; }

        public string Field14 { get; set; }

        public string Field15 { get; set; }

        public string Field16 { get; set; }

        public string Field17 { get; set; }

        public string Field18 { get; set; }

        public string Field19 { get; set; }

        public string Field20 { get; set; }

        public string Field21 { get; set; }

        public string Field22 { get; set; }

        public string Field23 { get; set; }

        public string Field24 { get; set; }

        public string Field25 { get; set; }

        public string Field26 { get; set; }

        public string Field27 { get; set; }

        public string Field28 { get; set; }

        public string Field29 { get; set; }

        public string Field30 { get; set; }

        public string Field31 { get; set; }

        public string Field32 { get; set; }

        public string Field33 { get; set; }

        public string Field34 { get; set; }

        public string Field35 { get; set; }

        public string Field36 { get; set; }

        public string Field37 { get; set; }

        public string Field38 { get; set; }

        public string Field39 { get; set; }

        public string Field40 { get; set; }

        public string Field41 { get; set; }

        public string Field42 { get; set; }

        public string Field43 { get; set; }

        public string Field44 { get; set; }

        public string Field45 { get; set; }

        public string Field46 { get; set; }

        public string Field47 { get; set; }

        public string Field48 { get; set; }
    }

    //public class TestDbSession : IDbSession
    //{
    //    public IEnumerable<TestEntity1> Collection1 { get; set; }
    //    public IEnumerable<TestEntity2> Collection2 { get; set; }
    //    public IEnumerable<TestEntity2> Collection3 { get; set; }
    //}

    public class IdEntity : SimpleEntity
    {
        public override int Id { get; set; }

        public int ParentId { get; set; }
    }

    public class TestEntity1 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity2 : IdEntity
    {
        public string Field1 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity3 : IdEntity
    {
        public string Field2 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity4 : IdEntity
    {
        public string Field3 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity5 : IdEntity
    {
        public string Field4 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity6 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity7 : IdEntity
    {
        public string Field6 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity8 : IdEntity
    {
        public string Field6 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity9 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity10 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity11 : IdEntity
    {
        public string Field9 { get; set; }
        public string Field10 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity12 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity13 : IdEntity
    {
        public string Field12 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity14 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity15 : IdEntity
    {
        public string Field14 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity16 : IdEntity
    {
        public string Field15 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity17 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity18 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity19 : IdEntity
    {
        public string Field17 { get; set; }
        public string Field18 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity20 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity21 : IdEntity
    {
        public string Field20 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity22 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity23 : IdEntity
    {
        public string Field22 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity24 : IdEntity
    {
        public string Field23 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity25 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity26 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity27 : IdEntity
    {
        public string Field25 { get; set; }
        public string Field26 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity28 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity29 : IdEntity
    {
        public string Field28 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity30 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity31 : IdEntity
    {
        public string Field30 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity32 : IdEntity
    {
        public string Field31 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity33 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity34 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity35 : IdEntity
    {
        public string Field33 { get; set; }
        public string Field34 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity36 : IdEntity
    {
        public string Field37 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity37 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestHDBK1 : IHDBK
    {
        public string TestEntity36 { get; set; }
    }

    public class TestEntity38 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity39 : IdEntity
    {
        public string Field40 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity40 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestHDBK2 : IHDBK
    {
        public string TestEntity38 { get; set; }
        public string TestEntity39 { get; set; }

        public string GroupValue { get; set; }
    }

    public class TestEntity41 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity42 : IdEntity
    {
        public string Field43 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity43 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestHDBK3 : IHDBK
    {
        public string TestEntity41 { get; set; }
        public string TestEntity42 { get; set; }
    }

    public class TestEntity44 : IdEntity
    {
        public string Field45 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity45 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity46 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestEntity47 : IdEntity
    {
        public string Field46 { get; set; }
        public override string Name { get; set; }
    }

    public class TestEntity48 : IdEntity
    {
        public override string Name { get; set; }
    }

    public class TestHDBK4 : IHDBK
    {
        public string TestEntity47 { get; set; }
        public string TestEntity48 { get; set; }
    }

    public class TestFlexCondition : IFlexCondition<TestRequest, TestEntity44>
    {
        public Expression<Func<TestEntity44, bool>> IsSatisfiedBy(TestRequest request)
        {
            return entity => entity.Name.EndsWith("2");
        }
    }

    public class TestGraphFlexCondition : IFlexCondition<GraphMapRequest, TestEntity44>
    {
        public Expression<Func<TestEntity44, bool>> IsSatisfiedBy(GraphMapRequest request)
        {
            return e => false;
        }
    }
}
