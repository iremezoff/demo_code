﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Test.Mocks.Originals
{
    public class GraphMapRequest : IRequest
    {
        [RequestField(typeof(TestEntity1))]
        public string Field1 { get; set; }

        [RequestField(typeof(TestEntity2), DependsOnFields = new[] { "Field1" })]
        public string Field2 { get; set; }

        [RequestField(typeof(TestEntity3))]
        public string Field3 { get; set; }

        [RequestField(typeof(TestEntity4))]
        public string Field4 { get; set; }

        [RequestField(typeof(TestEntity5), DependsOnFields = new[] { "Field3", "Field4" })]
        public string Field5 { get; set; }

        [RequestField(typeof(TestEntity6))]
        public string Field6 { get; set; }

        [RequestField(typeof(TestEntity7), DependsOnFields = new[] { "Field6" })]
        public string Field7 { get; set; }

        [RequestField(typeof(TestEntity8), DependsOnFields = new[] { "Field6" })]
        public string Field8 { get; set; }

        [CohesiveField(typeof(TestHDBK1), Order = 1)]
        [RequestField(typeof(TestEntity9))]
        public string Field9 { get; set; }

        [CohesiveField(typeof(TestHDBK1), Order = 2)]
        [RequestField(typeof(TestEntity10))]
        public string Field10 { get; set; }

        [CohesiveField(typeof(TestHDBK2), Order = 1)]
        [RequestField(typeof(TestEntity11))]
        public string Field11 { get; set; }

        [CohesiveField(typeof(TestHDBK2), Order = 2)]
        [RequestField(typeof(TestEntity12))]
        public string Field12 { get; set; }

        [RequestField(typeof(TestEntity44))]
        [FlexibleField(typeof(TestGraphFlexCondition))]
        public string Field44 { get; set; }
    }
}