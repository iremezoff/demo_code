﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using UGSK.K3.AppServices.DataDeriver.Test.Mocks.Originals;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Test.Mocks
{
    public interface ITestDbSession : IDbSession
    {
        IEnumerable<TestEntity1> Collection1 { get; set; }
        IEnumerable<TestEntity2> Collection2 { get; set; }
        IEnumerable<TestEntity3> Collection3 { get; set; }
        IEnumerable<TestEntity4> Collection4 { get; set; }
        IEnumerable<TestEntity5> Collection5 { get; set; }
        IEnumerable<TestEntity6> Collection6 { get; set; }
        IEnumerable<TestEntity7> Collection7 { get; set; }
        IEnumerable<TestEntity8> Collection8 { get; set; }
        IEnumerable<TestEntity9> Collection9 { get; set; }
        IEnumerable<TestEntity10> Collection10 { get; set; }
        IEnumerable<TestEntity11> Collection11 { get; set; }
        IEnumerable<TestEntity12> Collection12 { get; set; }
        IEnumerable<TestEntity13> Collection13 { get; set; }
        IEnumerable<TestEntity14> Collection14 { get; set; }
        IEnumerable<TestEntity15> Collection15 { get; set; }
        IEnumerable<TestEntity16> Collection16 { get; set; }
        IEnumerable<TestEntity17> Collection17 { get; set; }
        IEnumerable<TestEntity18> Collection18 { get; set; }
        IEnumerable<TestEntity19> Collection19 { get; set; }
        IEnumerable<TestEntity20> Collection20 { get; set; }
        IEnumerable<TestEntity21> Collection21 { get; set; }
        IEnumerable<TestEntity22> Collection22 { get; set; }
        IEnumerable<TestEntity23> Collection23 { get; set; }
        IEnumerable<TestEntity24> Collection24 { get; set; }
        IEnumerable<TestEntity25> Collection25 { get; set; }
        IEnumerable<TestEntity26> Collection26 { get; set; }
        IEnumerable<TestEntity27> Collection27 { get; set; }
        IEnumerable<TestEntity28> Collection28 { get; set; }
        IEnumerable<TestEntity29> Collection29 { get; set; }
        IEnumerable<TestEntity30> Collection30 { get; set; }
        IEnumerable<TestEntity31> Collection31 { get; set; }
        IEnumerable<TestEntity32> Collection32 { get; set; }
        IEnumerable<TestEntity33> Collection33 { get; set; }
        IEnumerable<TestEntity34> Collection34 { get; set; }
        IEnumerable<TestEntity35> Collection35 { get; set; }
        IEnumerable<TestEntity36> Collection36 { get; set; }
        IEnumerable<TestEntity37> Collection37 { get; set; }
        IEnumerable<TestEntity38> Collection38 { get; set; }
        IEnumerable<TestEntity39> Collection39 { get; set; }
        IEnumerable<TestEntity40> Collection40 { get; set; }
        IEnumerable<TestEntity41> Collection41 { get; set; }
        IEnumerable<TestEntity42> Collection42 { get; set; }
        IEnumerable<TestEntity43> Collection43 { get; set; }
        IEnumerable<TestEntity44> Collection44 { get; set; }
        IEnumerable<TestEntity45> Collection45 { get; set; }
        IEnumerable<TestEntity46> Collection46 { get; set; }
        IEnumerable<TestEntity47> Collection47 { get; set; }
        IEnumerable<TestEntity48> Collection48 { get; set; }
        IEnumerable<TestEntity2_1> Collection2_1 { get; set; }
        IEnumerable<TestEntity2_2> Collection2_2 { get; set; }
        IEnumerable<TestEntity2_3> Collection2_3 { get; set; }
        IEnumerable<TestEntity2_4> Collection2_4 { get; set; }

        IEnumerable<TestHDBK1> RestrictCollection1 { get; set; }
        IEnumerable<TestHDBK2> RestrictCollection2 { get; set; }
        IEnumerable<TestHDBK3> RestrictCollection3 { get; set; }
        IEnumerable<TestHDBK4> RestrictCollection4 { get; set; }
    }

    public class RepositoryPair
    {
        public IDictionary<int, SimpleEntity> Data { get; set; }
        public Type StoreType { get; set; }
    }

    public class RestrictorPair
    {
        public IDictionary<int, IHDBK> Data { get; set; }
        public Type StoreType { get; set; }
    }

    class MockDbSession : Mock<ITestDbSession>
    {
        public IDictionary<string, RepositoryPair> Repository { get; set; }
        public IDictionary<string, RestrictorPair> Restrictors { get; set; }

        public MockDbSession()
            : base(MockBehavior.Strict)
        {
            Repository = new Dictionary<string, RepositoryPair>();

            var collection1 = new Dictionary<int, SimpleEntity>();
            collection1.Add(collection1.Count, new TestEntity1 { Id = 0, Name = "Moscow" });
            collection1.Add(collection1.Count, new TestEntity1 { Id = 1, Name = "Spb" });
            Repository.Add("Field1", new RepositoryPair { Data = collection1, StoreType = typeof(TestEntity1) });

            var collection2 = new Dictionary<int, SimpleEntity>();
            collection2.Add(collection2.Count, new TestEntity2 { Id = 0, Field1 = "Moscow", Name = "Audi" });
            collection2.Add(collection2.Count, new TestEntity2 { Id = 1, Field1 = "Moscow", Name = "BMW" });
            collection2.Add(collection2.Count, new TestEntity2 { Id = 2, Field1 = "Spb", Name = "BMW" });
            Repository.Add("Field2", new RepositoryPair { Data = collection2, StoreType = typeof(TestEntity2) });

            var collection3 = new Dictionary<int, SimpleEntity>();
            collection3.Add(collection3.Count, new TestEntity3 { Id = 0, Name = "Moscow" });
            collection3.Add(collection3.Count, new TestEntity3 { Id = 1, Name = "Spb" });
            Repository.Add("Field3", new RepositoryPair { Data = collection3, StoreType = typeof(TestEntity3) });

            var collection4 = new Dictionary<int, SimpleEntity>();
            collection4.Add(collection4.Count, new TestEntity4 { Id = 0, Field3 = "Moscow", Name = "Audi" });
            collection4.Add(collection4.Count, new TestEntity4 { Id = 1, Field3 = "Moscow", Name = "BMW" });
            collection4.Add(collection4.Count, new TestEntity4 { Id = 2, Field3 = "Spb", Name = "BMW" });
            collection4.Add(collection4.Count, new TestEntity4 { Id = 3, Field3 = "Spb", Name = "Ford" });
            Repository.Add("Field4", new RepositoryPair { Data = collection4, StoreType = typeof(TestEntity4) });

            var collection5 = new Dictionary<int, SimpleEntity>();
            collection5.Add(collection5.Count, new TestEntity5 { Id = 0, Field4 = "Audi", Name = "A1" });
            collection5.Add(collection5.Count, new TestEntity5 { Id = 1, Field4 = "BMW", Name = "X3" });
            collection5.Add(collection5.Count, new TestEntity5 { Id = 2, Field4 = "BMW", Name = "X5" });
            collection5.Add(collection5.Count, new TestEntity5 { Id = 3, Field4 = "BMW", Name = "X6" });
            Repository.Add("Field5", new RepositoryPair { Data = collection5, StoreType = typeof(TestEntity5) });

            var collection6 = new Dictionary<int, SimpleEntity>();
            collection6.Add(collection6.Count, new TestEntity6 { Id = 0, Name = "Moscow" });
            collection6.Add(collection6.Count, new TestEntity6 { Id = 1, Name = "Spb" });
            Repository.Add("Field6", new RepositoryPair { Data = collection6, StoreType = typeof(TestEntity6) });

            var collection7 = new Dictionary<int, SimpleEntity>();
            collection7.Add(collection7.Count, new TestEntity7 { Id = 0, Field6 = "Moscow", Name = "Russia" });
            collection7.Add(collection7.Count, new TestEntity7 { Id = 1, Field6 = "Moscow", Name = "World" });
            collection7.Add(collection7.Count, new TestEntity7 { Id = 2, Field6 = "Spb", Name = "World" });
            Repository.Add("Field7", new RepositoryPair { Data = collection7, StoreType = typeof(TestEntity7) });

            var collection8 = new Dictionary<int, SimpleEntity>();
            collection8.Add(collection8.Count, new TestEntity8 { Id = 0, Field6 = "Moscow", Name = "Audi" });
            collection8.Add(collection8.Count, new TestEntity8 { Id = 1, Field6 = "Moscow", Name = "BMW" });
            collection8.Add(collection8.Count, new TestEntity8 { Id = 2, Field6 = "Spb", Name = "BMW" });
            Repository.Add("Field8", new RepositoryPair { Data = collection8, StoreType = typeof(TestEntity8) });

            var collection9 = new Dictionary<int, SimpleEntity>();
            collection9.Add(collection9.Count, new TestEntity9 { Id = 0, Name = "Moscow" });
            collection9.Add(collection9.Count, new TestEntity9 { Id = 1, Name = "Spb" });
            Repository.Add("Field9", new RepositoryPair { Data = collection9, StoreType = typeof(TestEntity9) });

            var collection10 = new Dictionary<int, SimpleEntity>();
            collection10.Add(collection10.Count, new TestEntity10 { Id = 0, Name = "RUR" });
            collection10.Add(collection10.Count, new TestEntity10 { Id = 1, Name = "USD" });
            Repository.Add("Field10", new RepositoryPair { Data = collection10, StoreType = typeof(TestEntity10) });

            var collection11 = new Dictionary<int, SimpleEntity>();
            collection11.Add(collection11.Count, new TestEntity11 { Id = 0, Field9 = "Moscow", Field10 = "RUR", Name = "Program1" });
            collection11.Add(collection11.Count, new TestEntity11 { Id = 1, Field9 = "Moscow", Field10 = "USD", Name = "Program1" });
            collection11.Add(collection11.Count, new TestEntity11 { Id = 2, Field9 = "Spb", Field10 = "USD", Name = "Program1" });
            collection11.Add(collection11.Count, new TestEntity11 { Id = 3, Field9 = "Moscow", Field10 = "RUR", Name = "Program2" });
            Repository.Add("Field11", new RepositoryPair { Data = collection11, StoreType = typeof(TestEntity11) });

            var collection12 = new Dictionary<int, SimpleEntity>();
            collection12.Add(collection12.Count, new TestEntity12 { Id = 0, Name = "Moscow" });
            collection12.Add(collection12.Count, new TestEntity12 { Id = 1, Name = "Spb" });
            Repository.Add("Field12", new RepositoryPair { Data = collection12, StoreType = typeof(TestEntity12) });

            var collection13 = new Dictionary<int, SimpleEntity>();
            collection13.Add(collection13.Count, new TestEntity13 { Id = 0, Field12 = "Moscow", Name = "Audi" });
            collection13.Add(collection13.Count, new TestEntity13 { Id = 1, Field12 = "Moscow", Name = "BMW" });
            collection13.Add(collection13.Count, new TestEntity13 { Id = 2, Field12 = "Moscow", Name = "Ford" });
            collection13.Add(collection13.Count, new TestEntity13 { Id = 3, Field12 = "Spb", Name = "BMW" });
            collection13.Add(collection13.Count, new TestEntity13 { Id = 4, Field12 = "Spb", Name = "Ford" });
            Repository.Add("Field13", new RepositoryPair { Data = collection13, StoreType = typeof(TestEntity13) });

            var collection14 = new Dictionary<int, SimpleEntity>();
            collection14.Add(collection14.Count, new TestEntity14 { Id = 0, Name = "Moscow" });
            collection14.Add(collection14.Count, new TestEntity14 { Id = 1, Name = "Spb" });
            Repository.Add("Field14", new RepositoryPair { Data = collection14, StoreType = typeof(TestEntity14) });

            var collection15 = new Dictionary<int, SimpleEntity>();
            collection15.Add(collection15.Count, new TestEntity15 { Id = 0, Field14 = "Moscow", Name = "Audi" });
            collection15.Add(collection15.Count, new TestEntity15 { Id = 1, Field14 = "Moscow", Name = "BMW" });
            collection15.Add(collection15.Count, new TestEntity15 { Id = 2, Field14 = "Spb", Name = "BMW" });
            collection15.Add(collection15.Count, new TestEntity15 { Id = 3, Field14 = "Spb", Name = "Ford" });
            Repository.Add("Field15", new RepositoryPair { Data = collection15, StoreType = typeof(TestEntity15) });

            var collection16 = new Dictionary<int, SimpleEntity>();
            collection16.Add(collection16.Count, new TestEntity16 { Id = 0, Field15 = "Audi", Name = "A1" });
            collection16.Add(collection16.Count, new TestEntity16 { Id = 1, Field15 = "BMW", Name = "X3" });
            collection16.Add(collection16.Count, new TestEntity16 { Id = 2, Field15 = "BMW", Name = "X5" });
            collection16.Add(collection16.Count, new TestEntity16 { Id = 3, Field15 = "Ford", Name = "Focus" });
            collection16.Add(collection16.Count, new TestEntity16 { Id = 4, Field15 = "Ford", Name = "Fiesta" });
            Repository.Add("Field16", new RepositoryPair { Data = collection16, StoreType = typeof(TestEntity16) });

            var collection17 = new Dictionary<int, SimpleEntity>();
            collection17.Add(collection17.Count, new TestEntity17 { Id = 0, Name = "Moscow" });
            collection17.Add(collection17.Count, new TestEntity17 { Id = 1, Name = "Spb" });
            Repository.Add("Field17", new RepositoryPair { Data = collection17, StoreType = typeof(TestEntity17) });

            var collection18 = new Dictionary<int, SimpleEntity>();
            collection18.Add(collection18.Count, new TestEntity18 { Id = 0, Name = "RUR" });
            collection18.Add(collection18.Count, new TestEntity18 { Id = 1, Name = "USD" });
            Repository.Add("Field18", new RepositoryPair { Data = collection18, StoreType = typeof(TestEntity18) });

            var collection19 = new Dictionary<int, SimpleEntity>();
            collection19.Add(collection19.Count, new TestEntity19 { Id = 0, Field17 = "Moscow", Field18 = "RUR", Name = "Program1" });
            collection19.Add(collection19.Count, new TestEntity19 { Id = 1, Field17 = "Spb", Field18 = "USD", Name = "Program1" });
            collection19.Add(collection19.Count, new TestEntity19 { Id = 2, Field17 = "Spb", Field18 = "RUR", Name = "Program2" });
            collection19.Add(collection19.Count, new TestEntity19 { Id = 3, Field17 = "Spb", Field18 = "RUR", Name = "Program3" });
            Repository.Add("Field19", new RepositoryPair { Data = collection19, StoreType = typeof(TestEntity19) });

            var collection20 = new Dictionary<int, SimpleEntity>();
            collection20.Add(collection20.Count, new TestEntity20 { Id = 0, Name = "Moscow" });
            collection20.Add(collection20.Count, new TestEntity20 { Id = 1, Name = "Spb" });
            Repository.Add("Field20", new RepositoryPair { Data = collection20, StoreType = typeof(TestEntity20) });

            var collection21 = new Dictionary<int, SimpleEntity>();
            collection21.Add(collection21.Count, new TestEntity21 { Id = 0, Field20 = "Moscow", Name = "BMW" });
            collection21.Add(collection21.Count, new TestEntity21 { Id = 1, Field20 = "Spb", Name = "BMW" });
            collection21.Add(collection21.Count, new TestEntity21 { Id = 2, Field20 = "Spb", Name = "Ford" });
            Repository.Add("Field21", new RepositoryPair { Data = collection21, StoreType = typeof(TestEntity21) });

            var collection22 = new Dictionary<int, SimpleEntity>();
            collection22.Add(collection22.Count, new TestEntity22 { Id = 0, Name = "Moscow" });
            collection22.Add(collection22.Count, new TestEntity22 { Id = 1, Name = "Spb" });
            Repository.Add("Field22", new RepositoryPair { Data = collection22, StoreType = typeof(TestEntity22) });

            var collection23 = new Dictionary<int, SimpleEntity>();
            collection23.Add(collection23.Count, new TestEntity23 { Id = 0, Field22 = "Moscow", Name = "BMW" });
            collection23.Add(collection23.Count, new TestEntity23 { Id = 1, Field22 = "Spb", Name = "BMW" });
            collection23.Add(collection23.Count, new TestEntity23 { Id = 2, Field22 = "Spb", Name = "Ford" });
            Repository.Add("Field23", new RepositoryPair { Data = collection23, StoreType = typeof(TestEntity23) });

            var collection24 = new Dictionary<int, SimpleEntity>();
            collection24.Add(collection24.Count, new TestEntity24 { Id = 0, Field23 = "BMW", Name = "X3" });
            collection24.Add(collection24.Count, new TestEntity24 { Id = 1, Field23 = "BMW", Name = "X5" });
            collection24.Add(collection24.Count, new TestEntity24 { Id = 2, Field23 = "Ford", Name = "Focus" });
            Repository.Add("Field24", new RepositoryPair { Data = collection24, StoreType = typeof(TestEntity24) });

            var collection25 = new Dictionary<int, SimpleEntity>();
            collection25.Add(collection25.Count, new TestEntity25 { Id = 0, Name = "Moscow" });
            collection25.Add(collection25.Count, new TestEntity25 { Id = 1, Name = "Spb" });
            Repository.Add("Field25", new RepositoryPair { Data = collection25, StoreType = typeof(TestEntity25) });

            var collection26 = new Dictionary<int, SimpleEntity>();
            collection26.Add(collection26.Count, new TestEntity26 { Id = 0, Name = "RUR" });
            Repository.Add("Field26", new RepositoryPair { Data = collection26, StoreType = typeof(TestEntity26) });

            var collection27 = new Dictionary<int, SimpleEntity>();
            collection27.Add(collection27.Count, new TestEntity27 { Id = 0, Field25 = "Moscow", Field26 = "RUR", Name = "Program1" });
            collection27.Add(collection27.Count, new TestEntity27 { Id = 1, Field25 = "Spb", Field26 = "RUR", Name = "Program1" });
            collection27.Add(collection27.Count, new TestEntity27 { Id = 3, Field25 = "Spb", Field26 = "RUR", Name = "Program2" });
            Repository.Add("Field27", new RepositoryPair { Data = collection27, StoreType = typeof(TestEntity27) });

            var collection28 = new Dictionary<int, SimpleEntity>();
            collection28.Add(collection28.Count, new TestEntity28 { Id = 0, Name = "Moscow" });
            collection28.Add(collection28.Count, new TestEntity28 { Id = 1, Name = "Spb" });
            Repository.Add("Field28", new RepositoryPair { Data = collection28, StoreType = typeof(TestEntity28) });

            var collection29 = new Dictionary<int, SimpleEntity>();
            collection29.Add(collection29.Count, new TestEntity29 { Id = 0, Field28 = "Moscow", Name = "Audi" });
            collection29.Add(collection29.Count, new TestEntity29 { Id = 1, Field28 = "Moscow", Name = "BMW" });
            collection29.Add(collection29.Count, new TestEntity29 { Id = 2, Field28 = "Spb", Name = "BMW" });
            Repository.Add("Field29", new RepositoryPair { Data = collection29, StoreType = typeof(TestEntity29) });

            var collection30 = new Dictionary<int, SimpleEntity>();
            collection30.Add(collection30.Count, new TestEntity30 { Id = 0, Name = "Moscow" });
            collection30.Add(collection30.Count, new TestEntity30 { Id = 1, Name = "Spb" });
            Repository.Add("Field30", new RepositoryPair { Data = collection30, StoreType = typeof(TestEntity30) });

            var collection31 = new Dictionary<int, SimpleEntity>();
            collection31.Add(collection31.Count, new TestEntity31 { Id = 0, Field30 = "Moscow", Name = "Audi" });
            collection31.Add(collection31.Count, new TestEntity31 { Id = 1, Field30 = "Moscow", Name = "BMW" });
            collection31.Add(collection31.Count, new TestEntity31 { Id = 2, Field30 = "Spb", Name = "BMW" });
            Repository.Add("Field31", new RepositoryPair { Data = collection31, StoreType = typeof(TestEntity31) });

            var collection32 = new Dictionary<int, SimpleEntity>();
            collection32.Add(collection32.Count, new TestEntity32 { Id = 0, Field31 = "Audi", Name = "A1" });
            collection32.Add(collection32.Count, new TestEntity32 { Id = 1, Field31 = "BMW", Name = "X3" });
            Repository.Add("Field32", new RepositoryPair { Data = collection32, StoreType = typeof(TestEntity32) });

            var collection33 = new Dictionary<int, SimpleEntity>();
            collection33.Add(collection33.Count, new TestEntity33 { Id = 0, Name = "Moscow" });
            collection33.Add(collection33.Count, new TestEntity33 { Id = 1, Name = "Spb" });
            Repository.Add("Field33", new RepositoryPair { Data = collection33, StoreType = typeof(TestEntity33) });

            var collection34 = new Dictionary<int, SimpleEntity>();
            collection34.Add(collection34.Count, new TestEntity34 { Id = 0, Name = "RUR" });
            Repository.Add("Field34", new RepositoryPair { Data = collection34, StoreType = typeof(TestEntity34) });

            var collection35 = new Dictionary<int, SimpleEntity>();
            collection35.Add(collection35.Count, new TestEntity35 { Id = 0, Field33 = "Moscow", Field34 = "RUR", Name = "Program1" });
            collection35.Add(collection35.Count, new TestEntity35 { Id = 1, Field33 = "Spb", Field34 = "RUR", Name = "Program2" });
            Repository.Add("Field35", new RepositoryPair { Data = collection35, StoreType = typeof(TestEntity35) });

            var collection36 = new Dictionary<int, SimpleEntity>();
            collection36.Add(collection36.Count, new TestEntity36() { Id = 0, Field37 = "Root", Name = "Program1" });
            collection36.Add(collection36.Count, new TestEntity36() { Id = 1, Field37 = "Root", Name = "Program2" });
            collection36.Add(collection36.Count, new TestEntity36() { Id = 2, Field37 = "Root", Name = "Program3" });
            Repository.Add("Field36", new RepositoryPair { Data = collection36, StoreType = typeof(TestEntity36) });

            var collection37 = new Dictionary<int, SimpleEntity>();
            collection37.Add(collection37.Count, new TestEntity37() { Id = 0, Name = "Root" });
            Repository.Add("Field37", new RepositoryPair { Data = collection37, StoreType = typeof(TestEntity37) });

            var collection38 = new Dictionary<int, SimpleEntity>();
            collection38.Add(collection38.Count, new TestEntity38() {Id = 0, Name = "Risk1"});
            collection38.Add(collection38.Count, new TestEntity38() {Id = 1, Name = "Risk2"});
            Repository.Add("Field38", new RepositoryPair { Data = collection38, StoreType = typeof(TestEntity38) });

            var collection39 = new Dictionary<int, SimpleEntity>();
            collection39.Add(collection39.Count, new TestEntity39() {Id = 0, Field40 = "Root", Name = "Remont1"});
            collection39.Add(collection39.Count, new TestEntity39() {Id = 1, Field40 = "Root", Name = "Remont2"});
            collection39.Add(collection39.Count, new TestEntity39() {Id = 2, Field40 = "Root", Name = "Remont3"});
            Repository.Add("Field39", new RepositoryPair { Data = collection39, StoreType = typeof(TestEntity39) });

            var collection40 = new Dictionary<int, SimpleEntity>();
            collection40.Add(collection40.Count, new TestEntity40() {Id = 0, Name = "Root"});
            Repository.Add("Field40", new RepositoryPair { Data = collection40, StoreType = typeof(TestEntity40) });

            var collection41 = new Dictionary<int, SimpleEntity>();
            collection41.Add(collection41.Count, new TestEntity41() { Id = 0, Name = "Program1" });
            collection41.Add(collection41.Count, new TestEntity41() { Id = 1, Name = "Program2" });
            Repository.Add("Field41", new RepositoryPair { Data = collection41, StoreType = typeof(TestEntity41) });

            var collection42 = new Dictionary<int, SimpleEntity>();
            collection42.Add(collection42.Count, new TestEntity42() { Id = 0, Field43 = "Root", Name = "Risk1" });
            collection42.Add(collection42.Count, new TestEntity42() { Id = 1, Field43 = "Root", Name = "Risk2" });
            collection42.Add(collection42.Count, new TestEntity42() { Id = 1, Field43 = "Root", Name = "Risk777" });
            Repository.Add("Field42", new RepositoryPair { Data = collection42, StoreType = typeof(TestEntity42) });

            var collection43 = new Dictionary<int, SimpleEntity>();
            collection43.Add(collection43.Count, new TestEntity43() { Id = 0, Name = "Root" });
            Repository.Add("Field43", new RepositoryPair { Data = collection43, StoreType = typeof(TestEntity43) });

            var collection44 = new Dictionary<int, SimpleEntity>();
            collection44.Add(collection44.Count, new TestEntity44() { Id = 0, Field45 = "Root", Name = "Remont1" });
            collection44.Add(collection44.Count, new TestEntity44() { Id = 1, Field45 = "Root", Name = "Remont2" });
            collection44.Add(collection44.Count, new TestEntity44() { Id = 2, Field45 = "Root", Name = "Remont3" });
            Repository.Add("Field44", new RepositoryPair { Data = collection44, StoreType = typeof(TestEntity44) });

            var collection45 = new Dictionary<int, SimpleEntity>();
            collection45.Add(collection45.Count, new TestEntity45() { Id = 0, Name = "Root" });
            Repository.Add("Field45", new RepositoryPair { Data = collection45, StoreType = typeof(TestEntity45) });

            var collection46 = new Dictionary<int, SimpleEntity>();
            collection46.Add(collection46.Count, new TestEntity46() { Id = 0, Name = "Program1" });
            collection46.Add(collection46.Count, new TestEntity46() { Id = 1, Name = "Program2" });
            Repository.Add("Field46", new RepositoryPair { Data = collection46, StoreType = typeof(TestEntity46) });

            var collection47 = new Dictionary<int, SimpleEntity>();
            collection47.Add(collection47.Count, new TestEntity47() { Id = 0, Field46 = "Program1", Name = "Form1" });
            collection47.Add(collection47.Count, new TestEntity47() { Id = 1, Field46 = "Program1", Name = "Form2" });
            collection47.Add(collection47.Count, new TestEntity47() { Id = 2, Field46 = "Program2", Name = "Form2" });
            collection47.Add(collection47.Count, new TestEntity47() { Id = 3, Field46 = "Program2", Name = "Form3" });
            Repository.Add("Field47", new RepositoryPair { Data = collection47, StoreType = typeof(TestEntity47) });

            var collection48 = new Dictionary<int, SimpleEntity>();
            collection48.Add(collection48.Count, new TestEntity48() { Id = 0, Name = "Amount1" });
            collection48.Add(collection48.Count, new TestEntity48() { Id = 1, Name = "Amount2" });
            collection48.Add(collection48.Count, new TestEntity48() { Id = 2, Name = "Amount3" });
            Repository.Add("Field48", new RepositoryPair { Data = collection48, StoreType = typeof(TestEntity48) });

            var collection2_1 = new Dictionary<int, SimpleEntity>();
            collection2_1.Add(collection2_1.Count, new TestEntity2_1() { Id = 0, Name = "Moscow" });
            collection2_1.Add(collection2_1.Count, new TestEntity2_1() { Id = 1, Name = "Spb" });
            Repository.Add("Field2_1", new RepositoryPair { Data = collection2_1, StoreType = typeof(TestEntity2_1) });

            var collection2_2 = new Dictionary<int, SimpleEntity>();
            collection2_2.Add(collection2_2.Count, new TestEntity2_2() { Id = 0, Name = "Audi" });
            collection2_2.Add(collection2_2.Count, new TestEntity2_2() { Id = 1, Name = "BMW" });
            Repository.Add("Field2_2", new RepositoryPair { Data = collection2_2, StoreType = typeof(TestEntity2_2) });

            var collection2_3 = new Dictionary<int, SimpleEntity>();
            collection2_3.Add(collection2_3.Count, new TestEntity2_3() { Id = 0, Field2 = "Audi", Field4 = "2008", Name = "A1" });
            collection2_3.Add(collection2_3.Count, new TestEntity2_3() { Id = 1, Field2 = "BMW", Field4 = "2009", Name = "X3" });
            Repository.Add("Field2_3", new RepositoryPair { Data = collection2_3, StoreType = typeof(TestEntity2_3) });

            var collection2_4 = new Dictionary<int, SimpleEntity>();
            collection2_4.Add(collection2_4.Count, new TestEntity2_4() { Id = 0, Name = "2008" });
            collection2_4.Add(collection2_4.Count, new TestEntity2_4() { Id = 1, Name = "2009" });
            collection2_4.Add(collection2_4.Count, new TestEntity2_4() { Id = 2, Name = "2010" });
            Repository.Add("Field2_4", new RepositoryPair { Data = collection2_4, StoreType = typeof(TestEntity2_4) });

            this.Setup(p => p.Collection1).Returns(Repository["Field1"].Data.Select(i => (TestEntity1)i.Value));
            this.Setup(p => p.Collection2).Returns(Repository["Field2"].Data.Select(i => (TestEntity2)i.Value));
            this.Setup(p => p.Collection3).Returns(Repository["Field3"].Data.Select(i => (TestEntity3)i.Value));
            this.Setup(p => p.Collection4).Returns(Repository["Field4"].Data.Select(i => (TestEntity4)i.Value));
            this.Setup(p => p.Collection5).Returns(Repository["Field5"].Data.Select(i => (TestEntity5)i.Value));
            this.Setup(p => p.Collection6).Returns(Repository["Field6"].Data.Select(i => (TestEntity6)i.Value));
            this.Setup(p => p.Collection7).Returns(Repository["Field7"].Data.Select(i => (TestEntity7)i.Value));
            this.Setup(p => p.Collection8).Returns(Repository["Field8"].Data.Select(i => (TestEntity8)i.Value));
            this.Setup(p => p.Collection9).Returns(Repository["Field9"].Data.Select(i => (TestEntity9)i.Value));
            this.Setup(p => p.Collection10).Returns(Repository["Field10"].Data.Select(i => (TestEntity10)i.Value));
            this.Setup(p => p.Collection11).Returns(Repository["Field11"].Data.Select(i => (TestEntity11)i.Value));
            this.Setup(p => p.Collection12).Returns(Repository["Field12"].Data.Select(i => (TestEntity12)i.Value));
            this.Setup(p => p.Collection13).Returns(Repository["Field13"].Data.Select(i => (TestEntity13)i.Value));
            this.Setup(p => p.Collection14).Returns(Repository["Field14"].Data.Select(i => (TestEntity14)i.Value));
            this.Setup(p => p.Collection15).Returns(Repository["Field15"].Data.Select(i => (TestEntity15)i.Value));
            this.Setup(p => p.Collection16).Returns(Repository["Field16"].Data.Select(i => (TestEntity16)i.Value));
            this.Setup(p => p.Collection17).Returns(Repository["Field17"].Data.Select(i => (TestEntity17)i.Value));
            this.Setup(p => p.Collection18).Returns(Repository["Field18"].Data.Select(i => (TestEntity18)i.Value));
            this.Setup(p => p.Collection19).Returns(Repository["Field19"].Data.Select(i => (TestEntity19)i.Value));
            this.Setup(p => p.Collection20).Returns(Repository["Field20"].Data.Select(i => (TestEntity20)i.Value));
            this.Setup(p => p.Collection21).Returns(Repository["Field21"].Data.Select(i => (TestEntity21)i.Value));
            this.Setup(p => p.Collection22).Returns(Repository["Field22"].Data.Select(i => (TestEntity22)i.Value));
            this.Setup(p => p.Collection23).Returns(Repository["Field23"].Data.Select(i => (TestEntity23)i.Value));
            this.Setup(p => p.Collection24).Returns(Repository["Field24"].Data.Select(i => (TestEntity24)i.Value));
            this.Setup(p => p.Collection25).Returns(Repository["Field25"].Data.Select(i => (TestEntity25)i.Value));
            this.Setup(p => p.Collection26).Returns(Repository["Field26"].Data.Select(i => (TestEntity26)i.Value));
            this.Setup(p => p.Collection27).Returns(Repository["Field27"].Data.Select(i => (TestEntity27)i.Value));
            this.Setup(p => p.Collection28).Returns(Repository["Field28"].Data.Select(i => (TestEntity28)i.Value));
            this.Setup(p => p.Collection29).Returns(Repository["Field29"].Data.Select(i => (TestEntity29)i.Value));
            this.Setup(p => p.Collection30).Returns(Repository["Field30"].Data.Select(i => (TestEntity30)i.Value));
            this.Setup(p => p.Collection31).Returns(Repository["Field31"].Data.Select(i => (TestEntity31)i.Value));
            this.Setup(p => p.Collection32).Returns(Repository["Field32"].Data.Select(i => (TestEntity32)i.Value));
            this.Setup(p => p.Collection33).Returns(Repository["Field33"].Data.Select(i => (TestEntity33)i.Value));
            this.Setup(p => p.Collection34).Returns(Repository["Field34"].Data.Select(i => (TestEntity34)i.Value));
            this.Setup(p => p.Collection35).Returns(Repository["Field35"].Data.Select(i => (TestEntity35)i.Value));
            this.Setup(p => p.Collection36).Returns(Repository["Field36"].Data.Select(i => (TestEntity36)i.Value));
            this.Setup(p => p.Collection37).Returns(Repository["Field37"].Data.Select(i => (TestEntity37)i.Value));
            this.Setup(p => p.Collection38).Returns(Repository["Field38"].Data.Select(i => (TestEntity38)i.Value));
            this.Setup(p => p.Collection39).Returns(Repository["Field39"].Data.Select(i => (TestEntity39)i.Value));
            this.Setup(p => p.Collection40).Returns(Repository["Field40"].Data.Select(i => (TestEntity40)i.Value));
            this.Setup(p => p.Collection41).Returns(Repository["Field41"].Data.Select(i => (TestEntity41)i.Value));
            this.Setup(p => p.Collection42).Returns(Repository["Field42"].Data.Select(i => (TestEntity42)i.Value));
            this.Setup(p => p.Collection43).Returns(Repository["Field43"].Data.Select(i => (TestEntity43)i.Value));
            this.Setup(p => p.Collection44).Returns(Repository["Field44"].Data.Select(i => (TestEntity44)i.Value));
            this.Setup(p => p.Collection45).Returns(Repository["Field45"].Data.Select(i => (TestEntity45)i.Value));
            this.Setup(p => p.Collection46).Returns(Repository["Field46"].Data.Select(i => (TestEntity46)i.Value));
            this.Setup(p => p.Collection47).Returns(Repository["Field47"].Data.Select(i => (TestEntity47)i.Value));
            this.Setup(p => p.Collection48).Returns(Repository["Field48"].Data.Select(i => (TestEntity48)i.Value));
            this.Setup(p => p.Collection2_1).Returns(Repository["Field2_1"].Data.Select(i => (TestEntity2_1)i.Value));
            this.Setup(p => p.Collection2_2).Returns(Repository["Field2_2"].Data.Select(i => (TestEntity2_2)i.Value));
            this.Setup(p => p.Collection2_3).Returns(Repository["Field2_3"].Data.Select(i => (TestEntity2_3)i.Value));
            this.Setup(p => p.Collection2_4).Returns(Repository["Field2_4"].Data.Select(i => (TestEntity2_4)i.Value));

            Restrictors = new Dictionary<string, RestrictorPair>();

            var restrictor1 = new Dictionary<int, IHDBK>();
            restrictor1.Add(restrictor1.Count, new TestHDBK1() { TestEntity36 = "Program1" });
            restrictor1.Add(restrictor1.Count, new TestHDBK1() { TestEntity36 = "Program1" });
            restrictor1.Add(restrictor1.Count, new TestHDBK1() { TestEntity36 = "Program2" });
            Restrictors.Add("HDBK1", new RestrictorPair() { Data = restrictor1, StoreType = typeof(TestHDBK1) });

            var restrictor2 = new Dictionary<int, IHDBK>();
            restrictor2.Add(restrictor2.Count, new TestHDBK2() { TestEntity39 = "Remont1", TestEntity38 = "Risk1", GroupValue = "Opt1" });
            restrictor2.Add(restrictor2.Count, new TestHDBK2() { TestEntity39 = "Remont2", TestEntity38 = "Risk1", GroupValue = "Opt2" });
            restrictor2.Add(restrictor2.Count, new TestHDBK2() { TestEntity39 = "Remont2", TestEntity38 = "Risk2", GroupValue = "Opt3" });
            restrictor2.Add(restrictor2.Count, new TestHDBK2() { TestEntity39 = "Remont1", TestEntity38 = "Risk1", GroupValue = "Opt1" }); // повторяющийся элемент (совпадает с первым) специально для проверки, что возвращен будет только один
            Restrictors.Add("HDBK2", new RestrictorPair() { Data = restrictor2, StoreType = typeof(TestHDBK2) });

            var restrictor3 = new Dictionary<int, IHDBK>();
            restrictor3.Add(restrictor3.Count, new TestHDBK3() {TestEntity41 = "Program1", TestEntity42 = "Risk1"});
            restrictor3.Add(restrictor3.Count, new TestHDBK3() {TestEntity41 = "Program1", TestEntity42 = "Risk2"});
            restrictor3.Add(restrictor3.Count, new TestHDBK3() {TestEntity41 = "Program2", TestEntity42 = "Risk1"});
            Restrictors.Add("HDBK3", new RestrictorPair() { Data = restrictor3, StoreType = typeof(TestHDBK3) });

            var restrictor4 = new Dictionary<int, IHDBK>();
            restrictor4.Add(restrictor4.Count, new TestHDBK4() { TestEntity47 = "Form1", TestEntity48 = "Amount1" });
            restrictor4.Add(restrictor4.Count, new TestHDBK4() { TestEntity47 = "Form2", TestEntity48 = "Amount2" });
            restrictor4.Add(restrictor4.Count, new TestHDBK4() { TestEntity47 = "Form2", TestEntity48 = "Amount3" });
            restrictor4.Add(restrictor4.Count, new TestHDBK4() { TestEntity47 = "Form3", TestEntity48 = "Amount3" });
            Restrictors.Add("HDBK4", new RestrictorPair() { Data = restrictor4, StoreType = typeof(TestHDBK4) });

            this.Setup(p => p.RestrictCollection1).Returns(Restrictors["HDBK1"].Data.Select(i => (TestHDBK1)i.Value));
            this.Setup(p => p.RestrictCollection2).Returns(Restrictors["HDBK2"].Data.Select(i => (TestHDBK2)i.Value));
            this.Setup(p => p.RestrictCollection3).Returns(Restrictors["HDBK3"].Data.Select(i => (TestHDBK3)i.Value));
            this.Setup(p => p.RestrictCollection4).Returns(Restrictors["HDBK4"].Data.Select(i => (TestHDBK4)i.Value));
        }
    }
}
