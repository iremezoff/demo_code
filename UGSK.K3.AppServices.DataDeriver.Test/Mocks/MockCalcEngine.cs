﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using UGSK.K3.AppServices.DataDeriver.Test.Mocks.Originals;
using UGSK.K3.Product.Contract;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Flexible;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Impl;
using UGSK.K3.AppServices.DataDeriver.Core.SelectionRules.Impl;
using UGSK.K3.Product.Contract.Annotations;
using UGSK.K3.AppServices.DataDeriver.Product.Field;

namespace UGSK.K3.AppServices.DataDeriver.Test.Mocks
{
    internal class MockCalcEngine2 : Mock<ITariffCalculationEngine<TestRequest2>>
    {
        private MockDbSession _dbSession;
        public Type RequestType { get { return typeof (TestRequest2); } }

        public MockDbSession DbSession
        {
            get { return _dbSession; }
        }

        public Tuple<string, IRequestFieldBatler> Parameter1 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter2 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter3 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter4 { get; private set; }

        public MockCalcEngine2()
            : base(MockBehavior.Default) // если какой-то метод/свойство не замокали, сразу говорить, не молчать!
        {
            

            InitRepositories();

            InitRequestType();

            InitCohesive();
        }

        private event EventHandler<NewRestrictorEventArgs> Handler;

        private void Registrator(EventHandler<NewRestrictorEventArgs> eventHandler, Type requestType)
        {
            Handler += eventHandler;
        }

        private void InitCohesive()
        {
            //this.Setup(r => r.Restrictors).Returns(new Dictionary<Type, IEnumerable<IHDBK>>());
            //this.Setup(c => c.CohesiveFields).Returns(new Dictionary<string, IEnumerable<CohesiveParameterInfo>>());
        }

        private void InitRequestType()
        {
            //RequestType = typeof (TestRequest2);

            this.Setup(p => p.RequestType).Returns(RequestType);
        }

        private FieldRepository Creating()
        {
            return new SingleFieldRepository();
        }

        private void InitRepositories()
        {
            

            _dbSession = new MockDbSession();

            const string paramName1 = "Field2_1", paramName2 = "Field2_2", paramName3 = "Field2_3", paramName4 = "Field2_4";

            var mockBuilder = new Mock<ITypeBuilder>();

            this.Setup(p => p.TypeBuilder).Returns(mockBuilder.Object);


            var singleRule = new SingleHavingSelectionRule<IRequest>();


            var restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            var rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);

            RequestFieldBatler<TestRequest2> batler = null;

            batler = new RequestFieldBatler<TestRequest2>(RequestType.GetProperty(paramName2),
                                                  DbSession.Repository[paramName2].Data.Values.Select(i => (TestEntity2_2) i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(
                new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof (TestEntity2_2), "Name") {DependsOnFields = new[] {paramName1}},
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));
            Parameter2 = Tuple.Create(paramName2, (IRequestFieldBatler) batler);

            //fieldsDic.Add("Field2", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity2)) { DependsOnFields = new[] { "Field1" } },
            //    FieldProperty = _request.GetType().GetProperty("Field2"),
            //    DependentFields = new[] { fieldsDic["Field3"] }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest2>(RequestType.GetProperty(paramName1),
                                                  DbSession.Repository[paramName1].Data.Values.Select(i => (TestEntity2_1) i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(
                new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof (TestEntity2_1), "Name") {DependsOnFields = new string[0]},
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter1 = Tuple.Create(paramName1, (IRequestFieldBatler) batler);

            //fieldsDic.Add("Field1", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity1)) { DependsOnFields = new string[0] },
            //    FieldProperty = _request.GetType().GetProperty("Field1"),
            //    DependentFields = new[] { fieldsDic["Field2"] }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest2>(RequestType.GetProperty(paramName3),
                                                  DbSession.Repository[paramName3].Data.Values.Select(i => (TestEntity2_3) i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(
                new DefaultFieldSourceRestrictor(
                    new RequestFieldAttribute(typeof (TestEntity2_3), "Name") {DependsOnFields = new[] {paramName2, paramName4}},
                    batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter3 = Tuple.Create(paramName3, (IRequestFieldBatler) batler);

            //fieldsDic.Add("Field3", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity3)) { DependsOnFields = new[] { "Field4", "Field2" } },
            //    FieldProperty = _request.GetType().GetProperty("Field3")
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest2>(RequestType.GetProperty(paramName4),
                                                  DbSession.Repository[paramName4].Data.Values.Select(i => (TestEntity2_4) i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(
                new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof (TestEntity2_4), "Name") {DependsOnFields = new string[0]},
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter4 = Tuple.Create(paramName4, (IRequestFieldBatler) batler);

            //fieldsDic.Add("Field4", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity4)) { DependsOnFields = new string[0] },
            //    FieldProperty = _request.GetType().GetProperty("Field4"),
            //    DependentFields = new[] { fieldsDic["Field3"] }
            //});

            this.Setup(p => p[Parameter1.Item1]).Returns(Parameter1.Item2);
            this.Setup(p => p[Parameter2.Item1]).Returns(Parameter2.Item2);
            this.Setup(p => p[Parameter3.Item1]).Returns(Parameter3.Item2);
            this.Setup(p => p[Parameter4.Item1]).Returns(Parameter4.Item2);

            this.Setup(p => p[Parameter1.Item2.SourceType]).Returns(Parameter1.Item2);
            this.Setup(p => p[Parameter2.Item2.SourceType]).Returns(Parameter2.Item2);
            this.Setup(p => p[Parameter3.Item2.SourceType]).Returns(Parameter3.Item2);
            this.Setup(p => p[Parameter4.Item2.SourceType]).Returns(Parameter4.Item2);

            var list = new List<IRequestFieldBatler>() {Parameter1.Item2, Parameter2.Item2, Parameter3.Item2, Parameter4.Item2};

            this.Setup(m => m.GetAllFields()).Returns(list);
            
        }

        
    }

    class MockCalcEngine : Mock<ITariffCalculationEngine<TestRequest>>
    {
        private MockDbSession _dbSession;
        public Type RequestType { get; private set; }

        public Tuple<string, IRequestFieldBatler> Parameter1 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter2 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter3 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter4 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter5 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter6 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter7 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter8 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter9 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter10 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter11 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter12 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter13 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter14 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter15 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter16 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter17 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter18 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter19 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter20 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter21 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter22 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter23 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter24 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter25 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter26 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter27 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter28 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter29 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter30 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter31 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter32 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter33 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter34 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter35 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter36 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter37 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter38 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter39 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter40 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter41 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter42 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter43 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter44 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter45 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter46 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter47 { get; private set; }
        public Tuple<string, IRequestFieldBatler> Parameter48 { get; private set; }

        public MockDbSession DbSession
        {
            get { return _dbSession; }
        }

        public MockCalcEngine()
            : base(MockBehavior.Default) // если какой-то метод/свойство не замокали, сразу говорить, не молчать! - это Strict
        {
            InitRequestType();

            InitRequestParameters();

            InitCohesive();
        }

        private event EventHandler<NewRestrictorEventArgs> Handler;

        private void Registrator(EventHandler<NewRestrictorEventArgs> eventHandler, Type requestType)
        {
            Handler += eventHandler;
        }

        private FieldRepository Creating()
        {
            return new SingleFieldRepository();
        }

        private void InitRequestParameters()
        {
            _dbSession = new MockDbSession();

            const string paramName1 = "Field1", paramName2 = "Field2";

            //var mockRegistrator = new Mock<Action<EventHandler<NewRestrictorEventArgs>>>();

            //var engine = (ITariffCalculationEngine<IRequest>) this.Object;

            var mockBuilder = new Mock<ITypeBuilder>();

            this.Setup(p => p.TypeBuilder).Returns(mockBuilder.Object);


            var singleRule = new SingleHavingSelectionRule<IRequest>();


            var restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            var rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);

            RequestFieldBatler<TestRequest> batler = null;

            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName2),
                                                  DbSession.Repository[paramName2].Data.Values.Select(i => (TestEntity2)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity2), "Name") { DependsOnFields = new[] { paramName1 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));
            Parameter2 = Tuple.Create(paramName2, (IRequestFieldBatler)batler);



            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName1),
                                                  DbSession.Repository[paramName1].Data.Values.Select(i => (TestEntity1)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity1), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter1 = Tuple.Create(paramName1, (IRequestFieldBatler)batler);



            const string paramName3 = "Field3", paramName4 = "Field4", paramName5 = "Field5";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName5),
                                                  DbSession.Repository[paramName5].Data.Values.Select(i => (TestEntity5)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity5), "Name") { DependsOnFields = new[] { paramName4 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter5 = Tuple.Create(paramName5, (IRequestFieldBatler)batler);


            //Parameter5 = Tuple.Create(paramName5, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity5), "Name") ,
            //    FieldProperty = RequestType.GetProperty(paramName5)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName4),
                                                  DbSession.Repository[paramName4].Data.Values.Select(i => (TestEntity4)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity4), "Name") { DependsOnFields = new[] { paramName3 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter4 = Tuple.Create(paramName4, (IRequestFieldBatler)batler);
            
            //Parameter4 = Tuple.Create(paramName4, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity4), "Name") { DependsOnFields = new[] { paramName3 } },
            //    FieldProperty = RequestType.GetProperty(paramName4),
            //    DependentFields = new[] { Parameter5.Item2 }
            //});
            
            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName3),
                                                  DbSession.Repository[paramName3].Data.Values.Select(i => (TestEntity3)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity3), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            batler.SourceTypeProperty = "Name";
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter3 = Tuple.Create(paramName3, (IRequestFieldBatler)batler);

            //Parameter3 = Tuple.Create(paramName3, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity3), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName3),
            //    DependentFields = new[] { Parameter4.Item2 }
            //});

            const string paramName6 = "Field6", paramName7 = "Field7", paramName8 = "Field8";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName8),
                                                  DbSession.Repository[paramName8].Data.Values.Select(i => (TestEntity8)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity8), "Name") { DependsOnFields = new[] { paramName6 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter8 = Tuple.Create(paramName8, (IRequestFieldBatler)batler);
            
            //Parameter8 = Tuple.Create(paramName8, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity8), "Name") { DependsOnFields = new[] { paramName6 } },
            //    FieldProperty = RequestType.GetProperty(paramName8)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName7),
                                                  DbSession.Repository[paramName7].Data.Values.Select(i => (TestEntity7)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity7), "Name") { DependsOnFields = new[] { paramName6 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter7 = Tuple.Create(paramName7, (IRequestFieldBatler)batler);


            //Parameter7 = Tuple.Create(paramName7, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity7), "Name") { DependsOnFields = new[] { paramName6 } },
            //    FieldProperty = RequestType.GetProperty(paramName7),
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName6),
                                                  DbSession.Repository[paramName6].Data.Values.Select(i => (TestEntity6)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity6), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter6 = Tuple.Create(paramName6, (IRequestFieldBatler)batler);

            //Parameter6 = Tuple.Create(paramName6, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity3), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName6),
            //    DependentFields = new[] { Parameter7.Item2, Parameter8.Item2 }
            //});

            const string paramName9 = "Field9", paramName10 = "Field10", paramName11 = "Field11";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName11),
                                                  DbSession.Repository[paramName11].Data.Values.Select(i => (TestEntity11)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity11), "Name") { DependsOnFields = new[] { paramName9, paramName10 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter11 = Tuple.Create(paramName11, (IRequestFieldBatler)batler);

            //Parameter11 = Tuple.Create(paramName11, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity11), "Name") { DependsOnFields = new[] { paramName9, paramName10 } },
            //    FieldProperty = RequestType.GetProperty(paramName11)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName10),
                                                  DbSession.Repository[paramName10].Data.Values.Select(i => (TestEntity10)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity10), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter10 = Tuple.Create(paramName10, (IRequestFieldBatler)batler);

            //Parameter10 = Tuple.Create(paramName10, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity10), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName10),
            //    DependentFields = new[] { Parameter11.Item2 }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName9),
                                                  DbSession.Repository[paramName9].Data.Values.Select(i => (TestEntity9)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity9), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter9 = Tuple.Create(paramName9, (IRequestFieldBatler)batler);

            //Parameter9 = Tuple.Create(paramName9, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity9), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName9),
            //    DependentFields = new[] { Parameter11.Item2 }
            //});

            const string paramName12 = "Field12", paramName13 = "Field13";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName13),
                                                  DbSession.Repository[paramName13].Data.Values.Select(i => (TestEntity13)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity13), "Name") { DependsOnFields = new[] { paramName12 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter13 = Tuple.Create(paramName13, (IRequestFieldBatler)batler);

            //Parameter13 = Tuple.Create(paramName13, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity13), "Name") { DependsOnFields = new[] { paramName12 } },
            //    FieldProperty = RequestType.GetProperty(paramName13)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName12),
                                                  DbSession.Repository[paramName12].Data.Values.Select(i => (TestEntity12)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity12), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter12 = Tuple.Create(paramName12, (IRequestFieldBatler)batler);

            //Parameter12 = Tuple.Create(paramName12, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity12), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName12),
            //    DependentFields = new[] { Parameter13.Item2 }
            //});

            const string paramName14 = "Field14", paramName15 = "Field15", paramName16 = "Field16";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName16),
                                                  DbSession.Repository[paramName16].Data.Values.Select(i => (TestEntity16)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity16), "Name") { DependsOnFields = new[] { paramName15 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter16 = Tuple.Create(paramName16, (IRequestFieldBatler)batler);

            //Parameter16 = Tuple.Create(paramName16, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity16), "Name") { DependsOnFields = new[] { paramName15 } },
            //    FieldProperty = RequestType.GetProperty(paramName16)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName15),
                                                  DbSession.Repository[paramName15].Data.Values.Select(i => (TestEntity15)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity15), "Name") { DependsOnFields = new[] { paramName14 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter15 = Tuple.Create(paramName15, (IRequestFieldBatler)batler);

            //Parameter15 = Tuple.Create(paramName15, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity15), "Name") { DependsOnFields = new[] { paramName14 } },
            //    FieldProperty = RequestType.GetProperty(paramName15),
            //    DependentFields = new[] { Parameter16.Item2 }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName14),
                                                  DbSession.Repository[paramName14].Data.Values.Select(i => (TestEntity14)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity14), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter14 = Tuple.Create(paramName14, (IRequestFieldBatler)batler);

            //Parameter14 = Tuple.Create(paramName14, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity14), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName14),
            //    DependentFields = new[] { Parameter15.Item2 }
            //});

            const string paramName17 = "Field17", paramName18 = "Field18", paramName19 = "Field19";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName19),
                                                  DbSession.Repository[paramName19].Data.Values.Select(i => (TestEntity19)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity19), "Name") { DependsOnFields = new[] { paramName17, paramName18 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter19 = Tuple.Create(paramName19, (IRequestFieldBatler)batler);

            //Parameter19 = Tuple.Create(paramName19, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity19), "Name") { DependsOnFields = new[] { paramName17, paramName18 } },
            //    FieldProperty = RequestType.GetProperty(paramName19)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName18),
                                                  DbSession.Repository[paramName18].Data.Values.Select(i => (TestEntity18)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity18), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter18 = Tuple.Create(paramName18, (IRequestFieldBatler)batler);

            //Parameter18 = Tuple.Create(paramName18, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity18), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName18),
            //    DependentFields = new[] { Parameter19.Item2 }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName17),
                                                  DbSession.Repository[paramName17].Data.Values.Select(i => (TestEntity17)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity17), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter17 = Tuple.Create(paramName17, (IRequestFieldBatler)batler);

            //Parameter17 = Tuple.Create(paramName17, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity17), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName17),
            //    DependentFields = new[] { Parameter19.Item2 }
            //});

            //===

            const string paramName20 = "Field20", paramName21 = "Field21";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName21),
                                                  DbSession.Repository[paramName21].Data.Values.Select(i => (TestEntity21)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity21), "Name") { DependsOnFields = new[] { paramName20 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter21 = Tuple.Create(paramName21, (IRequestFieldBatler)batler);

            //Parameter21 = Tuple.Create(paramName21, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity21), "Name") { DependsOnFields = new[] { paramName20 } },
            //    FieldProperty = RequestType.GetProperty(paramName21)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName20),
                                                  DbSession.Repository[paramName20].Data.Values.Select(i => (TestEntity20)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity20), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter20 = Tuple.Create(paramName20, (IRequestFieldBatler)batler);

            //Parameter20 = Tuple.Create(paramName20, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity20), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName20),
            //    DependentFields = new[] { Parameter21.Item2 }
            //});

            const string paramName22 = "Field22", paramName23 = "Field23", paramName24 = "Field24";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName24),
                                                  DbSession.Repository[paramName24].Data.Values.Select(i => (TestEntity24)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity24), "Name") { DependsOnFields = new[] { paramName23 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter24 = Tuple.Create(paramName24, (IRequestFieldBatler)batler);

            //Parameter24 = Tuple.Create(paramName24, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity24), "Name") { DependsOnFields = new[] { paramName23 } },
            //    FieldProperty = RequestType.GetProperty(paramName24)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName23),
                                                  DbSession.Repository[paramName23].Data.Values.Select(i => (TestEntity23)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity23), "Name") { DependsOnFields = new[] { paramName22 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter23 = Tuple.Create(paramName23, (IRequestFieldBatler)batler);

            //Parameter23 = Tuple.Create(paramName23, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity23), "Name") { DependsOnFields = new[] { paramName22 } },
            //    FieldProperty = RequestType.GetProperty(paramName23),
            //    DependentFields = new[] { Parameter24.Item2 }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName22),
                                                  DbSession.Repository[paramName22].Data.Values.Select(i => (TestEntity22)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity22), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter22 = Tuple.Create(paramName22, (IRequestFieldBatler)batler);

            //Parameter22 = Tuple.Create(paramName22, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity22), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName22),
            //    DependentFields = new[] { Parameter23.Item2 }
            //});

            const string paramName25 = "Field25", paramName26 = "Field26", paramName27 = "Field27";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName27),
                                                  DbSession.Repository[paramName27].Data.Values.Select(i => (TestEntity27)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity27), "Name") { DependsOnFields = new[] { paramName25, paramName26 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter27 = Tuple.Create(paramName27, (IRequestFieldBatler)batler);

            //Parameter27 = Tuple.Create(paramName27, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity27), "Name") { DependsOnFields = new[] { paramName25, paramName26 } },
            //    FieldProperty = RequestType.GetProperty(paramName27)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName26),
                                                  DbSession.Repository[paramName26].Data.Values.Select(i => (TestEntity26)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity26), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter26 = Tuple.Create(paramName26, (IRequestFieldBatler)batler);

            //Parameter26 = Tuple.Create(paramName26, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity26), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName26),
            //    DependentFields = new[] { Parameter27.Item2 }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName25),
                                                  DbSession.Repository[paramName25].Data.Values.Select(i => (TestEntity25)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity25), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter25 = Tuple.Create(paramName25, (IRequestFieldBatler)batler);

            //Parameter25 = Tuple.Create(paramName25, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity25), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName25),
            //    DependentFields = new[] { Parameter27.Item2 }
            //});

            //===

            const string paramName28 = "Field28", paramName29 = "Field29";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName29),
                                                  DbSession.Repository[paramName29].Data.Values.Select(i => (TestEntity29)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity29), "Name") { DependsOnFields = new[] { paramName28 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter29 = Tuple.Create(paramName29, (IRequestFieldBatler)batler);

            //Parameter29 = Tuple.Create(paramName29, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity29), "Name") { DependsOnFields = new[] { paramName28 } },
            //    FieldProperty = RequestType.GetProperty(paramName29)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName28),
                                                  DbSession.Repository[paramName28].Data.Values.Select(i => (TestEntity28)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity28), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter28 = Tuple.Create(paramName28, (IRequestFieldBatler)batler);

            //Parameter28 = Tuple.Create(paramName28, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity28), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName28),
            //    DependentFields = new[] { Parameter29.Item2 }
            //});

            const string paramName30 = "Field30", paramName31 = "Field31", paramName32 = "Field32";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName32),
                                                  DbSession.Repository[paramName32].Data.Values.Select(i => (TestEntity32)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity32), "Name") { DependsOnFields = new[] { paramName31 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter32 = Tuple.Create(paramName32, (IRequestFieldBatler)batler);

            //Parameter32 = Tuple.Create(paramName32, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity32), "Name") { DependsOnFields = new[] { paramName31 } },
            //    FieldProperty = RequestType.GetProperty(paramName32)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName31),
                                                  DbSession.Repository[paramName31].Data.Values.Select(i => (TestEntity31)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity31), "Name") { DependsOnFields = new[] { paramName30 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter31 = Tuple.Create(paramName31, (IRequestFieldBatler)batler);

            //Parameter31 = Tuple.Create(paramName31, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity31), "Name") { DependsOnFields = new[] { paramName30 } },
            //    FieldProperty = RequestType.GetProperty(paramName31),
            //    DependentFields = new[] { Parameter32.Item2 }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName30),
                                                  DbSession.Repository[paramName30].Data.Values.Select(i => (TestEntity30)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity30), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter30 = Tuple.Create(paramName30, (IRequestFieldBatler)batler);

            //Parameter30 = Tuple.Create(paramName30, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity30), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName30),
            //    DependentFields = new[] { Parameter31.Item2 }
            //});

            const string paramName33 = "Field33", paramName34 = "Field34", paramName35 = "Field35";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName35),
                                                  DbSession.Repository[paramName35].Data.Values.Select(i => (TestEntity35)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity35), "Name") { DependsOnFields = new[] { paramName33, paramName34 } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter35 = Tuple.Create(paramName35, (IRequestFieldBatler)batler);

            //Parameter35 = Tuple.Create(paramName35, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity35), "Name") { DependsOnFields = new[] { paramName33, paramName34 } },
            //    FieldProperty = RequestType.GetProperty(paramName35)
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName34),
                                                  DbSession.Repository[paramName34].Data.Values.Select(i => (TestEntity34)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity34), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter34 = Tuple.Create(paramName34, (IRequestFieldBatler)batler);

            //Parameter34 = Tuple.Create(paramName34, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity34), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName34),
            //    DependentFields = new[] { Parameter35.Item2 }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName33),
                                                  DbSession.Repository[paramName33].Data.Values.Select(i => (TestEntity33)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity33), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter33 = Tuple.Create(paramName33, (IRequestFieldBatler)batler);

            //Parameter33 = Tuple.Create(paramName33, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity33), "Name") { DependsOnFields = new string[0] },
            //    FieldProperty = RequestType.GetProperty(paramName33),
            //    DependentFields = new[] { Parameter35.Item2 }
            //});

            const string paramName36 = "Field36", paramName37 = "Field37";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName36),
                                                  DbSession.Repository[paramName36].Data.Values.Select(i => (TestEntity36)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity36), "Name") { DependsOnFields = new[] { "Field37" } },
                                                 batler, Registrator, null));
            batler.Restrictors.Add(new CohesiveFieldSourceRestrictor(new CohesiveFieldAttribute(typeof(TestHDBK1)) { Order = 1 }, batler,
                                                              type => DbSession.Restrictors["HDBK1"].Data.Values, Registrator));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[1]));

            Parameter36 = Tuple.Create(paramName36, (IRequestFieldBatler)batler);

            //var fieldsDic = new Dictionary<string, RequestParameterInfo>();
            //fieldsDic.Add(, new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity36)) { DependsOnFields = new[] { "Field37" } },
            //    FieldProperty = _request.GetType().GetProperty("Field36"),
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName37),
                                                  DbSession.Repository[paramName37].Data.Values.Select(i => (TestEntity37)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity37), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter37 = Tuple.Create(paramName37, (IRequestFieldBatler)batler);

            //fieldsDic.Add("Field37", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity37)),
            //    FieldProperty = _request.GetType().GetProperty("Field37"),
            //    DependentFields = new[] { fieldsDic["Field36"] }
            //});

            const string paramName38 = "Field38", paramName39 = "Field39", paramName40 = "Field40";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName38),
                                                  DbSession.Repository[paramName38].Data.Values.Select(i => (TestEntity38)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity38), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            batler.Restrictors.Add(new CohesiveFieldSourceRestrictor(new CohesiveFieldAttribute(typeof(TestHDBK2)) { Order = 2, GrouperPropertyName = "GroupValue" }, batler,
                                                              type => DbSession.Restrictors["HDBK2"].Data.Values, Registrator));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[1]));

            Parameter38 = Tuple.Create(paramName38, (IRequestFieldBatler)batler);

            //fieldsDic.Add("Field38", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity38)),
            //    FieldProperty = _request.GetType().GetProperty("Field38"),
            //});

            //cohFieldDic.Add("Field38", new CohesiveParameterInfo()
            //{
            //    FieldProperty = _request.GetType().GetProperty("Field38"),
            //    Attribute = new CohesiveFieldAttribute(typeof(TestHDBK2)) { Order = 2, GrouperPropertyName = "GroupValue" },
            //    ValuePropertyName = "TestEntity38"
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName39),
                                                  DbSession.Repository[paramName39].Data.Values.Select(i => (TestEntity39)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity39), "Name") { DependsOnFields = new[] { "Field40" } },
                                                 batler, Registrator, null));
            batler.Restrictors.Add(new CohesiveFieldSourceRestrictor(new CohesiveFieldAttribute(typeof(TestHDBK2)) { Order = 1, GrouperPropertyName = "GroupValue" }, batler,
                                                              type => DbSession.Restrictors["HDBK2"].Data.Values, Registrator));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[1]));

            Parameter39 = Tuple.Create(paramName39, (IRequestFieldBatler)batler);

            //var fieldsDic = new Dictionary<string, RequestParameterInfo>();
            //fieldsDic.Add("Field39", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity39)) { DependsOnFields = new[] { "Field40" } },
            //    FieldProperty = _request.GetType().GetProperty("Field39"),
            //});
            //cohFieldDic.Add("Field39", new CohesiveParameterInfo()
            //{
            //    FieldProperty = _request.GetType().GetProperty("Field39"),
            //    Attribute = new CohesiveFieldAttribute(typeof(TestHDBK2)) { Order = 1, GrouperPropertyName = "GroupValue" },
            //    ValuePropertyName = "TestEntity39"
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName40),
                                                  DbSession.Repository[paramName40].Data.Values.Select(i => (TestEntity40)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity40), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter40 = Tuple.Create(paramName40, (IRequestFieldBatler)batler);

            //fieldsDic.Add("Field40", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity40)),
            //    FieldProperty = _request.GetType().GetProperty("Field40"),
            //    DependentFields = new[] { fieldsDic["Field39"] }
            //});

            const string paramName41 = "Field41", paramName42 = "Field42", paramName43 = "Field43";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName41),
                                                  DbSession.Repository[paramName41].Data.Values.Select(i => (TestEntity41)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity41), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            batler.Restrictors.Add(new CohesiveFieldSourceRestrictor(new CohesiveFieldAttribute(typeof(TestHDBK3)) { Order = 1 }, batler,
                                                              type => DbSession.Restrictors["HDBK3"].Data.Values, Registrator));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[1]));

            Parameter41 = Tuple.Create(paramName41, (IRequestFieldBatler)batler);

            //fieldsDic.Add("Field41", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity41)),
            //    FieldProperty = _request.GetType().GetProperty("Field41"),
            //});
            //cohFieldDic.Add("Field41", new CohesiveParameterInfo()
            //{
            //    FieldProperty = _request.GetType().GetProperty("Field41"),
            //    Attribute = new CohesiveFieldAttribute(typeof(TestHDBK3)) { Order = 1 }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName42),
                                                  DbSession.Repository[paramName42].Data.Values.Select(i => (TestEntity42)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity42), "Name") { DependsOnFields = new[] { "Field43" } },
                                                 batler, Registrator, null));
            batler.Restrictors.Add(new CohesiveFieldSourceRestrictor(new CohesiveFieldAttribute(typeof(TestHDBK3)) { Order = 2 }, batler,
                                                              type => DbSession.Restrictors["HDBK3"].Data.Values, Registrator));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[1]));

            Parameter42 = Tuple.Create(paramName42, (IRequestFieldBatler)batler);

            //fieldsDic.Add("Field42", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity42)) { DependsOnFields = new[] { "Field43" } },
            //    FieldProperty = _request.GetType().GetProperty("Field42"),
            //});
            //cohFieldDic.Add("Field42", new CohesiveParameterInfo()
            //{
            //    FieldProperty = _request.GetType().GetProperty("Field42"),
            //    Attribute = new CohesiveFieldAttribute(typeof(TestHDBK3)) { Order = 2 }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName43),
                                                  DbSession.Repository[paramName43].Data.Values.Select(i => (TestEntity43)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity43), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter43 = Tuple.Create(paramName43, (IRequestFieldBatler)batler);

            //fieldsDic.Add("Field43", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity43)),
            //    FieldProperty = _request.GetType().GetProperty("Field43"),
            //    DependentFields = new[] { fieldsDic["Field42"] }
            //});

            const string paramName44 = "Field44", paramName45 = "Field45";

            var mockFlexCondition = new Mock<IFlexCondition<TestRequest, TestEntity44>>();
            mockFlexCondition.Setup(m => m.IsSatisfiedBy(It.IsAny<TestRequest>())).Returns(entity => entity.Name.EndsWith("2"));
            var mockConditionFactory = new Mock<IFlexConditionKitFactory>();
            mockConditionFactory.Setup(m => m.MakeConditionByType(mockFlexCondition.Object.GetType()))
                                .Returns(new ConditionKit(RequestType, typeof(TestEntity44), mockFlexCondition.Object));

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName44),
                                                  DbSession.Repository[paramName44].Data.Values.Select(i => (TestEntity44)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity44), "Name") { DependsOnFields = new[] { "Field45" } },
                                                 batler, Registrator, null));
            batler.Restrictors.Add(new FlexibleFieldSourceRestrictor(new FlexibleFieldAttribute(mockFlexCondition.Object.GetType()), batler, mockConditionFactory.Object, Registrator));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[1]));

            Parameter44 = Tuple.Create(paramName44, (IRequestFieldBatler)batler);

            //var fieldsDic = new Dictionary<string, RequestParameterInfo>();
            //fieldsDic.Add("Field44", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity44)) { DependsOnFields = new[] { "Field45" } },
            //    FieldProperty = _request.GetType().GetProperty("Field44"),
            //});
            //var spec44 = new RestrictSpecInfo()
            //{
            //    Attribute = new FlexibleFieldAttribute(typeof(TestGraphFlexCondition)),
            //    FieldProperty = _request.GetType().GetProperty("Field44")
            //};

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName45),
                                                  DbSession.Repository[paramName45].Data.Values.Select(i => (TestEntity45)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity45), "Name") { DependsOnFields = new[] { "Field45" } },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter45 = Tuple.Create(paramName45, (IRequestFieldBatler)batler);

            //fieldsDic.Add("Field45", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity45)),
            //    FieldProperty = _request.GetType().GetProperty("Field45"),
            //    DependentFields = new List<RequestParameterInfo>() { fieldsDic["Field44"] }
            //});

            const string paramName46 = "Field46", paramName47 = "Field47", paramName48 = "Field48";

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName46),
                                                  DbSession.Repository[paramName46].Data.Values.Select(i => (TestEntity46)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity46), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));

            Parameter46 = Tuple.Create(paramName46, (IRequestFieldBatler)batler);

            //fieldsDic.Add("Field46", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity46)),
            //    FieldProperty = _request.GetType().GetProperty("Field46"),
            //    DependentFields = new[] { fieldsDic["Field47"] }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName47),
                                                  DbSession.Repository[paramName47].Data.Values.Select(i => (TestEntity47)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity47), "Name") { DependsOnFields = new[] { "Field46" } },
                                                 batler, Registrator, null));
            batler.Restrictors.Add(new CohesiveFieldSourceRestrictor(new CohesiveFieldAttribute(typeof(TestHDBK4)) { Order = 1 },
                                                 batler, type => DbSession.Restrictors["HDBK4"].Data.Values, Registrator));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[1]));

            Parameter47 = Tuple.Create(paramName47, (IRequestFieldBatler)batler);

            //fieldsDic.Add("Field47", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity47)) { DependsOnFields = new[] { "Field46" } },
            //    FieldProperty = _request.GetType().GetProperty("Field47"),
            //});
            //cohFieldDic.Add("Field47", new CohesiveParameterInfo()
            //{
            //    FieldProperty = _request.GetType().GetProperty("Field47"),
            //    Attribute = new CohesiveFieldAttribute(typeof(TestHDBK4)) { Order = 1 }
            //});

            rules = new List<ISelectionRule<IRequest>>();
            rules.Add(singleRule);
            restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();
            batler = new RequestFieldBatler<TestRequest>(RequestType.GetProperty(paramName48),
                                                  DbSession.Repository[paramName48].Data.Values.Select(i => (TestEntity48)i).ToList(), restrictors,
                                                  rules, this.Object, Creating);
            batler.SourceTypeProperty = "Name";
            rules.Add(new PreviousRestoreSelectionRule<IRequest>(batler));
            batler.Restrictors.Add(new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(TestEntity48), "Name") { DependsOnFields = new string[0] },
                                                 batler, Registrator, null));
            batler.Restrictors.Add(new CohesiveFieldSourceRestrictor(new CohesiveFieldAttribute(typeof(TestHDBK4)) { Order = 2 },
                                                 batler, type => DbSession.Restrictors["HDBK4"].Data.Values, Registrator));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[0]));
            Handler(null, new NewRestrictorEventArgs(batler.Restrictors[1]));

            Parameter48 = Tuple.Create(paramName48, (IRequestFieldBatler)batler);

            //fieldsDic.Add("Field48", new RequestParameterInfo()
            //{
            //    Attribute = new RequestFieldAttribute(typeof(TestEntity48)) { DependsOnFields = new[] { "Field48" } },
            //    FieldProperty = _request.GetType().GetProperty("Field48"),
            //});
            //cohFieldDic.Add("Field48", new CohesiveParameterInfo()
            //{
            //    FieldProperty = _request.GetType().GetProperty("Field48"),
            //    Attribute = new CohesiveFieldAttribute(typeof(TestHDBK4)) { Order = 2 }
            //});

            this.Setup(i => i[Parameter1.Item1]).Returns(Parameter1.Item2);
            this.Setup(i => i[Parameter2.Item1]).Returns(Parameter2.Item2);
            this.Setup(i => i[Parameter3.Item1]).Returns(Parameter3.Item2);
            this.Setup(i => i[Parameter4.Item1]).Returns(Parameter4.Item2);
            this.Setup(i => i[Parameter5.Item1]).Returns(Parameter5.Item2);
            this.Setup(i => i[Parameter6.Item1]).Returns(Parameter6.Item2);
            this.Setup(i => i[Parameter7.Item1]).Returns(Parameter7.Item2);
            this.Setup(i => i[Parameter8.Item1]).Returns(Parameter8.Item2);
            this.Setup(i => i[Parameter9.Item1]).Returns(Parameter9.Item2);
            this.Setup(i => i[Parameter10.Item1]).Returns(Parameter10.Item2);
            this.Setup(i => i[Parameter11.Item1]).Returns(Parameter11.Item2);
            this.Setup(i => i[Parameter12.Item1]).Returns(Parameter12.Item2);
            this.Setup(i => i[Parameter13.Item1]).Returns(Parameter13.Item2);
            this.Setup(i => i[Parameter14.Item1]).Returns(Parameter14.Item2);
            this.Setup(i => i[Parameter15.Item1]).Returns(Parameter15.Item2);
            this.Setup(i => i[Parameter16.Item1]).Returns(Parameter16.Item2);
            this.Setup(i => i[Parameter17.Item1]).Returns(Parameter17.Item2);
            this.Setup(i => i[Parameter18.Item1]).Returns(Parameter18.Item2);
            this.Setup(i => i[Parameter19.Item1]).Returns(Parameter19.Item2);
            this.Setup(i => i[Parameter20.Item1]).Returns(Parameter20.Item2);
            this.Setup(i => i[Parameter21.Item1]).Returns(Parameter21.Item2);
            this.Setup(i => i[Parameter22.Item1]).Returns(Parameter22.Item2);
            this.Setup(i => i[Parameter23.Item1]).Returns(Parameter23.Item2);
            this.Setup(i => i[Parameter24.Item1]).Returns(Parameter24.Item2);
            this.Setup(i => i[Parameter25.Item1]).Returns(Parameter25.Item2);
            this.Setup(i => i[Parameter26.Item1]).Returns(Parameter26.Item2);
            this.Setup(i => i[Parameter27.Item1]).Returns(Parameter27.Item2);
            this.Setup(i => i[Parameter28.Item1]).Returns(Parameter28.Item2);
            this.Setup(i => i[Parameter29.Item1]).Returns(Parameter29.Item2);
            this.Setup(i => i[Parameter30.Item1]).Returns(Parameter30.Item2);
            this.Setup(i => i[Parameter31.Item1]).Returns(Parameter31.Item2);
            this.Setup(i => i[Parameter32.Item1]).Returns(Parameter32.Item2);
            this.Setup(i => i[Parameter33.Item1]).Returns(Parameter33.Item2);
            this.Setup(i => i[Parameter34.Item1]).Returns(Parameter34.Item2);
            this.Setup(i => i[Parameter35.Item1]).Returns(Parameter35.Item2);
            this.Setup(i => i[Parameter36.Item1]).Returns(Parameter36.Item2);
            this.Setup(i => i[Parameter37.Item1]).Returns(Parameter37.Item2);
            this.Setup(i => i[Parameter38.Item1]).Returns(Parameter38.Item2);
            this.Setup(i => i[Parameter39.Item1]).Returns(Parameter39.Item2);
            this.Setup(i => i[Parameter40.Item1]).Returns(Parameter40.Item2);
            this.Setup(i => i[Parameter41.Item1]).Returns(Parameter41.Item2);
            this.Setup(i => i[Parameter42.Item1]).Returns(Parameter42.Item2);
            this.Setup(i => i[Parameter43.Item1]).Returns(Parameter43.Item2);
            this.Setup(i => i[Parameter44.Item1]).Returns(Parameter44.Item2);
            this.Setup(i => i[Parameter45.Item1]).Returns(Parameter45.Item2);
            this.Setup(i => i[Parameter46.Item1]).Returns(Parameter46.Item2);
            this.Setup(i => i[Parameter47.Item1]).Returns(Parameter47.Item2);
            this.Setup(i => i[Parameter48.Item1]).Returns(Parameter48.Item2);

            this.Setup(i => i[Parameter1.Item2.SourceType]).Returns(Parameter1.Item2);
            this.Setup(i => i[Parameter2.Item2.SourceType]).Returns(Parameter2.Item2);
            this.Setup(i => i[Parameter3.Item2.SourceType]).Returns(Parameter3.Item2);
            this.Setup(i => i[Parameter4.Item2.SourceType]).Returns(Parameter4.Item2);
            this.Setup(i => i[Parameter5.Item2.SourceType]).Returns(Parameter5.Item2);
            this.Setup(i => i[Parameter6.Item2.SourceType]).Returns(Parameter6.Item2);
            this.Setup(i => i[Parameter7.Item2.SourceType]).Returns(Parameter7.Item2);
            this.Setup(i => i[Parameter8.Item2.SourceType]).Returns(Parameter8.Item2);
            this.Setup(i => i[Parameter9.Item2.SourceType]).Returns(Parameter9.Item2);
            this.Setup(i => i[Parameter10.Item2.SourceType]).Returns(Parameter10.Item2);
            this.Setup(i => i[Parameter11.Item2.SourceType]).Returns(Parameter11.Item2);
            this.Setup(i => i[Parameter12.Item2.SourceType]).Returns(Parameter12.Item2);
            this.Setup(i => i[Parameter13.Item2.SourceType]).Returns(Parameter13.Item2);
            this.Setup(i => i[Parameter14.Item2.SourceType]).Returns(Parameter14.Item2);
            this.Setup(i => i[Parameter15.Item2.SourceType]).Returns(Parameter15.Item2);
            this.Setup(i => i[Parameter16.Item2.SourceType]).Returns(Parameter16.Item2);
            this.Setup(i => i[Parameter17.Item2.SourceType]).Returns(Parameter17.Item2);
            this.Setup(i => i[Parameter18.Item2.SourceType]).Returns(Parameter18.Item2);
            this.Setup(i => i[Parameter19.Item2.SourceType]).Returns(Parameter19.Item2);
            this.Setup(i => i[Parameter20.Item2.SourceType]).Returns(Parameter20.Item2);
            this.Setup(i => i[Parameter21.Item2.SourceType]).Returns(Parameter21.Item2);
            this.Setup(i => i[Parameter22.Item2.SourceType]).Returns(Parameter22.Item2);
            this.Setup(i => i[Parameter23.Item2.SourceType]).Returns(Parameter23.Item2);
            this.Setup(i => i[Parameter24.Item2.SourceType]).Returns(Parameter24.Item2);
            this.Setup(i => i[Parameter25.Item2.SourceType]).Returns(Parameter25.Item2);
            this.Setup(i => i[Parameter26.Item2.SourceType]).Returns(Parameter26.Item2);
            this.Setup(i => i[Parameter27.Item2.SourceType]).Returns(Parameter27.Item2);
            this.Setup(i => i[Parameter28.Item2.SourceType]).Returns(Parameter28.Item2);
            this.Setup(i => i[Parameter29.Item2.SourceType]).Returns(Parameter29.Item2);
            this.Setup(i => i[Parameter30.Item2.SourceType]).Returns(Parameter30.Item2);
            this.Setup(i => i[Parameter31.Item2.SourceType]).Returns(Parameter31.Item2);
            this.Setup(i => i[Parameter32.Item2.SourceType]).Returns(Parameter32.Item2);
            this.Setup(i => i[Parameter33.Item2.SourceType]).Returns(Parameter33.Item2);
            this.Setup(i => i[Parameter34.Item2.SourceType]).Returns(Parameter34.Item2);
            this.Setup(i => i[Parameter35.Item2.SourceType]).Returns(Parameter35.Item2);
            this.Setup(i => i[Parameter36.Item2.SourceType]).Returns(Parameter36.Item2);
            this.Setup(i => i[Parameter37.Item2.SourceType]).Returns(Parameter37.Item2);
            this.Setup(i => i[Parameter38.Item2.SourceType]).Returns(Parameter38.Item2);
            this.Setup(i => i[Parameter39.Item2.SourceType]).Returns(Parameter39.Item2);
            this.Setup(i => i[Parameter40.Item2.SourceType]).Returns(Parameter40.Item2);
            this.Setup(i => i[Parameter41.Item2.SourceType]).Returns(Parameter41.Item2);
            this.Setup(i => i[Parameter42.Item2.SourceType]).Returns(Parameter42.Item2);
            this.Setup(i => i[Parameter43.Item2.SourceType]).Returns(Parameter43.Item2);
            this.Setup(i => i[Parameter44.Item2.SourceType]).Returns(Parameter44.Item2);
            this.Setup(i => i[Parameter45.Item2.SourceType]).Returns(Parameter45.Item2);
            this.Setup(i => i[Parameter46.Item2.SourceType]).Returns(Parameter46.Item2);
            this.Setup(i => i[Parameter47.Item2.SourceType]).Returns(Parameter47.Item2);
            this.Setup(i => i[Parameter48.Item2.SourceType]).Returns(Parameter48.Item2);
        }



        private void InitCohesive()
        {
            //this.Setup(r => r.Restrictors).Returns(new Dictionary<Type, IEnumerable<IHDBK>>());
            //this.Setup(c => c.CohesiveFields).Returns(new Dictionary<string, IEnumerable<CohesiveParameterInfo>>());
        }

        private void InitRequestType()
        {
            RequestType = typeof(TestRequest);

            this.Setup(p => p.RequestType).Returns(RequestType);
        }
    }
}
