﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Moq;
using NUnit.Framework;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Impl;

namespace UGSK.K3.AppServices.DataDeriver.Test
{
    [TestFixture]
    //[Ignore]
    class CohesiveFieldSourceRestrictorTest
    {
        private CohesiveFieldSourceRestrictor _target;
        private CohesiveFieldAttribute _attr;
        private Mock<IRequestFieldBatler> _mockBatler;
        private Mock<Action<EventHandler<NewRestrictorEventArgs>, Type>> _mockRegistrator;
        private List<CohesiveTestHDBK> _restrictorColl;
        private Mock<ObservableCollection<IRequestFieldBatler>> _mockConcernedList;
        private Mock<Func<Type, IEnumerable<IHDBK>>> _mockCollFactory;
        private Mock<ITariffCalculationEngine<IRequest>> _mockEngine;
        private Mock<ITypeBuilder> _mockBuilder;

        private event EventHandler<NewRestrictorEventArgs> Handlers
        {
            add { handlers.Add(value); }
            remove { handlers.Remove(value); }
        } // перехватчик зарегистрированных обработчиков нового ограничителя
        private List<EventHandler<NewRestrictorEventArgs>> handlers = new List<EventHandler<NewRestrictorEventArgs>>();

        private void Invocator(NewRestrictorEventArgs args)
        {
            handlers.ForEach(h => h(null, args));
        }

        [TearDown]
        public void Clean()
        {
            handlers.Clear(); // из-за отсутствия этой штуки при прогонке тестов не в Debug-режиме в handlers скапливается какой-то мусор от предыдущих тестов, что ломает один кейс
        }

        [SetUp]
        public void Init()
        {
            _mockBuilder = new Mock<ITypeBuilder>();
            _mockEngine = new Mock<ITariffCalculationEngine<IRequest>>();
            _mockEngine.Setup(p => p.TypeBuilder).Returns(_mockBuilder.Object);

            _mockCollFactory = new Mock<Func<Type, IEnumerable<IHDBK>>>();
            _restrictorColl = new List<CohesiveTestHDBK>();
            _mockCollFactory.Setup(d => d(It.IsAny<Type>())).Returns(_restrictorColl);

            _mockConcernedList = new Mock<ObservableCollection<IRequestFieldBatler>>();

            _mockBatler = new Mock<IRequestFieldBatler>();
            _mockBatler.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity1));
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field1"));
            _mockBatler.Setup(p => p.ConcernedFieldBatlers).Returns(_mockConcernedList.Object);
            _mockBatler.Setup(p => p.Engine).Returns(_mockEngine.Object);

            _attr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK)) { Order = 1 };
            _mockRegistrator = new Mock<Action<EventHandler<NewRestrictorEventArgs>, Type>>();
            _mockRegistrator.Setup(m => m(It.IsAny<EventHandler<NewRestrictorEventArgs>>(), It.IsAny<Type>())).Callback((EventHandler<NewRestrictorEventArgs> h, Type t) => Handlers += h);
            _target = new CohesiveFieldSourceRestrictor(_attr, _mockBatler.Object, _mockCollFactory.Object, _mockRegistrator.Object);
        }

        [Test]
        public void SimpleVerify()
        {
            _mockRegistrator.Verify(m => m(It.IsAny<EventHandler<NewRestrictorEventArgs>>(), It.IsAny<Type>()));

            Assert.AreSame(_attr, _target.Attribute);
            Assert.AreSame(_mockBatler.Object, _target.Owner);
        }

        [Test]
        [Ignore]
        public void OnCreate_ListOfBatlers_ChildFoundInDirectOrder()
        {
            List<IRequestFieldBatler> actualList = new List<IRequestFieldBatler>();

            var similarOwner = new Mock<IRequestFieldBatler>();
            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK)) { Order = 2 };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);

            _mockConcernedList.As<ICollection<IRequestFieldBatler>>().Setup(m => m.Add(similarOwner.Object)).Callback((IRequestFieldBatler batler) => actualList.Add(batler));
            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            Assert.IsNotNull(actualList);
            Assert.AreEqual(1, actualList.Count());

            var actual = actualList.First();
            Assert.AreSame(similarOwner.Object, actual);
        }

        [Test]
        [Ignore]
        public void OnCreate_ListOfBatlers_ChildNotFoundInDirectOrderAndOrderIsInterrupted() // следующее поле не должно попасть в "зону влияния" текущего поля, т.к. между их Order есть разрыв (1, 3)
        {
            List<IRequestFieldBatler> actualList = new List<IRequestFieldBatler>();

            var similarOwner = new Mock<IRequestFieldBatler>();
            similarOwner.Setup(m => m.ConcernedFieldBatlers).Returns(_mockConcernedList.Object);
            _mockConcernedList.Setup(m => m.Add(similarOwner.Object)).Callback((IRequestFieldBatler batler) => actualList.Add(batler));

            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK)) { Order = 3 };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);


            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            CollectionAssert.IsEmpty(actualList);
        }

        [Test]
        [Ignore]
        public void OnCreate_ListOfBatlers_ChildFoundInReverseOrder()
        {
            handlers.Clear();
            List<IRequestFieldBatler> actualList = new List<IRequestFieldBatler>();

            var similarOwner = new Mock<IRequestFieldBatler>();
            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK)) { Order = 2 };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            _mockConcernedList.Setup(m => m.Add(similarOwner.Object)).Callback((IRequestFieldBatler batler) => actualList.Add(batler));
            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            _attr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK)) { Order = 1 };
            _mockRegistrator = new Mock<Action<EventHandler<NewRestrictorEventArgs>, Type>>();
            _mockRegistrator.Setup(m => m(It.IsAny<EventHandler<NewRestrictorEventArgs>>(), It.IsAny<Type>())).Callback((EventHandler<NewRestrictorEventArgs> h, Type type) => Handlers += h);
            _target = new CohesiveFieldSourceRestrictor(_attr, _mockBatler.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(_target));

            Assert.IsNotNull(actualList);
            Assert.AreEqual(1, actualList.Count());

            var actual = actualList.First();
            Assert.AreSame(similarOwner.Object, actual);
        }

        [Test]
        [Ignore]
        public void OnCreate_ListOfBatlers_ChildNotFoundInReverseOrderAndOrderIsInterrupted()
        {
            handlers.Clear();
            List<IRequestFieldBatler> actualList = new List<IRequestFieldBatler>();

            var similarOwner = new Mock<IRequestFieldBatler>();
            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK)) { Order = 3 };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            _mockConcernedList.Setup(m => m.Add(similarOwner.Object)).Callback((IRequestFieldBatler batler) => actualList.Add(batler));
            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            _attr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK)) { Order = 1 };
            _mockRegistrator = new Mock<Action<EventHandler<NewRestrictorEventArgs>, Type>>();
            _mockRegistrator.Setup(m => m(It.IsAny<EventHandler<NewRestrictorEventArgs>>(), It.IsAny<Type>())).Callback((EventHandler<NewRestrictorEventArgs> h, Type t) => Handlers += h);
            _target = new CohesiveFieldSourceRestrictor(_attr, _mockBatler.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(_target));

            CollectionAssert.IsEmpty(actualList);
        }

        [Test]
        [Ignore]
        public void OnCreate_ListOfBatlers_NotRegisterOnInvalidOrderWithinAttribute()
        {
            List<IRequestFieldBatler> actualList = new List<IRequestFieldBatler>();
            _mockConcernedList.Setup(m => m.Add(It.IsAny<IRequestFieldBatler>())).Callback((IRequestFieldBatler batler) => actualList.Add(batler));

            //var mockList = new Mock<IList<IRequestFieldBatler>>();
            //mockList.Setup(m => m.Add(It.IsAny<IRequestFieldBatler>())).Callback((IRequestFieldBatler batler) => actualList.Add(batler));
            //_mockBatler.Setup(p => p.ConcernedFieldBatlers).Returns(mockList.Object);

            var similarOwner = new Mock<IRequestFieldBatler>();
            similarOwner.Setup(p => p.ConcernedFieldBatlers).Returns(new ObservableCollection<IRequestFieldBatler>());
            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK)) { Order = 1 };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            CollectionAssert.IsEmpty(actualList);
        }

        [Test]
        [Ignore]
        public void OnCreate_ListOfBatlers_NotRegisterOnDifferentHDBKSourceTypeWithinAttribute()
        {
            List<IRequestFieldBatler> actualList = new List<IRequestFieldBatler>();

            var mockList = new Mock<ObservableCollection<IRequestFieldBatler>>();
            mockList.Setup(m => m.Add(It.IsAny<IRequestFieldBatler>())).Callback((IRequestFieldBatler batler) => actualList.Add(batler));
            _mockBatler.Setup(p => p.ConcernedFieldBatlers).Returns(mockList.Object);

            var similarOwner = new Mock<IRequestFieldBatler>();
            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK2)) { Order = 2 };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            CollectionAssert.IsEmpty(actualList);
        }

        [Test]
        [Ignore]
        public void OnCreate_ListOfBatlers_NotRegisterOnDifferentRestrictorType()
        {
            List<IRequestFieldBatler> actualList = new List<IRequestFieldBatler>();

            _mockConcernedList.Setup(m => m.Add(It.IsAny<IRequestFieldBatler>())).Callback((IRequestFieldBatler batler) => actualList.Add(batler));
            _mockBatler.Setup(p => p.ConcernedFieldBatlers).Returns(_mockConcernedList.Object);

            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK2)) { Order = 2 };
            var similarRestrictor = new Mock<IFieldSourceRestrictor<CohesiveFieldAttribute>>();
            similarRestrictor.Setup(p => p.Attribute).Returns(similarAttr);

            CollectionAssert.IsEmpty(actualList);
        }

        [Test]
        public void OnCreate_RegistratorIvoked_HDBKTypeIsNotRegisteredOnGroupNameIsExist() // если у атрибута указана группа, то должен быть зарегистрирован возвращаемый тип HDBK в typeBuilder'е вместо типа SimpleEntity (из Owner.SourceType), а также указано новое свойство, где источник сменен со свойства Name на свойство из HDBK, а коенечное свойство осталось прежним
        {
            _attr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK4)) { Order = 1 };

            _target = new CohesiveFieldSourceRestrictor(_attr, _mockBatler.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(_target));

            _mockBuilder.Verify(m => m.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()), Times.Never);
            _mockBuilder.Verify(m => m.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [Test]
        [Ignore]
        public void OnCreate_RegistratorIvoked_HDBKTypeIsRegisteredInsteadSimpleEntityTypeOnGroupNameIsExist() // если у атрибута указана группа, то должен быть зарегистрирован возвращаемый тип HDBK в typeBuilder'е вместо типа SimpleEntity (из Owner.SourceType), а также указано новое свойство, где источник сменен со свойства Name на свойство из HDBK, а коенечное свойство осталось прежним
        {
            var source = new List<SimpleEntity>() { new CohesiveTestEntity3() { Name = "1" }, new CohesiveTestEntity3() { Name = "2" }, new CohesiveTestEntity3() { Name = "3" } };

            var restrictorColl = new List<CohesiveTestHDBK3>();
            _mockCollFactory.Setup(d => d(It.IsAny<Type>())).Returns(restrictorColl);

            restrictorColl.AddRange(new List<CohesiveTestHDBK3>() { new CohesiveTestHDBK3() { CohesiveTestEntity3 = "1", Grouper = "G1" }, new CohesiveTestHDBK3() { CohesiveTestEntity3 = "2", Grouper = "G2" }, new CohesiveTestHDBK3() { CohesiveTestEntity3 = "3", Grouper = "G3" } });

            _attr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK3)) { Order = 1, GrouperPropertyName = "Grouper" };
            Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>> converter = null;

            _mockBuilder.Setup(
                m =>
                m.RegisterType(_mockBatler.Object.FieldProperty, typeof(CohesiveTestEntity3), typeof(CohesiveTestHDBK3),
                               It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()))
                        .Callback((Type type1, Type type2, Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>> func) =>
                            {
                                converter = func;
                            });
            _mockBatler = new Mock<IRequestFieldBatler>();
            _mockBatler.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity3));
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field3"));
            _mockBatler.Setup(p => p.ConcernedFieldBatlers).Returns(_mockConcernedList.Object);
            _mockBatler.Setup(p => p.Engine).Returns(_mockEngine.Object);

            _target = new CohesiveFieldSourceRestrictor(_attr, _mockBatler.Object, _mockCollFactory.Object, _mockRegistrator.Object);

            _mockBuilder.Verify(m => m.RegisterType(typeof(CohesiveTestRequest).GetProperty("Field3"), typeof(CohesiveTestEntity3), typeof(CohesiveTestHDBK3), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));
            _mockBuilder.Verify(m => m.RegisterProperty(typeof(CohesiveTestRequest).GetProperty("Field3"), "CohesiveTestEntity3", "Field3"));
            _mockBuilder.Verify(m => m.RegisterProperty(typeof(CohesiveTestRequest).GetProperty("Field3"), "Grouper", _attr.GrouperPropertyName));

            var request = new CohesiveTestRequest();

            var actual = converter(source.ToDictionary(k => k, v => true), request);

            CollectionAssert.AreEqual(restrictorColl, actual.Select(i => i.Key));
        }

        [Test]
        [Ignore]
        public void OnCreate_RegistratorIvoked_AdditionalPropertiesIsRegistetred() // при добавлении другому полю Cohesive-атрибута с такой же группой, что и у текущего поля, должно быть зарегистрировано новое свойство у обоих полей
        {
            var source6 = new List<SimpleEntity>() { new CohesiveTestEntity6() { Name = "A" }, new CohesiveTestEntity6() { Name = "B" }, new CohesiveTestEntity6() { Name = "C" } };

            var restrictorColl = new List<CohesiveTestHDBK5>();
            _mockCollFactory.Setup(d => d(It.IsAny<Type>())).Returns(restrictorColl);

            restrictorColl.AddRange(new List<CohesiveTestHDBK5>()
                {
                    new CohesiveTestHDBK5() {CohesiveTestEntity5 = "1", CohesiveTestEntity6 = "A",Grouper = "G1"},
                    new CohesiveTestHDBK5() {CohesiveTestEntity5 = "1", CohesiveTestEntity6 = "B",Grouper = "G2"},
                    new CohesiveTestHDBK5() {CohesiveTestEntity5 = "1", CohesiveTestEntity6 = "C",Grouper = "G3"},
                    new CohesiveTestHDBK5() {CohesiveTestEntity5 = "2", CohesiveTestEntity6 = "A", Grouper = "G4"},
                    new CohesiveTestHDBK5() {CohesiveTestEntity5 = "3", CohesiveTestEntity6 = "A", Grouper = "G5"}
                });

            _mockBatler = new Mock<IRequestFieldBatler>();
            _mockBatler.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity5));
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field5"));
            _mockBatler.Setup(p => p.ConcernedFieldBatlers).Returns(_mockConcernedList.Object);
            _mockBatler.Setup(p => p.Engine).Returns(_mockEngine.Object);

            _attr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK5)) { Order = 1, GrouperPropertyName = "Grouper" };
            _target = new CohesiveFieldSourceRestrictor(_attr, _mockBatler.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(_target));

            Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>> converter = null;
            _mockBuilder.Setup(
                m =>
                m.RegisterType(typeof(CohesiveTestRequest).GetProperty("Field5"), typeof(CohesiveTestEntity6), typeof(CohesiveTestHDBK5),
                               It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()))
                        .Callback((Type type1, Type type2, Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>> func) =>
                        {
                            converter = func;
                        });

            var similarOwner = new Mock<IRequestFieldBatler>();
            similarOwner.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity6));
            similarOwner.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field6"));
            similarOwner.Setup(p => p.ConcernedFieldBatlers).Returns(_mockConcernedList.Object);
            similarOwner.Setup(p => p.Engine).Returns(_mockEngine.Object);
            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK5)) { Order = 2, GrouperPropertyName = "Grouper" };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            _mockBuilder.Verify(m => m.RegisterProperty(typeof(CohesiveTestRequest).GetProperty("Field6"), "CohesiveTestEntity6", "Field6")); // проверка, что в старом типе зарегистрировано новое свойство
            _mockBuilder.Verify(m => m.RegisterProperty(typeof(CohesiveTestRequest).GetProperty("Field5"), "CohesiveTestEntity5", "Field5")); // проверка, что в новом типе зарегистрировано свойство старого типа

            var request = new CohesiveTestRequest();

            var actual = converter(source6.ToDictionary(k => k, v => true), request); // вызов бессмысленен, т.к. в реальных приложениях отбор по хвостовому (не являющемуся первым в группе по Order) никогда не должен произойти, т.к. хвостовые всегда идут в сцепке с ведущим полем

            CollectionAssert.IsEmpty(actual);

            request.Field5 = "1";

            actual = converter(source6.ToDictionary(k => k, v => true), request);

            var expected = restrictorColl.GetRange(0, 3);

            CollectionAssert.AreEqual(expected, actual.Select(i => i.Key));
        }

        [Test]
        public void OnCreate_RegistratorInvoked_FirstFieldInGroupTakeAwayConcernedFieldOfTailField() // при регистрации нового ведущего Group-атрибута все зависящие поля хвостовых элементов группы перебрасываются на новый атрибут
        {
            handlers.Clear();
            //var migratingList = new List<IRequestFieldBatler>();
            ObservableCollection<IRequestFieldBatler> actualListOld = new ObservableCollection<IRequestFieldBatler>();
            ObservableCollection<IRequestFieldBatler> actualList = new ObservableCollection<IRequestFieldBatler>();

            actualListOld.Add(new Mock<IRequestFieldBatler>().Object);
            actualListOld.Add(new Mock<IRequestFieldBatler>().Object);
            actualListOld.Add(new Mock<IRequestFieldBatler>().Object);
            Assert.AreEqual(3, actualListOld.Count);

            _mockBatler = new Mock<IRequestFieldBatler>();
            _mockBatler.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity7));
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field7"));
            _mockBatler.Setup(p => p.ConcernedFieldBatlers).Returns(actualListOld);
            _mockBatler.Setup(p => p.Engine).Returns(_mockEngine.Object);

            _attr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK6)) { Order = 2, GrouperPropertyName = "Grouper" };
            _target = new CohesiveFieldSourceRestrictor(_attr, _mockBatler.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(_target));

            var similarOwner = new Mock<IRequestFieldBatler>();
            similarOwner.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity8));
            similarOwner.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field8"));
            similarOwner.Setup(p => p.ConcernedFieldBatlers).Returns(actualList);
            similarOwner.Setup(p => p.Engine).Returns(_mockEngine.Object);
            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK6)) { Order = 1, GrouperPropertyName = "Grouper" };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            //CollectionAssert.IsEmpty(actualListOld);

            CollectionAssert.AreEqual(actualListOld, actualList);

            // todo Ремезов: здесь нужен тест на перенаправление редиректинга  хвостового поля на ведущее
            //var similarOwner2 = new Mock<IRequestFieldBatler>();
            //similarOwner2.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity9));
            //similarOwner2.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field8"));
            //similarOwner2.Setup(p => p.ConcernedFieldBatlers).Returns(actualList);
            //similarOwner2.Setup(p => p.Engine).Returns(_mockEngine.Object);
            //var similarAttr2 = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK6)) { Order = 1, GrouperPropertyName = "Grouper" };
            //var similarRestrictor2 = new CohesiveFieldSourceRestrictor(similarAttr2, similarOwner2.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            //Invocator(new NewRestrictorEventArgs(similarRestrictor2));
        }

        [Test]
        public void OnCreate_RegistratorInvoked_ChildFoundOnFirstFieldInGroup() //
        {
            handlers.Clear();
            //var migratingList = new List<IRequestFieldBatler>();
            ObservableCollection<IRequestFieldBatler> tailFieldConcernedList = new ObservableCollection<IRequestFieldBatler>();
            ObservableCollection<IRequestFieldBatler> firstFieldConcernedList = new ObservableCollection<IRequestFieldBatler>();

            _mockBatler = new Mock<IRequestFieldBatler>();
            _mockBatler.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity10));
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field10"));
            _mockBatler.Setup(p => p.ConcernedFieldBatlers).Returns(tailFieldConcernedList);
            _mockBatler.Setup(p => p.Engine).Returns(_mockEngine.Object);

            _attr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK6)) { Order = 2, GrouperPropertyName = "Grouper" };
            _target = new CohesiveFieldSourceRestrictor(_attr, _mockBatler.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(_target));

            var similarOwner = new Mock<IRequestFieldBatler>();
            similarOwner.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity11));
            similarOwner.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field11"));
            similarOwner.Setup(p => p.ConcernedFieldBatlers).Returns(firstFieldConcernedList);
            similarOwner.Setup(p => p.Engine).Returns(_mockEngine.Object);
            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK6)) { Order = 1, GrouperPropertyName = "Grouper" };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            //CollectionAssert.IsEmpty(actualListOld);

            CollectionAssert.AreEqual(tailFieldConcernedList, firstFieldConcernedList);

            var similarOwner2 = new Mock<IRequestFieldBatler>();
            similarOwner2.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity12));
            similarOwner2.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field12"));
            similarOwner2.Setup(p => p.Engine).Returns(_mockEngine.Object);
            var similarAttr2 = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK6)) { Order = 3 };
            var similarRestrictor2 = new CohesiveFieldSourceRestrictor(similarAttr2, similarOwner2.Object, _mockCollFactory.Object, _mockRegistrator.Object);
            Invocator(new NewRestrictorEventArgs(similarRestrictor2));

            Assert.AreEqual(1, firstFieldConcernedList.Count);
            CollectionAssert.IsEmpty(tailFieldConcernedList);
            CollectionAssert.Contains(firstFieldConcernedList, similarOwner2.Object);
        }

        [Test]
        public void GetRestrictedSource_RestrictedSource_ReturnOriginalCollectionOnAdjacentsAreAbsent()
        {
            var source = new List<CohesiveTestEntity1>() { new CohesiveTestEntity1() { Name = "1" }, new CohesiveTestEntity1() { Name = "2" }, new CohesiveTestEntity1() { Name = "3" } };
            _restrictorColl.Add(new CohesiveTestHDBK() { CohesiveTestEntity1 = "3" });

            var request = new CohesiveTestRequest();

            var expected = source.GetRange(2, 1); // 3
            var actual = _target.GetRestrictedSource(source, request);
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void GetRestrictedSource_RestrictedSource_OnNameGroupedCollectionWithFirstTookElementFromGroup()
        {
            var source = new List<CohesiveTestEntity1>() { new CohesiveTestEntity1() { Name = "1" }, new CohesiveTestEntity1() { Name = "3" }, new CohesiveTestEntity1() { Name = "3" } };
            _restrictorColl.Add(new CohesiveTestHDBK() { CohesiveTestEntity1 = "3" });

            var request = new CohesiveTestRequest();

            var expected = source.GetRange(1, 1); // 3
            var actual = _target.GetRestrictedSource(source, request);
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void GetRestrictedSource_RestrictedSource_OneAheadRestrictorImpacts()
        {
            _attr.Order = 2; // меняем обычный порядок. Теперь отборка сперва будет по полю Field2 в связке с CohesiveTestHDBK.CohesiveTestEntity2, а потом уже по полю Field1


            var similarMockList = new Mock<ObservableCollection<IRequestFieldBatler>>();
            var similarOwner = new Mock<IRequestFieldBatler>();
            similarOwner.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity2));
            similarOwner.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field2"));
            similarOwner.Setup(p => p.ConcernedFieldBatlers).Returns(similarMockList.Object);
            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK)) { Order = 1 };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);

            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            var source = new List<CohesiveTestEntity1>() { new CohesiveTestEntity1() { Name = "First" }, new CohesiveTestEntity1() { Name = "Second" }, new CohesiveTestEntity1() { Name = "Third" } };
            _restrictorColl.Add(new CohesiveTestHDBK() { CohesiveTestEntity2 = "3", CohesiveTestEntity1 = "First" });

            var request = new CohesiveTestRequest();

            var actual = _target.GetRestrictedSource(source, request).ToList();
            CollectionAssert.IsEmpty(actual);

            request.Field2 = "2";
            CollectionAssert.IsEmpty(actual);

            request.Field2 = "3";
            var expected = source.GetRange(0, 1);
            actual = _target.GetRestrictedSource(source, request).ToList();
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void GetRestrictedSource_RestrictedSource_OneBehindRestrictorDoNotMakeImpact() // Позади идущий ограничитель не оказывает влияния на выборку
        {
            var similarMockList = new Mock<ObservableCollection<IRequestFieldBatler>>();
            var similarOwner = new Mock<IRequestFieldBatler>();
            similarOwner.Setup(p => p.SourceType).Returns(typeof(CohesiveTestEntity2));
            similarOwner.Setup(p => p.FieldProperty).Returns(typeof(CohesiveTestRequest).GetProperty("Field2"));
            similarOwner.Setup(p => p.ConcernedFieldBatlers).Returns(similarMockList.Object);
            var similarAttr = new CohesiveFieldAttribute(typeof(CohesiveTestHDBK)) { Order = 2 };
            var similarRestrictor = new CohesiveFieldSourceRestrictor(similarAttr, similarOwner.Object, _mockCollFactory.Object, _mockRegistrator.Object);

            Invocator(new NewRestrictorEventArgs(similarRestrictor));

            var source = new List<CohesiveTestEntity1>() { new CohesiveTestEntity1() { Name = "First" }, new CohesiveTestEntity1() { Name = "Second" }, new CohesiveTestEntity1() { Name = "Third" } };
            _restrictorColl.Add(new CohesiveTestHDBK() { CohesiveTestEntity2 = "3", CohesiveTestEntity1 = "First" });

            var request = new CohesiveTestRequest();

            var actual = _target.GetRestrictedSource(source, request);
            var expected = source.GetRange(0, 1);
            CollectionAssert.AreEqual(expected, actual);

            request.Field2 = "2";
            var actual2 = _target.GetRestrictedSource(source, request);
            CollectionAssert.AreEqual(expected, actual2);
            CollectionAssert.AreEquivalent(actual, actual2);

            request.Field2 = "2";
            actual2 = _target.GetRestrictedSource(source, request);
            CollectionAssert.AreEqual(expected, actual2);
            CollectionAssert.AreEquivalent(actual, actual2);
        }
    }


    public class CohesiveTestHDBK : IHDBK
    {
        public string CohesiveTestEntity1 { get; set; }

        public string CohesiveTestEntity2 { get; set; }
    }

    public class CohesiveTestHDBK2 : IHDBK { }

    public class CohesiveTestHDBK3 : IHDBK
    {
        public string Grouper { get; set; }
        public string CohesiveTestEntity3 { get; set; }
    }

    public class CohesiveTestHDBK4 : IHDBK { }

    public class CohesiveTestHDBK5 : IHDBK
    {
        public string Grouper { get; set; }
        public string CohesiveTestEntity5 { get; set; }
        public string CohesiveTestEntity6 { get; set; }
    }

    public class CohesiveTestHDBK6 : IHDBK
    {
        public string Grouper { get; set; }
        public string CohesiveTestEntity7 { get; set; }
        public string CohesiveTestEntity8 { get; set; }
    }

    public class CohesiveTestRequest : IRequest
    {
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
        public string Field9 { get; set; }
        public string Field10 { get; set; }
        public string Field11 { get; set; }
        public string Field12 { get; set; }
    }

    public class CohesiveTestEntity1 : SimpleEntity { }

    public class CohesiveTestEntity2 : SimpleEntity { }

    public class CohesiveTestEntity3 : SimpleEntity { }

    public class CohesiveTestEntity4 : SimpleEntity { }

    public class CohesiveTestEntity5 : SimpleEntity { }

    public class CohesiveTestEntity6 : SimpleEntity { }

    public class CohesiveTestEntity7 : SimpleEntity { }

    public class CohesiveTestEntity8 : SimpleEntity { }

    public class CohesiveTestEntity9 : SimpleEntity { }

    public class CohesiveTestEntity10 : SimpleEntity { }

    public class CohesiveTestEntity11 : SimpleEntity { }

    public class CohesiveTestEntity12 : SimpleEntity { }
}
