﻿using Moq;
using NUnit.Framework;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.AppServices.DataDeriver.Core.Impl;
using UGSK.K3.AppServices.DataDeriver.Strategies;
using UGSK.K3.AppServices.DataDeriver.Test.Mocks;
using UGSK.K3.AppServices.DataDeriver.Test.Mocks.Originals;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Test
{
    [TestFixture]
    public class TariffCalculationEngineTest
    {
        private TariffCalculationEngine<GraphMapRequest> _target;
        private Mock<ICalculator> _calculatorMock;
        private GraphMapRequest _request;
        private MockDbSession _dbSessionMock;
        private Mock<IDeriveStrategyFactory> _facStratMock;
        private Mock<ISourceDeriveStrategy> _strategyMock;
        private Mock<IBatlerFactory> _mockFactoryBatler;
        private Mock<ITypeBuilder> _mockBuilder;

        [SetUp]
        public void Init()
        {
            _mockBuilder = new Mock<ITypeBuilder>();
            _mockFactoryBatler = new Mock<IBatlerFactory>();
            _request = new GraphMapRequest();
            _calculatorMock = new Mock<ICalculator>();
            _dbSessionMock = new MockDbSession();
            _facStratMock = new Mock<IDeriveStrategyFactory>();
            _facStratMock = new Mock<IDeriveStrategyFactory>();
            _strategyMock = new Mock<ISourceDeriveStrategy>();
            //_facStratMock.Setup(m => m.GetStrategy("WithField")).Returns();

            //var fakeCollection = new Mock<IEnumerable<BaseEntity>>();
            //dbSessionMock.SetupProperty(p => p.Collection3, fakeCollection.Object);

            _calculatorMock.SetupGet(p => p.DbSession).Returns(_dbSessionMock.Object);
            _calculatorMock.Setup(p => p.ProductName).Returns("KASKO");

            _target = new TariffCalculationEngine<GraphMapRequest>(_mockFactoryBatler.Object, _mockBuilder.Object);
        }

        //[Test]
        //public void RequestType_Type_TypeMatchsGeneric()
        //{
        //    Assert.AreEqual(typeof(GraphMapRequest), _target.RequestType);
        //}

        //[Test]
        //public void Fields_SingleRelation()
        //{
        //    var actual1 = _target.Fields.FirstOrDefault(i => i.Key == "Field1").Value;

        //    Assert.IsNotNull(actual1);
        //    Assert.AreEqual(_request.GetType().GetProperty("Field1"), actual1.FieldProperty);
        //    Assert.AreEqual(typeof(TestEntity1), actual1.Attribute.SourceType);
        //    Assert.IsTrue(actual1.IsIndependent);
        //    Assert.AreEqual("Name", actual1.Attribute.ValuePropertyName);
        //    CollectionAssert.IsEmpty(actual1.Attribute.DependsOnFields);
        //    CollectionAssert.Contains(_target.Repositories.Keys, typeof(TestEntity1));

        //    var actual2 = actual1.DependentFields[0];

        //    var actual2Diff = actual1 = _target.Fields.FirstOrDefault(i => i.Key == "Field2").Value;
        //    Assert.AreSame(actual2, actual2Diff);

        //    Assert.IsNotNull(actual2);
        //    Assert.AreEqual(_request.GetType().GetProperty("Field2"), actual1.FieldProperty);
        //    Assert.AreEqual(typeof(TestEntity2), actual2.Attribute.SourceType);
        //    Assert.IsFalse(actual2.IsIndependent);
        //    Assert.AreEqual("Name", actual2.Attribute.ValuePropertyName);
        //    CollectionAssert.IsEmpty(actual2.DependentFields);
        //}

        //[Test]
        //public void Fields_DoubleRelation()
        //{
        //    var actual3 = _target.Fields.FirstOrDefault(i => i.Key == "Field3").Value;

        //    Assert.IsNotNull(actual3);
        //    Assert.AreEqual(_request.GetType().GetProperty("Field3"), actual3.FieldProperty);
        //    Assert.AreEqual(typeof(TestEntity3), actual3.Attribute.SourceType);
        //    Assert.IsTrue(actual3.IsIndependent);
        //    Assert.AreEqual("Name", actual3.Attribute.ValuePropertyName);
        //    CollectionAssert.IsEmpty(actual3.Attribute.DependsOnFields);
        //    CollectionAssert.Contains(_target.Repositories.Keys, typeof(TestEntity3));

        //    var actual4 = _target.Fields.FirstOrDefault(i => i.Key == "Field4").Value;

        //    Assert.IsNotNull(actual4);
        //    Assert.AreEqual(_request.GetType().GetProperty("Field4"), actual4.FieldProperty);
        //    Assert.AreEqual(typeof(TestEntity4), actual4.Attribute.SourceType);
        //    Assert.IsTrue(actual4.IsIndependent);
        //    Assert.AreEqual("Name", actual4.Attribute.ValuePropertyName);            
        //    CollectionAssert.IsEmpty(actual4.Attribute.DependsOnFields);
        //    CollectionAssert.Contains(_target.Repositories.Keys, typeof(TestEntity4));

        //    var actual5 = actual3.DependentFields[0];
        //    var actual5Double = actual4.DependentFields[0];

        //    Assert.AreSame(actual5, actual5Double);

        //    var actual5Diff = _target.Fields.FirstOrDefault(i => i.Key == "Field5").Value;
        //    Assert.AreSame(actual5, actual5Diff);

        //    Assert.IsNotNull(actual5);
        //    Assert.AreEqual(_request.GetType().GetProperty("Field5"), actual5.FieldProperty);
        //    Assert.AreEqual(typeof(TestEntity5), actual5.Attribute.SourceType);
        //    Assert.IsFalse(actual5.IsIndependent);
        //    Assert.AreEqual("Name", actual5.Attribute.ValuePropertyName);
        //    CollectionAssert.IsEmpty(actual5.DependentFields);
        //}

        //[Test]
        //public void Fields_TwoChild()
        //{
        //    var actual6 = _target.Fields.FirstOrDefault(i => i.Key == "Field6").Value;

        //    Assert.IsNotNull(actual6);
        //    Assert.AreEqual(_request.GetType().GetProperty("Field6"), actual6.FieldProperty);
        //    Assert.AreEqual(typeof(TestEntity6), actual6.Attribute.SourceType);
        //    CollectionAssert.Contains(_target.Repositories.Keys, typeof(TestEntity6));

        //    var actual7 = _target.Fields.FirstOrDefault(i => i.Key == "Field7").Value;
        //    var actual8 = _target.Fields.FirstOrDefault(i => i.Key == "Field8").Value;

        //    CollectionAssert.Contains(actual6.DependentFields, actual7);
        //    CollectionAssert.Contains(actual6.DependentFields, actual8);
        //}

        //[Test]
        //public void CohesiveFields_SameAdjacents()
        //{
        //    CollectionAssert.Contains(_target.Restrictors.Keys, typeof(TestHDBK1));

        //    var actual9 = _target.Fields.FirstOrDefault(i => i.Key == "Field9").Value;

        //    Assert.IsNotNull(actual9);

        //    var actualCoh9 = _target.CohesiveFields.FirstOrDefault(i => i.Key == "Field9").Value.Single();

        //    Assert.AreEqual(_request.GetType().GetProperty("Field9"), actualCoh9.FieldProperty);
        //    Assert.AreEqual(actualCoh9.FieldProperty, actual9.FieldProperty);

        //    Assert.AreEqual("TestEntity9", actualCoh9.ValuePropertyName);

        //    var actualCoh10 = _target.CohesiveFields.FirstOrDefault(i => i.Key == "Field10").Value.Single();

        //    CollectionAssert.AreEqual(actualCoh9.AdjacentFields, actualCoh10.AdjacentFields);
        //}

    }
}