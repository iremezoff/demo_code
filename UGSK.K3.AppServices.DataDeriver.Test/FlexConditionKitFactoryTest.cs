﻿using System;
using System.Linq.Expressions;
using Moq;
using NUnit.Framework;
using UGSK.K3.Product.Contract;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Flexible.Impl;

namespace UGSK.K3.AppServices.DataDeriver.Test
{
    [TestFixture]
    class FlexConditionKitFactoryTest
    {
        private FlexConditionKitFactory _target;
        private Mock<Func<Type, object>> _mockFunc;

        [SetUp]
        public void Init()
        {
            _mockFunc = new Mock<Func<Type, object>>();
            _mockFunc.Setup(p => p(typeof(FlexConditionKitFactoryTestCondition))).Returns(new FlexConditionKitFactoryTestCondition());

            _target = new FlexConditionKitFactory(_mockFunc.Object);
        }

        [Test]
        public void MakeSpecificationByType_SpecKit_TypesMatchs()
        {
            var actual = _target.MakeConditionByType(typeof(FlexConditionKitFactoryTestCondition));

            Assert.AreEqual(typeof(FlexConditionKitFactoryTestRequest), actual.RequestType);

            Assert.AreEqual(typeof(FlexConditionKitFactoryTestEntity), actual.EntityType);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "FlexConditionKitFactoryBadTestCondition", MatchType = MessageMatch.Contains)]
        public void MakeSpecificationByType_SpecKit_ExceptionOnNonRestrictType()
        {
            var actual = _target.MakeConditionByType(typeof(FlexConditionKitFactoryBadTestCondition));
        }
    }

    public class FlexConditionKitFactoryBadTestCondition { }

    public class FlexConditionKitFactoryTestRequest : IRequest { }

    public class FlexConditionKitFactoryTestEntity : SimpleEntity { }

    public class FlexConditionKitFactoryTestCondition : IFlexCondition<FlexConditionKitFactoryTestRequest, FlexConditionKitFactoryTestEntity>
    {
        public Expression<Func<FlexConditionKitFactoryTestEntity, bool>> IsSatisfiedBy(FlexConditionKitFactoryTestRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
