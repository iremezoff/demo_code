﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using Moq;
using NUnit.Framework;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Impl;

namespace UGSK.K3.AppServices.DataDeriver.Test
{
    [TestFixture]
    //[Ignore]
    class DefaultFieldSourceRestrictorTest
    {
        private DefaultFieldSourceRestrictor _target;
        private RequestFieldAttribute _attr;
        private Mock<IRequestFieldBatler> _mockBatler;
        private Mock<Action<EventHandler<NewRestrictorEventArgs>, Type>> _mockRegistrator;
        private DefaultFieldSourceRestrictor _mockSimilar;
        private Mock<IRequestFieldBatler> _similarOwner;
        private event EventHandler<NewRestrictorEventArgs> Handlers; // перехватчик зарегистрированных обработчиков нового ограничителя
        private DefaultRestrictorRequest _request;

        [SetUp]
        public void Init()
        {
            _request = new DefaultRestrictorRequest();
            _attr = new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity1));
            _mockBatler = new Mock<IRequestFieldBatler>();
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(DefaultRestrictorRequest).GetProperty("Field1"));
            _mockBatler.Setup(m => m.Engine.TypeBuilder.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()));
            _mockBatler.Setup(m => m.Engine.TypeBuilder.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));

            _mockRegistrator = new Mock<Action<EventHandler<NewRestrictorEventArgs>, Type>>();

            _similarOwner = new Mock<IRequestFieldBatler>();
            _similarOwner.Setup(p => p.FieldProperty).Returns(typeof(DefaultRestrictorRequest).GetProperty("Field2"));
            _similarOwner.Setup(m => m.Engine.TypeBuilder.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()));
            _similarOwner.Setup(m => m.Engine.TypeBuilder.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));


            _mockRegistrator.Setup(m => m(It.IsAny<EventHandler<NewRestrictorEventArgs>>(), It.IsAny<Type>())).Callback((EventHandler<NewRestrictorEventArgs> h, Type type) =>
            {
                Handlers += h;
            });

            _target = new DefaultFieldSourceRestrictor(_attr, _mockBatler.Object, _mockRegistrator.Object, null);
        }

        [Test]
        public void SimpleVerify()
        {
            _mockRegistrator.Verify(m => m(It.IsAny<EventHandler<NewRestrictorEventArgs>>(), It.IsAny<Type>()));

            Assert.AreSame(_attr, _target.Attribute);
            Assert.AreSame(_mockBatler.Object, _target.Owner);
        }

        [Test]
        [Ignore]
        public void GetDependentsFieldBatlers_ListOfBatlers_ChildFound()
        {
            ObservableCollection<IRequestFieldBatler> actual = new ObservableCollection<IRequestFieldBatler>();

            var mockList = new Mock<ObservableCollection<IRequestFieldBatler>>();
            mockList.Setup(m => m.Add(It.IsAny<IRequestFieldBatler>())).Callback((IRequestFieldBatler batler) => actual.Add(batler));
            _mockBatler.Setup(p => p.ConcernedFieldBatlers).Returns(mockList.Object);

            _similarOwner = new Mock<IRequestFieldBatler>();
            _similarOwner.Setup(p => p.FieldProperty).Returns(typeof(DefaultRestrictorRequest).GetProperty("Field2"));
            _similarOwner.Setup(m => m.Engine.TypeBuilder.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()));
            _similarOwner.Setup(m => m.Engine.TypeBuilder.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));

            _mockSimilar = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity2)) { DependsOnFields = new[] { "Field1" } }, _similarOwner.Object, _mockRegistrator.Object, null);

            Handlers(null, new NewRestrictorEventArgs(_mockSimilar));

            //var actual = _target.GetDependentsFieldBatlers();
            var expected = new List<IRequestFieldBatler>() { _similarOwner.Object };

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        [Ignore]
        public void GetDependentsFieldBatlers_ListOfBatlers_ChildFound2()
        {
            Handlers = null;

            List<IRequestFieldBatler> actual = new List<IRequestFieldBatler>();

            var mockList = new Mock<ObservableCollection<IRequestFieldBatler>>();
            mockList.Setup(m => m.Add(It.IsAny<IRequestFieldBatler>())).Callback((IRequestFieldBatler batler) => actual.Add(batler));

            _similarOwner = new Mock<IRequestFieldBatler>();
            _similarOwner.Setup(p => p.FieldProperty).Returns(typeof(DefaultRestrictorRequest).GetProperty("Field3"));
            _similarOwner.Setup(m => m.Engine.TypeBuilder.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()));
            _similarOwner.Setup(m => m.Engine.TypeBuilder.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));

            _mockSimilar = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity3)) { DependsOnFields = new[] { "Field4" } }, _similarOwner.Object, _mockRegistrator.Object, null);
            Handlers(null, new NewRestrictorEventArgs(_mockSimilar));

            _mockBatler = new Mock<IRequestFieldBatler>();
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(DefaultRestrictorRequest).GetProperty("Field4"));
            _mockBatler.Setup(p => p.ConcernedFieldBatlers).Returns(mockList.Object);
            _mockBatler.Setup(m => m.Engine.TypeBuilder.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()));
            _mockBatler.Setup(m => m.Engine.TypeBuilder.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));

            _target = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity4)), _mockBatler.Object, _mockRegistrator.Object, null);

            Handlers(null, new NewRestrictorEventArgs(_target));

            var expected = new List<IRequestFieldBatler>() { _similarOwner.Object };

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void GetDependentsFieldBatlers_ListOfBatlers_NotRegisterOnInvalidRequestFieldAttribute()
        {
            _mockSimilar = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity5)) { DependsOnFields = new[] { "Field5" } }, _similarOwner.Object, _mockRegistrator.Object, null); // попытка указать в качестве родительского поля самого себя

            Handlers(null, new NewRestrictorEventArgs(_mockSimilar));

            List<IRequestFieldBatler> actual = new List<IRequestFieldBatler>();

            var mockList = new Mock<IList<IRequestFieldBatler>>();
            mockList.Setup(m => m.Add(It.IsAny<IRequestFieldBatler>())).Callback((IRequestFieldBatler batler) => actual.Add(batler));

            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void GetRestrictedSource_RestrictedSource_ReturnOriginalCollectionOnDependencesAreAbsent()
        {
            var source = new List<DefaultRestrictorTestEntity6>()
                {
                    new DefaultRestrictorTestEntity6() {Name = "1"},
                    new DefaultRestrictorTestEntity6() {Name = "2"},
                    new DefaultRestrictorTestEntity6() {Name = "3"}
                };

            _target = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity6)), _mockBatler.Object, _mockRegistrator.Object, null);
            var actual = _target.GetRestrictedSource(source, _request);

            CollectionAssert.AreEqual(source, actual);
        }

        [Test]
        public void GetRestrictedSource_RestrictedSource_GroupedResultWithFirstTookElement()
        {
            var source = new List<DefaultRestrictorTestEntity6>()
                {
                    new DefaultRestrictorTestEntity6() {Name = "1"},
                    new DefaultRestrictorTestEntity6() {Name = "1"},
                    new DefaultRestrictorTestEntity6() {Name = "3"}
                };
            var expected = new List<DefaultRestrictorTestEntity6> { source[0], source[2] };

            _target = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity6)), _mockBatler.Object, _mockRegistrator.Object, null);
            var actual = _target.GetRestrictedSource(source, _request);

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        [Ignore]
        public void GetRestrictedSource_RestrictedSource_ReturnCollectionOnOneDependences()
        {
            _mockBatler = new Mock<IRequestFieldBatler>();

            _similarOwner = new Mock<IRequestFieldBatler>();
            _similarOwner.Setup(p => p.SourceType).Returns(typeof(DefaultRestrictorTestEntity6));
            _similarOwner.Setup(p => p.FieldProperty).Returns(typeof(DefaultRestrictorRequest).GetProperty("Field6"));
            _similarOwner.Setup(p => p.ConcernedFieldBatlers).Returns(new ObservableCollection<IRequestFieldBatler>());
            _similarOwner.Setup(m => m.Engine.TypeBuilder.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()));
            _similarOwner.Setup(m => m.Engine.TypeBuilder.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));

            _mockSimilar = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity6)), _similarOwner.Object, _mockRegistrator.Object, null);
            Handlers(null, new NewRestrictorEventArgs(_mockSimilar));

            _mockBatler.Setup(p => p.SourceType).Returns(typeof(DefaultRestrictorTestEntity7));
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(DefaultRestrictorRequest).GetProperty("Field7"));
            _mockBatler.Setup(m => m.Engine.TypeBuilder.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()));
            _mockBatler.Setup(m => m.Engine.TypeBuilder.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));

            _target = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity7)) { DependsOnFields = new[] { "Field6" } }, _mockBatler.Object, _mockRegistrator.Object, null);
            Handlers(null, new NewRestrictorEventArgs(_target));

            var source = new List<DefaultRestrictorTestEntity7>()
                {
                    new DefaultRestrictorTestEntity7() {Field6="Test", Name = "1"},
                    new DefaultRestrictorTestEntity7() {Name = "1"},
                    new DefaultRestrictorTestEntity7() {Name = "3"}
                };

            var actual = _target.GetRestrictedSource(source, _request);
            CollectionAssert.IsEmpty(actual);

            _request.Field6 = "Test";
            actual = _target.GetRestrictedSource(source, _request);
            var expected = new List<DefaultRestrictorTestEntity7>() { source[0] };

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void GetRestrictedSource_RestrictedSource_ReturnCollectionOnTwiceDependences()
        {
            _mockBatler = new Mock<IRequestFieldBatler>();

            _similarOwner = new Mock<IRequestFieldBatler>();
            _similarOwner.Setup(p => p.SourceType).Returns(typeof(DefaultRestrictorTestEntity8));
            _similarOwner.Setup(p => p.FieldProperty).Returns(typeof(DefaultRestrictorRequest).GetProperty("Field8"));
            _similarOwner.Setup(p => p.ConcernedFieldBatlers).Returns(new ObservableCollection<IRequestFieldBatler>());
            _similarOwner.Setup(m => m.Engine.TypeBuilder.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()));
            _similarOwner.Setup(m => m.Engine.TypeBuilder.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));

            _mockSimilar = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity6)), _similarOwner.Object, _mockRegistrator.Object, null);
            Handlers(null, new NewRestrictorEventArgs(_mockSimilar));

            var similarOwner2 = new Mock<IRequestFieldBatler>();
            similarOwner2.Setup(p => p.SourceType).Returns(typeof(DefaultRestrictorTestEntity9));
            similarOwner2.Setup(p => p.FieldProperty).Returns(typeof(DefaultRestrictorRequest).GetProperty("Field9"));
            similarOwner2.Setup(p => p.ConcernedFieldBatlers).Returns(new ObservableCollection<IRequestFieldBatler>());
            similarOwner2.Setup(m => m.Engine.TypeBuilder.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()));
            similarOwner2.Setup(m => m.Engine.TypeBuilder.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));

            var mockSimilar2 = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity6)), similarOwner2.Object, _mockRegistrator.Object, null);
            Handlers(null, new NewRestrictorEventArgs(mockSimilar2));

            _mockBatler.Setup(p => p.SourceType).Returns(typeof(DefaultRestrictorTestEntity10));
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(DefaultRestrictorRequest).GetProperty("Field10"));
            _mockBatler.Setup(m => m.Engine.TypeBuilder.RegisterProperty(It.IsAny<PropertyInfo>(), It.IsAny<string>(), It.IsAny<string>()));
            _mockBatler.Setup(m => m.Engine.TypeBuilder.RegisterType(It.IsAny<PropertyInfo>(), It.IsAny<Type>(), It.IsAny<Type>(), It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));

            _target = new DefaultFieldSourceRestrictor(new RequestFieldAttribute(typeof(DefaultRestrictorTestEntity10)) { DependsOnFields = new[] { "Field8", "Field9" } }, _mockBatler.Object, _mockRegistrator.Object, null);
            Handlers(null, new NewRestrictorEventArgs(_target));

            var source = new List<DefaultRestrictorTestEntity10>()
                {
                    new DefaultRestrictorTestEntity10() {Field8="Test", Field9="Tset", Name = "1"},
                    new DefaultRestrictorTestEntity10() {Field8="Test",Field9="Tset2",Name = "2"},
                    new DefaultRestrictorTestEntity10() {Field8="Test2",Field9="Tset",Name = "3"}
                };

            var actual = _target.GetRestrictedSource(source, _request);
            CollectionAssert.IsEmpty(actual);

            _request.Field8 = "Test";

            actual = _target.GetRestrictedSource(source, _request);

            CollectionAssert.IsEmpty(actual);

            _request.Field9 = "Tset";
            actual = _target.GetRestrictedSource(source, _request);
            var expected = new List<DefaultRestrictorTestEntity10>() { source[0] };

            CollectionAssert.AreEqual(expected, actual);
        }
    }

    public class DefaultRestrictorTestEntity1 : SimpleEntity { }

    public class DefaultRestrictorTestEntity2 : SimpleEntity
    {
        public string Field1 { get; set; }
    }

    public class DefaultRestrictorTestEntity3 : SimpleEntity
    {
        public string Field4 { get; set; }
    }

    public class DefaultRestrictorTestEntity4 : SimpleEntity { }

    public class DefaultRestrictorTestEntity5 : SimpleEntity { }

    public class DefaultRestrictorTestEntity6 : SimpleEntity { }

    public class DefaultRestrictorTestEntity7 : SimpleEntity
    {
        public string Field6 { get; set; }
    }

    public class DefaultRestrictorTestEntity8 : SimpleEntity { }

    public class DefaultRestrictorTestEntity9 : SimpleEntity { }

    public class DefaultRestrictorTestEntity10 : SimpleEntity
    {
        public string Field8 { get; set; }
        public string Field9 { get; set; }
    }

    public class DefaultRestrictorRequest : IRequest
    {
        public string Field1 { get; set; }

        public string Field2 { get; set; }

        public string Field3 { get; set; }

        public string Field4 { get; set; }

        public string Field5 { get; set; }

        public string Field6 { get; set; }

        public string Field7 { get; set; }

        public string Field8 { get; set; }

        public string Field9 { get; set; }

        public string Field10 { get; set; }
    }
}
