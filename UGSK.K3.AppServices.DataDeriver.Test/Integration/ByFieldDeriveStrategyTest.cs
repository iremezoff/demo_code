﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using UGSK.K3.AppServices.DataDeriver.Product.Field;
using UGSK.K3.AppServices.DataDeriver.Strategies.Impl;
using UGSK.K3.AppServices.DataDeriver.Test.Mocks;
using UGSK.K3.AppServices.DataDeriver.Core.Impl;
using UGSK.K3.AppServices.DataDeriver.Test.Mocks.Originals;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Test.Integration
{
    [TestFixture]
    public class ByFieldDeriveStrategyTest
    {
        private ByFieldDeriveStrategy<TestRequest> _target;
        private MockCalcEngine _engineMock;
        private TestRequest _request;
        private Mock<ICalculator> _calculatorMock;
        private MockDbSession _dbSessionMock;

        [SetUp]
        public void Setup()
        {
            _request = new TestRequest();

            _engineMock = new MockCalcEngine();
            _dbSessionMock = _engineMock.DbSession;
            _engineMock.Setup(p => p.RequestType).Returns(_request.GetType());
            _calculatorMock = new Mock<ICalculator>();
            _calculatorMock.Setup(p => p.DbSession).Returns(_dbSessionMock.Object);
            //_engineMock.Setup(p => p.Calculator).Returns(_calculatorMock.Object);
        }

        // todo Ремезов: вомзожно этот тест стоить расширить, например для сложных связей
        /// <summary>
        /// Тест проверяет, что при изменении какого-то параметра запроса он не будет выдан в списке параметров. Элемент списка параметров содержит набор значений для переинициализации, т.е. допустимый выбор для переданного текущего запроса
        /// </summary>
        [Test]
        public void ExtractRepositories_ListOfRepository_ThereIsNoChangedFieldRepositoryInReturnList()
        {
            //_engineMock.Setup(p => p.SpecRestrictors).Returns(new Dictionary<Type, IEnumerable<RestrictSpecInfo>>());

            _request.Field1 = _dbSessionMock.Repository[_engineMock.Parameter1.Item1].Data[0].Name;
            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, _engineMock.Parameter1.Item1);
            var actual = _target.ExtractRepositories();

            CollectionAssert.DoesNotContain(actual.Select(i => i.FieldName), _engineMock.Parameter1.Item1, "При изменении поля запроса нельзя возвращать FieldRepository для этого же поля");
        }

        #region RelationsTests

        #region CascadeTwoUnitChain

        private static object[] CascadeTwoUnitChain = new object[]
            {
                new object[]
                    {
                        null,
                        null,
                        Tuple.Create("Field1", (int?)1),
                        new Dictionary<string, bool> {{"Field2", true}}
                    },
                new object[]
                    {
                        0,
                        null,
                        Tuple.Create("Field1", (int?)1),
                        new Dictionary<string, bool> {{"Field2", true}}
                    },
                new object[]
                    {
                        0,
                        1,
                        Tuple.Create("Field1", (int?)1),
                        new Dictionary<string, bool> {{"Field2", true}}
                    },
                new object[]
                    {
                        0,
                        0,
                        Tuple.Create("Field1", (int?)1),
                        new Dictionary<string,bool> {{"Field2", true}}
                    },
                new object[]
                    {
                        1,
                        1,
                        Tuple.Create("Field1", (int?)null),
                        new Dictionary<string, bool> {{"Field2", false}}
                    }
            };

        /// <summary>
        /// Проверка возвращаемых списков значений при изменении исходного состояния запроса. Также проверяет, что если элемент имеется в дочерних коллекциях исходного и выбранного элемента, то должен быть автоматически подставлен
        /// </summary>
        /// <param name="field1">ключ элемента в коллекции для поля 1</param>
        /// <param name="field2">для поля 2 </param>
        /// <param name="fieldToChange">название и ключ элемента в коллекции для изменения значения</param>
        /// <param name="expectedState">ожидаемый ответ, состоящий из названия поля репозитория, а также флаг наличия элементов в коллекции и возможное автоустановленное значение</param>
        [Test]
        [TestCaseSource("CascadeTwoUnitChain")]
        public void ExtractRepositories_ListOfRepository_CascadeTwoUnitChain(int? field1, int? field2, Tuple<string, int?> fieldToChange, Dictionary<string, bool> expectedState)
        {
            var dic = new Dictionary<Type, IEnumerable<SimpleEntity>>();
            dic.Add(typeof(TestEntity1), _dbSessionMock.Object.Collection1);

            // устанавливаем первоначальное значение поля 1
            if (field1 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter1.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter1.Item1].Data[field1.Value].Name);
            }

            // и поля 2
            if (field2 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter2.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter2.Item1].Data[field2.Value].Name);
            }

            // меняем состояние
            ChangeState(fieldToChange);

            // сужаем коллекцию поля 2
            if (_request.Field1 != null)
            {
                dic.Add(typeof(TestEntity2), _dbSessionMock.Object.Collection2.Where(i => i.Field1 == _request.Field1));
            }
            else
            {
                dic.Add(typeof(TestEntity2), new List<TestEntity2>());
            }
            // подкладываем ожидаемые значения
            //_engineMock.Setup(p => p.Repositories).Returns(dic);
            //_engineMock.Setup(p => p.SpecRestrictors).Returns(new Dictionary<Type, IEnumerable<RestrictSpecInfo>>());

            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, fieldToChange.Item1);

            var actual = _target.ExtractRepositories();

            // провреяем кол-во ожидаемых репозиториев с фактическим
            Assert.AreEqual(expectedState.Count, actual.Count(), "Количество репозиториев полей не совпадает"); // поля, которые в одних тестах отсутствуют, а вдругих присутствуют, в первом случае равны null, отчего лишняя информация прийти в ответе не должна

            // проверка соответствия репозиториев ожиданиям
            VerifyResult(expectedState, actual, dic);
        }

        #endregion

        #region CascadeThreeUnitChainTest

        static readonly object[] ThreeUnitChain = new object[]
            {
                // three-unit chains
                new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        0, 1, 2, // Moscow, BMW, X5
                        Tuple.Create("Field3", (int?)1), // Spb
                        new Dictionary<string, bool> {{"Field4", true}, {"Field5", true}} // Spb, BMW, X5
                    },
                new object[]
                    {
                        0,0,0, // Moscow, Audi, A1
                        Tuple.Create("Field3", (int?)1), // Spb
                        new Dictionary<string,bool> {{"Field4", true}, {"Field5", false}} // Spb
                    },
                new object[]
                    {
                        0,1,1, // Moscow, BMW, X3
                        Tuple.Create("Field3", (int?)null), 
                        new Dictionary<string, bool> {{"Field4", false}, {"Field5", false}}
                    },
                new object[]
                    {
                        0,1,null, // Moscow, BMW
                        Tuple.Create("Field3", (int?)null), 
                        new Dictionary<string, bool> {{"Field4", false}, {"Field5", false}}
                    },
                new object[]
                    {
                        null,null,null, 
                        Tuple.Create("Field3", (int?)0), // Moscow
                        new Dictionary<string, bool> {{"Field4", true}}
                    }
            };

        /// <summary>
        /// Проверка возвращаемых списков значений при изменении исходного состояния запроса. Также проверяет, что если элемент имеется в дочерних коллекциях исходного и выбранного элемента, то должен быть автоматически подставлен
        /// </summary>
        /// <param name="field3">ключ элемента в коллекции для поля 1</param>
        /// <param name="field4">для поля 2 </param>
        /// <param name="field5">для поля 3</param>
        /// <param name="fieldToChange">название и ключ элемента в коллекции для изменения значения</param>
        /// <param name="expectedState">ожидаемый ответ, состоящий из названия поля репозитория, а также флаг наличия элементов в коллекции и возможное автоустановленное значение</param>
        [Test]
        [TestCaseSource("ThreeUnitChain")]
        public void ExtractRepositories_ListOfRepository_CascadeThreeUnitChain(int? field3, int? field4, int? field5, Tuple<string, int?> fieldToChange, Dictionary<string, bool> expectedState)
        {
            var dic = new Dictionary<Type, IEnumerable<SimpleEntity>>();
            dic.Add(typeof(TestEntity3), _dbSessionMock.Object.Collection3);

            // устанавливаем первоначальное значение поля 1
            if (field3 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter3.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter3.Item1].Data[field3.Value].Name);
            }

            // и поля 2
            if (field4 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter4.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter4.Item1].Data[field4.Value].Name);
            }

            // и поля 3
            if (field5 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter5.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter5.Item1].Data[field5.Value].Name);
            }

            // меняем состояние
            ChangeState(fieldToChange);

            // сужаем коллекцию поля 4
            if (_request.Field3 != null)
            {
                dic.Add(typeof(TestEntity4), _dbSessionMock.Object.Collection4.Where(i => i.Field3 == _request.Field3));
                // и коллекцию поля 5
                if (_request.Field4 != null)
                {
                    dic.Add(typeof(TestEntity5), _dbSessionMock.Object.Collection5.Where(i => i.Field4 == _request.Field4));
                }
                else
                {
                    dic.Add(typeof(TestEntity5), new List<TestEntity5>());
                }
            }
            else
            {
                dic.Add(typeof(TestEntity4), new List<TestEntity4>());
                dic.Add(typeof(TestEntity5), new List<TestEntity5>());
            }
            // подкладываем ожидаемые значения
            //_engineMock.Setup(p => p.Repositories).Returns(dic);
            //_engineMock.Setup(p => p.SpecRestrictors).Returns(new Dictionary<Type, IEnumerable<RestrictSpecInfo>>());

            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, fieldToChange.Item1);

            var actual = _target.ExtractRepositories();

            // провреяем кол-во ожидаемых репозиториев с фактическим
            Assert.AreEqual(expectedState.Count, actual.Count(), "Количество репозиториев полей не совпадает"); // поля, которые в одних тестах отсутствуют, а вдругих присутствуют, в первом случае равны null, отчего лишняя информация прийти в ответе не должна

            // проверка соответствия репозиториев ожиданиям
            VerifyResult(expectedState, actual, dic);
        }

        #endregion

        #region TwoDependentChainTest

        static readonly object[] TwoDependentChain = new object[]
            {
                new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        null,null,null,
                        Tuple.Create("Field6", (int?)0), // Moscow
                        new Dictionary<string, bool> {{"Field7", true}, {"Field8", true}} // Moscow
                    },
                new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        0,null,null, // Moscow
                        Tuple.Create("Field6", (int?)null), 
                        new Dictionary<string, bool> {{"Field7", false}, {"Field8", false}} //
                    },
                    new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        0,1,1, // Moscow, World, BMW
                        Tuple.Create("Field6", (int?)1), 
                        new Dictionary<string, bool> {{"Field7", true}, {"Field8", true}} //
                    },
                    new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        0,1,1, // Moscow, World, BMW
                        Tuple.Create("Field6", (int?)1), 
                        new Dictionary<string, bool> {{"Field7", true}, {"Field8", true}} //
                    },
                    new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        0,0,0, // Moscow, Russia, Audi
                        Tuple.Create("Field6", (int?)1), 
                        new Dictionary<string, bool> {{"Field7", true}, {"Field8", true}} //
                    },
                    new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        0,0,null, // Moscow, Russia
                        Tuple.Create("Field6", (int?)1), 
                        new Dictionary<string, bool> {{"Field7", true}, {"Field8", true}} //
                    },
            };

        /// <summary>
        /// Проверка возвращаемых списков значений при изменении исходного состояния запроса. Также проверяет, что если элемент имеется в дочерних коллекциях исходного и выбранного элемента, то должен быть автоматически подставлен
        /// </summary>
        /// <param name="field6">ключ элемента в коллекции для поля 1</param>
        /// <param name="field7">для поля 2 </param>
        /// <param name="field8">для поля 3</param>
        /// <param name="fieldToChange">название и ключ элемента в коллекции для изменения значения</param>
        /// <param name="expectedState">ожидаемый ответ, состоящий из названия поля репозитория, а также флаг наличия элементов в коллекции и возможное автоустановленное значение</param>
        [Test]
        [TestCaseSource("TwoDependentChain")]
        public void ExtractRepositories_ListOfRepository_TwoDependentChain(int? field6, int? field7, int? field8, Tuple<string, int?> fieldToChange, Dictionary<string, bool> expectedState)
        {
            var dic = new Dictionary<Type, IEnumerable<SimpleEntity>>();
            dic.Add(typeof(TestEntity6), _dbSessionMock.Object.Collection6);

            // устанавливаем первоначальное значение поля 1
            if (field6 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter6.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter6.Item1].Data[field6.Value].Name);
            }

            // и поля 2
            if (field7 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter7.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter7.Item1].Data[field7.Value].Name);
            }

            // и поля 3
            if (field8 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter8.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter8.Item1].Data[field8.Value].Name);
            }

            // меняем состояние
            ChangeState(fieldToChange);

            // сужаем коллекцию поля 7 и 8
            if (_request.Field6 != null)
            {
                dic.Add(typeof(TestEntity7), _dbSessionMock.Object.Collection7.Where(i => i.Field6 == _request.Field6));
                dic.Add(typeof(TestEntity8), _dbSessionMock.Object.Collection8.Where(i => i.Field6 == _request.Field6));
            }
            else
            {
                dic.Add(typeof(TestEntity7), new List<TestEntity7>());
                dic.Add(typeof(TestEntity8), new List<TestEntity8>());
            }

            // подкладываем ожидаемые значения
            //_engineMock.Setup(p => p.Repositories).Returns(dic);
            //_engineMock.Setup(p => p.SpecRestrictors).Returns(new Dictionary<Type, IEnumerable<RestrictSpecInfo>>());

            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, fieldToChange.Item1);

            var actual = _target.ExtractRepositories();

            // провреяем кол-во ожидаемых репозиториев с фактическим
            Assert.AreEqual(expectedState.Count, actual.Count(), "Количество репозиториев полей не совпадает"); // поля, которые в одних тестах отсутствуют, а вдругих присутствуют, в первом случае равны null, отчего лишняя информация прийти в ответе не должна

            // проверка соответствия репозиториев ожиданиям
            VerifyResult(expectedState, actual, dic);
        }

        #endregion

        #region TwoDependenceUnionTest

        static readonly object[] TwoDependenceUnion = new object[]
            {
                new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        null,null,null,
                        Tuple.Create("Field9", (int?)0), // Moscow
                        new Dictionary<string, bool> {{"Field11", false}}
                    },
                new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        0,null,null, // Moscow
                        Tuple.Create("Field10", (int?)0), // RUR
                        new Dictionary<string, bool> {{"Field11", true}}
                    },
                new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        0,0,null, // Moscow, RUR
                        Tuple.Create("Field9", (int?)1), // Spb
                        new Dictionary<string, bool> {{"Field11", false}}
                    },
                new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        0,0,0, // Moscow, RUR, Program1
                        Tuple.Create("Field9", (int?)null), //
                        new Dictionary<string, bool> {{"Field11",false}}
                    },
                new object[]
                    {
                        // предполагается, что Field2Collection[1] == Field2Collection[2] и Field3Collection[2] == Field3Collection[3]
                        0,0,0, // Moscow, RUR, Program1
                        Tuple.Create("Field10", (int?)1), // USD
                        new Dictionary<string, bool> {{"Field11", true}}
                    },
            };

        [Test]
        [TestCaseSource("TwoDependenceUnion")]
        public void ExtractRepositories_ListOfRepository_TwoDependeceUnion(int? field9, int? field10, int? field11, Tuple<string, int?> fieldToChange, Dictionary<string, bool> expectedState)
        {
            var dic = new Dictionary<Type, IEnumerable<SimpleEntity>>();
            dic.Add(typeof(TestEntity9), _dbSessionMock.Object.Collection9);
            dic.Add(typeof(TestEntity10), _dbSessionMock.Object.Collection10);

            // устанавливаем первоначальное значение поля 9
            if (field9 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter9.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter9.Item1].Data[field9.Value].Name);
            }

            // и поля 2
            if (field10 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter10.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter10.Item1].Data[field10.Value].Name);
            }

            // и поля 3
            if (field11 != null)
            {
                _request.GetType().GetProperty(_engineMock.Parameter11.Item1).SetValue(_request, _dbSessionMock.Repository[_engineMock.Parameter11.Item1].Data[field11.Value].Name);
            }

            // меняем состояние
            ChangeState(fieldToChange);

            // сужаем коллекцию поля 11
            if (_request.Field9 != null && _request.Field10 != null)
            {
                dic.Add(typeof(TestEntity11), _dbSessionMock.Object.Collection11.Where(i => i.Field9 == _request.Field9 && i.Field10 == _request.Field10));
            }
            else
            {
                dic.Add(typeof(TestEntity11), new List<TestEntity11>());
            }

            // подкладываем ожидаемые значения
            //_engineMock.Setup(p => p.Repositories).Returns(dic);
            //_engineMock.Setup(p => p.SpecRestrictors).Returns(new Dictionary<Type, IEnumerable<RestrictSpecInfo>>());

            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, fieldToChange.Item1);

            var actual = _target.ExtractRepositories();

            // проверка соответствия репозиториев ожиданиям
            VerifyResult(expectedState, actual, dic);
        }

        #endregion

        #region Common

        private void ChangeState(Tuple<string, int?> fieldToChange)
        {
            if (fieldToChange.Item2 != null)
            {
                _request.GetType()
                        .GetProperty(fieldToChange.Item1)
                        .SetValue(_request, _dbSessionMock.Repository[fieldToChange.Item1].Data[fieldToChange.Item2.Value].Name);
            }
            else
            {
                _request.GetType()
                        .GetProperty(fieldToChange.Item1)
                        .SetValue(_request, null);
            }
        }

        private void VerifyResult(Dictionary<string, bool> expectedState, IEnumerable<FieldRepository> actual, Dictionary<Type, IEnumerable<SimpleEntity>> dic)
        {
            // провреяем кол-во ожидаемых репозиториев с фактическим
            Assert.AreEqual(expectedState.Count, actual.Count(), "Количество репозиториев полей не совпадает"); // поля, которые в одних тестах отсутствуют, а вдругих присутствуют, в первом случае равны null, отчего лишняя информация прийти в ответе не должна

            foreach (var expectedItem in expectedState)
            {
                var actualInState = actual.FirstOrDefault(i => i.FieldName == expectedItem.Key);
                Assert.AreEqual(expectedItem.Key, actualInState.FieldName,
                                "Несоответствие ожиданиям наличия элементов в репозитории для поля " + actualInState.FieldName);

                //var expected = expectedItem.Value.Item2 == null // ожидаем не получить значение
                //                   ? null
                //                   : _dbSessionMock.Repository[expectedItem.Key].Data[expectedItem.Value.Item2.Value].Name;

                //Assert.AreEqual(expected, actualInState.Value,
                //                "Возвращен элемент, отсутствующий в ожидаем списке репоиториев полей: " + actualInState.FieldName);
                Assert.AreEqual(expectedItem.Value, actualInState.CollectionSource.Any());

                // проверяем, возвращена ли таже коллекции, по которой производился отбор
                if (expectedItem.Value == true)
                {
                    CollectionAssert.AreEqual(dic[_dbSessionMock.Repository[expectedItem.Key].StoreType], actualInState.CollectionSource.Keys);
                }
                else // если список пришел пустым, то значения по умолчанию быть не может
                {
                    Assert.IsNull(actualInState.GetLiteValue());
                }
            }
        }

        #endregion

        #endregion

        #region DefaultValueTest

        #region DefaultValueIsNotAssignedTest

        static readonly object[] DefaultValueIsNotAssignedSource = new object[]
            {
                new object[]
                    {
                        new Dictionary<string, int> {{"Field12", 0}, {"Field13", 0}},
                        Tuple.Create("Field12", 1),
                        "Field13"
                    },
                new object[]
                    {
                        new Dictionary<string, int> {{"Field14", 0}, {"Field15", 0}, {"Field16", 0}},
                        Tuple.Create("Field14", 1),
                        "Field16"
                    },
                new object[]
                    {
                        new Dictionary<string, int> {{"Field17", 0}, {"Field18", 0}, {"Field19", 0}},
                        Tuple.Create("Field17", 1),
                        "Field19"
                    }
            };

        [Test]
        [TestCaseSource("DefaultValueIsNotAssignedSource")]
        public void ExtractRepositories_ListOfRepository_DefaultValueIsNotAssigned(Dictionary<string, int> initState, Tuple<string, int> fieldToChange, string fieldForCheck)
        {
            var dic = new Dictionary<Type, IEnumerable<SimpleEntity>>();
            // устаналиваем первоначальное состояние, выбираем из репозитория сперва коллекцию для поля, потом элемент из коллекции по поряковому полю
            foreach (var initField in initState)
            {
                var repositoryPair = _dbSessionMock.Repository[initField.Key];
                _request.GetType().GetProperty(initField.Key).SetValue(_request, repositoryPair.Data[initField.Value].Name);
                dic.Add(repositoryPair.StoreType, repositoryPair.Data.Values);
            }

            //_engineMock.Setup(p => p.Repositories).Returns(dic);
            //_engineMock.Setup(p => p.SpecRestrictors).Returns(new Dictionary<Type, IEnumerable<RestrictSpecInfo>>());

            _request.GetType().GetProperty(fieldToChange.Item1).SetValue(_request, _dbSessionMock.Repository[fieldToChange.Item1].Data[fieldToChange.Item2].Name);

            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, fieldToChange.Item1);

            var answer = _target.ExtractRepositories();

            var actualInItem = answer.SingleOrDefault(r => r.FieldName.Equals(fieldForCheck));

            Assert.IsNotNull(actualInItem);

            Assert.IsNull(actualInItem.GetLiteValue());

            Assert.IsNull(_request.GetType().GetProperty(fieldForCheck).GetValue(_request));
        }

        #endregion

        #region DefaultValueIsNullIfParentIsNullTest

        static readonly object[] DefaultValueIsNullIfParentIsNullSource = new object[]
            {
                new object[]
                    {
                        new Dictionary<string, int> {{"Field12", 0}, {"Field13", 0}},
                        "Field12", 
                        "Field13"
                    },
                new object[]
                    {
                        new Dictionary<string, int> {{"Field14", 0}, {"Field15", 0}, {"Field16", 0}},
                        "Field14",
                        "Field16"
                    },
                new object[]
                    {
                        new Dictionary<string, int> {{"Field17", 0}, {"Field18", 0}, {"Field19", 0}},
                        "Field17",
                        "Field19"
                    }
            };

        [Test]
        [TestCaseSource("DefaultValueIsNullIfParentIsNullSource")]
        public void ExtractRepositories_ListOfRepository_DefaultValueIsNullIfParentIsNull(Dictionary<string, int> initState, string fieldToChange, string fieldForCheck)
        {
            var dic = new Dictionary<Type, IEnumerable<SimpleEntity>>();
            // устаналиваем первоначальное состояние, выбираем из репозитория сперва коллекцию для поля, потом элемент из коллекции по поряковому полю
            foreach (var initField in initState)
            {
                var repositoryPair = _dbSessionMock.Repository[initField.Key];
                _request.GetType().GetProperty(initField.Key).SetValue(_request, repositoryPair.Data[initField.Value].Name);
                dic.Add(repositoryPair.StoreType, repositoryPair.Data.Values);
            }

            //_engineMock.Setup(p => p.Repositories).Returns(dic);
            //_engineMock.Setup(p => p.SpecRestrictors).Returns(new Dictionary<Type, IEnumerable<RestrictSpecInfo>>());

            _request.GetType().GetProperty(fieldToChange).SetValue(_request, null);

            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, fieldToChange);

            var answer = _target.ExtractRepositories();

            var actualInItem = answer.SingleOrDefault(r => r.FieldName.Equals(fieldForCheck));

            Assert.IsNotNull(actualInItem);

            Assert.IsNull(actualInItem.GetLiteValue());

            Assert.IsNull(_request.GetType().GetProperty(fieldForCheck).GetValue(_request));
        }

        #endregion

        #region DefaultValueIsAssignedIfSameExistTest

        static readonly object[] DefaultValueIsAssignedIfSameExistSource = new object[]
            {
                new object[]
                    {
                        new Dictionary<string, int> {{"Field20", 0}, {"Field21", 0}},
                        Tuple.Create("Field20", 1),
                        Tuple.Create("Field21", 1)
                    },
                new object[]
                    {
                        new Dictionary<string, int> {{"Field22", 0}, {"Field23", 0}, {"Field24", 0}},
                        Tuple.Create("Field22", 1),
                        Tuple.Create("Field24", 0)
                    },
                new object[]
                    {
                        new Dictionary<string, int> {{"Field25", 0}, {"Field26", 0}, {"Field27", 0}},
                        Tuple.Create("Field25", 1),
                        Tuple.Create("Field27", 1)
                    }
            };

        [Test]
        [TestCaseSource("DefaultValueIsAssignedIfSameExistSource")]
        public void ExtractRepositories_ListOfRepository_DefaultValueIsAssignedIfSameExist(Dictionary<string, int> initState, Tuple<string, int> fieldToChange, Tuple<string, int> expectedField)
        {
            var dic = new Dictionary<Type, IEnumerable<SimpleEntity>>();
            // устаналиваем первоначальное состояние, выбираем из репозитория сперва коллекцию для поля, потом элемент из коллекции по поряковому полю
            foreach (var initField in initState)
            {
                var repositoryPair = _dbSessionMock.Repository[initField.Key];
                _request.GetType().GetProperty(initField.Key).SetValue(_request, repositoryPair.Data[initField.Value].Name);
                dic.Add(repositoryPair.StoreType, repositoryPair.Data.Values);
            }

            //_engineMock.Setup(p => p.Repositories).Returns(dic);
            //_engineMock.Setup(p => p.SpecRestrictors).Returns(new Dictionary<Type, IEnumerable<RestrictSpecInfo>>());

            _request.GetType().GetProperty(fieldToChange.Item1).SetValue(_request, _dbSessionMock.Repository[fieldToChange.Item1].Data[fieldToChange.Item2].Name);

            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, fieldToChange.Item1);

            var answer = _target.ExtractRepositories();

            var actualInItem = answer.SingleOrDefault(r => r.FieldName.Equals(expectedField.Item1));

            Assert.IsNotNull(actualInItem);

            Assert.IsNotNull(actualInItem.GetLiteValue());

            var expected = _dbSessionMock.Repository[expectedField.Item1].Data[expectedField.Item2].Name;

            Assert.AreEqual(expected, actualInItem.GetLiteValue());

            Assert.AreEqual(expected, _request.GetType().GetProperty(expectedField.Item1).GetValue(_request));
        }

        #endregion

        #region DefaultValueIsAssignedIfSingleExistTest

        static readonly object[] DefaultValueIsAssignedIfSingleExistSource = new object[]
            {
                new object[]
                    {
                        new Dictionary<string, int> {{"Field28", 0}, {"Field29", 0}},
                        Tuple.Create("Field28", 1),
                        Tuple.Create("Field29", 1)
                    },
                new object[]
                    {
                        new Dictionary<string, int> {{"Field30", 0}, {"Field31", 0}, {"Field32", 0}},
                        Tuple.Create("Field30", 1),
                        Tuple.Create("Field31", 2)
                    },
                new object[]
                    {
                        new Dictionary<string, int> {{"Field33", 0}, {"Field34", 0}, {"Field35", 0}},
                        Tuple.Create("Field33", 1),
                        Tuple.Create("Field35", 1)
                    }
            };

        [Test]
        [TestCaseSource("DefaultValueIsAssignedIfSingleExistSource")]
        public void ExtractRepositories_ListOfRepository_DefaultValueIsAssignedIfSingleExist(Dictionary<string, int> initState, Tuple<string, int> fieldToChange, Tuple<string, int> expectedField)
        {
            var dic = new Dictionary<Type, IEnumerable<SimpleEntity>>();
            // устаналиваем первоначальное состояние, выбираем из репозитория сперва коллекцию для поля, потом элемент из коллекции по поряковому полю
            foreach (var initField in initState)
            {
                _request.GetType().GetProperty(initField.Key).SetValue(_request, _dbSessionMock.Repository[initField.Key].Data[initField.Value].Name);
                dic.Add(_dbSessionMock.Repository[initField.Key].StoreType, _dbSessionMock.Repository[initField.Key].Data.Values);
            }

            //_engineMock.Setup(p => p.Repositories).Returns(dic);
            //_engineMock.Setup(p => p.SpecRestrictors).Returns(new Dictionary<Type, IEnumerable<RestrictSpecInfo>>());

            _request.GetType().GetProperty(fieldToChange.Item1).SetValue(_request, _dbSessionMock.Repository[fieldToChange.Item1].Data[fieldToChange.Item2].Name);

            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, fieldToChange.Item1);

            var answer = _target.ExtractRepositories();

            var actualInItem = answer.SingleOrDefault(r => r.FieldName.Equals(expectedField.Item1));

            Assert.IsNotNull(actualInItem);

            Assert.IsNotNull(actualInItem.GetLiteValue());

            string expected = _dbSessionMock.Repository[expectedField.Item1].Data[expectedField.Item2].Name;

            Assert.AreEqual(expected, actualInItem.GetLiteValue());

            Assert.AreEqual(expected, _request.GetType().GetProperty(expectedField.Item1).GetValue(_request));
        }

        #endregion

        #endregion

        #region RestrictRelationsTest (HDBK)

        #region HDBKWithoutGroupingTest

        static readonly object[] HDBKCascadeRestrictWithoutGrouping = new object[]
            {
                // three-unit chains
                new object[]
                    {
                        null, null, null, 
                        Tuple.Create("Field46", (int?)0), // Program1
                        new Dictionary<string, bool> {{"Field47", true}} 
                    },
                new object[]
                    {
                        0, 0, null, // Program1, Form1
                        Tuple.Create("Field46", (int?)1), // Program2
                        new Dictionary<string, bool> {{"Field47", true}, {"Field48", false}} // т.к. field48 идёт в одной hdbk с field47 БЕЗ группировки
                    }
                    // Ремезов: todo дописать тест
            };

        [Test]
        [TestCaseSource("HDBKCascadeRestrictWithoutGrouping")]
        public void ExtractRepositories_ListOfRepositories_HDBKCascadeRestrictWithoutGrouping(int? field46, int? field47, int? field48,
                                                                                       Tuple<string, int?> fieldToChange,
                                                                                       Dictionary<string, bool> expectedState)
        {
            var dic = new Dictionary<Type, IEnumerable<SimpleEntity>>();
            dic.Add(typeof(TestEntity47), new List<TestEntity47>());
            dic.Add(typeof(TestEntity48), new List<TestEntity48>());

            if (field46 != null)
            {
                _request.GetType().GetProperty("Field46").SetValue(_request, _dbSessionMock.Repository["Field46"].Data[field46.Value].Name);
            }
            if (field47 != null)
            {
                _request.GetType().GetProperty("Field47").SetValue(_request, _dbSessionMock.Repository["Field47"].Data[field47.Value].Name);
                //_request.GetType().GetProperty(fieldsDic["Field47"].FieldProperty.Name).SetValue(_request, ((IList<TestEntity47>)repoDic[typeof(TestEntity47)])[field47.Value].Name);
            }
            if (field48 != null)
            {
                _request.GetType().GetProperty("Field48").SetValue(_request, _dbSessionMock.Repository["Field48"].Data[field48.Value].Name);
                //_request.GetType().GetProperty(fieldsDic["Field48"].FieldProperty.Name).SetValue(_request, ((IList<TestEntity48>)repoDic[typeof(TestEntity48)])[field48.Value].Name);
            }

            _request.Field46 = _dbSessionMock.Repository[fieldToChange.Item1].Data[fieldToChange.Item2.Value].Name;// ((IList<TestEntity46>)repoDic[typeof(TestEntity46)])[fieldToChange.Item2.Value].Name;

            if (_request.Field46 != null)
            {
                dic[typeof(TestEntity47)] = _dbSessionMock.Object.Collection47.Where(i => i.Field46 == _request.Field46);
            }
            if (_request.Field47 != null)
            {
                dic[typeof(TestEntity48)] =
                    _dbSessionMock.Object.Collection48.Join(_dbSessionMock.Object.RestrictCollection4.Where(r => r.TestEntity47 == _request.Field47),
                                                            i => i.Name, r => r.TestEntity48, (i, r) => i);
            }

            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, fieldToChange.Item1); // изменяем родителя и ожидаем получить Field36

            var actual = _target.ExtractRepositories();

            Assert.AreEqual(expectedState.Count, actual.Count(), "Количество репозиториев полей не совпадает");

            VerifyResult(expectedState, actual, dic);
        }

        [Test]
        public void ExtractRepositories_ListOfRepositories_HDBKRestrictWithoutGrouping()
        {
            _request.Field37 = _dbSessionMock.Repository["Field37"].Data[0].Name;// "Root"
            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, "Field37"); // изменяем родителя и ожидаем получить Field36

            var actual = _target.ExtractRepositories();

            var actualItem = actual.FirstOrDefault(i => i.FieldName == "Field36");

            var expected = _dbSessionMock.Repository["Field36"].Data.Values.ToList().GetRange(0, 2); // Program1, Program2 // new List<TestEntity36>() { collection[0], collection[1] };
            CollectionAssert.AreEqual(expected, actualItem.CollectionSource.Keys);
        }

        #endregion

        #region HDBKOrderTest

        /// <summary>
        /// Учитывается порядок 
        /// </summary>
        [Test]
        public void ExtractRepositories_ListOfRepositories_HDBKOrderConsidering()
        {
            // проверка, что ничего не придёт, т.к. у поля 42 порядок второй, а в первом по порядку поле пусто
            _request.Field43 = _dbSessionMock.Repository["Field43"].Data[0].Name;// "Root"
            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, "Field43");

            var actual = _target.ExtractRepositories();

            var actualItem = actual.FirstOrDefault(i => i.FieldName == "Field42");

            CollectionAssert.IsEmpty(actualItem.CollectionSource);

            // проверка, что коллекция вернётся, т.к. в первом по порядку поле есть значение
            _request.Field41 = "Program1";
            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, "Field43");

            actual = _target.ExtractRepositories();

            actualItem = actual.FirstOrDefault(i => i.FieldName == "Field42");
            Assert.IsNotNull(actualItem);
            var expected = _dbSessionMock.Repository["Field42"].Data.Values.ToList().GetRange(0, 2);// new[] { collection42[0], collection42[1] };
            CollectionAssert.AreEqual(expected, actualItem.CollectionSource.Keys);
            //CollectionAssert.AreEqual(expected.Select(i => "Field42"), actualItem.CollectionSource.Select(i => i[0].FieldName));
        }

        #endregion

        #region HDBKWithGroupingTest

        [Test]
        public void ExtractRepositories_ListOfRepositories_HDBKRestrictWithGrouping()
        {
            _request.Field40 = _dbSessionMock.Repository["Field40"].Data[0].Name; // "Root"
            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, "Field40");

            var actual = _target.ExtractRepositories();

            var actualItem = actual.SingleOrDefault(i => i.FieldName == "Field39");

            var expected = _dbSessionMock.Restrictors["HDBK2"].Data.Values.ToList().GetRange(0, 2); // new[] { restrictColl2[0], restrictColl2[1] };

            Assert.AreEqual(expected.Count, actualItem.CollectionSource.Count);

            //foreach (var item in expected)
            //{
            //todo Ремезов: добавить проверку на наличие группирующего поля
            //    var actualItemInColl = actualItem.CollectionSource.SingleOrDefault(i => string.IsNullOrEmpty(i[2].FieldName) && i[2].Value == item.GroupValue); // ожидаем именно в 3 элементе массива группирующее поле
            //    Assert.IsNotNull(actualItemInColl);

            //    Assert.AreEqual(item.TestEntity39, actualItemInColl[1].Value);
            //    Assert.AreEqual("Field39", actualItemInColl[1].FieldName);
            //    Assert.AreEqual(item.TestEntity38, actualItemInColl);
            //    Assert.AreEqual("Field38", actualItemInColl[0].FieldName);
            //}
        }

        #endregion

        #endregion

        #region RestrictRelationsTest (Specs)

        #region RestrictSpecificationTest

        [Test]
        public void ExtractRepositories_ListOfRepositories_CollectionRestricted()
        {
            _target = new ByFieldDeriveStrategy<TestRequest>(_request, _engineMock.Object, "Field45");

            _request.Field45 = _dbSessionMock.Repository["Field45"].Data[0].Name; // "Root"
            var result = _target.ExtractRepositories();

            var expected = _dbSessionMock.Repository["Field44"].Data.Values.ToList().GetRange(1, 1);// new[] { collection44[1] };

            Assert.AreEqual(1, result.Count());

            var actual = result.First();

            CollectionAssert.AreEqual(expected, actual.CollectionSource.Keys);
        }

        #endregion

        #endregion
    }
}
