﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using UGSK.K3.AppServices.DataDeriver.Strategies.Impl;
using UGSK.K3.AppServices.DataDeriver.Test.Mocks;
using UGSK.K3.AppServices.DataDeriver.Test.Mocks.Originals;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Test.Integration
{
    [TestFixture]
    class ByRequestDeriveStrategyTest
    {
        private ByRequestDeriveStrategy<TestRequest2> _target;
        private MockCalcEngine2 _engineMock;
        private TestRequest2 _request;
        private Mock<ICalculator> _calculatorMock;
        private MockDbSession _dbSessionMock;

        [SetUp]
        public void Setup()
        {
            _request = new TestRequest2();
            _engineMock = new MockCalcEngine2();

            _dbSessionMock = _engineMock.DbSession;
            _engineMock.Setup(p => p.RequestType).Returns(_request.GetType());
            _calculatorMock = new Mock<ICalculator>();
            _calculatorMock.Setup(p => p.DbSession).Returns(_dbSessionMock.Object);
            //_engineMock.Setup(p => p.Calculator).Returns(_calculatorMock.Object);
        }

        [Test]
        public void ExtractRepositories_ListOfRepositories_IndependenceFieldRepositoriesWithoutHDBK()
        {
            _target = new ByRequestDeriveStrategy<TestRequest2>(_request, _engineMock.Object);

            var actual = _target.ExtractRepositories();

            var expected = new Dictionary<string, IEnumerable<SimpleEntity>> {
                {"Field2_1", _dbSessionMock.Repository["Field2_1"].Data.Values.ToList()},
                {"Field2_4", _dbSessionMock.Repository["Field2_4"].Data.Values.ToList()}
            };

            Assert.AreEqual(expected.Count(), actual.Count());

            foreach (var @enum in expected)
            {
                var actualItem = actual.FirstOrDefault(i => i.FieldName.Equals(@enum.Key));

                Assert.IsNotNull(actualItem);

                Assert.AreEqual(@enum.Key, actualItem.FieldName);

                CollectionAssert.AreEqual(@enum.Value, actualItem.CollectionSource.Keys);
            }
        }
    }
}
