﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UGSK.K3.Product.Contract;
using UGSK.K3.AppServices.DataDeriver.Core.SelectionRules.Impl;

namespace UGSK.K3.AppServices.DataDeriver.Test
{
    [TestFixture]
    class DefaultSelectorRuleBuilderTest
    {
        private DefaultSelectorRuleBuilder _target;
        private Mock<Func<Type, int, ISelectionRule<IRequest>>> _mockBuilder;
        private Mock<ISelectionRule<IRequest>> _mockRule;

        [SetUp]
        public void Init()
        {
            _mockRule = new Mock<ISelectionRule<IRequest>>();
            _mockBuilder = new Mock<Func<Type, int, ISelectionRule<IRequest>>>();
            _mockBuilder.Setup(m => m(It.IsAny<Type>(), It.IsAny<int>()))
                        .Returns(_mockRule.Object)
                        .Callback((Type type, int pr) => _mockRule.Setup(p => p.Priority).Returns(pr));

            _target = new DefaultSelectorRuleBuilder(_mockBuilder.Object);
        }

        [Test]
        public void BuildRule_SelectorRule_RuleFound()
        {
            var expected = 777;
            var actual = _target.BuildRule(typeof(DefaultSelectorTestRule), expected);

            Assert.AreEqual(_mockRule.Object.Priority, expected);
            Assert.AreSame(_mockRule.Object, actual);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "DefaultSelectorBadTestRule", MatchType = MessageMatch.Contains)]
        public void BuildRule_SelectorRule_ExceptionOnInvalidType()
        {
            var actual = _target.BuildRule(typeof(DefaultSelectorBadTestRule), 888);

            Assert.AreSame(_mockRule.Object, actual);
        }
    }

    class DefaultSelectorBadTestRule { }

    class DefaultSelectorTestRule : ISelectionRule<IRequest>
    {
        public bool NeedStateChanging { get; private set; }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            throw new NotImplementedException();
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedValues)
        {
            throw new NotImplementedException();
        }

        public int Priority { get; set; }
        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
