﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Moq;
using NUnit.Framework;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Flexible;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Impl;

namespace UGSK.K3.AppServices.DataDeriver.Test
{
    [TestFixture]
    //[Ignore]
    class FlexibleFieldSourceRestrictorTest
    {
        private FlexibleFieldSourceRestrictor _target;
        private FlexibleFieldAttribute _attr;
        private Mock<IRequestFieldBatler> _mockBatler;
        private Mock<IFlexConditionKitFactory> _mockSpecFactory;
        private ConditionKit _conditionKit;
        private event EventHandler<NewRestrictorEventArgs> Handlers; // перехватчик зарегистрированных обработчиков нового ограничителя
        private Mock<Action<EventHandler<NewRestrictorEventArgs>, Type>> _mockRegistrator;
        private Type _expectedRequestType;

        [SetUp]
        public void Init()
        {
            _mockBatler = new Mock<IRequestFieldBatler>();
            _mockBatler.Setup(p => p.SourceType).Returns(typeof(RestrictorTestEntity1));
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(RestrictorTestEntity1).GetProperty("Field1"));

            _conditionKit = new ConditionKit(typeof(RestrictorTestRequest), typeof(RestrictorTestEntity1), new RestrictorTestFlexCondition());

            _mockSpecFactory = new Mock<IFlexConditionKitFactory>();
            _mockSpecFactory.Setup(m => m.MakeConditionByType(typeof(RestrictorTestFlexCondition))).Returns(_conditionKit);

            _mockRegistrator = new Mock<Action<EventHandler<NewRestrictorEventArgs>, Type>>();

            _mockRegistrator.Setup(m => m(It.IsAny<EventHandler<NewRestrictorEventArgs>>(), It.IsAny<Type>()))
                            .Callback((EventHandler<NewRestrictorEventArgs> h, Type t) =>
                                {
                                    Handlers += h;
                                    _expectedRequestType = t;
                                });

            _attr = new FlexibleFieldAttribute(typeof(RestrictorTestFlexCondition));
            _target = new FlexibleFieldSourceRestrictor(_attr, _mockBatler.Object, _mockSpecFactory.Object, _mockRegistrator.Object);
        }

        [Test]
        public void SimpleVerify()
        {
            Assert.AreSame(_attr, _target.Attribute);
            Assert.AreSame(_mockBatler.Object, _target.Owner);
        }

        [Test]
        public void GetRestrictedSource_ListOfSimpleEntity_RestrictedBySpecCollection()
        {
            var source = new List<RestrictorTestEntity1>() { new RestrictorTestEntity1() { Name = "1" }, new RestrictorTestEntity1() { Name = "2" }, new RestrictorTestEntity1() { Name = "3" } };
            //var expected = source.GroupBy(e => e.Name, (k, g) => g.First()); // 1, 3

            var request = new RestrictorTestRequest();

            var actual = _target.GetRestrictedSource(source, request);

            var expected = source.GetRange(0, 1);

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        [Ignore]
        public void OnAnyRestrictorCreated_ListOfBatlers_AddToConcernedList()
        {
            var actualList = new List<IRequestFieldBatler>();
            var mockRestrictor = new Mock<IFieldSourceRestrictor<FieldAttributeBase>>();
            mockRestrictor.Setup(p => p.Owner.ConcernedFieldBatlers.Add(_mockBatler.Object))
                          .Callback((IRequestFieldBatler batler) => actualList.Add(batler));
            mockRestrictor.Setup(p => p.Owner.FieldProperty.Name).Returns("Field");

            Handlers(null, new NewRestrictorEventArgs(mockRestrictor.Object));

            CollectionAssert.IsEmpty(actualList);

            _attr.ObservedFields = new[]{"nonField"};

            Handlers(null, new NewRestrictorEventArgs(mockRestrictor.Object));

            CollectionAssert.IsEmpty(actualList);

            _attr.ObservedFields = new []{"Field"};

            Handlers(null, new NewRestrictorEventArgs(mockRestrictor.Object));

            CollectionAssert.Contains(actualList, _mockBatler.Object);
        }
    }

    public class RestrictorTestFlexCondition : IFlexCondition<RestrictorTestRequest, RestrictorTestEntity1>
    {
        public Expression<Func<RestrictorTestEntity1, bool>> IsSatisfiedBy(RestrictorTestRequest request)
        {
            return entity => entity.Name == "1";
        }
    }

    public class RestrictorTestRequest : IRequest
    {
        public string Field1 { get; set; }
    }

    public class RestrictorTestEntity1 : SimpleEntity { }
}
