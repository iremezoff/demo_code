﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using UGSK.K3.Product.Contract;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.AppServices.DataDeriver.Core.SelectionRules.Impl;

namespace UGSK.K3.AppServices.DataDeriver.Test
{
    [TestFixture]
    class SingleHavingSelectionRuleTest
    {
        private SingleHavingRuleTestRequest _request;
        private SingleHavingSelectionRule<SingleHavingRuleTestRequest> _target;
        private Mock<IRequestFieldBatler> _mockBatler;


        [SetUp]
        public void Init()
        {
            _mockBatler = new Mock<IRequestFieldBatler>();
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(SingleHavingRuleTestRequest).GetProperty("Field"));
            _mockBatler.Setup(p => p.SourceType).Returns(typeof(SingleHavingRuleTestEntity1));

            _target = new SingleHavingSelectionRule<SingleHavingRuleTestRequest>();
        }

        [Test]
        public void SimpleTestOfDefaultBehaviour()
        {
            //Assert.AreSame(_mockBatler.Object, _target.Owner);
            Assert.AreEqual(0, _target.Priority);

            var actual = _target.GetGeneralDenyState(_request, Enumerable.Empty<SimpleEntity>());
            Assert.IsFalse(actual);

            var coll = new List<SingleHavingRuleTestEntity2>()
                {
                    new SingleHavingRuleTestEntity2(),
                    new SingleHavingRuleTestEntity2(),
                    new SingleHavingRuleTestEntity2()
                };

            var actualStates = _target.GetCollectionSourceWithAllowable(coll, null); // ему параллельно
            CollectionAssert.AreEqual(Enumerable.Range(0, coll.Count).Select(i => true), actualStates.Select(i => i.Value));
        }

        [Test]
        public void GetAssignedValue_ListOfSimpleEntity_EmptyIfCollectionIsEmpty()
        {
            var coll = new List<SingleHavingRuleTestEntity1>();
            var request = new SingleHavingRuleTestRequest();

            var actual = _target.GetAssignedValue(coll, request);

            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void GetAssignedValue_ListOfSimpleEntity_SingleHavingEntity()
        {
            var expected = new SingleHavingRuleTestEntity1();
            var coll = new List<SingleHavingRuleTestEntity1>();
            coll.Add(expected);
            var request = new SingleHavingRuleTestRequest();

            var actual = _target.GetAssignedValue(coll, request);

            Assert.AreEqual(1, actual.Count());

            Assert.AreSame(expected, actual.First());
        }

        [Test]
        public void GetAssignedValue_ListOfSimpleEntity_EmptyIfThereAreManyEntities()
        {
            var coll = new List<SingleHavingRuleTestEntity1>();
            coll.Add(new SingleHavingRuleTestEntity1());
            coll.Add(new SingleHavingRuleTestEntity1());
            var request = new SingleHavingRuleTestRequest();

            var actual = _target.GetAssignedValue(coll, request);

            CollectionAssert.IsEmpty(actual);
        }

    }

    class SingleHavingRuleTestRequest : IRequest
    {
        public string Field { get; set; }
    }

    class SingleHavingRuleTestEntity1 : SimpleEntity
    {

    }

    class SingleHavingRuleTestEntity2 : SimpleEntity
    {

    }
}
