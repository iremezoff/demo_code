﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;
using UGSK.K3.AppServices.DataDeriver.Core.SelectionRules.Impl;
using UGSK.K3.Product.Contract;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.AppServices.DataDeriver.Core.SelectionRules;
using UGSK.K3.Product.Contract.Annotations;


namespace UGSK.K3.AppServices.DataDeriver.Test
{
    [TestFixture]
    class PreviousRestoreSelectionRuleTest
    {
        private PreviousRestoreSelectionRuleTestRequest _request;
        private PreviousRestoreSelectionRule<PreviousRestoreSelectionRuleTestRequest> _target;
        private Mock<IRequestFieldBatler> _mockBatler;
        private Mock<ITariffCalculationEngine<IRequest>> _mockEngine;

        [SetUp]
        public void Init()
        {
            _mockEngine = new Mock<ITariffCalculationEngine<IRequest>>();
            

            _mockBatler = new Mock<IRequestFieldBatler>();
            _mockBatler.Setup(p => p.FieldProperty).Returns(typeof(PreviousRestoreSelectionRuleTestRequest).GetProperty("Field"));
            _mockBatler.Setup(p => p.SourceType).Returns(typeof(PreviousRestoreSelectionRuleTestEntity1));
            _mockBatler.Setup(p => p.Restrictors).Returns(new List<IFieldSourceRestrictor<FieldAttributeBase>>());
            _mockBatler.Setup(p => p.SourceTypeProperty).Returns("Name");

            _target = new PreviousRestoreSelectionRule<PreviousRestoreSelectionRuleTestRequest>(_mockBatler.Object);
        }

        [Test]
        public void SimpleTestOfDefaultBehaviour()
        {
            //Assert.AreSame(_mockBatler.Object, _target.Owner);
            Assert.AreEqual(5, _target.Priority);

            var actual = _target.GetGeneralDenyState(_request, Enumerable.Empty<SimpleEntity>());
            Assert.IsFalse(actual);

            var coll = new List<SingleHavingRuleTestEntity2>()
                {
                    new SingleHavingRuleTestEntity2(),
                    new SingleHavingRuleTestEntity2(),
                    new SingleHavingRuleTestEntity2()
                };

            var actualStates = _target.GetCollectionSourceWithAllowable(coll, null); // ему параллельно
            CollectionAssert.AreEqual(Enumerable.Range(0, coll.Count).Select(i => true), actualStates.Select(i => i.Value));
        }

        [Test]
        public void GetAssignedValue_ListOfSimpleEntity_EmptyIfFieldIsNotAssigned()
        {
            var coll = new List<SingleHavingRuleTestEntity1>();
            var request = new PreviousRestoreSelectionRuleTestRequest();

            var actual = _target.GetAssignedValue(coll, request);

            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void GetAssignedValue_ListOfSimpleEntity_SimilarEntity()
        {
            var expected = new PreviousRestoreSelectionRuleTestEntity1() { Name = "Xaxa" };
            var coll = new List<PreviousRestoreSelectionRuleTestEntity1>();
            coll.Add(new PreviousRestoreSelectionRuleTestEntity1() { Name = "Xoxo" });
            coll.Add(expected);
            coll.Add(new PreviousRestoreSelectionRuleTestEntity1() { Name = "Xexe" });

            var request = new PreviousRestoreSelectionRuleTestRequest();

            request.Field = "Xaxa";

            var actual = _target.GetAssignedValue(coll, request);

            Assert.AreEqual(1, actual.Count());
            Assert.AreEqual(expected, actual.First());
        }

        [Test]
        public void GetAssignedValue_ListOfSimpleEntity_EmptyIfThereIsNotSimilarEntity()
        {
            var coll = new List<PreviousRestoreSelectionRuleTestEntity1>();
            coll.Add(new PreviousRestoreSelectionRuleTestEntity1() { Name = "Xoxo" });
            coll.Add(new PreviousRestoreSelectionRuleTestEntity1() { Name = "Xexe" });

            var request = new PreviousRestoreSelectionRuleTestRequest();

            request.Field = "Xuxu";

            var actual = _target.GetAssignedValue(coll, request);

            CollectionAssert.IsEmpty(actual);
        }
    }

    class PreviousRestoreSelectionRuleTestRequest : IRequest
    {
        public string Field { get; set; }
    }

    class PreviousRestoreSelectionRuleTestEntity1 : SimpleEntity { }
}
