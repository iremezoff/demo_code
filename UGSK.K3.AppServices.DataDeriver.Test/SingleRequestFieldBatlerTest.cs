﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Moq;
using NUnit.Framework;
using UGSK.K3.AppServices.DataDeriver.Product.Field;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;

namespace UGSK.K3.AppServices.DataDeriver.Test
{
    [TestFixture]
    class RequestFieldBatlerTest
    {
        private RequestFieldBatler<BatlerTestRequest> _target;
        private Mock<IFieldSourceRestrictor<FieldAttributeBase>> _firstRestrictor;
        private Mock<IFieldSourceRestrictor<FieldAttributeBase>> _secondRestrictor;
        private List<BatlerTestEntity> _mockColl;
        private PropertyInfo _property;
        private BatlerTestRequest _request;
        private List<ISelectionRule<IRequest>> _rules;
        private Mock<ITariffCalculationEngine<BatlerTestRequest>> _mockEngine;
        private Mock<ITypeBuilder> _mockBuilder;
        private List<IFieldSourceRestrictor<FieldAttributeBase>> _restrictors;

        [SetUp]
        public void Init()
        {
            _restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();

            _mockBuilder = new Mock<ITypeBuilder>();
            _mockEngine = new Mock<ITariffCalculationEngine<BatlerTestRequest>>();
            _mockEngine.Setup(p => p.TypeBuilder).Returns(_mockBuilder.Object);

            _rules = new List<ISelectionRule<IRequest>>();
            _mockColl = new List<BatlerTestEntity>();
            _request = new BatlerTestRequest();
            _firstRestrictor = new Mock<IFieldSourceRestrictor<FieldAttributeBase>>();
            _secondRestrictor = new Mock<IFieldSourceRestrictor<FieldAttributeBase>>();

            _property = typeof(BatlerTestRequest).GetProperty("Field");
            _target = new RequestFieldBatler<BatlerTestRequest>(_property, _mockColl, _restrictors, _rules, _mockEngine.Object,
                                             () => new SingleFieldRepository());
        }

        [Test]
        public void SimpleTest()
        {
            _mockColl.Add(new BatlerTestEntity());
            _mockColl.Add(new BatlerTestEntity());
            Assert.AreEqual(_property, _target.FieldProperty);
            Assert.AreEqual(_mockColl, _target.CollectionSource);

            CollectionAssert.AreEquivalent(_mockColl, _target.CollectionSource);
        }

        [Test]
        public void CreateFieldRepository_FieldRepository_ReturnOriginalOnRestrictorsAreEmpty()
        {
            var actual = _target.CreateFieldRepository(_request);

            Assert.AreEqual(_property.Name, actual.FieldName);
            CollectionAssert.AreEquivalent(_mockColl, actual.CollectionSource);
        }

        [Test]
        public void CreateFieldRepository_FieldRepository_RestrictorListCanBeExtendedFromOutside() // ограничитель может быть задан уже после передачи в конструктор fieldBatler'а, и также должен участвовать в отборе
        {
            var thirdRestrictor = new Mock<IFieldSourceRestrictor<FieldAttributeBase>>();
            _target.Restrictors.Add(thirdRestrictor.Object);

            var actual = _target.CreateFieldRepository(_request);

            thirdRestrictor.Verify(m => m.GetRestrictedSource(_mockColl, _request), Times.Once);
        }

        [Test]
        public void CreateFieldRepository_FieldRepository_ReturnRestrcitedOnSingleRestrictor()
        {
            _mockColl.AddRange(new List<BatlerTestEntity>() { new BatlerTestEntity(), new BatlerTestEntity(), new BatlerTestEntity() });

            var expected = new List<SimpleEntity>() { _mockColl[0], _mockColl[1] };

            _firstRestrictor.Setup(m => m.GetRestrictedSource(_mockColl, _request)).Returns(expected);

            _target = new RequestFieldBatler<BatlerTestRequest>(_property, _mockColl,
                                                   new List<IFieldSourceRestrictor<FieldAttributeBase>>() { _firstRestrictor.Object }, _rules,
                                                   _mockEngine.Object, () => new SingleFieldRepository());
            var actual = _target.CreateFieldRepository(_request);

            Assert.AreEqual(_property.Name, actual.FieldName);
            CollectionAssert.AreEqual(expected, actual.CollectionSource.Keys);
            CollectionAssert.IsSubsetOf(actual.CollectionSource.Keys, expected);
        }

        [Test]
        public void CreateFieldRepository_FieldRepository_ReturnOnTwiceRestrictor()
        {
            _mockColl.AddRange(new List<BatlerTestEntity>() { new BatlerTestEntity(), new BatlerTestEntity(), new BatlerTestEntity() });

            var restrictedAfterFirst = new List<SimpleEntity>() { _mockColl[0], _mockColl[1] };

            var expected = new List<SimpleEntity>() { _mockColl[1] };

            _firstRestrictor.Setup(m => m.GetRestrictedSource(_mockColl, _request)).Returns(restrictedAfterFirst);

            _secondRestrictor.Setup(m => m.GetRestrictedSource(restrictedAfterFirst, _request)).Returns(expected);

            _target = new RequestFieldBatler<BatlerTestRequest>(_property, _mockColl,
                                                   new List<IFieldSourceRestrictor<FieldAttributeBase>>()
                                                       {
                                                           _firstRestrictor.Object,
                                                           _secondRestrictor.Object
                                                       }, _rules, _mockEngine.Object, () => new SingleFieldRepository());
            var actual = _target.CreateFieldRepository(_request);

            Assert.AreEqual(_property.Name, actual.FieldName);
            CollectionAssert.AreEqual(expected, actual.CollectionSource.Keys);
            CollectionAssert.IsSubsetOf(actual.CollectionSource.Keys, expected);
        }

        [Test]
        public void CreateFieldRepository_FieldRepository_ValidStateOfFieldRepository()
        {
            bool expected = true;

            var mockSelectionRule = new Mock<ISelectionRule<IRequest>>();
            mockSelectionRule.Setup(p => p.GetGeneralDenyState(_request, It.IsAny<IEnumerable<SimpleEntity>>())).Returns(expected);

            _rules.Add(mockSelectionRule.Object);

            var actual = _target.CreateFieldRepository(_request);

            Assert.AreEqual(expected, actual.IsDeny);
        }

        [Test]
        public void CreateFieldRepository_FieldRepository_ThereIsAssignedValueIfAnySelectorGivesValue()
        {
            var entity1 = new BatlerTestEntity() {Name = "Value"};
            _mockColl.Add(entity1);
            var entity2 = new BatlerTestEntity();
            _mockColl.Add(entity2);

            var mockSelectionRule1 = new Mock<ISelectionRule<IRequest>>();
            mockSelectionRule1.Setup(p => p.GetAssignedValue(_mockColl, _request)).Returns(new SimpleEntity[0]); // пустая колекция говорит, что никакого значения выбрано не было

            var mockSelectionRule2 = new Mock<ISelectionRule<IRequest>>();
            mockSelectionRule2.Setup(p => p.GetAssignedValue(_mockColl, _request)).Returns(new[] { entity1 });

            var mockSelectionRule3 = new Mock<ISelectionRule<IRequest>>();
            mockSelectionRule3.Setup(p => p.GetAssignedValue(_mockColl, _request)).Returns(new SimpleEntity[0]); // третье правило возвращает null, но он не должен повлиять на выбранное по итогам второго правила значение

            _rules.Add(mockSelectionRule1.Object);
            _rules.Add(mockSelectionRule2.Object);
            _rules.Add(mockSelectionRule3.Object);

            var actual = _target.CreateFieldRepository(_request) as SingleFieldRepository;

            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.HasValue);
            Assert.AreEqual(entity1.Name, actual.Value);
            mockSelectionRule3.Verify(m => m.GetAssignedValue(It.IsAny<IEnumerable<SimpleEntity>>(), It.IsAny<IRequest>()), Times.Never, "После однозначного определения выбранного значения последующие поиск не требуется");
        }

        [Test]
        public void CreateFieldRepository_FieldRepository_StatesOfEntitiesIsMerged()
        {
            _mockColl.Add(new BatlerTestEntity());
            _mockColl.Add(new BatlerTestEntity());
            _mockColl.Add(new BatlerTestEntity());

            var subColl1 = new Dictionary<SimpleEntity, bool>();
            subColl1.Add(_mockColl[0], false);
            subColl1.Add(_mockColl[1], true);
            subColl1.Add(_mockColl[2], true);

            var subColl2 = new Dictionary<SimpleEntity, bool>();
            subColl2.Add(_mockColl[0], true);
            subColl2.Add(_mockColl[1], true);
            subColl2.Add(_mockColl[2], false);

            var expected = new Dictionary<SimpleEntity, bool>()
                {
                    {_mockColl[0], subColl1[_mockColl[0]] & subColl2[_mockColl[0]]},
                    {_mockColl[1], subColl1[_mockColl[1]] & subColl2[_mockColl[1]]},
                    {_mockColl[2], subColl1[_mockColl[2]] & subColl2[_mockColl[2]]}
                };

            var mockRule1 = new Mock<ISelectionRule<IRequest>>();
            mockRule1.Setup(p => p.NeedStateChanging).Returns(true);
            mockRule1.Setup(m => m.GetCollectionSourceWithAllowable(_mockColl, _request)).Returns(subColl1);
            var mockRule2 = new Mock<ISelectionRule<IRequest>>();
            mockRule2.Setup(p => p.NeedStateChanging).Returns(true);
            mockRule2.Setup(m => m.GetCollectionSourceWithAllowable(_mockColl, _request)).Returns(subColl2);

            _rules.Add(mockRule1.Object);
            _rules.Add(mockRule2.Object);

            var fieldRepo = _target.CreateFieldRepository(_request);
            var actual = fieldRepo.CollectionSource;

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void CreateFieldRepository_FieldRepository_DoNotInvokeGetCollectionSourceWithAllowableIfNeddingFlagIsTrue() // методы GetCollectionSourceWithAllowable не должен быть вызван, если этого не требуется из-за флага
        {
            var entity1 = new BatlerTestEntity();
            _mockColl.Add(entity1);
            var entity2 = new BatlerTestEntity();
            _mockColl.Add(entity2);

            var mockSelectionRule1 = new Mock<ISelectionRule<IRequest>>();
            mockSelectionRule1.Setup(p => p.NeedStateChanging).Returns(false);

            _rules.Add(mockSelectionRule1.Object);

            var actual = _target.CreateFieldRepository(_request) as SingleFieldRepository;

            mockSelectionRule1.Verify(m => m.GetCollectionSourceWithAllowable(It.IsAny<IEnumerable<SimpleEntity>>(), It.IsAny<IRequest>()),
                                      Times.Never);
        }

        // todo Ремезов: перенести в DefaultFieldSourceRestrictor
        //[Test]
        //public void CreateFieldRepository_FieldRepository_SameTypeWithPropertyMustBeRegisteredInBuilder() // тип и свойство этого типа должны быть зарегистрированы в typeBuilder'е
        //{
        //    _mockColl.AddRange(new List<BatlerTestEntity>()
        //        {
        //            new BatlerTestEntity(){Name = "1"}, new BatlerTestEntity(){Name = "2"}, new BatlerTestEntity(){Name = "3"}, new BatlerTestEntity(){Name = "4"}
        //        });

        //    Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>> converter = null;

        //    _mockBuilder.Setup(m => m.RegisterType(typeof(BatlerTestEntity), typeof(BatlerTestEntity),
        //                       It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()))
        //                .Callback((Type entityType, Type returnType, Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>> func) =>
        //                        {
        //                            converter = func;
        //                        });

        //    _target = new RequestFieldBatler(_property, _mockColl, new List<IFieldSourceRestrictor<FieldAttributeBase>>(), _rules, _mockEngine.Object, () => new SingleFieldRepository());
        //    var actual = _target.CreateFieldRepository(_request);

        //    _mockBuilder.Verify(
        //        m =>
        //        m.RegisterType(typeof(BatlerTestEntity), typeof(BatlerTestEntity),
        //                       It.IsAny<Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>>>()));
        //    _mockBuilder.Verify(m => m.RegisterProperty(typeof(BatlerTestEntity), "Name", "Field"));

        //    var converted = converter(actual.CollectionSource, _request);

        //    CollectionAssert.AreEqual(_mockColl, converted.Select(i => i.Key));
        //}
    }

    public class BatlerTestRequest : IRequest
    {
        [RequestField(typeof(BatlerTestEntity))]
        public string Field { get; set; }

        [RequestField(typeof(BatlerTestEntity2))]
        public string Field2 { get; set; }

        [RequestField(typeof(BatlerTestEntity3))]
        public string Field3 { get; set; }
    }

    public class BatlerTestEntity : SimpleEntity { }

    public class BatlerTestEntity2 : SimpleEntity { }

    public class BatlerTestEntity3 : SimpleEntity { }

    public class OtherType
    {
        public string Property { get; set; }
    }
}
