﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Moq;
using NUnit.Framework;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.AppServices.DataDeriver.Core.Impl;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;
using UGSK.K3.AppServices.DataDeriver.Core.SelectionRules;

namespace UGSK.K3.AppServices.DataDeriver.Test
{
    [TestFixture]
    class BatlerFactoryTest
    {
        private BatlerFactory _target;
        private Mock<Func<FieldAttributeBase, IRequestFieldBatler, Action<EventHandler<NewRestrictorEventArgs>, Type>, IFieldSourceRestrictor<FieldAttributeBase>>> _delegateFactory;
        private Mock<IFieldSourceRestrictor<FieldAttributeBase>> _mockRestrictor;
        private FactoryDbSession _mockDbSession;
        private FactoryTestRequest3 _request3;
        private List<ISelectionRule<IRequest>> _defaultRules;
        private Mock<ISelectorRuleBuilder> _mockSelector;
        private Mock<ITariffCalculationEngine<IRequest>> _mockEngine;
        private Mock<ITypeBuilder> _mockBuilder;
        private Mock<ICalculatorDispatcher> _mockCalcDisp;

        [SetUp]
        public void Init()
        {
            _mockBuilder = new Mock<ITypeBuilder>();
            _mockEngine = new Mock<ITariffCalculationEngine<IRequest>>();
            _mockEngine.Setup(p => p.TypeBuilder).Returns(_mockBuilder.Object);

            _mockSelector = new Mock<ISelectorRuleBuilder>();
            _defaultRules = new List<ISelectionRule<IRequest>>();
            _request3 = new FactoryTestRequest3();
            _delegateFactory = new Mock<Func<FieldAttributeBase, IRequestFieldBatler, Action<EventHandler<NewRestrictorEventArgs>, Type>, IFieldSourceRestrictor<FieldAttributeBase>>>();
            _mockRestrictor = new Mock<IFieldSourceRestrictor<FieldAttributeBase>>();
            _mockDbSession = new FactoryDbSession();
            var mockCalc = new Mock<ICalculator<IRequest>>();
            mockCalc.Setup(p => p.DbSession).Returns(_mockDbSession);
            //_mockEngine.Setup(p => p.Calculator).Returns(mockCalc.Object);
            _mockCalcDisp = new Mock<ICalculatorDispatcher>();
            _mockCalcDisp.Setup(m => m.GetCalculator<IRequest>()).Returns(mockCalc.Object);
            _mockCalcDisp.Setup(m => m.GetCalculator<FactoryTestRequest>()).Returns(mockCalc.Object);
            _target = new BatlerFactory(_delegateFactory.Object, _mockSelector.Object, _mockCalcDisp.Object, null,null);
        }

        [Test]
        public void MakeFieldBatler_FieldBatler_SimpleTestOfDefaultBehaviour()
        {
            var property = typeof(FactoryTestRequest3).GetProperty("Field");
            _mockDbSession.Collection1 = new List<FactoryTestEntity>() { new FactoryTestEntity(), new FactoryTestEntity() };

            var actual = _target.MakeFieldBatler(property, _mockEngine.Object);

            Assert.IsNotNull(actual);

            Assert.AreSame(property, actual.FieldProperty);
        }

        [Test]
        [ExpectedException(typeof(InvalidDataException), ExpectedMessage = "BadField1", MatchType = MessageMatch.Contains)]
        public void MakeFieldBatler_FieldBatler_ExceptionOnAttributedProperty()
        {
            var property = typeof(FactoryTestRequest3).GetProperty("BadField1");

            _target.MakeFieldBatler(property, _mockEngine.Object);
        }

        [Test]
        public void MakeFieldBatler_FieldBatler_AttributelessFieldMustHaveBatlerWithSimpleEntitySource()
        {
            var property = typeof(FactoryTestRequest3).GetProperty("AttrlessField");

            var actual = _target.MakeFieldBatler(property, _mockEngine.Object);

            Assert.AreEqual(typeof(SimpleEntity), actual.SourceType);
        }

        // тест убран по причине того, что источника данных может и не быть. Теперь такая ситуация ошибочной не является (пример: например, стоимость авто источника данных не имеет, но значения возвращать может), и исключения побуждаться не должно
        //#region ExceptionOnCollectionAbsent
        //[Test]
        //[ExpectedException(Handler = "CollectionAbsentHandler")]// ExpectedMessage = "CollectionlessField", MatchType = MessageMatch.Contains)]
        //public void MakeFieldBatler_FieldBatler_ExceptionOnCollectionAbsent()
        //{
        //    var property = typeof(FactoryTestRequest3).GetProperty("CollectionlessField");

        //    _target.MakeFieldBatler(property, _mockEngine.Object);
        //}

        //public void CollectionAbsentHandler(Exception exception)
        //{
        //    Assert.IsInstanceOf<InvalidDataException>(exception);

        //    StringAssert.Contains("CollectionlessField", exception.Message);

        //    StringAssert.Contains("FactoryTestEntity2", exception.Message);

        //    StringAssert.Contains("FactoryDbSession", exception.Message);
        //}
        //#endregion

        [Test]
        public void MakeFieldBatler_FieldBatler_BatlerOfPropertyInvokeRestrictorFactory()
        {
            _mockDbSession.Collection1 = new List<FactoryTestEntity>() { new FactoryTestEntity(), new FactoryTestEntity() };

            var property = typeof(FactoryTestRequest3).GetProperty("Field");

            var actual = _target.MakeFieldBatler(property, _mockEngine.Object);

            _delegateFactory.Verify(m => m(It.IsAny<RequestFieldAttribute>(), It.IsAny<IRequestFieldBatler>(), It.IsAny<Action<EventHandler<NewRestrictorEventArgs>, Type>>()), Times.Once);
        }

        [Test]
        public void MakeFieldBatler_FieldBatler_VerifyRegistratorInvoke()
        {
            _mockDbSession.Collection1 = new List<FactoryTestEntity>() { new FactoryTestEntity(), new FactoryTestEntity() };

            var property = typeof(FactoryTestRequest).GetProperty("Field");

            var registerMethod = _target.GetType().GetMethod("OnRegister", BindingFlags.NonPublic | BindingFlags.Instance);

            Delegate handler = null;

            _delegateFactory.Setup(m => m(It.IsAny<FieldAttributeBase>(), It.IsAny<IRequestFieldBatler>(), It.IsAny<Action<EventHandler<NewRestrictorEventArgs>, Type>>()))
                            .Returns(_mockRestrictor.Object)
                            .Callback((FieldAttributeBase attr, IRequestFieldBatler request, Action<EventHandler<NewRestrictorEventArgs>, Type> handlr) =>
                                {
                                    handler = handlr;
                                });

            _target.MakeFieldBatler(property, _mockEngine.Object);

            Assert.AreEqual(registerMethod, handler.Method);

            _delegateFactory.Verify(m => m(It.IsAny<FieldAttributeBase>(), It.IsAny<IRequestFieldBatler>(), It.IsAny<Action<EventHandler<NewRestrictorEventArgs>, Type>>()), Times.Once);
        }

        [Test]
        public void MakeFieldBatler_FieldBatler_NotifyOfNewRestrictor()
        {
            var mockEngine = new Mock<ITariffCalculationEngine<FactoryTestRequest>>();
            mockEngine.Setup(p => p.TypeBuilder).Returns(_mockBuilder.Object);
            
            _mockDbSession.Collection1 = new List<FactoryTestEntity>() { new FactoryTestEntity(), new FactoryTestEntity() };
            _mockDbSession.Collection3 = new List<FactoryTestEntity3>() { new FactoryTestEntity3(), new FactoryTestEntity3() };

            var eventDelegate = new Mock<EventHandler<NewRestrictorEventArgs>>();

            _delegateFactory.Setup(
                m =>
                m(It.IsAny<FieldAttributeBase>(), It.IsAny<IRequestFieldBatler>(),
                  It.IsAny<Action<EventHandler<NewRestrictorEventArgs>, Type>>()))
                            .Returns(_mockRestrictor.Object)
                            .Callback(
                                (FieldAttributeBase attr, IRequestFieldBatler batler,
                                 Action<EventHandler<NewRestrictorEventArgs>, Type> handlr) =>
                                handlr(eventDelegate.Object, typeof (FactoryTestRequest)));

            IFieldSourceRestrictor<FieldAttributeBase> actualRestrictor = null;
            BatlerFactory actualFactory = null;

            eventDelegate.Setup(p => p(_target, It.IsAny<NewRestrictorEventArgs>())).Callback((object factory, NewRestrictorEventArgs args) =>
                {
                    actualFactory = factory as BatlerFactory;
                    actualRestrictor = args.NewRestrictor;
                });

            _target.MakeFieldBatler(typeof(FactoryTestRequest).GetProperty("Field"), mockEngine.Object); // вызов 1

            _target.MakeFieldBatler(typeof(FactoryTestRequest).GetProperty("Field3"), mockEngine.Object); // вызов 2

            eventDelegate.Verify(p => p(_target, It.IsAny<NewRestrictorEventArgs>()), Times.Exactly(3)); // при вызове 1 оповещение только что зарегистрированному ограничителю и при вызове 2 оповещаются сперва старый, потом новый. Итого 3 раза

            Assert.AreSame(_mockRestrictor.Object, actualRestrictor);
            Assert.AreSame(_target, actualFactory);
        }

        // удалён, т.к. дефолтовые правила отбора перенесены непосредственно в BatlerFactory
        //[Test]
        //public void MakeFieldBatler_FieldBatler_DefaultSelectorsAssignedByPriority()
        //{
        //    var req = new FactoryTestRequest();

        //    _mockRestrictor.Setup(m => m.GetRestrictedSource(It.IsAny<IEnumerable<SimpleEntity>>(), req))
        //                   .Returns((IEnumerable<SimpleEntity> coll, IRequest request) => coll);

        //    _delegateFactory.Setup(
        //        d => d(It.IsAny<RequestFieldAttribute>(), It.IsAny<IRequestFieldBatler>(), It.IsAny<Action<EventHandler<NewRestrictorEventArgs>>>()))
        //                    .Returns(_mockRestrictor.Object);

            

        //    var expected = new List<FactoryTestEntity5>();
        //    expected.Add(new FactoryTestEntity5());
        //    expected.Add(new FactoryTestEntity5());
        //    expected.Add(new FactoryTestEntity5());
        //    _mockDbSession.Collection5 = expected;


        //    Mock<ISelectionRule<IRequest>> firstInvoked = null;

        //    var mockRule1 = new Mock<ISelectionRule<IRequest>>();
        //    mockRule1.Setup(p => p.Priority).Returns(0); // приоритет ниже
        //    mockRule1.Setup(m => m.GetAssignedValue(expected, req))
        //             .Returns(new FactoryTestEntity5[0])
        //             .Callback((IEnumerable<SimpleEntity> coll, IRequest request) => firstInvoked = firstInvoked ?? mockRule1);
        //    _defaultRules.Add(mockRule1.Object);

        //    var mockRule2 = new Mock<ISelectionRule<IRequest>>();
        //    mockRule2.Setup(p => p.Priority).Returns(10);
        //    mockRule2.Setup(m => m.GetAssignedValue(expected, req))
        //             .Returns(new FactoryTestEntity5[0])
        //             .Callback((IEnumerable<SimpleEntity> coll, IRequest request) => firstInvoked = firstInvoked ?? mockRule2);

        //    _defaultRules.Add(mockRule2.Object);
            
        //    var batler = _target.MakeFieldBatler(typeof(FactoryTestRequest).GetProperty("Field5"), _mockEngine.Object);
        //    var fieldRepo = batler.CreateFieldRepository(req);

        //    mockRule1.Verify(m => m.GetAssignedValue(expected, req));

        //    mockRule2.Verify(m => m.GetAssignedValue(expected, req));

        //    Assert.AreSame(firstInvoked, mockRule2);
        //}

        [Test]
        public void MakeFieldBatler_FieldBatler_FromAttributeSelectorsAreAssignedByPriority()
        {
            var req = new FactoryTestRequest();

            _mockRestrictor.Setup(m => m.GetRestrictedSource(It.IsAny<IEnumerable<SimpleEntity>>(), req))
                           .Returns((IEnumerable<SimpleEntity> coll, IRequest request) => coll);

            _delegateFactory.Setup(
                d => d(It.IsAny<RequestFieldAttribute>(), It.IsAny<IRequestFieldBatler>(), It.IsAny<Action<EventHandler<NewRestrictorEventArgs>, Type>>()))
                            .Returns(_mockRestrictor.Object);

            var expected = new List<FactoryTestEntity6>();
            expected.Add(new FactoryTestEntity6());
            expected.Add(new FactoryTestEntity6());
            expected.Add(new FactoryTestEntity6());
            _mockDbSession.Collection6 = expected;

            Mock<ISelectionRule<IRequest>> firstInvoked = null;

            var mockRule1 = new Mock<ISelectionRule<IRequest>>();
            mockRule1.Setup(p => p.Priority).Returns(0); // приоритет ниже
            mockRule1.Setup(m => m.GetAssignedValue(expected, req))
                     .Returns(new FactoryTestEntity6[0])
                     .Callback((IEnumerable<SimpleEntity> coll, IRequest request) => firstInvoked = firstInvoked ?? mockRule1);

            var mockRule2 = new Mock<ISelectionRule<IRequest>>();
            mockRule2.Setup(p => p.Priority).Returns(10);
            mockRule2.Setup(m => m.GetAssignedValue(expected, req))
                     .Returns(new FactoryTestEntity6[0])
                     .Callback((IEnumerable<SimpleEntity> coll, IRequest request) => firstInvoked = firstInvoked ?? mockRule2);
            _defaultRules.Add(mockRule1.Object);
            _defaultRules.Add(mockRule2.Object);

            var atrMockRule = new Mock<ISelectionRule<IRequest>>();
            atrMockRule.Setup(p => p.Priority).Returns(999);
            atrMockRule.Setup(m => m.GetAssignedValue(expected, req))
                     .Returns(new FactoryTestEntity6[0])
                     .Callback((IEnumerable<SimpleEntity> coll, IRequest request) => firstInvoked = firstInvoked ?? atrMockRule);
            _mockSelector.Setup(m => m.BuildRule(typeof(FactoryTestSelectorFakeRule), 999)).Returns(atrMockRule.Object); // !!! FactoryTestSelectorFakeRule не является реализацией правила, вставлен для соответствия типу из атрибута для Field6

            var batler = _target.MakeFieldBatler(typeof(FactoryTestRequest).GetProperty("Field6"), _mockEngine.Object);
            var fieldRepo = batler.CreateFieldRepository(req);

            atrMockRule.Verify(m => m.GetAssignedValue(expected, req));

            Assert.AreSame(firstInvoked, atrMockRule);
        }

        [Test]
        public void MakeFieldBatler_FieldBatler_UnRestrictedAndUnDeselectableCollection() // возвращаем список, который ничем не сужен и каждый элемент не задисаблен
        {
            var request = new FactoryTestRequest();

            _mockRestrictor.Setup(m => m.GetRestrictedSource(It.IsAny<IEnumerable<SimpleEntity>>(), request))
                           .Returns((IEnumerable<SimpleEntity> coll, IRequest req) => coll);

            _delegateFactory.Setup(
                d => d(It.IsAny<RequestFieldAttribute>(), It.IsAny<IRequestFieldBatler>(), It.IsAny<Action<EventHandler<NewRestrictorEventArgs>, Type>>()))
                            .Returns(_mockRestrictor.Object);

            var expected = new List<FactoryTestEntity7>();
            expected.Add(new FactoryTestEntity7());
            expected.Add(new FactoryTestEntity7());
            expected.Add(new FactoryTestEntity7());
            _mockDbSession.Collection7 = expected;

            var batler = _target.MakeFieldBatler(typeof(FactoryTestRequest).GetProperty("Field7"), _mockEngine.Object);

            var fieldRepo = batler.CreateFieldRepository(request);

            var actual = fieldRepo.CollectionSource;

            CollectionAssert.AreEqual(expected.ToDictionary(i => i, i => true), actual);
        }
    }

    public class TestFieldAttribute : FieldAttributeBase { }

    public class FactoryTestRequest : IRequest
    {
        [RequestField(typeof(FactoryTestEntity))]
        public string Field { get; set; }

        [RequestField(typeof(FactoryTestEntity3))]
        public string Field3 { get; set; }

        [RequestField(typeof(FactoryTestEntity3))]
        public string[] Field4 { get; set; }

        [RequestField(typeof(FactoryTestEntity5))]
        public string Field5 { get; set; }

        [RequestField(typeof(FactoryTestEntity6))]
        [FieldSelector(typeof(FactoryTestSelectorFakeRule), 999)]
        public string Field6 { get; set; }

        [RequestField(typeof(FactoryTestEntity7))]
        public string Field7 { get; set; }
    }

    public class FactoryTestRequest3 : IRequest
    {
        [RequestField(typeof(FactoryTestEntity))]
        public string Field { get; set; }

        [NonField]
        public string BadField1 { get; set; }

        public string AttrlessField { get; set; }

        [RequestField(typeof(FactoryTestEntity2))]
        public string CollectionlessField { get; set; }
    }

    public class FactoryTestRequest2 : IRequest
    {
        [RequestField(typeof(FactoryTestEntity))]
        public string Field { get; set; }

        public string BadField1 { get; set; }

        [RequestField(typeof(FactoryTestEntity2))]
        public string CollectionlessField { get; set; }
    }

    public class FactoryDbSession : IDbSession
    {
        public IEnumerable<FactoryTestEntity> Collection1 { get; set; }

        public IEnumerable<FactoryTestEntity3> Collection3 { get; set; }

        public IEnumerable<FactoryTestEntity4> Collection4 { get; set; }

        public IEnumerable<FactoryTestEntity5> Collection5 { get; set; }

        public IEnumerable<FactoryTestEntity6> Collection6 { get; set; }

        public IEnumerable<FactoryTestEntity7> Collection7 { get; set; }

        public string TariffVersion
        {
            get { throw new NotImplementedException(); }
        }

        public string TariffDate { get; private set; }
    }

    public class FactoryTestEntity : SimpleEntity { }

    public class FactoryTestEntity2 : SimpleEntity { }

    public class FactoryTestEntity3 : SimpleEntity { }

    public class FactoryTestEntity4 : SimpleEntity { }

    public class FactoryTestEntity5 : SimpleEntity { }

    public class FactoryTestEntity6 : SimpleEntity { }

    public class FactoryTestEntity7 : SimpleEntity { }

    public class FactoryTestSelectorFakeRule { }
}
