﻿using System;
using NUnit.Framework;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Model.Test
{
    [TestFixture]
    class UserExtensionsTest
    {

        [Test]
        [TestCase("2015-09-21T00:00:00+03:00", "2016-09-20T00:00:00+03:00", Result = 12)]
        [TestCase("2015-09-21T00:00:00+03:00", "2015-09-20T00:00:00+03:00", Result = 0)]
        [TestCase("2015-09-21T00:00:00+03:00", "2015-10-01T00:00:00+03:00", Result = 1)]
        [TestCase("2015-09-21T00:00:00+03:00", "2016-10-01T00:00:00+03:00", Result = 13)]
        [TestCase("2015-09-21T00:00:00+03:00", "2015-09-22T00:00:00+03:00", Result = 1)]
        [TestCase("2015-09-21T00:00:00+03:00", "2016-09-22T00:00:00+03:00", Result = 13)]
        [TestCase("2015-12-31T00:00:00+03:00", "2016-01-01T00:00:00+03:00", Result = 1)]
        [TestCase("2015-12-31T00:00:00+03:00", "2016-01-31T00:00:00+03:00", Result = 2)]
        [TestCase("2015-12-31T00:00:00+03:00", "2016-01-30T00:00:00+03:00", Result = 1)]
        [TestCase("2015-02-01T00:00:00+03:00", "2015-03-31T00:00:00+03:00", Result = 2)]
        [TestCase("2015-02-01T00:00:00+03:00", "2015-04-01T00:00:00+03:00", Result = 3)]
        [TestCase("2016-02-29T00:00:00+03:00", "2016-03-29T00:00:00+03:00", Result = 2)]
        [TestCase("2015-09-29T00:00:00+03:00", "2016-03-29T00:00:00+03:00", Result = 7)]
        [TestCase("2015-09-29T00:00:00+03:00", "2016-09-29T00:00:00+03:00", Result = 13)]
        [TestCase("2016-01-31T00:00:00+03:00", "2016-03-01T00:00:00+03:00", Result = 2)]
        [TestCase("2016-01-31T00:00:00+03:00", "2016-02-29T00:00:00+03:00", Result = 1)]
        [TestCase("2015-01-31T00:00:00+03:00", "2015-03-01T00:00:00+03:00", Result = 2)]
        [TestCase("2015-10-01T00:00:00+03:00", "2015-12-30T00:00:00+03:00", Result = 3)]
        [TestCase("2015-10-01T00:00:00+03:00", "2015-12-31T00:00:00+03:00", Result = 3)]
        [TestCase("2015-10-01T00:00:00+03:00", "2016-01-01T00:00:00+03:00", Result = 4)]
        public int GetFullMonthsTo_Test(string dateStrFrom, string dateStrTo)
        {
            var dateFrom = DateTimeOffset.Parse(dateStrFrom);
            var dateTo = DateTimeOffset.Parse(dateStrTo);

            return dateFrom.GetMonthsTo(dateTo);
        }

        [TestCase("1.05", 1.05)]
        [TestCase("1", 1)]
        [TestCase("1024", 1024)]
        [TestCase("0.1", 0.1)]
        [TestCase("-10.01", -10.01)]
        public void ConvertToDecimal_Test(string value, decimal expected) 
        {
            var actual = value.To<decimal>();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ConvertTostring_Test()
        {
            var expected = "Abcdef 1234567 Абвгд";

            var actual = "Abcdef 1234567 Абвгд".To<string>();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ConvertToInt_Test()
        {
            var expected = 10236;

            var actual = "10236".To<int>();

            Assert.AreEqual(expected, actual);
        }

        [TestCase("1.05", 1.05)]
        [TestCase("1", 1)]
        [TestCase("1024", 1024)]
        [TestCase("0.1", 0.1)]
        [TestCase("-10.01", -10.01)]
        public void ConvertToFloat_Test(string value, decimal expected)
        {
            var actual = value.To<float>();

            Assert.AreEqual(expected, actual);
        }
    }
}
