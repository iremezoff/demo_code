﻿using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    //public class GlobalRegion : Entity<int>
    //{
    //    public string Name { get; set; }
    //    public int Code { get; set; }
    //}

    public class Country : Entity<int>
    {        
        public const string RussiaIsoCode = "643";
        public const string StringRussia = "Россия";
        public string Name { get; set; }
        public string ISOCode { get; set; }

        public int SortOrder { get; set; }
    }
}
