﻿namespace UGSK.K3.Service.Model.Extensions
{
    public static class PhoneExtensions
    {
        public const string PhoneVerificationMarker = "#V";
        public const string PhoneExceptionMarker = "#X";
        public const string PhoneMask = @"\+7 \(\d{0,3}\) \d{3}\-\d{2}\-\d{2}";
        public const string PersonalPhoneMask = @"\+7 \(9\d{2}\) \d{3}\-\d{2}\-\d{2}";
    }
}
