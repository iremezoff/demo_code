﻿using System.Text;

namespace UGSK.K3.Service.Model.Extensions
{
    public static class AddressExtensions
    {
        public static string MakeFullAddressString(this FiasAddress addressItem)
        {
            var result = new StringBuilder();

            result.Append(addressItem.Region);
            if (!string.IsNullOrEmpty(addressItem.District) || !string.IsNullOrWhiteSpace(addressItem.District))
            {
                result.Append(", ");
                result.Append(addressItem.District);
            }
            if (!string.IsNullOrEmpty(addressItem.City) || !string.IsNullOrWhiteSpace(addressItem.City))
            {
                result.Append(", ");
                result.Append(addressItem.City);
            }
            if (!string.IsNullOrEmpty(addressItem.CityArea) || !string.IsNullOrWhiteSpace(addressItem.CityArea))
            {
                result.Append(", ");
                result.Append(addressItem.CityArea);
            }
            if (!string.IsNullOrEmpty(addressItem.Locality) || !string.IsNullOrWhiteSpace(addressItem.Locality))
            {
                result.Append(", ");
                result.Append(addressItem.Locality);
            }
            if (!string.IsNullOrEmpty(addressItem.Street) || !string.IsNullOrWhiteSpace(addressItem.Street))
            {
                result.Append(", ");
                result.Append(addressItem.Street);
            }
            if (!string.IsNullOrEmpty(addressItem.AddArea) || !string.IsNullOrWhiteSpace(addressItem.AddArea))
            {
                result.Append(", ");
                result.Append(addressItem.AddArea);
            }
            if (!string.IsNullOrEmpty(addressItem.AddStreet) || !string.IsNullOrWhiteSpace(addressItem.AddStreet))
            {
                result.Append(", ");
                result.Append(addressItem.AddStreet);
            }

            if (result.Length != 0)
            {
                result.Append(", ");
            }

            return result.ToString();
        }
    }


}
