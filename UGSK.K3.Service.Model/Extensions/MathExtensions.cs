﻿using System;

namespace UGSK.K3.Service.Model.Extensions
{
     public static class MathExtensions
    {
         public static int ToNearest(this decimal d, int roundingValue)
         {
             return ((int)Math.Round(d / roundingValue)) * roundingValue;
         }

         public static int ToNearestTen(this decimal t)
         {
             return ToNearest(t, 10);
         }

         public static int ToNearestHundred(this decimal h)
         {
             return ToNearest(h, 100);
         }

         public static decimal GetNormilizedFraction(this decimal value)
         {
             return value / (value > 1 ? 100 : 1);
         }
    }
}
