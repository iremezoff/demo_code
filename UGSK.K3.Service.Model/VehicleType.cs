﻿using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    public abstract class VehicleTypeBase : DictionaryEntity
    {
        public override bool Equals(object obj)
        {
            var target = obj as VehicleTypeBase;
            if (target == null) return false;
            return base.Equals(obj);
        }
    }

    public class PassengerType : VehicleTypeBase
    {
    }

    public class LorryType : VehicleTypeBase
    {
    }

    public class BusType : VehicleTypeBase
    {
    }

    public class TractorType : VehicleTypeBase
    {
    }

    public class MotorcycleType : VehicleTypeBase
    {
    }

    public class TrailerForLorryType : VehicleTypeBase
    {
    }

    public class TrailerForPassengerType : VehicleTypeBase
    {
    }

    public class TrailerForTractorType : VehicleTypeBase
    {
    }

    public class TramwayType : VehicleTypeBase
    {
    }

    public class TrolleybusType : VehicleTypeBase
    {
    }

    public class TrailerType : VehicleTypeBase { }

    
}
