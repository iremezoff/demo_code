﻿using System;
using System.Collections.Generic;
using System.Text;
using UGSK.K3.Infrastructure;
using UGSK.K3.Service.Model.Extensions;

namespace UGSK.K3.Service.Model
{
    public class FullAddress
    {
        /// <summary>
        /// Регион
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// Район
        /// </summary>
        public string District { get; set; }
        /// <summary>
        /// Город
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Район города
        /// </summary>
        public string CityArea { get; set; }
        /// <summary>
        /// Населенный пункт
        /// </summary>
        public string Locality { get; set; }
        /// <summary>
        /// Улица
        /// </summary>
        public string Street { get; set; }
        /// <summary>
        /// Доп. зона, лагерь, СНТ и тд
        /// </summary>
        public string AddArea { get; set; }
        /// <summary>
        /// Улица в доп. зоне
        /// </summary>
        public string AddStreet { get; set; }

        public Guid? Aoguid { get; set; }
        public string CodeFias { get; set; }
        public string CodeKLADR { get; set; }
        /// <summary>
        /// Почтовый код
        /// </summary>
        public string PostCode { get; set; }
        /// <summary>
        /// Уровень нижнего адресообразующего элемента
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// Дом
        /// </summary>
        public string House { get; set; }
        /// <summary>
        /// Корпус
        /// </summary>
        public string Corps { get; set; }
        /// <summary>
        /// Квартира/комната
        /// </summary>
        public string Room { get; set; }
        /// <summary>
        /// Определенный пользователем адрес
        /// </summary>
        public string UserDefinedAddress { get; set; }


        public string MakeFullAddressString()
        {
            var result = new StringBuilder();

            result.Append(Region);
            if (!string.IsNullOrEmpty(District) || !string.IsNullOrWhiteSpace(District))
            {
                result.Append(", ");
                result.Append(District);
            }
            if (!string.IsNullOrEmpty(City) || !string.IsNullOrWhiteSpace(City))
            {
                result.Append(", ");
                result.Append(City);
            }
            if (!string.IsNullOrEmpty(CityArea) || !string.IsNullOrWhiteSpace(CityArea))
            {
                result.Append(", ");
                result.Append(CityArea);
            }
            if (!string.IsNullOrEmpty(Locality) || !string.IsNullOrWhiteSpace(Locality))
            {
                result.Append(", ");
                result.Append(Locality);
            }
            if (!string.IsNullOrEmpty(Street) || !string.IsNullOrWhiteSpace(Street))
            {
                result.Append(", ");
                result.Append(Street);
            }
            if (!string.IsNullOrEmpty(AddArea) || !string.IsNullOrWhiteSpace(AddArea))
            {
                result.Append(", ");
                result.Append(AddArea);
            }
            if (!string.IsNullOrEmpty(AddStreet) || !string.IsNullOrWhiteSpace(AddStreet))
            {
                result.Append(", ");
                result.Append(AddStreet);
            }
            if (!string.IsNullOrEmpty(House) || !string.IsNullOrWhiteSpace(House))
            {
                result.Append(", ");
                result.Append(House);
            }
            if (!string.IsNullOrEmpty(Room) || !string.IsNullOrWhiteSpace(Room))
            {
                result.Append(", ");
                result.Append(Room);
            }

            return result.ToString();
        }

        private static readonly string[] Markers = new[]
        {
            "АО", "Аобл", "г", "край", "обл", "округ", "Респ", "Чувашия", "АО", "АО", "п", "р-н", "тер", "у", "волость",
            "г", "дп", "кп", "массив", "п", "п/о", "пгт", "рп", "с/а", "с/мо", "с/о", "с/п", "с/с", "тер", "р-н", "тер",
            "аал", "автодорога", "арбан", "аул", "волость", "высел", "г", "городок", "д", "дп", "ж/д_будка",
            "ж/д_казарм", "ж/д_оп", "ж/д_платф", "ж/д_пост", "ж/д_рзд", "ж/д_ст", "жилзона", "жилрайон", "заимка",
            "казарма", "кв-л", "кордон", "кп", "лпх", "м", "массив", "мкр", "нп", "остров", "п", "п/о", "п/р", "п/ст",
            "пгт", "погост", "починок", "промзона", "рзд", "рп", "с", "сл", "снт", "ст", "ст-ца", "тер", "у", "х", "а/я",
            "аал", "аллея", "арбан", "аул", "балка", "берег", "б-р", "бугор", "бухта", "вал", "въезд", "высел", "горка",
            "городок", "гск", "д", "дор", "ж/д_будка", "ж/д_казарм", "ж/д_оп", "ж/д_платф", "ж/д_пост", "ж/д_рзд",
            "ж/д_ст", "жт", "заезд", "зона", "казарма", "канал", "кв-л", "км", "кольцо", "коса", "линия", "лпх", "м",
            "маяк", "местность", "мкр", "мост", "мыс", "наб", "нп", "остров", "п", "п/о", "п/р", "п/ст", "парк", "пер",
            "переезд", "пл", "платф", "пл-ка", "полустанок", "починок", "причал", "пр-кт", "проезд", "просек", "просека",
            "проселок", "проток", "протока", "проулок", "рзд", "ряды", "с", "сад", "сквер", "сл", "снт", "спуск", "ст",
            "стр", "тер", "тоннель", "тракт", "туп", "ул", "уч-к", "ферма", "х", "ш", "эстакада", "ДОМ", "гск", "днп",
            "местность", "мкр", "н/п", "промзона", "сад", "снт", "тер", "ф/х", "а/я", "аал", "аллея", "арбан", "аул",
            "берег", "б-р", "бугор", "вал", "въезд", "высел", "городок", "гск", "д", "дор", "ж/д_будка", "ж/д_казарм",
            "ж/д_оп", "ж/д_платф", "ж/д_пост", "ж/д_рзд", "ж/д_ст", "жт", "заезд", "зона", "казарма", "канал", "кв-л",
            "км", "кольцо", "коса", "линия", "лпх", "м", "мкр", "мост", "наб", "нп", "остров", "п", "п/о", "п/р", "п/ст",
            "парк", "пер", "переезд", "пл", "платф", "пл-ка", "полустанок", "починок", "пр-кт", "проезд", "просек",
            "просека", "проселок", "проток", "протока", "проулок", "рзд", "ряды", "с", "сад", "сквер", "сл", "снт",
            "спуск", "ст", "стр", "тер", "тракт", "туп", "ул", "уч-к", "ферма", "х", "ш"
        };

        // требует замены при возможности, а именно когда адрес будет храниться в индексе без маркера адресообразующего элемента, из-за чего не понадобится проводить очистку от них
        private static FullAddress GetCleanedFromMarkers(FullAddress fullAddress)
        {
            var cleanedSegments = new List<string>(7);


            foreach (var addrElement in new[] { fullAddress.Region, fullAddress.District, fullAddress.City, fullAddress.CityArea, fullAddress.Locality, fullAddress.Street })
            {
                if (string.IsNullOrWhiteSpace(addrElement))
                {
                    cleanedSegments.Add(addrElement);
                    continue;
                }

                var mutatedSegment = addrElement;
                foreach (var marker in Markers)
                {
                    if (addrElement.StartsWith(marker + " "))
                    {
                        mutatedSegment = mutatedSegment.Substring(marker.Length + 1);
                        break;
                    }
                    if (addrElement.StartsWith(marker + ". "))
                    {
                        mutatedSegment = mutatedSegment.Substring(marker.Length + 2);
                        break;
                    }
                    if (addrElement.EndsWith(" " + marker))
                    {
                        mutatedSegment = mutatedSegment.Substring(0, mutatedSegment.Length - marker.Length);
                        break;
                    }
                    if (addrElement.EndsWith(" " + marker + "."))
                    {
                        mutatedSegment = mutatedSegment.Substring(0, mutatedSegment.Length - marker.Length - 2);
                        break;
                    }
                }
                cleanedSegments.Add(mutatedSegment);
            }

            return new FullAddress
            {
                Region = cleanedSegments[0],
                District = cleanedSegments[1],
                City = cleanedSegments[2],
                CityArea = cleanedSegments[3],
                Locality = cleanedSegments[4],
                Street = cleanedSegments[5],
                Aoguid = fullAddress.Aoguid,
                UserDefinedAddress = fullAddress.UserDefinedAddress,
                AddArea = fullAddress.AddArea,
                AddStreet = fullAddress.AddStreet,
                CodeFias = fullAddress.CodeFias,
                CodeKLADR = fullAddress.CodeKLADR,
                Corps = fullAddress.Corps,
                House = fullAddress.House,
                Level = fullAddress.Level,
                PostCode = fullAddress.PostCode,
                Room = fullAddress.Room
            };
        }

        public bool CheckFullAddressString()
        {
            var fullAddressToCheck = GetCleanedFromMarkers(this);

            if (string.IsNullOrWhiteSpace(UserDefinedAddress))
                return false;

            if (string.IsNullOrEmpty(Region) || UserDefinedAddress.IndexOf(fullAddressToCheck.Region, StringComparison.InvariantCultureIgnoreCase) < 0)
                return false;


            if (!string.IsNullOrEmpty(Locality) || !string.IsNullOrWhiteSpace(Locality))
            {
                if (UserDefinedAddress.IndexOf(fullAddressToCheck.Locality, StringComparison.InvariantCultureIgnoreCase) < 0)
                    return false;
            }

            if (string.IsNullOrEmpty(Locality) && (!string.IsNullOrEmpty(City) || !string.IsNullOrWhiteSpace(City)))
            {
                if (UserDefinedAddress.IndexOf(fullAddressToCheck.City, StringComparison.InvariantCultureIgnoreCase) < 0)
                    return false;
            }

            if (!string.IsNullOrEmpty(Street) || !string.IsNullOrWhiteSpace(Street))
            {
                if (UserDefinedAddress.IndexOf(fullAddressToCheck.Street, StringComparison.InvariantCultureIgnoreCase) < 0)
                    return false;
            }

            return true;
        }

        public override string ToString()
        {
            return MakeFullAddressString();
        }
    }

    public class FiasAddress : Entity<int>
    {
        public string Region { get; set; } //Регион
        public string District { get; set; } //Район
        public string City { get; set; } //Город
        public string CityArea { get; set; } //Район города
        public string Locality { get; set; } //Населенный пункт
        public string Street { get; set; } //Улица
        public string AddArea { get; set; } //Доп. зона, лагерь, СНТ и тд
        public string AddStreet { get; set; } //Улица в доп. зоне

        public Guid? Aoguid { get; set; }
        public string CodeFias { get; set; }
        public string CodeKLADR { get; set; }
        public string PostCode { get; set; } //почтовый код
        public int Level { get; set; } //уровень нижнего адресообразующего элемента

        public string UserDefinedAddress { get; set; } //Определенный пользователем адрес

        public override string ToString()
        {
            return this.MakeFullAddressString();
        }
    }
}
