﻿using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    public class PaymentKind : DictionaryEntity
    {
        public int PaymentMethodId { get; set; }
        public virtual PaymentMethod PaymentMethod { get; set; }
        public override bool Equals(object obj)
        {
            var target = obj as PaymentKind;
            if (target == null)
            {
                return false;
            }

            return base.Equals(obj);
        }
    }
}
