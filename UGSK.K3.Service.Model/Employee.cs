using System;
using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    public partial class Employee : Entity<int>
    {


        public Employee()
        {
        }
        public Employee(int id)
        {
            base.Id = id;
        }

        public int FilialId { get; set; }
        public string Name { get; set; }
        public string PartnerCode { get; set; }
        public string SaleChannel { get; set; }
        public string UnderwriterDocument { get; set; }
        public decimal MaxPercAcquisition { get; set; }
        public decimal PartnerFactor { get; set; }
        public DateTimeOffset? UnderwriterDocumentDate { get; set; }
        public string DistributionChannel { get; set; }
        public DateTimeOffset Created { get; set; }
        public DateTimeOffset? Updated { get; set; }
        public virtual Filial Filial { get; set; }
        public int PositionId { get; set; }
        public decimal LossAdjustmentFactor { get; set; }
        public Guid UUID { get; set; }

        public int IntermediateSellerId { get; set; }
        public int PointOfSaleId { get; set; }

        public bool IsActive { get; set; }

    }
}
