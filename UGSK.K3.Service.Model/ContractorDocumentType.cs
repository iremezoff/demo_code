﻿using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    public class ContractorDocumentType : DictionaryEntity
    {
        public override bool Equals(object obj)
        {
            var target = obj as ContractorDocumentType;
            if (target == null) return false;
            return base.Equals(obj);
        }
    }
}
