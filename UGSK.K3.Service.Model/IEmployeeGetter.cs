﻿namespace UGSK.K3.Service.Model
{
    public interface IEmployeeGetter
    {
        Employee GetEmployee();
    }
}