﻿using System;
using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    /// <summary>
    /// Сущность модели "Метод оплаты по договору"
    /// </summary>
    public class PaymentMethod : Entity<int>
    {
        public string Name { get; set; }
        public PaymentMethodCode PaymentMethodCode { get; set; }

        public override bool Equals(object obj)
        {
            var target = obj as PaymentMethod;
            if (target == null)
            {
                return false;
            }

            return this.Id == target.Id &&
                this.PaymentMethodCode == target.PaymentMethodCode &&
                object.Equals(this.Name, target.Name);
        }
    }

    /// <summary>
    /// Код метода оплаты
    /// </summary>
    [Flags]
    public enum PaymentMethodCode : byte
    {
        Cash = 1,
        Cashless = 1 << 1
    }
}
