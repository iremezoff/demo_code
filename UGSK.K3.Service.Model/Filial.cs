using System;
using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    public partial class Filial : DictionaryEntity
    {
        public Filial()
        {
        }
        public Filial(int id, string name, string description, string guid, string code, bool notForSale, string locationFiasGuidString = null)
        {
            Id = id;
            Name = name;
            Description = description;
            Guid = guid;
            Code = code;
            NotForSale = notForSale;
            if (!string.IsNullOrEmpty(locationFiasGuidString))
                LocationFiasGuid = System.Guid.Parse(locationFiasGuidString);
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Guid { get; set; }
        public string Code { get; set; }
        public bool NotForSale { get; set; }
        public Guid? LocationFiasGuid { get; set; }
    }
}
