﻿using Newtonsoft.Json;
using System;
using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    public class VehicleModel : DictionaryEntity, IComparable<VehicleModel>
    {
        public int VehicleBrandId { get; set; }
        public virtual VehicleBrand VehicleBrand { get; set; }
        public int VehicleTypeId { get; set; }
        // hack Иванов: Временное решение. Нужно что-то декларативное на уровень повыше, чтобы не мусорить в модели
        [JsonProperty(TypeNameHandling = TypeNameHandling.Auto)]
        // todo Ремезов: OData не может раскрыть через $expand свойство VehicleType (при получении договоров), поэтому выносим свойство сюда, игнорируя маппинги в EF
        public virtual VehicleTypeBase VehicleType
        {
            get { return _vehicleType; }
            set
            {
                _vehicleType = value;
                if (value != null)
                    _vehicleTypeName = value.Name;
            }
        }

        private VehicleTypeBase _vehicleType;
        private string _vehicleTypeName;

        public string VehicleTypeName
        {
            get { return _vehicleTypeName; }
            set
            {
                _vehicleTypeName = value;
            }
        }

        public string RSACode { get; set; }
        public string Category { get; set; }
        public Guid Guid { get; set; }
        public int VehicleManufacturerTypeId { get; set; }
        public virtual VehicleManufacturerType VehicleManufacturerType { get; set; }
        public int CompareTo(VehicleModel other)
        {
            return other.Id.CompareTo(Id);
        }
        public bool IsActive { get; set; }
        public override bool Equals(object obj)
        {
            var target = obj as VehicleModel;
            if (target == null) return false;

            return base.Equals(obj);
        }
    }
}
