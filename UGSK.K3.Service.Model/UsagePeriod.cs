﻿using System;

namespace UGSK.K3.Service.Model
{
    public class UsagePeriod
    {
        public int Id { get; set; }
        public DateTimeOffset From { get; set; }
        public DateTimeOffset To { get; set; }
        
    }
}