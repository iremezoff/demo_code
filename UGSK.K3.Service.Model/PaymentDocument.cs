﻿using System;
using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    public class PaymentDocument : Entity<int>
    {
        public decimal PayAmount { get; set; }
        public string Serial { get; set; }
        public string Number { get; set; }
        
        public virtual PaymentKind PaymentKind { get; set; }
        public DateTimeOffset? DatePay { get; set; }
    }
}
