﻿using System;
using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    public class PolicySerial : DictionaryEntity, ISafetyDeletable
    {
        public PolicySerial()
        {
            Created = DateTimeOffset.Now.DateTime;
        }
        public string Value { get; set; }
        public int ProductId { get; set; }
        
        public bool IsDeleted { get; set; }

        public DateTimeOffset Created { get; set; }

        public DateTimeOffset Updated { get; set; }
    }
}
