﻿using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    public class VehicleCheckupDocumentType : Entity<int>
    {
        public string Name { get; set; }
    }
}
