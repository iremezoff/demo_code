﻿using System;
namespace UGSK.K3.Service.Model
{
    public static class UserExtensions
    {
        public static int GetFullYearsTo(this DateTimeOffset from, DateTimeOffset to)
        {
            if (from > to) return to.GetFullYearsTo(from);

            TimeSpan ts = to - from;
            var baseDate = ts.TotalDays < 0 ? DateTime.MaxValue : DateTime.MinValue; // дата, от которой будем отталкиваться, чтобы случайно не вычесть из 01.01.01 или приабавить к 31.12.9999
            return baseDate.Add(ts).Year - baseDate.Year; // выполняем операцию и делаем поправку на год базовой даты           
        }

        public static int GetFullYearsFrom(this DateTimeOffset to, DateTimeOffset from)
        {
            TimeSpan ts = to - from;
            var baseDate = ts.TotalDays < 0 ? DateTime.MaxValue : DateTime.MinValue; // дата, от которой будем отталкиваться, чтобы случайно не вычесть из 01.01.01 или приабавить к 31.12.9999
            return baseDate.Add(ts).Year - baseDate.Year; // выполняем операцию и делаем поправку на год базовой даты
        }

        /// <summary>
        /// Определяет кол-во полных месяцев между двумя датами 
        /// </summary>
        /// <param name="from">Начальная дата</param>
        /// <param name="to">Конечная дата</param>
        /// <returns></returns>
        public static int GetMonthsTo(this DateTimeOffset from, DateTimeOffset to)
        {
            int fullMonths = Math.Abs((to.Month - from.Month) + 12 * (to.Year - from.Year));
            int tail = to.Day >= from.Day && to > from ? 1 : 0;
            return fullMonths + tail;
        }

        public static T To<T>(this string value)
        {
            var provider = new System.Globalization.CultureInfo("en-US");
            return (T)Convert.ChangeType(value, typeof(T), provider);
        }
    }
}