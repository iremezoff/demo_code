﻿using System;

namespace UGSK.K3.Service.Model.Helpers
{
    public class PrefixGeneratorHelper
    {
        public static string GetPrefix(string theCode, DateTimeOffset theDate)
        {
            return string.Format("{0}/{1:D2}", theCode, theDate.Year % 100);
        }
    }
}
