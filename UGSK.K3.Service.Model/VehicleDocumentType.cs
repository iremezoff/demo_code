﻿using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.Model
{
    public class VehicleDocumentType : Entity<int>
    {
        public const int Passport = 30;
        public const int RegistrationCertificate = 31;
        public const int SelfPropelledMachinePassport = 32;
        public const int TechPassport = 33;
        public const int TechCard = 34;
        public const int ForeignTechPassport = 35;
        public const int ForeignPassport = 36;
        public const int ForeignRegistrationCertificate = 37;
        public const int ForeignTechCard = 38;
        public const int ForeignSelfPropelledMachinePassport = 39;
        public const int SimilarDocument = 40;

        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            var target = obj as VehicleDocumentType;
            if (target == null) return false;
            return this.Id == target.Id &&
                object.Equals(this.Name, target.Name);
        }
    }
}
