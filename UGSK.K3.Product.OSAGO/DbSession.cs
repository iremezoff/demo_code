﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Model;
using System.Collections.Generic; 

namespace UGSK.K3.Product.OSAGO
{
    public interface IOSAGODbSession : IDbSession
    {
        IEnumerable<AgeExperience> AgeExperiences { get; }
        IEnumerable<City> Cities { get; }
        IEnumerable<InsuranceTerm> InsuranceTerms { get; }
        IEnumerable<KBMClass> KBMClassesSession { get; }
        IEnumerable<Model.Factor> Factors { get; }
        IEnumerable<KBMTransition> KBMTransitions { get; }
        IEnumerable<DriverRestriction> LimitLists { get; }
        IEnumerable<PayoutCount> PayoutCounts { get; }
        IEnumerable<UsagePeriodReference> UsagePeriodTypes { get; }
        IEnumerable<EnginePower> EnginePowers { get; }
        IEnumerable<Region> Regions { get; }
        IEnumerable<VehicleRegistration> VehicleRegistrations { get; }
        IEnumerable<BaseTariff> BaseTariffs { get; }
        //IEnumerable<VehicleType> VehicleTypes { get; }        
        IEnumerable<Violation> Violations { get; }
        IEnumerable<UGSK.K3.Product.OSAGO.Model.PersonType> PersonTypes { get; }
        IEnumerable<TrailerFactor> TrailerFactors { get; }        
    }
}
