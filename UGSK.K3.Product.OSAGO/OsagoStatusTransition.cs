﻿namespace UGSK.K3.Product.OSAGO
{
    public enum OsagoContractStatusCode
    {
        Draft = 1, Signed = 2, SentToRSA = 3, AcceptedByRSA = 4, ReturnedForRevision = 5, Annulled = 6, SentToAccount = 7, Accounted = 8
    }
}
