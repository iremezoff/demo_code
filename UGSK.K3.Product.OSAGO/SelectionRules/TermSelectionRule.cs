using UGSK.K3.Product.OSAGO.Specifications;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class TermSelectionRule : KBMClassSelectionRuleBase
    {
        public TermSelectionRule(IsKpEnabledSpecification kvsSpec)
            : base(kvsSpec)
        {
        }
    }
}