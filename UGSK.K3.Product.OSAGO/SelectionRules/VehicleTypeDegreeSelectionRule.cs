using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class VehicleTypeDegreeSelectionRule : ISelectionRule<IRequest>
    {
        public bool NeedStateChanging { get; private set; }

        public int Priority { get; set; }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            throw new NotImplementedException();
        }

        public bool GetGeneralDenyState(IRequest commonRequest, IEnumerable<SimpleEntity> selectedEntities)
        {
            return selectedEntities == null || !selectedEntities.Any();
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            return Enumerable.Empty<SimpleEntity>();
            //return sourceCollection.Cast<GenericSimpleEntity<OsagoVehicleTypeDegree>>().Select(i => i.Value.Name);
        }
    }
}