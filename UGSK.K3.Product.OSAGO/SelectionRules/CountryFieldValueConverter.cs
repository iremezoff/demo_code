using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Infrastructure;
using UGSK.K3.Product.Contract;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class CountryFieldValueConverter : IFieldValueConverter
    {
        public Type SourceType { get { return typeof(GenericSimpleEntity<string>); } }
        public Type ExpectedType { get { return typeof(GenericSimpleEntity<string>); } }

        public IEnumerable<KeyValuePair<object, bool>> Convert(IDictionary<SimpleEntity, bool> source, IRequest request)
        {
            return source.Select(i => new KeyValuePair<object, bool>(new GenericSimpleEntity<string>(Convert(((GenericSimpleEntity<Country>)i.Key).Value) as string), i.Value));
        }

        public object Convert(Entity<int> entity)
        {
            return (entity as Country).Name;
        }
    }
}