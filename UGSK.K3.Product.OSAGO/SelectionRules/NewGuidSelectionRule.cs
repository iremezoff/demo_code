﻿using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public abstract class NewGuidSelectionRule : ISelectionRule<IRequest>
    {
        public bool NeedStateChanging
        {
            get { return false; }
        }

        public int Priority { get; set; }


        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            return new Dictionary<SimpleEntity, bool>();
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedEntities)
        {
            return true;
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            var nullValue = Enumerable.Empty<SimpleEntity>();

            var sRequest = request as SimpleRequest;
            if (sRequest == null)
            {
                return nullValue;
            }
            var oldValue = GetOldValue(request);
            return new[] { new GenericSimpleEntity<Guid>() { Value = oldValue != Guid.Empty ? oldValue : Guid.NewGuid() } };
        }

        protected abstract Guid GetOldValue(IRequest request);
    }

    public class OwnerNewGuidSelectionRule : NewGuidSelectionRule
    {
        protected override Guid GetOldValue(IRequest request)
        {
            return (request is SimpleRequest) ? ((SimpleRequest)request).OwnerGuid : Guid.Empty;
        }
    }

    public class InsuredNewGuidSelectionRule : NewGuidSelectionRule
    {
        protected override Guid GetOldValue(IRequest request)
        {
            return (request is SimpleRequest) ? ((SimpleRequest)request).InsuredGuid : Guid.Empty;
        }
    }
}
