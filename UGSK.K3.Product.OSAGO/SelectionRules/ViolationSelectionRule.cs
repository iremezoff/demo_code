using UGSK.K3.Product.OSAGO.Specifications;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class ViolationSelectionRule : KBMClassSelectionRuleBase
    {
        public ViolationSelectionRule(IsKnEnabledSpecification kvsSpec)
            : base(kvsSpec)
        {
        }
    }
}