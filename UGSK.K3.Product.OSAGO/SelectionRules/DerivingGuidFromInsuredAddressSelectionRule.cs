﻿using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Product.Contract;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class DerivingGuidFromInsuredAddressSelectionRule : ISelectionRule<IRequest>
    {
        public bool NeedStateChanging
        {
            get { return false; }
        }

        public int Priority { get; set; }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            return new Dictionary<SimpleEntity, bool>();
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedEntities)
        {
            return true;
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            var nullValue = Enumerable.Empty<SimpleEntity>();

            var sRequest = request as SimpleRequest;
            if (sRequest == null)
            {
                return nullValue;
            }
            IEnumerable<SimpleEntity> result = nullValue;
            if (sRequest.InsuredCommonRealAddress != null && sRequest.InsuredCommonRealAddress.Aoguid.HasValue)
            {
                result = new[]
                {
                    new GenericSimpleEntity<string>()
                    {
                        Value = sRequest.InsuredCommonRealAddress.Aoguid.Value.ToString()
            }
                };
            }
            return result;
        }
    }





    public abstract class RussiaIsOurNativelandSelectionRule : ISelectionRule<IRequest>
    {
        public bool NeedStateChanging { get; private set; }
        public int Priority { get; set; }
        public virtual IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            throw new System.NotImplementedException();
        }

        public abstract bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> currentValue);

        protected abstract Country GetCurrentCountry(SimpleRequest request);


        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            var osagoRequest = request as SimpleRequest;

            var currentCountry = GetCurrentCountry(osagoRequest);

            if (!GetGeneralDenyState(request, null))
            {
                return new[] { new GenericSimpleEntity<Country>(currentCountry) };
            }
            var countries = sourceCollection.Cast<GenericSimpleEntity<Country>>();

            return countries.Where(c => Country.RussiaIsoCode.Equals(c.Value.ISOCode));
        }
    }

    public class InsuredRussiaIsOurNativelandSelectionRule : RussiaIsOurNativelandSelectionRule
    {
        public override bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> currentValue)
        {
            return !((request as SimpleRequest).InsuredCountry != null && (request as SimpleRequest).InsuredIsNotResident);
        }

        protected override Country GetCurrentCountry(SimpleRequest request)
        {
            return request.InsuredCountry;
        }
    }

    public class OwnerRussiaIsOurNativelandSelectionRule : RussiaIsOurNativelandSelectionRule
    {
        public override bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> currentValue)
        {
            return !((request as SimpleRequest).OwnerCountry != null && (request as SimpleRequest).OwnerIsNotResident);
        }

        protected override Country GetCurrentCountry(SimpleRequest request)
        {
            return request.OwnerCountry;
        }
    }
}