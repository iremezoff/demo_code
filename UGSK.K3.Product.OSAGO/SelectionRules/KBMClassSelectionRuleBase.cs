using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public abstract class KBMClassSelectionRuleBase : ISelectionRule<IRequest>
    {
        private readonly ISpecification<SimpleRequest> _spec;
        public bool NeedStateChanging { get { return false; } }
        public int Priority { get; set; }


        protected KBMClassSelectionRuleBase(ISpecification<SimpleRequest> spec)
        {
            _spec = spec;
        }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            return Enumerable.Empty<SimpleEntity>();
        }

        public virtual bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedValues)
        {
            var fullRequest = request as SimpleRequest;
            return !_spec.IsSatisfiedBy(fullRequest);
        }
    }
}