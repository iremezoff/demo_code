﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.SelectionRules;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class PaymentIsCashSelectionRule : BasePaymentIsCashSelectionRule
    {
        protected override PaymentMethodCode PaymentCode(IRequest request)
        {
            return (request as SimpleRequest).PaymentKind.PaymentMethod.PaymentMethodCode;
        }
    }
}