﻿using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class DerivingGuidFromOwnerAddressSelectionRule : ISelectionRule<IRequest>
    {
        public bool NeedStateChanging
        {
            get { return false; }
        }

        public int Priority { get; set; }

        public DerivingGuidFromOwnerAddressSelectionRule()
        {
        }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            return new Dictionary<SimpleEntity, bool>();
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedEntities)
        {
            return true;
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            var nullValue = Enumerable.Empty<SimpleEntity>();

            var sRequest = request as SimpleRequest;
            if (sRequest == null)
            {
                return nullValue;
            }
            IEnumerable<SimpleEntity> result = nullValue;
            if (sRequest.OwnerCommonRealAddress != null && sRequest.OwnerCommonRealAddress.Aoguid.HasValue)
            {
                result = new[] { new GenericSimpleEntity<string>() { 
                    Value = sRequest.OwnerCommonRealAddress.Aoguid.Value.ToString()
                } };
            }
            return result;
        }
    }
}
