using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications.BaseSpecs;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class WithTrailerSelectionRule : ISelectionRule<IRequest>
    {
        readonly IsOwnerPhysicalPersonSpecification _isOwnerPhysicalPersonSpecification;
        readonly IsPassengerCarSpecification _isPassengerCarSpecification;

        public WithTrailerSelectionRule(IsOwnerPhysicalPersonSpecification isOwnerPhysicalPersonSpecification, IsPassengerCarSpecification isPassengerCarSpecification)
        {
            _isOwnerPhysicalPersonSpecification = isOwnerPhysicalPersonSpecification;
            _isPassengerCarSpecification = isPassengerCarSpecification;
        }

        public bool NeedStateChanging
        {
            get { return true; }
        }

        public int Priority { get; set; }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            return new Dictionary<SimpleEntity, bool>();
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedEntities)
        {
            return GetGeneralDenyState(request);
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            if (GetGeneralDenyState(request))
            {
                return new[] { new GenericSimpleEntity<bool>() { Value = false } };
            }
            else
            {
                return new[] { new GenericSimpleEntity<bool>() { Value = (request as SimpleRequest).WithTrailer } };
            }
        }

        private bool GetGeneralDenyState(IRequest simpleRequest)
        {
            var nullValue = Enumerable.Empty<SimpleEntity>();
            var r = simpleRequest as SimpleRequest;
            if (r == null || r.VehicleModel == null || r.OwnerContractorType == null)
            {
                return true;
            }
            return _isOwnerPhysicalPersonSpecification.IsSatisfiedBy(r) && _isPassengerCarSpecification.IsSatisfiedBy(r);
        }
    }
}
