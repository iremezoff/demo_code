using UGSK.K3.Product.OSAGO.Specifications;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class AgeExperienceSelectionRule : KBMClassSelectionRuleBase
    {
        public AgeExperienceSelectionRule(IsKvsEnabledSpecification kvsSpec)
            : base(kvsSpec)
        {
        }
    }
}