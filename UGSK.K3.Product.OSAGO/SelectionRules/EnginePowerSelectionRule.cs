using UGSK.K3.Product.OSAGO.Specifications;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class EnginePowerSelectionRule : KBMClassSelectionRuleBase
    {
        public EnginePowerSelectionRule(IsKmEnabledSpecification kvsSpec)
            : base(kvsSpec)
        {
        }
    }
}