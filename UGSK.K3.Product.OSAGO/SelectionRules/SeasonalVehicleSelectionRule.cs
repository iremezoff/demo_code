﻿using System.Collections.Generic;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications.BaseSpecs;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class SeasonalVehicleSelectionRule : ISelectionRule<IRequest>
    {
        readonly IsOwnerPhysicalPersonSpecification _isOwnerPhysicalPersonSpecification;

        public SeasonalVehicleSelectionRule(IsOwnerPhysicalPersonSpecification isOwnerPhysicalPersonSpecification)
        {
            _isOwnerPhysicalPersonSpecification = isOwnerPhysicalPersonSpecification;
        }

        public bool NeedStateChanging
        {
            get { return false; }
        }

        public int Priority { get; set; }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            return new Dictionary<SimpleEntity, bool>();
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedEntities)
        {
            return GetGeneralDenyState(request);
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            return new[] { new GenericSimpleEntity<bool>() 
            {
                Value = (GetGeneralDenyState(request))? false:(request as SimpleRequest).IsSeason  
            }};
        }

        private bool GetGeneralDenyState(IRequest simpleRequest)
        {
            var r = simpleRequest as SimpleRequest;
            if (r == null || r.OwnerContractorType == null)
            {
                return true;
            }
            return _isOwnerPhysicalPersonSpecification.IsSatisfiedBy(r);
        }
    }
}
