using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Infrastructure;
using UGSK.K3.Product.Contract;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class CollectionOutsideVehicleModelSelectionRule : ISelectionRule<IRequest>
    {
        private readonly Func<IRepositoryWithTypedId<VehicleModel, int>> _repoFunc;
        public bool NeedStateChanging { get; private set; }
        public int Priority { get; set; }

        public CollectionOutsideVehicleModelSelectionRule(Func<IRepositoryWithTypedId<VehicleModel, int>> repoFunc)
        {
            _repoFunc = repoFunc;
        }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            throw new NotImplementedException();
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> currentValue)
        {
            return false;
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            var osagoRequest = request as SimpleRequest;
            if (osagoRequest != null && osagoRequest.VehicleModel != null)
            {
                var model = _repoFunc().GetById(osagoRequest.VehicleModel.Id);
                if (model != null)
                {
                    return new[] { new GenericSimpleEntity<VehicleModel>(model) };
                }
            }
            return Enumerable.Empty<SimpleEntity>();
        }
    }
}