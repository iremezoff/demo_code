using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class DriverRestrictionSelectionRule : ISelectionRule<IRequest>
    {

        IsKoEnabledSpecification _koEnabledSpec;
        public DriverRestrictionSelectionRule(IsKoEnabledSpecification koEnabledSpec)
        {
            _koEnabledSpec = koEnabledSpec;
        }

        public bool NeedStateChanging
        {
            get { return false; }
        }

        public int Priority { get; set; }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            throw new NotImplementedException();
        }

        public virtual bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedValues)
        {
            var fullRequest = request as SimpleRequest;
            return !_koEnabledSpec.IsSatisfiedBy(fullRequest);
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            if (_koEnabledSpec.IsSatisfiedBy(request as SimpleRequest))
                return new[] { sourceCollection.First(r => r.Name.Equals("��", StringComparison.InvariantCultureIgnoreCase)) };

            return new[] { sourceCollection.First(r => r.Name.Equals("���",StringComparison.InvariantCultureIgnoreCase)) };
        }
    }
}