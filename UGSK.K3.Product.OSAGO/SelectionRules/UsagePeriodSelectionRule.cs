using UGSK.K3.Product.OSAGO.Specifications;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class UsagePeriodSelectionRule : KBMClassSelectionRuleBase
    {
        public UsagePeriodSelectionRule(IsKsEnabledSpecification kvsSpec)
            : base(kvsSpec)
        {
        }
    }
}