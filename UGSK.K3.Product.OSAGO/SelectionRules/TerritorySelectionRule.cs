using UGSK.K3.Product.OSAGO.Specifications;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class TerritorySelectionRule : KBMClassSelectionRuleBase
    {
        public TerritorySelectionRule(IsKtEnabledSpecification ktSpec)
            : base(ktSpec)
        {
        }
    }
}