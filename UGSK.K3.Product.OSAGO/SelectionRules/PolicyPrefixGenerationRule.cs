﻿using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Product.Contract;
using UGSK.K3.Service.Model.Helpers;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public class PolicyPrefixGenerationRule : ISelectionRule<IRequest>
    {
        public bool NeedStateChanging
        {
            get { return false; }
        }

        public int Priority { get; set; }


        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            return new Dictionary<SimpleEntity, bool>();
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedEntities)
        {
            return true; // всегда не позволять изменять это поле
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            var nullValue = Enumerable.Empty<SimpleEntity>();
            var sRequest = request as SimpleRequest;
            if (sRequest == null)
            {
                return nullValue;
            }
            
            return new[] { new GenericSimpleEntity<string>() { Value = PrefixGeneratorHelper.GetPrefix("00", sRequest.SigningDate ?? DateTimeOffset.Now) } };
        }
    }
}
