using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Product.Contract;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.SelectionRules
{
    public abstract class CityOnFilialSelectionRuleBase : UndenierSelectionRule
    {
        protected readonly Func<IEmployeeGetter> _employeeGetterFactory;

        public CityOnFilialSelectionRuleBase(Func<IEmployeeGetter> employeeGetterFactory)
        {
            _employeeGetterFactory = employeeGetterFactory;
        }

        protected abstract bool IsThereNeedToSelect(IEnumerable<SimpleEntity> cities, IRequest request);

        public override IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> cities, IRequest request)
        {
            if (!IsThereNeedToSelect(cities, request))
            {
                return Enumerable.Empty<SimpleEntity>();
            }

            var employeeFilial = _employeeGetterFactory().GetEmployee().Filial;

            var found = cities.Where(r => r.Name.Substring(0, Math.Min(5, r.Name.Length)).Equals(employeeFilial.Name.Substring(0, Math.Min(5, employeeFilial.Name.Length))));
            if (found.Any())
            {
                return found.Take(1); // ����� �� ����, ���� �� ������
            }
            if (cities.Any())
            {
                return new[] { cities.LastOrDefault() }; // ���������. ��� ��� ������� "������ ������ � ���.������"
            }
            return Enumerable.Empty<SimpleEntity>(); // ������ ��� ������
        }
    }
}