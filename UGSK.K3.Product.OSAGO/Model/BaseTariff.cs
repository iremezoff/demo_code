//using UGSK.K3.Product.OSAGO.Model;

using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class BaseTariff : SimpleEntity
    {
        public int VehicleTypeId { get; set; }
        public virtual VehicleTypeBase VehicleType { get; set; }

        
        public bool IsTaxi { get; set; }
        public int PersonTypeId { get; set; }
        public virtual PersonType PersonType { get; set; }
        public decimal Value { get; set; }
        public string FullCodeFias { get; set; }
    }
}

