﻿using System;
using UGSK.K3.Infrastructure;

namespace UGSK.K3.Product.OSAGO.Model
{
    public enum PurposeEnum : int
    {        
        Personal = 1, // Личная        
        Taxi = 2, // Транспортное средство используется в такси        
        Study = 3, // Учебная езда        
        Service = 4, // Дорожные и специальные транспортные средства        
        Other = 5, // Прочее
        RouteTaxi = 6, // Регулярные пассажирские перевозки/перевозки пассажиров по заказам        
        TransportOfDangerousGoods = 7, // Перевозка опасных и легко воспламеняющихся грузов        
        Rental = 8, // Прокат/краткосрочная аренда        
        EmergencyАndUtilities = 9, // Экстренные и коммунальные службы        
    }

    public class UsagePurpose : Entity<int>, IComparable<UsagePurpose>
    {
        public string Name { get; set; }
        public PurposeEnum Code { get; set; }

        public int CompareTo(UsagePurpose other)
        {
            return other.Id.CompareTo(Id);
        }
    }

}
