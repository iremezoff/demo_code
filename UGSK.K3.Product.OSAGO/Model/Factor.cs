namespace UGSK.K3.Product.OSAGO.Model
{
    using System;
    using System.Xml.Linq;
    using Contract;

    // урать этот коэффициент, т.к. не используется
    public class Factor : SimpleEntity
    {
        public String XmlContent { get; set; }

        public XElement FactorContent
        {
            get
            {
                return XElement.Parse(XmlContent);
            }
        }
    }
}
