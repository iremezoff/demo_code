namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class Violation : SimpleEntity
    {        
        public bool HasViolation { get; set; }
        public decimal Value { get; set; }
        
    }
}
