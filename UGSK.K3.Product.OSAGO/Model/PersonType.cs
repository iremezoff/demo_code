﻿using UGSK.K3.Product.Contract;

namespace UGSK.K3.Product.OSAGO.Model
{
    // todo Ремезов: это перечисление должно быть синхронизовано с аналогичным в namespace UGSK.K3.Product.OSAGO.Model
    public enum EPersonType : int
    {
        Artifical = 0,
        Private = 1
    }
    public class PersonType : SimpleEntity
    {
        public EPersonType Type { get; set; }

        public bool IsKoEnabled { get; set; }
        public bool IsKvsEnabled { get; set; }
    }
    }
