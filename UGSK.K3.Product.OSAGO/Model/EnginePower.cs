namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class EnginePower : SimpleEntity
    {
        public decimal Value { get; set; }

        public int From { get; set; }
        public int To { get; set; }
    }
}
