namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class Region : SimpleEntity
    {        
        public string CodeFias { get; set; }
    }
}
