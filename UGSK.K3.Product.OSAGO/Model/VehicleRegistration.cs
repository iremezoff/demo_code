namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class VehicleRegistration : SimpleEntity
    {        
        public bool IsKtEnabled { get; set; }
        public bool IsKbmEnabled { get; set; }
        public bool IsKvsEnabled { get; set; }
        public bool IsKpEnabled { get; set; }
        public bool IsKnEnabled { get; set; }
        public bool IsKsEnabled { get; set; }

    }
}
