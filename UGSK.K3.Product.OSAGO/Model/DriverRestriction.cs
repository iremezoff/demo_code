namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    // v
    public class DriverRestriction : SimpleEntity
    {        
        public bool HasLimit { get; set; }
        public decimal Value { get; set; }
    }
}
