namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class KBMTransition : SimpleEntity
    {
        public int KbmClassId { get; set; }
        public virtual KBMClass KbmClass { get; set; }
        public int PayoutCountId { get; set; }
        public virtual PayoutCount PayoutCount { get; set; }
        public int NextKbmClassId { get; set; }
        public virtual KBMClass NextKbmClass { get; set; }

    }
}
