﻿using UGSK.K3.Product.Contract;

namespace UGSK.K3.Product.OSAGO.Model
{
    public class TrailerFactor : SimpleEntity
    {
        public int? VehicleTypeId { get; set; }
        public virtual Service.Model.VehicleTypeBase VehicleType { get; set; }

        public int? VehicleSubTypeId { get; set; }

        public int? PersonTypeId { get; set; }
        public virtual PersonType PersonType { get; set; }
        public decimal Value { get; set; }
    }
}
