namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;
    
    public class AgeExperience : SimpleEntity
    {
        public int AgeFrom { get; set; }
        public int AgeTo { get; set; }
        public int ExperienceFrom { get; set; }
        public int ExperienceTo { get; set; }
        public decimal Value { get; set; }           
    }
}
