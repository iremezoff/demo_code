namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class KBMClass : SimpleEntity
    {        
        public bool IsBMPEnabled { get; set; }
        public bool IsDefault { get; set; } // �������� �� ��������?
        public decimal Value { get; set; }
    }
}
