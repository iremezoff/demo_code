namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class City : SimpleEntity
    {        
        public int RegionId { get; set; }
        public virtual Region Region { get; set; }        
        public decimal KT1 { get; set; }        
        public decimal KT2 { get; set; }
        public string FullCodeFias { get; set; }
    }
}
