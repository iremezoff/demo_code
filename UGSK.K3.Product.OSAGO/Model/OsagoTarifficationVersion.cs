﻿using System;

namespace UGSK.K3.Product.OSAGO.Model
{
    public class OsagoTarifficationVersion
    {
        public int Id { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public int UnderwriterVersion { get; set; }
        public int MarketingVersion { get; set; }
        public int TechVersion { get; set; }
        public int FormulaVersion { get; set; }
    }
}
