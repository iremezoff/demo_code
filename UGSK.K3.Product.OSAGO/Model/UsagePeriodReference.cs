namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class UsagePeriodReference : SimpleEntity
    {
        public int PersonTypeId { get; set; }
        public int Months { get; set; }
        public PersonType PersonType { get; set; }
        public bool IsSeason { get; set; } // проблема!
        public decimal Value { get; set; }
    }
}
