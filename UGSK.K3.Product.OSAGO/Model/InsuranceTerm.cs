namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class InsuranceTerm : SimpleEntity
    {        
        public int RegistrationId { get; set; }
        public virtual VehicleRegistration Registration { get; set; }
        public decimal Value { get; set; }
    }
}
