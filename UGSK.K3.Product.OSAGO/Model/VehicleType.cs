namespace UGSK.K3.Product.OSAGO.Model
{
    using Contract;

    public class VehicleType : SimpleEntity
    {
        public VehicleType()
        {
          //  Tariffs = new List<BaseTariff>();
        }
        
        public bool IsKmEnabled { get; set; }
        public bool IsKvsEnabled { get; set; }
        public bool IsKoEnabled { get; set; }
        public bool IsKbmEnabled { get; set; }
        public bool IsKnEnabled { get; set; }
        public bool IsSeason { get; set; }
        //public virtual ICollection<BaseTariff> Tariffs { get; set; }
    }
}
