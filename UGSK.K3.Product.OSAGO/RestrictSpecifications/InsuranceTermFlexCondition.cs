﻿using System;
using System.Linq;
using System.Linq.Expressions;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Model;

namespace UGSK.K3.Product.OSAGO.RestrictSpecifications
{
    public class InsuranceTermFlexCondition : IFlexCondition<SimpleRequest, InsuranceTerm>
    {
        private readonly IOSAGODbSession _dbSession;

        public InsuranceTermFlexCondition(ICalculator<SimpleRequest> calculator)
        {
            _dbSession = calculator.DbSession as IOSAGODbSession;
        }

        public Expression<Func<InsuranceTerm, bool>> IsSatisfiedBy(SimpleRequest request)
        {
            var registration = _dbSession.VehicleRegistrations.FirstOrDefault(r => r.Name.Equals(request.VehicleRegistration));
            if (registration != null)
                return term => term.RegistrationId == registration.Id;
            return term => false;
        }
    }
}