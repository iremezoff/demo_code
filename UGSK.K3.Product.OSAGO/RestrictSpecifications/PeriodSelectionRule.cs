﻿using System;
using System.Linq;
using System.Linq.Expressions;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Model;

namespace UGSK.K3.Product.OSAGO.RestrictSpecifications
{
    public class PeriodSelectionRule : IFlexCondition<SimpleRequest, UsagePeriodReference>
    {
        private readonly IOSAGODbSession _dbSession;

        public PeriodSelectionRule(ICalculator<SimpleRequest> calculator)
        {
            _dbSession = calculator.DbSession as IOSAGODbSession;
        }

        public Expression<Func<UsagePeriodReference, bool>> IsSatisfiedBy(SimpleRequest request)
        {
            var vehicleType = request.VehicleModel == null ? null : request.VehicleModel.VehicleType.Name;
            var ownerPersonType = _dbSession.PersonTypes.FirstOrDefault(r => r.Name.Equals(request.OwnerContractorType));

            if (vehicleType != null && ownerPersonType != null)
            {
                return period => period.IsSeason == request.IsSeason && period.PersonTypeId == ownerPersonType.Id;
            }

            return period => false;
        }
    }
}
