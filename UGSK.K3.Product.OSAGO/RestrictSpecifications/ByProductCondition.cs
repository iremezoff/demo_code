﻿using System;
using System.Linq.Expressions;
using UGSK.K3.Product.Contract;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.RestrictSpecifications
{
    public class ByProductCondition : IFlexCondition<SimpleRequest, GenericSimpleEntity<PolicySerial>>
    {
        public Expression<Func<GenericSimpleEntity<PolicySerial>, bool>> IsSatisfiedBy(SimpleRequest request)
        {
            return v => v.Value.ProductId == OsagoInsuranceProduct.ProductId;
        }
    }
}
