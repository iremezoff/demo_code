﻿using System;
using System.Linq.Expressions;
using UGSK.K3.Product.Contract;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.RestrictSpecifications
{
    public class ActualSerialFlexCondition : IFlexCondition<IRequest, GenericSimpleEntity<PolicySerial>>
    {
        public Expression<Func<GenericSimpleEntity<PolicySerial>, bool>> IsSatisfiedBy(IRequest request)
        {
            int isDraft = (int)OsagoContractStatusCode.Draft;
            var osagoRequest = ((SimpleRequest)request);
            if (osagoRequest.StatusId == isDraft || osagoRequest.StatusId == default(int))
            {
                return x=>!x.Value.IsDeleted;
            }
            return x => !x.Value.IsDeleted || x.Value.IsDeleted && x.Value.Value.Equals(osagoRequest.PolicySerial.Value);
        }
    }
}