﻿using System;
using System.Linq.Expressions;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.RestrictSpecifications;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.RestrictSpecifications
{
    public class OwnerDocumentTypeFlexCondition : ContractorDocumentTypeFlexCondition<SimpleRequest>
    {        
        public override Expression<Func<GenericSimpleEntity<ContractorDocumentType>, bool>> IsSatisfiedBy(SimpleRequest request)
        {
            if (request.OwnerContractorType.StartsWith("физ", StringComparison.InvariantCultureIgnoreCase))
                return document => document.Value.Id == LegalPersonDocumentId;
            else
                return document => document.Value.Id != LegalPersonDocumentId;
        }
    }
}
