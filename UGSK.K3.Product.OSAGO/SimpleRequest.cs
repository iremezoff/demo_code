﻿using System;
using System.ComponentModel.DataAnnotations;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;
using UGSK.K3.Product.Contract.SelectionRules;
using UGSK.K3.Product.Contract.SharedConverters;
using UGSK.K3.Product.OSAGO.Model;
using UGSK.K3.Product.OSAGO.RestrictSpecifications;
using UGSK.K3.Product.OSAGO.SelectionRules;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO
{
    public class SimpleRequest : IRequest
    {
        [NonField]
        public int StatusId { get; set; }

        [RequestField(typeof(GenericSimpleEntity<bool>), ValuePropertyName = "Value")]
        [FlexibleField(typeof(EverTrueCondition), ObservedFields = new[] { "VehicleModel", "OwnerContractorType" })]
        [FieldSelector(typeof(WithTrailerSelectionRule), 10)]
        public bool WithTrailer { get; set; }

        [NonField]
        public DateTimeOffset ContractFrom { get; set; }

        [RequestField(typeof(UsagePeriodReference), DoNotRequireGroup = true)]
        [FlexibleField(typeof(PeriodSelectionRule), ObservedFields = new[] { "VehicleModel", "OwnerContractorType", "VehicleRegistration", "IsSeason" })]
        [FieldSelector(typeof(UsagePeriodSelectionRule), 10)]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public string UsagePeriod { get; set; }

        [RequestField(typeof(GenericSimpleEntity<bool>), ValuePropertyName = "Value")]
        [FlexibleField(typeof(EverTrueCondition), ObservedFields = new[] { "OwnerContractorType" })]
        [FieldSelector(typeof(SeasonalVehicleSelectionRule), 10)]
        public bool IsSeason { get; set; }

        [RequestField(typeof(DriverRestriction))]
        [FlexibleField(typeof(EverTrueCondition), ObservedFields = new[] { "VehicleModel", "OwnerContractorType", "VehicleRegistration" })]
        [FieldSelector(typeof(DriverRestrictionSelectionRule), 10)]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public string DriverRestriction { get; set; }

        [RequestField(typeof(AgeExperience))]
        [FlexibleField(typeof(EverTrueCondition), ObservedFields = new[] { "VehicleModel", "InsuredContractorType", "VehicleRegistration" })]
        [FieldSelector(typeof(AgeExperienceSelectionRule), 10)]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public string AgeExperience { get; set; }

        [RequestField(typeof(GenericSimpleEntity<ContractorDocumentType>), ValuePropertyName = "Value", Converter = typeof(ContractorDocumentTypeFieldValueConverter))]
        [FlexibleField(typeof(InsuredDocumentTypeFlexCondition), ObservedFields = new[] { "InsuredContractorType" })]
        [FieldSelector(typeof(ContractorDocumentTypeSelectionRule), 10)]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public ContractorDocumentType InsuredDocumentType { get; set; }

        [Required(ErrorMessage = "Не указан тип страхователя")]
        [RequestField(typeof(PersonType))]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public string InsuredContractorType { get; set; }

        #region addresses

        [RequestField(typeof(GenericSimpleEntity<string>), ValuePropertyName = "Value")]
        [FlexibleField(typeof(EverTrueCondition), ObservedFields = new[] { "InsuredCommonRealAddress" })]
        [FieldSelector(typeof(DerivingGuidFromInsuredAddressSelectionRule), 3)]
        public string InsuredCommonRealAddressFiasId { get; set; }

        [RequestField(typeof(GenericSimpleEntity<string>), ValuePropertyName = "Value")]
        [FlexibleField(typeof(EverTrueCondition), ObservedFields = new[] { "OwnerCommonRealAddress" })]
        [FieldSelector(typeof(DerivingGuidFromOwnerAddressSelectionRule), 3)]
        public string OwnerCommonRealAddressFiasId { get; set; }

        [RequestField(typeof(GenericSimpleEntity<Country>), ValuePropertyName = "Value", Converter = typeof(CountryFieldValueConverter), SortColumn = "SortOrder")]
        [FieldSelector(typeof(InsuredRussiaIsOurNativelandSelectionRule), 10)]
        public Country InsuredCountry { get; set; }

        [NonField]
        public bool InsuredIsNotResident { get; set; }

        [RequestField(typeof(GenericSimpleEntity<FiasAddress>), ValuePropertyName = "Value", DoNotDerive = true, Converter = typeof(FiasAddressFieldValueConverter))]
        public FiasAddress InsuredCommonRealAddress { get; set; }

        #endregion

        [FlexibleField(typeof(OwnerDocumentTypeFlexCondition), ObservedFields = new[] { "OwnerContractorType" })]
        [RequestField(typeof(GenericSimpleEntity<ContractorDocumentType>), ValuePropertyName = "Value", Converter = typeof(ContractorDocumentTypeFieldValueConverter))]
        [FieldSelector(typeof(ContractorDocumentTypeSelectionRule), 10)]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public ContractorDocumentType OwnerDocumentType { get; set; }

        [Required(ErrorMessage = "Не указан тип собственника")]
        [RequestField(typeof(PersonType))]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public string OwnerContractorType { get; set; }

        [RequestField(typeof(GenericSimpleEntity<string>), ValuePropertyName = "Value")]
        [FieldSelector(typeof(PolicyPrefixGenerationRule), 10)]
        public string PolicyPrefix { get; set; }

        [NonField]
        public DateTimeOffset? SigningDate { get; set; }

        [RequestField(typeof(GenericSimpleEntity<PolicySerial>), ValuePropertyName = "Value", Converter = typeof(PolicySerialValueConverter))]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        [FlexibleField(typeof(ByProductCondition))]
        [FlexibleField(typeof(ActualSerialFlexCondition))]
        public PolicySerial PolicySerial { get; set; }

        [Required(ErrorMessage = "Не указана страна собственника")]
        [RequestField(typeof(GenericSimpleEntity<Country>), ValuePropertyName = "Value", Converter = typeof(CountryFieldValueConverter), SortColumn = "SortOrder")]
        [FieldSelector(typeof(OwnerRussiaIsOurNativelandSelectionRule), 10)]
        public Country OwnerCountry { get; set; }

        [NonField]
        public bool OwnerIsNotResident { get; set; }

        //Переименовал, чтобы при валидации на форме подсвечивалось поле с таким же именем
        [RequestField(typeof(GenericSimpleEntity<FiasAddress>), ValuePropertyName = "Value", DoNotDerive = true, Converter = typeof(FiasAddressFieldValueConverter))]
        public FiasAddress OwnerCommonRealAddress { get; set; }

        [NonField]
        public string OwnerDocumentNumber { get; set; }

        [RequestField(typeof(Violation))]
        [FlexibleField(typeof(EverTrueCondition), ObservedFields = new[] { "VehicleModel", "VehicleRegistration" })]
        [FieldSelector(typeof(ViolationSelectionRule), 10)]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public string AreViolationsExist { get; set; }

        [RequestField(typeof(InsuranceTerm))]
        [FlexibleField(typeof(InsuranceTermFlexCondition), ObservedFields = new[] { "VehicleModel", "InsuredContractorType", "VehicleRegistration" })]
        [FieldSelector(typeof(TermSelectionRule), 10)]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public string InsuranceTerm { get; set; }

        [RequestField(typeof(GenericSimpleEntity<VehicleModel>), ValuePropertyName = "Value", DoNotDerive = true, Converter = typeof(VehicleModelFieldValueConverter))]
        [FieldSelector(typeof(CollectionOutsideVehicleModelSelectionRule), 10)]
        public VehicleModel VehicleModel { get; set; }

        [Required(ErrorMessage = "Не указана цель использования")]
        [RequestField(typeof(GenericSimpleEntity<UsagePurpose>), ValuePropertyName = "Value", Converter = typeof(UsagePurposeFieldValueConverter))]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public UsagePurpose UsagePurpose { get; set; }
        

        [RequestField(typeof(PayoutCount))]
        public int PayoutCount { get; set; }

        [RequestField(typeof(GenericSimpleEntity<VehicleDocumentType>), ValuePropertyName = "Value", DoNotRequireGroup = true, Converter = typeof(VehicleDocumentTypeFieldValueConverter))]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public VehicleDocumentType VehicleDocumentType { get; set; }

        [Required(ErrorMessage = "Не указана регистрация транспортного средства")]
        [RequestField(typeof(VehicleRegistration))]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public string VehicleRegistration { get; set; }

        [RequestField(typeof(GenericSimpleEntity<VehicleCheckupDocumentType>), ValuePropertyName = "Value", DoNotRequireGroup = true, Converter = typeof(VehicleCheckupDocTypeFieldValueConverter))]
        [FieldSelector(typeof(FirstHavingSelectionRule), 3)]
        public VehicleCheckupDocumentType VehicleCheckupDocumentType { get; set; }

        [RequestField(typeof(GenericSimpleEntity<Guid>), ValuePropertyName = "Value")]
        [FieldSelector(typeof(InsuredNewGuidSelectionRule), 10)]
        public Guid InsuredGuid { get; set; }

        [RequestField(typeof(GenericSimpleEntity<Guid>), ValuePropertyName = "Value")]
        [FieldSelector(typeof(OwnerNewGuidSelectionRule), 10)]
        public Guid OwnerGuid { get; set; }

        [RequestField(typeof(GenericSimpleEntity<VehicleBrand>), ValuePropertyName = "Value", Converter = typeof(VehicleModelBaseValueConverter))]
        public VehicleBrand VehicleModelSpelt { get; set; }

        #region документы оплаты

        [RequestField(typeof(GenericSimpleEntity<bool>), ValuePropertyName = "Value")]
        [FlexibleField(typeof(EverTrueCondition), ObservedFields = new[] { "PaymentKind" })]
        [FieldSelector(typeof(PaymentIsCashSelectionRule), 3)]
        public bool IsCash { get; set; }

        [RequestField(typeof(GenericSimpleEntity<PaymentKind>), ValuePropertyName = "Value", Converter = typeof(PaymentKindFieldValueConverter))]
        [FieldSelector(typeof(FirstHavingSelectionRule), 10)]
        public PaymentKind PaymentKind { get; set; }

        #endregion
    }

    public static class SimpleRequestExtensions
    {
        public static bool DriverListUnresticted(this SimpleRequest simpleRequest)
        {
            return !string.IsNullOrEmpty(simpleRequest.DriverRestriction)
                && simpleRequest.DriverRestriction.IndexOf("нет", StringComparison.InvariantCultureIgnoreCase) > -1;
        }

        public static bool IsLegalPerson(this SimpleRequest simpleRequest)
        {
            return simpleRequest.OwnerContractorType != null && simpleRequest.OwnerContractorType.StartsWith("юр", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
