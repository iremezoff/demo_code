﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications.BaseSpecs;

namespace UGSK.K3.Product.OSAGO.Specifications
{
    public class IsKbmEnabledSpecification : ISpecification<SimpleRequest>
    {
        readonly RegisteredInRussiaSpecification _registeredInRussiaSpecification;
        readonly ForeignRegisteredSpecification _foreignRegisteredSpecification;

        public IsKbmEnabledSpecification(RegisteredInRussiaSpecification registeredInRussiaSpecification, ForeignRegisteredSpecification foreignRegisteredSpecification)
        {
            _registeredInRussiaSpecification = registeredInRussiaSpecification;
            _foreignRegisteredSpecification = foreignRegisteredSpecification;
        }
        public bool IsSatisfiedBy(SimpleRequest request)
        {
            return _registeredInRussiaSpecification.IsSatisfiedBy(request) || _foreignRegisteredSpecification.IsSatisfiedBy(request);
        }
    }
}