﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications.BaseSpecs;

namespace UGSK.K3.Product.OSAGO.Specifications
{
    public class IsKmEnabledSpecification : ISpecification<SimpleRequest>
    {
        private readonly IsPassengerCarSpecification _isPassengerCarSpecification;

        public IsKmEnabledSpecification(IsPassengerCarSpecification isPassengerCarSpecification)
        {
            _isPassengerCarSpecification = isPassengerCarSpecification;
        }

        public bool IsSatisfiedBy(SimpleRequest request)
        {
            return _isPassengerCarSpecification.IsSatisfiedBy(request);
        }
    }
}