﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications.BaseSpecs;

namespace UGSK.K3.Product.OSAGO.Specifications
{
    public class IsKprEnabledSpecification : ISpecification<SimpleRequest>
    {
        readonly IsPassengerCarSpecification _isPassengerCarSpecification;
        readonly IsPhysicalPersonSpecification _isPhysicalPersonSpecification;

        public IsKprEnabledSpecification(IsPassengerCarSpecification isPassengerCarSpecification, IsPhysicalPersonSpecification isPhysicalPersonSpecification)
        {
            _isPassengerCarSpecification = isPassengerCarSpecification;
            _isPhysicalPersonSpecification = isPhysicalPersonSpecification;
        }

        public bool IsSatisfiedBy(SimpleRequest request)
        {
            return !(_isPassengerCarSpecification.IsSatisfiedBy(request) &&
                _isPhysicalPersonSpecification.IsSatisfiedBy(request)) && 
                request.WithTrailer;
        }
    }
}
