﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications.BaseSpecs;

namespace UGSK.K3.Product.OSAGO.Specifications
{
    public class IsKpEnabledSpecification : ISpecification<SimpleRequest>
    {
        private readonly FollowedToRegistrationSpecification _followedToRegistrationSpecification;
        private readonly ForeignRegisteredSpecification _foreignRegisteredSpecification;

        public IsKpEnabledSpecification(FollowedToRegistrationSpecification followedToRegistrationSpecification, ForeignRegisteredSpecification foreignRegisteredSpecification)
        {
            _followedToRegistrationSpecification = followedToRegistrationSpecification;
            _foreignRegisteredSpecification = foreignRegisteredSpecification;
        }

        public bool IsSatisfiedBy(SimpleRequest request)
        {
            return _followedToRegistrationSpecification.IsSatisfiedBy(request) || _foreignRegisteredSpecification.IsSatisfiedBy(request);
        }
    }
}