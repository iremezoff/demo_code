﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications.BaseSpecs;

namespace UGSK.K3.Product.OSAGO.Specifications
{
    public class IsKnEnabledSpecification : ISpecification<SimpleRequest>
    {
        private readonly ForeignRegisteredSpecification _foreignRegisteredSpecification;
        private readonly RegisteredInRussiaSpecification _registeredInRussiaSpecification;

        public IsKnEnabledSpecification(ForeignRegisteredSpecification foreignRegisteredSpecification, RegisteredInRussiaSpecification registeredInRussiaSpecification)
        {
            _foreignRegisteredSpecification = foreignRegisteredSpecification;
            _registeredInRussiaSpecification = registeredInRussiaSpecification;
        }

        public bool IsSatisfiedBy(SimpleRequest request)
        {
            return _registeredInRussiaSpecification.IsSatisfiedBy(request) || _foreignRegisteredSpecification.IsSatisfiedBy(request);
        }
    }
}