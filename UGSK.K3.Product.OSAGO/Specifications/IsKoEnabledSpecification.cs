﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications.BaseSpecs;

namespace UGSK.K3.Product.OSAGO.Specifications
{
    public class IsKoEnabledSpecification : ISpecification<SimpleRequest>
    {
        readonly IsOwnerPhysicalPersonSpecification _isOwnerPhysicalPersonSpecification;
        readonly ForeignRegisteredSpecification _foreignRegisteredSpecification;

        public IsKoEnabledSpecification(IsOwnerPhysicalPersonSpecification isOwnerPhysicalPersonSpecification, 
            ForeignRegisteredSpecification foreignRegisteredSpecification)
        {
            _foreignRegisteredSpecification = foreignRegisteredSpecification;
            _isOwnerPhysicalPersonSpecification = isOwnerPhysicalPersonSpecification;
        }

        public bool IsSatisfiedBy(SimpleRequest request)
        {
            return _isOwnerPhysicalPersonSpecification.IsSatisfiedBy(request) &&
                !_foreignRegisteredSpecification.IsSatisfiedBy(request);
        }
    }
}