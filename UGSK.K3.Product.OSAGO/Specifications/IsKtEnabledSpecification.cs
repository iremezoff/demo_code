﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications.BaseSpecs;

namespace UGSK.K3.Product.OSAGO.Specifications
{
    public class IsKtEnabledSpecification : ISpecification<SimpleRequest>
    {
        readonly RegisteredInRussiaSpecification _registeredInRussiaSpecification;
        
        public IsKtEnabledSpecification(RegisteredInRussiaSpecification registeredInRussiaSpecification)
        {
            _registeredInRussiaSpecification = registeredInRussiaSpecification;            
        }
        public bool IsSatisfiedBy(SimpleRequest request)
        {
            return _registeredInRussiaSpecification.IsSatisfiedBy(request);
        }
    }
}