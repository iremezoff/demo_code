﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Specifications.BaseSpecs;

namespace UGSK.K3.Product.OSAGO.Specifications
{
    public class IsKvsEnabledSpecification : ISpecification<SimpleRequest>
    {
        private readonly IsOwnerPhysicalPersonSpecification _isOwnerPhysicalPersonSpecification;
        private readonly IsUnrestrictedDriversListSpeification _isUnrestrictedDriversListSpeification;

        public IsKvsEnabledSpecification(IsOwnerPhysicalPersonSpecification isOwnerPhysicalPersonSpecification,
            IsUnrestrictedDriversListSpeification isUnrestrictedDriversListSpeification)
        {
            _isOwnerPhysicalPersonSpecification = isOwnerPhysicalPersonSpecification;
            _isUnrestrictedDriversListSpeification = isUnrestrictedDriversListSpeification;
        }

        public bool IsSatisfiedBy(SimpleRequest entity)
        {
            return _isOwnerPhysicalPersonSpecification.IsSatisfiedBy(entity) &&
                !_isUnrestrictedDriversListSpeification.IsSatisfiedBy(entity);
        }
    }
}