﻿using UGSK.K3.Product.Contract;

namespace UGSK.K3.Product.OSAGO.Specifications.BaseSpecs
{
    public class IsUnrestrictedDriversListSpeification : ISpecification<SimpleRequest>
    {
        public bool IsSatisfiedBy(SimpleRequest request)
        {
            return request.DriverListUnresticted();
        }
    }
}
