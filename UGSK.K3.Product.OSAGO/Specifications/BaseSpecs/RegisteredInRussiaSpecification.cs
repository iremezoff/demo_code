﻿using System;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.Product.OSAGO.Specifications.BaseSpecs
{
    public class RegisteredInRussiaSpecification : ISpecification<SimpleRequest>
    {
        public const string RegisteredInRussia = "зарегистрированные в РФ";
        public bool IsSatisfiedBy(SimpleRequest entity)
        {
            return entity.VehicleRegistration != null && entity.VehicleRegistration.IndexOf(RegisteredInRussia, StringComparison.InvariantCultureIgnoreCase) > -1;
        }
    }
}