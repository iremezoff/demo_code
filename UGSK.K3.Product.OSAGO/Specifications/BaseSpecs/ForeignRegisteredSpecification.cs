﻿using System;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.Product.OSAGO.Specifications.BaseSpecs
{
    public class ForeignRegisteredSpecification : ISpecification<SimpleRequest>
    {
        private const string Substring = "временно используемые";
        public bool IsSatisfiedBy(SimpleRequest entity)
        {
            return entity.VehicleRegistration!=null && entity.VehicleRegistration.IndexOf(Substring, StringComparison.InvariantCultureIgnoreCase) > -1;
        }
    }
}