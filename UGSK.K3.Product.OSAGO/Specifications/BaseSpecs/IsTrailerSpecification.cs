﻿using UGSK.K3.Product.Contract;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.Specifications.BaseSpecs
{
    public class IsTrailerSpecification : ISpecification<SimpleRequest>
    {
        public bool IsSatisfiedBy(SimpleRequest entity)
        {
            return entity.VehicleModel != null && (entity.VehicleModel.VehicleType is TrailerForLorryType || entity.VehicleModel.VehicleType is TrailerForPassengerType ||
                   entity.VehicleModel.VehicleType is TrailerForTractorType);
        }
    }    

    // прежняя версия
    //public class IsTrailerSpecification : ISpecification<SimpleRequest>
    //{
    //    private const string Substring = "Прицепы";
    //    public bool IsSatisfiedBy(SimpleRequest entity)
    //    {
    //        return entity.VehicleType != null && entity.VehicleType.IndexOf(Substring, StringComparison.InvariantCultureIgnoreCase) > -1;
    //    }
    //}
}