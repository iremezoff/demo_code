﻿using System;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.Product.OSAGO.Specifications.BaseSpecs
{
    public class IsOwnerPhysicalPersonSpecification : ISpecification<SimpleRequest>
    {
        private const string Substring = "физич";

        public bool IsSatisfiedBy(SimpleRequest entity)
        {
            return entity.OwnerContractorType != null && entity.OwnerContractorType.IndexOf(Substring, StringComparison.InvariantCultureIgnoreCase) > -1;
        }
    }
}