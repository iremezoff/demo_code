using UGSK.K3.Product.Contract;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.Specifications.BaseSpecs
{
    public class IsTractorSpecification : ISpecification<SimpleRequest>
    {
        public bool IsSatisfiedBy(SimpleRequest entity)
        {
            return entity.VehicleModel != null &&
                   (entity.VehicleModel.VehicleType is TractorType ||
                    entity.VehicleModel.VehicleType is TrailerForTractorType);
        }
    }
}