using UGSK.K3.Product.Contract;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.Specifications.BaseSpecs
{
    public class IsPassengerCarSpecification : ISpecification<SimpleRequest>
    {
        public bool IsSatisfiedBy(SimpleRequest entity)
        {
            return entity.VehicleModel.VehicleType is PassengerType;
        }
    }
}