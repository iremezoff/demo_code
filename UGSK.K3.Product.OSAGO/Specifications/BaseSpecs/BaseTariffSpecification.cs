﻿using System;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.OSAGO.Model;
using UGSK.K3.Service.Model;

namespace UGSK.K3.Product.OSAGO.Specifications.BaseSpecs
{
    // todo превратить в настояющую спецификацию, котораая возвращает Expression вместо булева ответа (чтобы не прогонять 4500 записей из БД через всю проверку)
    public class BaseTariffSpecification : ISpecification<BaseTariff>
    {
        private readonly SimpleRequest _request;
        private bool _cityExists;
        private readonly ForeignRegisteredSpecification _foreignRegisteredSpec;

        public BaseTariffSpecification(SimpleRequest request, bool cityExists, ForeignRegisteredSpecification foreignRegisteredSpec)
        {
            _request = request;
            _cityExists = cityExists;
            _foreignRegisteredSpec = foreignRegisteredSpec;
        }

        public bool IsSatisfiedBy(BaseTariff entity)
        {
            // для всех случаев, когда ТС зарегистрированно в РФ, в адресе собственника должен стоять адрес регистрации ТС, а не фактический адрес собственника, при этом не важно, является он резидентом или нет
            if (!_foreignRegisteredSpec.IsSatisfiedBy(_request) && Country.RussiaIsoCode.Equals(_request.OwnerCountry.ISOCode))
            {
                //если кода фиас нету в базовых тарифах, подменяем регионом
                var codeFias = (_cityExists)
                    ? _request.OwnerCommonRealAddress.CodeFias
                    : string.Format("{0}0000000", _request.OwnerCommonRealAddress.CodeFias.Substring(0, 2));

                if (string.IsNullOrEmpty(entity.FullCodeFias) || !codeFias.StartsWith(entity.FullCodeFias))
                {
                    return false;
                }
            }
            else
            {
                // необходим коэффициент без указанного кода ФИАС
                if (!string.IsNullOrEmpty(entity.FullCodeFias))
                    return false;
            }

            if (_request.VehicleModel.VehicleType.GetType() != entity.VehicleType.GetType() ||
                _request.UsagePurpose == null ||
                entity.IsTaxi != (_request.UsagePurpose.Code == PurposeEnum.Taxi || _request.UsagePurpose.Code == PurposeEnum.RouteTaxi)
                )
            {
                return false;
            }

            if (_request.VehicleModel.VehicleType is PassengerType)
            {
                if (!entity.PersonType.Name.StartsWith(_request.OwnerContractorType, StringComparison.InvariantCultureIgnoreCase))
                {
                    return false;
                }
            }

           return true;
        }
    }
}