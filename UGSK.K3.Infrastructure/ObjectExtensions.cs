﻿using System;

namespace UGSK.K3.Infrastructure
{
    public static class ObjectExtensions
    {
        public static string TrimTypeName(this Type targetToTrim, string whatToTrim)
        {
            string targetString = targetToTrim.Name;
            int startingIndexToDelete = targetString.Contains(whatToTrim) ? targetString.Length - whatToTrim.Length : targetString.Length;
            return targetString.Remove(startingIndexToDelete, targetString.Length - startingIndexToDelete);
        }

        public static Type GetRealType(this object entity)
        {
            var type = entity.GetType();
            if (type.Namespace != null && type.Namespace.Equals(UnproxingTypesBinder.ProxyClassMarker))
            {
                return type.BaseType;
            }

            return type;
        }
    }
}
