﻿using System.Text;
using System.Threading.Tasks;

namespace UGSK.K3.Infrastructure
{
    public interface IDbSession
    {
        //IDisposable BeginTransaction(); до лучших времён

        void CommitChanges();

        Task CommitChangesAsync();

        //void CommitTransaction();  до лучших времён

        //void RollbackTransaction();
    }
}
