﻿using System.Linq;

namespace UGSK.K3.Infrastructure
{
    public interface IRepositoryWithTypedId<T, TId>
    {
        void Delete(T target);
        void Delete(TId id);
        void Save(T entity);
        void Edit(T entity);
        T GetById(TId id);
        T GetBySpecification(ISimpleSpecification<T> specification);
        IQueryable<T> GetAll();
        IQueryable<T> GetAllBySpecification(ISimpleSpecification<T> specification);
    }
}