using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UGSK.K3.Infrastructure.Events;

[assembly: InternalsVisibleTo("UGSK.K3.Infrastructure.Test")]

namespace UGSK.K3.Infrastructure
{
    public abstract class DbSessionBase : IDbSession
    {
        private readonly IEventPublisher _eventPublisher;

        protected DbSessionBase(IEventPublisher eventPublisher)
        {
            _eventPublisher = eventPublisher;
        }

        public void Publish(Event @event)
        {
            _eventPublisher.Publish(@event);
        }

        public void CommitChanges()
        {
            CommitChangesCore();

            _eventPublisher.PublishDelayed();
        }

        public async Task CommitChangesAsync()
        {
            await CommitChangesAsyncCore();

            _eventPublisher.PublishDelayed();
        }

        protected abstract void CommitChangesCore();
        protected abstract Task CommitChangesAsyncCore();
    }    
}