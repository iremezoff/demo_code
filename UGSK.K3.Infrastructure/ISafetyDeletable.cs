﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UGSK.K3.Infrastructure
{
    public interface ISafetyDeletable
    {
        DateTimeOffset Created { get; set; }
        DateTimeOffset Updated { get; set; }
        bool IsDeleted { get; set; }
    }
}
