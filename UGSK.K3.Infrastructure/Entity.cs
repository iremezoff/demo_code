﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UGSK.K3.Infrastructure
{
    public abstract class Entity<TId> : IEntityWithTypedId<TId>
    {
        public TId Id { get; set; }

        /// <summary>
        /// Определяет, был ли договор сохранен в БД по признаку присвоения ему Id, отличного от null или 0.
        /// </summary>
        /// <returns>Возвращает true, если договор еще не был сохранен в БД.</returns>
        public virtual bool IsTransient()
        {
            return this.Id == null || this.Id.Equals(default(TId));
        }        
    }

    public interface IEntityWithTypedId<TId>
    {
        TId Id { get; }

        bool IsTransient();
    }

    public abstract class ValueEntity : Entity<int>
    {
        protected bool Equals(ValueEntity other)
        {
            return other.Id == Id;
        }

        public override bool Equals(object obj)
        {
            return obj is ValueEntity && Equals((ValueEntity)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return Id.GetHashCode();
            }
        }

        public override string ToString()
        {
            return string.Format("Id = {0}", Id);
        }
    }

    public abstract class DictionaryEntity : ValueEntity
    {
        public string Name { get; set; }
    }
}
