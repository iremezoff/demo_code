﻿using System.Threading.Tasks;

namespace UGSK.K3.Infrastructure.Events
{    
    public interface ICommandSender
    {
        void Send<T>(T command) where T : Command;
        Task SendAsync<T>(T command) where T : Command;
    }
}
