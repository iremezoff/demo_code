﻿using System;

namespace UGSK.K3.Infrastructure.Events
{
    [AttributeUsage(AttributeTargets.Class)]
    public class RemoteHandlerAttribute : Attribute { }
}
