﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UGSK.K3.Infrastructure.Events
{
    public abstract class AggregateRoot : Entity<int>
    {
        private readonly ICollection<Event> _events = new List<Event>();

        public IEnumerable<Event> GetEvents()
        {
            return _events;
        }

        protected void Apply<T>(T @event) where T : Event
        {
            if (!_events.Contains(@event))
                _events.Add(@event);
        }
    }
}
