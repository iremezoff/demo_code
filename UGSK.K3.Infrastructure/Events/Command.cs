﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UGSK.K3.Infrastructure.Events
{
    /// <summary>
    /// Marker-class for Command derived types
    /// </summary>
    public class Command : IMessage { }
}
