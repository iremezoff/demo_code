﻿using Newtonsoft.Json;
using System.IO;
using System.Text;

namespace UGSK.K3.Infrastructure.Tools
{
    /// <summary>
    /// Утилита сериализации/десериализации объектов. Настройки сериалиации проверены для сохранения
    /// в БД через EF и использовались при сохранении caclulationresponse
    /// </summary>
    public static class SerializeTools
    {
        /// <summary>
        /// Сериализует объект как строку UTF8
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>строка в UTF8</returns>
        public static string SerializeToUTF8String(object obj)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                TypeNameHandling = TypeNameHandling.Objects,
                Binder = new UnproxingTypesBinder(),
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            };

            string serializedObject = JsonConvert.SerializeObject(obj, settings);
            return serializedObject;
        }

        /// <summary>
        /// Сериализует объект как строку UTF8 и преобразовывает в массив
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>Массив строи UTF8</returns>
        public static byte[] SerializeToUTF8Array(object obj)
        {
            return Encoding.UTF8.GetBytes(SerializeToUTF8String(obj));
        }

        /// <summary>
        /// Десериализует строку UFT8 в объект
        /// </summary>
        /// <typeparam name="T">Тип результатирующего объекта</typeparam>
        /// <param name="serializedObject">входная строка в UTF8</param>
        /// <returns>Десериализованный объект</returns>
        public static T DeserializeObjectFromUTF8<T>(string serializedObject)
        {
            T response = JsonConvert.DeserializeObject<T>(serializedObject,
                                    new JsonSerializerSettings()
                                    {
                                        TypeNameHandling = TypeNameHandling.Objects, Binder = new UnproxingTypesBinder()
                                    });
            return response;
        }

        /// <summary>
        /// Десериализует массив строки UFT8 в объект
        /// </summary>
        /// <typeparam name="T">Тип результатирующего объекта</typeparam>
        /// <param name="serializedObject">входной массив строки в UFT8</param>
        /// <returns>Десериализованный объект</returns>
        public static T DeserializeObjectFromUTF8<T>(byte[] serializedObject)
        {
            using (var ms = new MemoryStream(serializedObject))
            {
                using (var sr = new StreamReader(ms))
                {
                    var response = DeserializeObjectFromUTF8<T>(sr.ReadToEnd());
                    return response;
                }
            }
        }
    }
}