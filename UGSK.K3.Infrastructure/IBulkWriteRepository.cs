﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UGSK.K3.Infrastructure
{
    public interface IBulkWriteRepository<TEntity, TId>
    {
        void SaveBulk(IEnumerable<TEntity> entities);
    }
}
