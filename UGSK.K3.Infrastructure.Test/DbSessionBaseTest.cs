﻿using Moq;
using Moq.Protected;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using UGSK.K3.Infrastructure.Events;
using UGSK.K3.Infrastructure.Test.Environment;

namespace UGSK.K3.Infrastructure.Test
{
    [TestFixture]
    public class DbSessionBaseTest
    {
        private DbSessionBase _target;
        private Mock<IEventPublisher> _mockEventPublisher;
        private Mock<FakeDbSession> _mockDbSession;

        [SetUp]
        public void Init()
        {
            _mockEventPublisher = new Mock<IEventPublisher>();

            FakeDbSession.EventPublisher = _mockEventPublisher.Object;
            _mockDbSession = new Mock<FakeDbSession>();

            _target = _mockDbSession.Object;
        }

        [Test]
        public void PushEvent_void_ImmediatelyRunLocalHandler()
        {
            var mockHandler = new Mock<IEventHandler<Event>>();

            var someEvent = new SomeEvent();
            _mockEventPublisher
                .Setup(m => m.Publish(It.IsAny<Event>()))
                .Callback((Event evt) => mockHandler.Object.Handle(evt));
            _target.Publish(someEvent);

            mockHandler.Verify(m => m.Handle(someEvent), Times.Once);
        }

        [Test]
        public void PushEvent_void_NotRunRemoteHandler()
        {
            var mockHandler = new Mock<IEventHandler<SomeEvent>>();
            var fakeHandler = new FakeHandler(mockHandler.Object);
            var someEvent = new SomeEvent();
            _target.Publish(someEvent);

            mockHandler.Verify(m => m.Handle(someEvent), Times.Never);
        }

        [Test]
        public void PushEvent_void_NotRunRemoteHandlerOnCommitException()
        {
            _mockDbSession.Protected().Setup("CommitChangesCore").Throws<Exception>();

            var mockHandler = new Mock<IEventHandler<SomeEvent>>();
            var fakeHandler = new FakeHandler(mockHandler.Object);

            var someEvent = new SomeEvent();
            _target.Publish(someEvent);

            Assert.Throws<Exception>(_target.CommitChanges);

            mockHandler.Verify(m => m.Handle(someEvent), Times.Never);
        }

        [Test]
        public void PushEvent_void_RunRemoteHandlerAfterCommit()
        {
            var mockHandler = new Mock<IEventHandler<SomeEvent>>();
            var fakeHandler = new FakeHandler(mockHandler.Object);

            var someEvent = new SomeEvent();
            _mockEventPublisher
                .Setup(m => m.PublishDelayed())
                .Callback(() => fakeHandler.Handle(someEvent));

            _target.Publish(someEvent);

            _target.CommitChanges();

            mockHandler.Verify(m => m.Handle(someEvent), Times.Once);
        }
    }
}
