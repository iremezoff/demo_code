﻿using Moq;
using NUnit.Framework;
using UGSK.K3.Infrastructure.Events;
using UGSK.K3.Infrastructure.Test.Environment;

namespace UGSK.K3.Infrastructure.Test
{
    [TestFixture]
    class RepositoryBaseTest
    {
        private RepositoryBase<FakeAggregateRoot, int> _target;
        private Mock<FakeDbSession> _mockDbSession;
        private Mock<IEventPublisher> _mockPublisher;

        [SetUp]
        public void Init()
        {
            _mockPublisher = new Mock<IEventPublisher>();
            _mockPublisher.Name = "Publisher mock 01";

            FakeDbSession.EventPublisher = _mockPublisher.Object;

            _mockDbSession = new Mock<FakeDbSession>();
            FakeRepository.DbSessionBase = _mockDbSession.Object;

            var mockTarget = new Mock<FakeRepository>();
            _target = mockTarget.Object;
        }

        [Test]
        public void Save_NothingToPublish()
        {
            _target.Save(new FakeAggregateRoot());

            _mockPublisher.Verify(m => m.Publish(It.IsAny<Event>()), Times.Never);

            _target.Edit(new FakeAggregateRoot());

            _mockPublisher.Verify(m => m.Publish(It.IsAny<Event>()), Times.Never);
        }

        [Test]
        public void Save_PushSomeEvent()
        {
            var someEvent = new SomeEvent { Name = "SaveSomeEvent" };

            _target.Save(new FakeAggregateRoot(new[] { someEvent }));

            _mockPublisher.Verify(m => m.Publish<Event>(It.Is<Event>(se => se == someEvent)), Times.Once);
        }

        [Test]
        public void Edit_PushSomeEvent()
        {
            var someEvent = new SomeEvent { Name = "EditSomeEvent" };

            _target.Edit(new FakeAggregateRoot(new[] { someEvent }));

            _mockPublisher.Verify(m => m.Publish<Event>(It.Is<Event>(se => se == someEvent)), Times.Once);
        }
    }





}
