﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UGSK.K3.Infrastructure.Test.Environment
{
    public abstract class FakeRepository : RepositoryBase<FakeAggregateRoot, int>
    {
        //[ThreadStatic]
        public static DbSessionBase DbSessionBase;

        public FakeRepository()
            : base(DbSessionBase)
        {
        }
    }
}
