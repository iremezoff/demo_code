﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UGSK.K3.Infrastructure.Events;

namespace UGSK.K3.Infrastructure.Test.Environment
{
    [RemoteHandler]
    public class FakeHandler : IEventHandler<Event>
    {
        private readonly IEventHandler<SomeEvent> _innerHandler;

        public FakeHandler(IEventHandler<SomeEvent> innerHandler)
        {
            _innerHandler = innerHandler;
        }

        public void Handle(Event @event)
        {
            _innerHandler.Handle(@event as SomeEvent);
        }
    }
}
