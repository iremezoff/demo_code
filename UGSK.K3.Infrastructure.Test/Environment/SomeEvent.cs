﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UGSK.K3.Infrastructure.Events;

namespace UGSK.K3.Infrastructure.Test.Environment
{
    public class SomeEvent : Event
    {
        public string Name { get; set; }
    }
}
