﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UGSK.K3.Infrastructure.Events;

namespace UGSK.K3.Infrastructure.Test.Environment
{
    public abstract class FakeDbSession : DbSessionBase
    {
        //[ThreadStatic]
        public static IEventPublisher EventPublisher;

        public FakeDbSession()
            : base(EventPublisher)
        {
        }
    }
}
