﻿using NUnit.Framework;
using UGSK.K3.Infrastructure.Tools;

namespace UGSK.K3.Infrastructure.Test
{
    [TestFixture]
    class SerializeToolsTest
    {
        [Test]
        public void DeserializeSimpleRequestFromJSONString_Test()
        {
            var serializedObject = "{\"$type\":\"UGSK.K3.Infrastructure.Test.SomeClass, UGSK.K3.Infrastructure.Test\",\"InternalMember\":{\"$type\":\"System.Data.Entity.DynamicProxies.SomeOtherCla_C74FEB144DDA29FE2556391DCDCDCBC0F1202311B88D50C4D39AFF7947B44255, EntityFrameworkDynamicProxies-UGSK.K3.Infrastructure.Test\",\"Foo\":1,\"Bar\":\"Hey-hey\"}}";
            var result = SerializeTools.DeserializeObjectFromUTF8<SomeClass>(serializedObject);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.InternalMember);
        }

        [Test]
        public void SerializeSimpleRequestToJSONStringWithoutEFProxyTypes_Test()
        {
            var testObject = new SomeClass
            {
                InternalMember = new System.Data.Entity.DynamicProxies.SomeOtherClass_C74FEB144DDA29FE2556391DCDCDCBC0F1202311B88D50C4D39AFF7947B44255 { Foo = 1, Bar = "Hey-hey" }
            };

            var json = SerializeTools.SerializeToUTF8String(testObject);

            StringAssert.Contains("UGSK.K3.Infrastructure.Test.SomeOtherClass", json);
            StringAssert.DoesNotContain("System.Data.Entity.DynamicProxies", json);
        }

    }

    public class SomeClass 
    {
        public SomeOtherClass InternalMember { get; set; }
    }

    public class SomeOtherClass 
    {
        public int Foo { get; set; }
        public string Bar { get; set; }
    }    
}

namespace System.Data.Entity.DynamicProxies
{
    //
    using UGSK.K3.Infrastructure.Test;
    class SomeOtherClass_C74FEB144DDA29FE2556391DCDCDCBC0F1202311B88D50C4D39AFF7947B44255 : SomeOtherClass { }
}
