﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace UGSK.K3.Infrastructure.Mvc.Attribute
{
    public class TransactionAttribute : ActionFilterAttribute
    {
        public Func<IDbSession> DbContextFactory { get; set; }

        public async override Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancelattionToken)
        {
            if (actionExecutedContext.Exception == null)
            {
                await DbContextFactory().CommitChangesAsync();
            }
            await base.OnActionExecutedAsync(actionExecutedContext, cancelattionToken);
        }
    }
}