﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.OData.Routing;
using System.Web.OData.Extensions;
using System.Web.Http.Routing;
using System.Net.Http;

namespace UGSK.K3.Infrastructure.Mvc.Attribute
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ObserveODataConventionsAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Request.Method == HttpMethod.Post)
                ServeODataPost(actionExecutedContext);

            base.OnActionExecuted(actionExecutedContext);
        }

        private void ServeODataPost(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Response == null)
                return;

            var responseContent = actionExecutedContext.Response.Content as ObjectContent;

            if (responseContent == null)
                return;

            var responseValue = responseContent.Value as Entity<int>;

            if (responseValue == null)
                return;

            var segments = new List<ODataPathSegment>();
            segments.Add(new EntitySetPathSegment(actionExecutedContext.ActionContext.ControllerContext.RouteData.Values["controller"].ToString()));
            segments.Add(new KeyValuePathSegment(responseValue.Id.ToString()));
            var _urlHelper = new UrlHelper(actionExecutedContext.Request);

            actionExecutedContext.Response.Headers.Location = new Uri(
                _urlHelper.CreateODataLink(
                actionExecutedContext.Request.ODataProperties().RouteName,
                actionExecutedContext.Request.ODataProperties().PathHandler, segments)
            );

            actionExecutedContext.Response.StatusCode = System.Net.HttpStatusCode.Created;
        }
    }
}
