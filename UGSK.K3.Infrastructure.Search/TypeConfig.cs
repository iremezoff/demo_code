using System;

namespace UGSK.K3.Infrastructure.Search
{
    public class TypeConfig
    {
        public string IndexTypeName { get; set; }
        public string IndexName { get; set; }
        public string KeyName { get; set; }
        public Type ResultType { get; set; }
    }
}