﻿using System.Collections.Generic;

namespace UGSK.K3.Infrastructure.Search
{
    public interface IFulltextEngineProxy<T>
    {
        IEnumerable<T> Search(string query, string[] sort = null);
        T GetByKey(string key);
        IEnumerable<T> SearchByAnd(string[] query, string[] sortFields = null, int resultCapcity = 10);
    }
}
