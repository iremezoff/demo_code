﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using PlainElastic.Net;
using PlainElastic.Net.Queries;
using PlainElastic.Net.Serialization;

namespace UGSK.K3.Infrastructure.Search
{
    public class ElasticProxy<T> : IFulltextEngineProxy<T>
    {
        private readonly ISearchConfig _config;
        private TypeConfig _typeConfig;

        public IEnumerable<T> Search(string fulltextString, string[] sort = null)
        {
            //этот вариант с весами опять перестал работать
            //Нужно придумать более ёмкое условие
            /*string jsonQuery = new QueryBuilder<T>()
            .Query(q => q
                .Bool(b => b
                    .Must(m => m
                        .QueryString(qs => qs.Fields(new[] { "region^100000", "district^5000", "city^50000", "cityarea^2500", "locality^1000", "street^10000", "addarea^10", "addstreet^1" })
                                .Query(fulltextString)
                        )
                    )
                )
            ).From(0).Size(10).BuildBeautified();*/
            var queryBuilder = new QueryBuilder<T>()
            .Query(q => q
                .Bool(b => b
                    .Must(m => m
                        .QueryString(qs => qs.DefaultField("_all")
                                .Query(fulltextString)
                        )
                    )
                )
            ).From(0).Size(10);
            if (sort != null && sort.Any())
            {
                queryBuilder = sort.Aggregate(queryBuilder, (current, field) => current.Sort(s => s.Field(field)));
            }

            var rawQuery = queryBuilder.BuildBeautified();
            return SendQuery(rawQuery);
        }

        public IEnumerable<T> SearchByAnd(string[] query, string[] sort = null, int resultCapcity = 10)
        {
            string queryString = string.Join(" AND ", query);

            var queryBuilder = new QueryBuilder<T>()
                .Query(q => q
                    .Bool(b => b
                        .Must(m => m
                            .QueryString(qs => qs.DefaultField("_all").Query(queryString)
                            )
                        )
                    )
                ).From(0).Size(resultCapcity);

            if (sort != null && sort.Any())
            {
                queryBuilder = sort.Aggregate(queryBuilder, (current, field) => current.Sort(s => s.Field(field)));
            }
            var rawQuery = queryBuilder.BuildBeautified();
            return SendQuery(rawQuery);
        }

        private string PrepareValue(object value)
        {
            if (value is bool)
            {
                return value.ToString().ToLower();
            }
            return value.ToString();
        }

        public T GetByKey(string key)
        {
            T result = default(T);
            if (string.IsNullOrEmpty(key)) return result;
            string jsonQuery = new QueryBuilder<T>()
                .Filter(f =>
                        f.Term(t =>
                            t.Field(_typeConfig.KeyName).Value(key)))
                .BuildBeautified();

            var resultList = SendQuery(jsonQuery);
            result = (resultList != null) ? resultList.SingleOrDefault() : default(T);
            return result;
        }

        public T GetByKLADRKey(string key)
        {
            T result = default(T);
            if (string.IsNullOrEmpty(key)) return result;
            var jsonQuery = new QueryBuilder<T>()
                .Filter(f =>
                    f.Term(t =>
                        t.Field("codeKLADR").Value(key)))
                .BuildBeautified();

            var resultList = SendQuery(jsonQuery);
            result = (resultList != null) ? resultList.SingleOrDefault() : default(T);
            return result;
        }

        private List<T> SendQuery(string jsonQuery)
        {
            var connection = new ElasticConnection(_config.Host, _config.Port);
            var serializer = new JsonNetSerializer();
            string command = Commands.Search(_typeConfig.IndexName, _typeConfig.IndexTypeName);
            string result = connection.Post(command, jsonQuery);
            var found = serializer.ToSearchResult<T>(result).Documents.ToList();
            return found;
        }

        public ElasticProxy(ISearchConfig config)
        {
            _config = config;
            _typeConfig = _config[typeof(T)];
            if (_typeConfig == null)
            {
                throw new InvalidDataException(string.Format("Отсутствует конфигурация для типа {0}", typeof(T)));
            }
        }
    }
}
