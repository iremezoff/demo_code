﻿using System;
using System.Collections.Generic;

namespace UGSK.K3.Infrastructure.Search
{
    public interface IFiasRepository
    {
        /// <summary>
        /// Получение адреса по коду ФИАС
        /// </summary>
        /// <param name="aoGuid">Код ФИАС</param>
        /// <returns></returns>
        AddressObject GetAddressObjectByGuid(string aoGuid);

        /// <summary>
        /// Получение адреса по коду КЛАДР
        /// </summary>
        /// <param name="code">Код КЛАДР</param>
        /// <returns></returns>
        AddressObject GetAddressObjectByCode(string code);
        IEnumerable<AddressObject> GetAddressObjectByGuidList(IEnumerable<string> aoGuids);
    }
}
