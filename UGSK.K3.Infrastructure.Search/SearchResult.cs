﻿using System;

namespace UGSK.K3.Infrastructure.Search
{
    public class SearchResult
    {
        public string Region { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string CityArea { get; set; }
        public string Locality { get; set; }
        public string Street { get; set; }
        public string AddArea { get; set; }
        public string AddStreet { get; set; }
        public Guid? Guid { get; set; }
        public string CodeKLADR { get; set; }
        public int Level { get; set; }
        public string CodeFias { get; set; }
    }
}