﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using Dapper;

namespace UGSK.K3.Infrastructure.Search
{
    public class AddressRepository : IFiasRepository
    {
        private const string _baseSelect = @"SELECT AOGUID, REGIONCODE, AUTOCODE, AREACODE, CITYCODE, CTARCODE, PLACECODE, STREETCODE, EXTRCODE, SEXTCODE, POSTALCODE
            FROM [dbo].[AddrObj] Where [ACTSTATUS] =1";


        private IEnumerable<AddressObject> GetAddressObject(string selectParam, object obj)
        {
            IEnumerable<AddressObject> result;
            using (var connection = DataProvider.Instance.GetConnection())
            {
                connection.Open();
                result = connection.Query<AddressObject>(_baseSelect + selectParam, obj);
            }
            return result;
        }

        public AddressObject GetAddressObjectByGuid(string aoGuid)
        {
            return GetAddressObject(" and AOGUID=@AOGUID", new {AOGUID = aoGuid}).FirstOrDefault();
        }

        public AddressObject GetAddressObjectByCode(string code)
        {            
            return GetAddressObject(" and CODE=@CODE", new { CODE = code }).FirstOrDefault();
        }

        public IEnumerable<AddressObject> GetAddressObjectByGuidList(IEnumerable<string> aoGuids)
        {
            return GetAddressObject(" and AOGUID in @AOGUIDs", new { AOGUIDs = aoGuids });
        }
    }

    public class AddressObject
    {
        public Guid AOGUID { get; set; }
        public string FORMALNAME { get; set; }
        public string REGIONCODE { get; set; }
        public string AUTOCODE { get; set; }
        public string AREACODE { get; set; }
        public string CITYCODE { get; set; }
        public string CTARCODE { get; set; }
        public string PLACECODE { get; set; }
        public string STREETCODE { get; set; }
        public string EXTRCODE { get; set; }
        public string SEXTCODE { get; set; }
        public string OFFNAME { get; set; }
        public string POSTALCODE { get; set; }
        public string IFNSFL { get; set; }
        public string TERRIFNSFL { get; set; }
        public string IFNSUL { get; set; }
        public string TERRIFNSUL { get; set; }
        public string OKATO { get; set; }
        public string OKTMO { get; set; }
        public string UPDATEDATE { get; set; }
        public string SHORTNAME { get; set; }
        public string AOLEVEL { get; set; }
        public string PARENTGUID { get; set; }
        public string AOID { get; set; }
        public string PREVID { get; set; }
        public string NEXTID { get; set; }
        public string CODE { get; set; }
        public string PLAINCODE { get; set; }
        public string ACTSTATUS { get; set; }
        public string CENTSTATUS { get; set; }
        public string OPERSTATUS { get; set; }
        public string CURRSTATUS { get; set; }
        public string STARTDATE { get; set; }
        public string ENDDATE { get; set; }
        public string LIVESTATUS { get; set; }
        public string NORMDOC { get; set; }
    }

    internal sealed class DataProvider
    {
        private const string ConnectionStringName = "Fias";
        static readonly ConnectionStringSettings ConnectionConfiguration = ConfigurationManager.ConnectionStrings[ConnectionStringName];
        private static DataProvider _instance = null;

        /**
         * Конструктор класса не используется, вместо него используйте метод getInstance()
         * @see getInstance()
         */
        private DataProvider()
        {
            IDbConnection testConnection = GetConnection();
            try
            {
                testConnection.Open();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Не удалось открыть БД. Строка подключения \"" + ConnectionConfiguration.ConnectionString + "\"");
                throw new Exception("Ошибка при попытке подключиться к БД. " + e.StackTrace, e);
            }
            finally
            {
                testConnection.Close();
            }
        }

        /**
         * Функция для получения экземпляра класса для доступа к данным 		   
         * @return экземпляр класса DataProvider (Singleton) для доступа к БД
         */
        public static DataProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    try
                    {
                        _instance = new DataProvider();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error in DataProvider.getInstance()", ex);
                    }
                }
                return _instance;
            }
        }

        public IDbConnection GetConnection()
        {
            if (ConnectionConfiguration == null)
                throw new Exception(String.Format("Не найдена строка подключения с именем \"{0}\". Необходимо прописать строку подключения", ConnectionStringName));

            return new SqlConnection(ConnectionConfiguration.ConnectionString);
        }
    }
}
