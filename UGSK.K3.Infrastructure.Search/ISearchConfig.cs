using System;

namespace UGSK.K3.Infrastructure.Search
{
    public interface ISearchConfig
    {
        string Host { get; set; }
        int Port { get; set; }
        TypeConfig this[Type runtimeType] { get; }
    }
}