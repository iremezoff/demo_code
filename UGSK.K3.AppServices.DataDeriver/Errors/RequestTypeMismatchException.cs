﻿using System;

namespace UGSK.K3.AppServices.DataDeriver.Errors
{
    public class RequestTypeMismatchException : Exception
    {
        public RequestTypeMismatchException(string message, Type expectedType, Type actualType)
            : this(message, null, expectedType, actualType) { }

        public Type ActualType { get; private set; }
        public Type ExpectedType { get; private set; }

        public RequestTypeMismatchException(string message, Exception inner, Type expectedType, Type actualType)
            : base(message, inner)
        {
            ActualType = actualType;
            ExpectedType = expectedType;
        }
    }
}