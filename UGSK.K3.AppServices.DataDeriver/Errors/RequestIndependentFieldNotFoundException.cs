﻿using System;

namespace UGSK.K3.AppServices.DataDeriver.Errors
{
    public class RequestIndependentFieldNotFoundException : Exception
    {
        public RequestIndependentFieldNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        { }

        public RequestIndependentFieldNotFoundException(string message)
            : base(message)
        { }
    }
}