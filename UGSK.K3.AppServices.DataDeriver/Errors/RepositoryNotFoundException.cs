﻿using System;

namespace UGSK.K3.AppServices.DataDeriver.Errors
{
    public class RepositoryNotFoundException : Exception
    {
        public RepositoryNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        { }

        public RepositoryNotFoundException(string message)
            : base(message)
        { }
    }
}