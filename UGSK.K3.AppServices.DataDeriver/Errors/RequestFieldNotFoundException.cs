﻿using System;

namespace UGSK.K3.AppServices.DataDeriver.Errors
{
    public class RequestFieldNotFoundException : ApplicationException
    {
        public RequestFieldNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        { }

        public RequestFieldNotFoundException(string message)
            : base(message)
        { }
    }
}