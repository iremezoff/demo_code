﻿using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Strategies
{
    public interface IStrategyBuilder
    {
        ISourceDeriveStrategy BuildStrategy(IRequest request, string fieldName);
        ISourceDeriveStrategy BuildStrategy(IRequest request);
    }
}