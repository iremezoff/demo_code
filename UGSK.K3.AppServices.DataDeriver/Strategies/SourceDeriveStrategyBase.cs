﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UGSK.K3.AppServices.DataDeriver.Product.Field;
using UGSK.K3.Product.Contract;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Strategies
{
    public abstract class SourceDeriveStrategyBase<T> : ISourceDeriveStrategy where T : IRequest
    {
        protected readonly IDictionary<string, FieldRepository> FieldsRepos;
        protected readonly ITariffCalculationEngine<T> Engine;
        protected readonly T Request;
        protected readonly List<IRequestFieldBatler> UnvisitedBatlers = new List<IRequestFieldBatler>();

        protected SourceDeriveStrategyBase(IRequest request, ITariffCalculationEngine<T> engine)
        {
            Engine = engine;
            Request = (T)request;
            FieldsRepos = new Dictionary<string, FieldRepository>();
        }

        public abstract IEnumerable<FieldRepository> ExtractRepositories();

        protected virtual IEnumerable<FieldRepository> GetRepositories(IRequestFieldBatler parentParameterInfo)
        {
            return GetValues(parentParameterInfo.ConcernedFieldBatlers);
        }

        protected virtual IEnumerable<FieldRepository> GetValues(IEnumerable<IRequestFieldBatler> dependentFieldBatlers)
        {
            UnvisitedBatlers.AddRange(dependentFieldBatlers);
            foreach (var childField in dependentFieldBatlers)
            {
                if (Engine.GetDependences(childField).Any(b => UnvisitedBatlers.Contains(b)))
                {
                    continue;
                }
                UnvisitedBatlers.RemoveAll(b => b == childField);

                var childParamRepo = childField.CreateFieldRepository(Request);

                var fieldValue = childField.FieldProperty.GetValue(Request);
                childField.FieldProperty.SetValue(Request, childParamRepo.GetLiteValue()); // todo Ремезов: избавиться от рефлексии + добавить возможность сеттить сразу несколько значений (для HDBK)

                if (fieldValue != null || childParamRepo.HasValue || childField.FieldProperty.IsDefined(typeof(PassToDependentIfNull)))
                {
                    // необходимо обновить информацию обо всех репозиториях по каскаду
                    foreach (var dependentFieldBatler in GetRepositories(childField))
                    {
                        FieldsRepos[dependentFieldBatler.FieldName] = dependentFieldBatler;
                        yield return dependentFieldBatler;
                    }
                }
                FieldsRepos[childParamRepo.FieldName] = childParamRepo;
                yield return childParamRepo;
            }
        }
    }
}