﻿using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Strategies
{
    public interface IDeriveStrategyFactory
    {
        ISourceDeriveStrategy GetStrategy(IRequest request);
        ISourceDeriveStrategy GetStrategy(IRequest request, string fieldName);
    }
}