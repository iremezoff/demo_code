﻿using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.AppServices.DataDeriver.Errors;
using UGSK.K3.AppServices.DataDeriver.Product.Field;
using UGSK.K3.Product.Contract;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Strategies.Impl
{
    public class ByFieldDeriveStrategy<T> : SourceDeriveStrategyBase<T> where T : IRequest
    {
        private readonly string _fieldName;

        public ByFieldDeriveStrategy(IRequest request, ITariffCalculationEngine<T> engine, string fieldName)
            : base(request, engine)
        {
            _fieldName = fieldName;
        }

        public override IEnumerable<FieldRepository> ExtractRepositories()
        {
            IRequestFieldBatler fieldBatler = Engine[_fieldName];

            if (fieldBatler == null)
            {
                throw new RequestFieldNotFoundException("Поле запроса, которому устанавливается значение, отсутствует");
            }

            foreach (var childField in fieldBatler.ConcernedFieldBatlers)
            {
                if (childField.FieldProperty.IsDefined(typeof(NullingOnParentChanged), false))
                {
                    childField.FieldProperty.SetValue(Request,
                        childField.FieldProperty.PropertyType.IsPrimitive || !childField.FieldProperty.PropertyType.IsArray
                            ? Activator.CreateInstance(childField.FieldProperty.PropertyType)
                            : null);
                }
            }

            //SignAsVisited(fieldBatler);

            var repos = GetRepositories(fieldBatler).ToList(); // todo Ремезов: создать тест и реализацию на каскадное заполнение репозиториев, чтобы они не повторялись

            return FieldsRepos.Values.ToList();
        }

        private void SignAsVisited(IRequestFieldBatler batler)
        {
            UnvisitedBatlers.Add(batler);
            foreach (var dependence in Engine.GetDependences(batler))
            {
                SignAsVisited(dependence);
            }
        }

    }
}