﻿using System.Collections.Generic;
using System.Linq;
using UGSK.K3.AppServices.DataDeriver.Product.Field;
using UGSK.K3.Product.Contract;
using UGSK.K3.AppServices.DataDeriver.Core;

namespace UGSK.K3.AppServices.DataDeriver.Strategies.Impl
{
    public class ByRequestDeriveStrategy<T> : SourceDeriveStrategyBase<T> where T : IRequest
    {
        public ByRequestDeriveStrategy(IRequest request, ITariffCalculationEngine<T> engine)
            : base(request, engine)
        { }

        public override IEnumerable<FieldRepository> ExtractRepositories()
        {
            // интересуют независмые поля не с дефолтовым типом
            var requestFieldBatlers = Engine.GetAllFields().Where(i => i.IsIndependent && i.SourceType != typeof(SimpleEntity));

            foreach (var field in requestFieldBatlers)
            {
                var repo = field.CreateFieldRepository(Request);

                FieldsRepos[repo.FieldName] = repo;
                if (repo.HasValue)
                {
                    field.FieldProperty.SetValue(Request, repo.GetLiteValue()); // Ремезов: todo избавиться от рефлексии + добавить возможность сеттить сразу несколько значений (для HDBK)
                    var subRepo = GetRepositories(field).ToList();
                }
            }
            return FieldsRepos.Values.ToList();
        }
    }
}