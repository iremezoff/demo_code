﻿using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Strategies.Impl
{
    public class StrategyBuilder<T> : IStrategyBuilder where T:IRequest
    {
        private readonly ITariffCalculationEngine<T> _engine;

        public StrategyBuilder(ITariffCalculationEngine<T> engine)
        {
            _engine = engine;
        }

        public ISourceDeriveStrategy BuildStrategy(IRequest request, string fieldName)
        {
            return new ByFieldDeriveStrategy<T>(request, _engine, fieldName);
        }

        public ISourceDeriveStrategy BuildStrategy(IRequest request)
        {
            return new ByRequestDeriveStrategy<T>(request, _engine);
        }
    }
}