﻿using System;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Strategies.Impl
{
    public class DeriveStrategyFactory : IDeriveStrategyFactory
    {
        private readonly Func<Type, IStrategyBuilder> _factoryBuilder;
        //private readonly Func<IRequest, ISourceDeriveStrategy> _factoryByRequest;
        //private readonly Func<IRequest, string, ISourceDeriveStrategy> _factoryByField;

        public DeriveStrategyFactory(Func<Type, IStrategyBuilder> factoryBuilder)
        {
            _factoryBuilder = factoryBuilder;
            //_factoryByField = factoryByField;
            //_factoryByRequest = factoryByRequest;
        }

        public ISourceDeriveStrategy GetStrategy(IRequest request)
        {
            // todo Ремезов: дополнительная логика по определению продукта
            return _factoryBuilder(request.GetType()).BuildStrategy(request);
        }

        public ISourceDeriveStrategy GetStrategy(IRequest request, string fieldName)
        {
            return _factoryBuilder(request.GetType()).BuildStrategy(request, fieldName);
        }
    }
}