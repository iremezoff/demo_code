﻿using System.Collections.Generic;
using UGSK.K3.AppServices.DataDeriver.Product.Field;

namespace UGSK.K3.AppServices.DataDeriver.Strategies
{
    public interface ISourceDeriveStrategy
    {
        IEnumerable<FieldRepository> ExtractRepositories();
    }
}