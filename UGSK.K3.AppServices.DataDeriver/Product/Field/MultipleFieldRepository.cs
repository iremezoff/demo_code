﻿using System.Collections.Generic;
using System.Linq;

namespace UGSK.K3.AppServices.DataDeriver.Product.Field
{
    public class MultipleFieldRepository : FieldRepository
    {
        public object[] Value { get; internal set; }

        public override bool HasValue
        {
            get { return Value != null; }
        }

        public override object GetLiteValue()
        {
            return Value;
        }

        public override void SetValue(IEnumerable<object> selectedValue)
        {
            Value = selectedValue != null ? selectedValue.ToArray() : null;
        }
    }
}