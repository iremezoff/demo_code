﻿using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Infrastructure;

namespace UGSK.K3.AppServices.DataDeriver.Product.Field
{
    public class SingleFieldRepository : FieldRepository
    {
        public object Value { get; private set; }

        public override bool HasValue
        {
            get { return Value != null; }
        }

        public override object GetLiteValue()
        {
            return Value;// Value != null ? Convert.ChangeType(Value.Name, Creator.FieldProperty.PropertyType) : null;
        }

        public override void SetValue(IEnumerable<object> selectedValue)
        {
            Value = selectedValue != null ? selectedValue.SingleOrDefault() : null;
        }
    }

    public class SingleFieldBasedOnEntityRepository : FieldRepository
    {
        private readonly Func<Entity<int>, object> _converter;
        public object Value { get; private set; }

        public override bool HasValue
        {
            get { return Value != null; }
        }

        public SingleFieldBasedOnEntityRepository(Func<Entity<int>, object> converter)
        {
            _converter = converter;
        }

        public override object GetLiteValue()
        {
            return Value;// Value != null ? Convert.ChangeType(Value.Name, Creator.FieldProperty.PropertyType) : null;
        }

        public object GetUnboxValue()
        {
            return Value != null ? _converter(Value as Entity<int>) : null;// Value != null ? Convert.ChangeType(Value.Name, Creator.FieldProperty.PropertyType) : null;
        }

        public override void SetValue(IEnumerable<object> selectedValue)
        {
            Value = selectedValue != null ? selectedValue.SingleOrDefault() : null;
        }
    }
}