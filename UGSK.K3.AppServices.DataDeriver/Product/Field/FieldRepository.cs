﻿using System.Collections.Generic;
using System.Diagnostics;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Product.Field
{
    [DebuggerDisplay("FieldName = {FieldName}")]
    public abstract class FieldRepository
    {
        public string FieldName { get; internal set; }

        public IRequestFieldBatler Creator { get; internal set; }

        public IDictionary<SimpleEntity, bool> CollectionSource { get; set; }
        
        public bool IsDeny { get; set; }

        public abstract bool HasValue { get; }

        public IRequest Request { get; internal set; }

        public abstract object GetLiteValue();

        public abstract void SetValue(IEnumerable<object> selectedValue);
    }
}