﻿using System.Diagnostics;

namespace UGSK.K3.AppServices.DataDeriver.Product.Calculation
{
    [DebuggerDisplay("{Name} - {Value}")]
    public class CalculationRepository
    {
        public bool IsPrintable { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Role  { get; set; }
        public object Value { get; set; }
        public int SortOrder { get; set; }
    }
}