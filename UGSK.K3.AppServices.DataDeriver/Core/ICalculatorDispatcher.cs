﻿using System;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Core
{
    public interface ICalculatorDispatcher
    {
        ICalculator<T> GetCalculator<T>() where T : IRequest;
        ICalculator GetCalculator(Type type);
    }
}
