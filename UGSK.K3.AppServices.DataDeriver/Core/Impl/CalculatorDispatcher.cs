﻿using System;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Core.Impl
{
    public class CalculatorDispatcher : ICalculatorDispatcher
    {
        private Func<Type, ICalculator> _factory;

        public CalculatorDispatcher(Func<Type, ICalculator> factory)
        {
            _factory = factory;
        }

        public ICalculator<T> GetCalculator<T>() where T : IRequest
        {
            return (ICalculator<T>)_factory(typeof(T));
        }

        public ICalculator GetCalculator(Type type)
        {
            return _factory(type);
        }
    }
}