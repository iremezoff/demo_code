﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using EntityFramework.Caching;
using EntityFramework.Extensions;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;
using UGSK.K3.AppServices.DataDeriver.Core.SelectionRules;
using UGSK.K3.AppServices.DataDeriver.Core.SelectionRules.Impl;
using UGSK.K3.AppServices.DataDeriver.Product.Field;
using UGSK.K3.Infrastructure;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;
using IDbSession = UGSK.K3.Infrastructure.IDbSession;

namespace UGSK.K3.AppServices.DataDeriver.Core.Impl
{
    public sealed class BatlerFactory : IBatlerFactory
    {
        private readonly Func<FieldAttributeBase, IRequestFieldBatler, Action<EventHandler<NewRestrictorEventArgs>, Type>, IFieldSourceRestrictor<FieldAttributeBase>> _restrictorFactory;
        private readonly IDictionary<Type, IDictionary<string, IRequestFieldBatler>> _registeredBatlers;
        private readonly ISelectorRuleBuilder _ruleBuilder;
        private IDictionary<Type, List<EventHandler<NewRestrictorEventArgs>>> NewRestrictorsHandlers;

        private readonly SingleHavingSelectionRule<IRequest> _singleHavingRule = new SingleHavingSelectionRule<IRequest>();
        private readonly ICalculatorDispatcher _calculatorDispatcher;
        private readonly Func<Type, object> _repositoryFactory;
        private readonly Func<Type, IFieldValueConverter> _converterFactory;

        // todo Ремезов: быть может дефолтовые правила запихать в билдер правил, предусмотрев логику по их отдаче на каждый запрос без пересоздания
        public BatlerFactory(
            Func<FieldAttributeBase, IRequestFieldBatler, Action<EventHandler<NewRestrictorEventArgs>, Type>, IFieldSourceRestrictor<FieldAttributeBase>> restrictorFactory,
            ISelectorRuleBuilder ruleBuilder,
            ICalculatorDispatcher calcDispatcher,
            Func<Type, object> repositoryFactory,
            Func<Type, IFieldValueConverter> converterFactory)
        {
            _restrictorFactory = restrictorFactory;
            _registeredBatlers = new Dictionary<Type, IDictionary<string, IRequestFieldBatler>>();
            _ruleBuilder = ruleBuilder;
            _calculatorDispatcher = calcDispatcher;
            _repositoryFactory = repositoryFactory;
            _converterFactory = converterFactory;
            NewRestrictorsHandlers = new Dictionary<Type, List<EventHandler<NewRestrictorEventArgs>>>();
        }

        private void OnNewRestrictor<T>(NewRestrictorEventArgs e) where T : IRequest
        {
            List<EventHandler<NewRestrictorEventArgs>> handler = null;
            if (NewRestrictorsHandlers.TryGetValue(typeof(T), out handler))
            {
                handler.ForEach(h => h(this, e));
            }
        }

        private void OnRegister(EventHandler<NewRestrictorEventArgs> handler, Type requestType)
        {
            List<EventHandler<NewRestrictorEventArgs>> handlers;

            if (!NewRestrictorsHandlers.TryGetValue(requestType, out handlers))
            {
                handlers = new List<EventHandler<NewRestrictorEventArgs>>();
                NewRestrictorsHandlers.Add(requestType, handlers);
            }
            handlers.Add(handler);
        }

        public IRequestFieldBatler MakeFieldBatler<T>(PropertyInfo property, ITariffCalculationEngine<T> engine) where T : IRequest
        {
            var fieldAttrs = Attribute.GetCustomAttributes(property, typeof(FieldAttributeBase)).Cast<FieldAttributeBase>().ToList();

            var requestFieldAttr = fieldAttrs.FirstOrDefault(p => p is RequestFieldAttribute) as RequestFieldAttribute;

            // todo Ремезов: возможно, есть более подходящий тип исключения, чем ниже. может быть, придумать свой
            if (property.IsDefined(typeof(NonFieldAttribute)))
            {
                throw new InvalidDataException(string.Format("Поле {0} помечено как не являющееся полем запроса", property.Name));
            }

            IEnumerable<SimpleEntity> collection = null;

            var calculator = _calculatorDispatcher.GetCalculator<T>();

            if (calculator == null)
            {
                throw new CalculatorNotFoundException(string.Format("Не найден калькулятор для типа {0}", typeof(T)), typeof(T));
            }

            var dbSession = calculator.DbSession;

            PropertyInfo collectionProperty = null;

            string sourceTypeProperty = null;
            if (requestFieldAttr != null)
            {
                collectionProperty = (dbSession.GetType()
                                              .GetInterfaces()
                                              .FirstOrDefault(i => typeof(IDbSession).IsAssignableFrom(i) && i != typeof(IDbSession)) ?? dbSession.GetType())
                                              .GetProperties()
                                              .FirstOrDefault(
                                                  p =>
                                                  typeof(IEnumerable<SimpleEntity>).IsAssignableFrom(p.PropertyType) &&
                                                  p.PropertyType.GenericTypeArguments[0] == requestFieldAttr.SourceType);
                sourceTypeProperty = requestFieldAttr.ValuePropertyName;
            }

            bool fieldBasedOnRepoSource = requestFieldAttr != null && requestFieldAttr.SourceType.IsGenericType
                                         &&
                                         typeof(Entity<int>).IsAssignableFrom(
                                             requestFieldAttr.SourceType.GetGenericArguments()[0])
                                         &&
                                         typeof(GenericSimpleEntity<>).IsAssignableFrom(
                                             requestFieldAttr.SourceType.GetGenericTypeDefinition());

            Func<Entity<int>, object> converter = null;

            if (collectionProperty == null)
            {
                if (requestFieldAttr != null &&
                    typeof(SimpleEntity).IsAssignableFrom(requestFieldAttr.SourceType))
                {

                    if (fieldBasedOnRepoSource)
                    {
                        var entityType = requestFieldAttr.SourceType.GetGenericArguments()[0];

                        ParameterExpression param = Expression.Parameter(typeof(Entity<int>));
                        if (requestFieldAttr.Converter != null)
                        {
                            converter = _converterFactory(requestFieldAttr.Converter).Convert;
                        }
                        else
                        {
                            converter = Expression.Lambda<Func<Entity<int>, object>>(
                                Expression.Property(Expression.Convert(param, entityType), "Name"), param).Compile();
                        }

                        if (!requestFieldAttr.DoNotDerive)
                        {
                            var repoType = typeof(IRepositoryWithTypedId<,>).MakeGenericType(entityType, typeof(int));
                            var repository = _repositoryFactory(repoType);


                            var gseType = typeof(GenericSimpleEntity<>).MakeGenericType(entityType);
                            Expression<Func<Entity<int>, SimpleEntity>> selector =
                                Expression.Lambda<Func<Entity<int>, SimpleEntity>>(
                                    Expression.New(gseType.GetConstructor(new[] { entityType }),
                                        Expression.Convert(param, entityType)), param);

                            // зачем tolist, потом снова iqueryable? чтобы материлизовать коллекцию (иначе любой выражение будет пытаться транслироваться в sql, что не всегда возможно), 
                            // а потом снова применять любые Expression
                            var originalCollection =
                                (repoType.GetMethod("GetAll").Invoke(repository, null) as IQueryable<Entity<int>>);

                            // очищаем кеш. Для приложения в рантайме ничего не изменится (кеширование запустится 1 раз при старте), а для тестов позволит использовать данные из моков
                            originalCollection.RemoveCache();

                            originalCollection =
                                originalCollection.FromCache(CachePolicy.WithAbsoluteExpiration(DateTime.MaxValue),
                                    new[] {string.Format("EngineDictionaryCache_{0}", property.PropertyType.FullName)})
                                    .AsQueryable();

                            if (!string.IsNullOrEmpty(requestFieldAttr.SortColumn))
                            {
                                var columnType = entityType.GetProperty(requestFieldAttr.SortColumn,
                                    BindingFlags.Public | BindingFlags.Instance).PropertyType;
                                if (columnType != typeof(int))
                                    throw new InvalidDataException("В параметре сортировки допустимо указывать только тип Int32");
                                Expression<Func<Entity<int>, int>> sorter =
                                    Expression.Lambda<Func<Entity<int>, int>>(
                                        Expression.Property(Expression.Convert(param, entityType),
                                            requestFieldAttr.SortColumn), param);
                                originalCollection = originalCollection.OrderBy(sorter);
                            }

                            collection = originalCollection.Select(selector).ToList();

                            if (!collection.Any())
                            {
                                var prop = (dbSession.GetType()
                                    .GetInterfaces()
                                        .FirstOrDefault(
                                            i => typeof(IDbSession).IsAssignableFrom(i) && i != typeof(IDbSession)) ??
                                            dbSession.GetType())
                                    .GetProperties()
                                    .FirstOrDefault(p => p.PropertyType.IsGenericType && p.PropertyType.GenericTypeArguments[0] == entityType);
                                if (prop != null)
                                    collection =
                                        (prop.GetValue(dbSession) as IEnumerable<Entity<int>>).ToList()
                                            .AsQueryable()
                                            .Select(selector)
                                            .ToList();
                                
                            }
                        }
                       
                    }
                   
                    if (collection == null)
                    {
                        collection =
                            Activator.CreateInstance(typeof(List<>).MakeGenericType(requestFieldAttr.SourceType)) as
                                IEnumerable<SimpleEntity>;
                    }
                }
                else
                {
                    collection = new List<SimpleEntity>();
                }
            }
            else
            {
                collection = collectionProperty.GetValue(dbSession) as IEnumerable<SimpleEntity>;
            }



            var selectorAttrs = Attribute.GetCustomAttributes(property, typeof(FieldSelectorAttribute)).Cast<FieldSelectorAttribute>().ToList();
            var rules = selectorAttrs.Select(s => _ruleBuilder.BuildRule(s.RuleType, s.Priority)).ToList();

            var restrictors = new List<IFieldSourceRestrictor<FieldAttributeBase>>();

            var createStrategy = (typeof(Array).IsAssignableFrom(property.PropertyType))
                                     ? (() => new MultipleFieldRepository())
                                     : fieldBasedOnRepoSource ? new Func<FieldRepository>(() => new SingleFieldBasedOnEntityRepository(converter))
                                     : () => new SingleFieldRepository();

            var instance = new RequestFieldBatler<T>(property, collection, restrictors,
                rules.OrderByDescending(r => r.Priority), engine, createStrategy)
            {
                SourceTypeProperty = sourceTypeProperty
            };

            // todo Ремезов: проверить этот факт тестами, чтобы ограничители могли опираться только при наличии RequestFieldAttribute
            foreach (var attribute in fieldAttrs.OrderByDescending(a => a.AttributePriority).ToList())
            {
                var restrictor = _restrictorFactory(attribute, instance, OnRegister);
                if (restrictor == null) continue;

                instance.Restrictors.Add(restrictor);
                OnNewRestrictor<T>(new NewRestrictorEventArgs(restrictor));
            }

            // добавляем к полю со списочным источником данных правило отбора предыдущего значения
            if (requestFieldAttr != null && !rules.Any(r => r is PreviousRestoreSelectionRule<IRequest>))
            {
                // todo Ремезов: убрать или придумать, как сделать по другому
                rules.Add(new PreviousRestoreSelectionRule<IRequest>(instance));
            }
            rules.Add(_singleHavingRule);

            IDictionary<string, IRequestFieldBatler> batlerDic = null;
            if (!_registeredBatlers.TryGetValue(typeof(T), out batlerDic))
            {
                batlerDic = new Dictionary<string, IRequestFieldBatler>();
            }
            batlerDic.Add(property.Name, instance);
            //var batlerDic = _registeredBatlers[typeof (T)];
            return instance;
        }
    }

    public class CalculatorNotFoundException : Exception
    {
        public Type RequestType { get; private set; }

        public CalculatorNotFoundException(string message, Type requestType)
            : base(message)
        {
            RequestType = requestType;
        }
    }
}