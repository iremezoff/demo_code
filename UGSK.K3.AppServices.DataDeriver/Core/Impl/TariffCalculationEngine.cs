﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Core.Impl
{
    public class TariffCalculationEngine<T> : ITariffCalculationEngine<T> where T : IRequest
    {
        public Type RequestType
        {
            get { return typeof(T); }
        }

        private readonly Dictionary<string, IRequestFieldBatler> _fieldsByName = new Dictionary<string, IRequestFieldBatler>();
        private readonly Dictionary<Type, IRequestFieldBatler> _fieldsByType = new Dictionary<Type, IRequestFieldBatler>();

        private readonly Dictionary<IRequestFieldBatler, List<IRequestFieldBatler>> _dependenceFields = new Dictionary<IRequestFieldBatler, List<IRequestFieldBatler>>();

        public IEnumerable<IRequestFieldBatler> GetDependences(IRequestFieldBatler batler)
        {
            List<IRequestFieldBatler> dependences;
            if (_dependenceFields.TryGetValue(batler, out dependences))
            {
                return dependences;
            };
            return Enumerable.Empty<IRequestFieldBatler>();
        }

        public IRequestFieldBatler this[string fieldName]
        {
            get
            {
                IRequestFieldBatler batler;
                return !_fieldsByName.TryGetValue(fieldName, out batler)
                    ? null
                    : batler;
            }
        }

        public IRequestFieldBatler this[Type entityType]
        {
            get
            {
                IRequestFieldBatler batler;
                return !_fieldsByType.TryGetValue(entityType, out batler)
                    ? null
                    : batler;
            }
        }

        public IEnumerable<IRequestFieldBatler> GetAllFields()
        {
            return _fieldsByName.Values;
        }

        public ITypeBuilder TypeBuilder { get; private set; }

        public TariffCalculationEngine(IBatlerFactory batlerFactory, ITypeBuilder typeBuilder)
        {
            TypeBuilder = typeBuilder;

            foreach (var property in typeof(T).GetProperties().Where(p => !p.IsDefined(typeof(NonFieldAttribute), false)))
            {
                var batler = batlerFactory.MakeFieldBatler(property, this);
                _fieldsByName.Add(property.Name, batler);

                foreach (var dependent in batler.ConcernedFieldBatlers)
                {
                    UpdateDependences(dependent, batler);
                }

                batler.ConcernedFieldBatlers.CollectionChanged += (s, e) => ConcernedFieldBatlersOnCollectionChanged(batler, e);

                if (batler.SourceType != typeof(SimpleEntity) && (!batler.SourceType.IsGenericType || batler.SourceType.IsGenericType && batler.SourceType.GetGenericTypeDefinition() != typeof(GenericSimpleEntity<>)))
                    _fieldsByType[batler.SourceType] = batler;
            }
        }

        private void UpdateDependences(IRequestFieldBatler dependent, IRequestFieldBatler batler)
        {
            List<IRequestFieldBatler> dependences;
            if (!_dependenceFields.TryGetValue(dependent, out dependences))
            {
                dependences = new List<IRequestFieldBatler>();
                _dependenceFields.Add(dependent, dependences);
            }
            if (!dependences.Contains(batler))
                dependences.Add(batler);
        }


        private void ConcernedFieldBatlersOnCollectionChanged(IRequestFieldBatler dependence, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add) return;
            foreach (var dependent in e.NewItems.OfType<IRequestFieldBatler>())
            {
                UpdateDependences(dependent, dependence);
            }
        }
    }
}
