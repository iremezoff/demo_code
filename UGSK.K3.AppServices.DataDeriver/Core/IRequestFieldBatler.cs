﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;
using UGSK.K3.AppServices.DataDeriver.Product.Field;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Core
{
    public interface IRequestFieldBatler
    {
        PropertyInfo FieldProperty { get; }
        Type SourceType { get; }
        bool IsIndependent { get; set; }
        Type RequestType { get; }
        ObservableCollection<IRequestFieldBatler> ConcernedFieldBatlers { get; }
        ITariffCalculationEngine<IRequest> Engine { get; }
        IList<IFieldSourceRestrictor<FieldAttributeBase>> Restrictors { get; }
        string SourceTypeProperty { get; }

        // todo Ремезов: переделать на коллекцию без вызова исключения в случае отсутствия элемента в словаре
        FieldRepository CreateFieldRepository(IRequest request);
    }
}