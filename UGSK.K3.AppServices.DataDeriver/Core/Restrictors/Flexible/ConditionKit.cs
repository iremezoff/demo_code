﻿using System;

namespace UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Flexible
{
    public class ConditionKit
    {
        public object FlexConditionInstance { get; private set; }
        public Type RequestType { get; private set; }
        public Type EntityType { get; private set; }

        public ConditionKit(Type requestType, Type entityType, object instance)
        {
            RequestType = requestType;
            EntityType = entityType;
            FlexConditionInstance = instance;
        }
    }
}