﻿using System;
using System.Linq;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Flexible.Impl
{
    public class FlexConditionKitFactory : IFlexConditionKitFactory
    {
        private Func<Type, object> _funcFactory;

        public FlexConditionKitFactory(Func<Type, object> funcFactory)
        {
            _funcFactory = funcFactory;
        }

        public ConditionKit MakeConditionByType(Type type)
        {
            var specRestrInterface = type.GetInterfaces()
                                         .FirstOrDefault(t => t.IsGenericType && t.GetGenericTypeDefinition() == typeof(IFlexCondition<,>));

            if (specRestrInterface == null)
            {
                throw new ArgumentException(string.Format("Тип {0} не реализует интерфейс {1}, поэтому не может выступать в качестве спецификации", type.Name, typeof(IFlexCondition<,>).Name));
            }

            var instance = _funcFactory(type);

            var entityType = specRestrInterface.GetGenericArguments()[1];
            var requestType = specRestrInterface.GetGenericArguments()[0];

            var specKit = new ConditionKit(requestType, entityType, instance);
            return specKit;
        }
    }
}