﻿using System;

namespace UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Flexible
{
    public interface IFlexConditionKitFactory
    {
        ConditionKit MakeConditionByType(Type type);
    }
}