﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Flexible;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Impl
{
    public class FlexibleFieldSourceRestrictor : IFieldSourceRestrictor<FlexibleFieldAttribute>
    {
        public FlexibleFieldAttribute Attribute { get; private set; }
        public IRequestFieldBatler Owner { get; private set; }

        private readonly Func<IRequest, LambdaExpression> _satisfiedDelegate;
        private readonly ParameterExpression _paramExpr = Expression.Parameter(typeof(SimpleEntity));
        private readonly ConditionKit _conditionKit;

        public FlexibleFieldSourceRestrictor(FlexibleFieldAttribute attribute, IRequestFieldBatler owner, IFlexConditionKitFactory specFactory, Action<EventHandler<NewRestrictorEventArgs>, Type> registrator)
        {
            Attribute = attribute;
            Owner = owner;
            _conditionKit = specFactory.MakeConditionByType(attribute.SpecificationType);

            var perRequestConditionMethod = attribute.SpecificationType.GetTypeInfo().GetMethod("GetFlexConditionPerRequest");

            var satisfiedMethod = attribute.SpecificationType.GetTypeInfo().GetMethod("IsSatisfiedBy");

            var reqParamExpr = Expression.Parameter(typeof(IRequest));

            // Для тех FlexCondition, которые предоставляют codition на каждый из запросов, а не существуют как singleton
            if (perRequestConditionMethod != null)
            {
                var methodCallExpr =
                    Expression.Call(
                        Expression.TypeAs(Expression.Call(Expression.Constant(_conditionKit.FlexConditionInstance),
                            perRequestConditionMethod,
                            new Expression[] { Expression.TypeAs(reqParamExpr, _conditionKit.RequestType) }), attribute.SpecificationType),
                        satisfiedMethod,
                        new Expression[] { Expression.TypeAs(reqParamExpr, _conditionKit.RequestType) });

                _satisfiedDelegate =
                    Expression.Lambda<Func<IRequest, LambdaExpression>>(methodCallExpr, reqParamExpr).Compile();
            }
            else
            {
                var methodCallExpr = Expression.Call(Expression.Constant(_conditionKit.FlexConditionInstance),
                    satisfiedMethod,
                    new Expression[] { Expression.TypeAs(reqParamExpr, _conditionKit.RequestType) });

                _satisfiedDelegate =
                    Expression.Lambda<Func<IRequest, LambdaExpression>>(methodCallExpr, reqParamExpr).Compile();
            }

            registrator(OnRestrictorMade, owner.RequestType);
        }

        private void OnRestrictorMade(object sender, NewRestrictorEventArgs newRestrictorEventArgs)
        {
            if (Attribute.ObservedFields == null ||
                !Attribute.ObservedFields.Any(f => f.Equals(newRestrictorEventArgs.NewRestrictor.Owner.FieldProperty.Name)))
                return;
            newRestrictorEventArgs.NewRestrictor.Owner.ConcernedFieldBatlers.Add(this.Owner);
            this.Owner.IsIndependent = !Attribute.Strict && this.Owner.IsIndependent;
        }

        public IEnumerable<SimpleEntity> GetRestrictedSource(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            //var condition = _satisfiedDelegate.Invoke(_conditionKit.FlexConditionInstance, new object[] { request }) as LambdaExpression;
            var condition = _satisfiedDelegate(request);

            // todo Ремезов: вызывается тело лямбды как метода, но при возможной трансляции в sql метода быть не может, отчего каждый из экземпляров внутри тела
            // необходимо преобразовать к требуемого типу либо изменить инетрфейс IFlexCondition, чтобы он возвращал лямбду с входным параметров SimpleEntity, 
            // а все преобразования типа ложились на плечи этой реализации IFlexCondition
            var invokeExpr = Expression.Invoke(condition, Expression.TypeAs(_paramExpr, _conditionKit.EntityType));
            var lambda = Expression.Lambda<Func<SimpleEntity, bool>>(invokeExpr, _paramExpr);

            return sourceCollection.AsQueryable().Where(lambda);
        }
    }
}