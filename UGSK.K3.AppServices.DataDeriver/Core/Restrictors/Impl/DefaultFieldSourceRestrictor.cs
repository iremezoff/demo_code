﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Impl
{
    /// <summary>
    /// Ограничитель выборки элементов коллекции тарифного справочника по зависимостям от родительского поля, т.е. по отношению "зависимое-зависящее поле"
    /// </summary>
    public class DefaultFieldSourceRestrictor : IFieldSourceRestrictor<RequestFieldAttribute>
    {
        private readonly Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>> _converter =
            (original, request) => original.Select(i => new KeyValuePair<object, bool>(i.Key, i.Value));

        public RequestFieldAttribute Attribute { get; private set; }

        public IRequestFieldBatler Owner { get; private set; }

        private readonly IList<DefaultFieldSourceRestrictor> _dependenceRestrictors; // parent

        private readonly ParameterExpression _paramExpr = Expression.Parameter(typeof(SimpleEntity));

        public DefaultFieldSourceRestrictor(RequestFieldAttribute attribute, IRequestFieldBatler owner,
            Action<EventHandler<NewRestrictorEventArgs>, Type> registrator,
            Func<Type, IFieldValueConverter> converterFactory)
        {
            Attribute = attribute;
            Owner = owner;
            _dependenceRestrictors = new List<DefaultFieldSourceRestrictor>();

            if (Attribute.Converter != null && typeof (IFieldValueConverter).IsAssignableFrom(Attribute.Converter))
            {
                var converterInstance = converterFactory(Attribute.Converter);
                _converter = converterInstance.Convert;
                Owner.Engine.TypeBuilder.RegisterType(Owner.FieldProperty, converterInstance.SourceType, converterInstance.ExpectedType, _converter);
            }
            else
            {
                Owner.Engine.TypeBuilder.RegisterType(Owner.FieldProperty, attribute.SourceType, attribute.SourceType, _converter);
            }
            
            Owner.Engine.TypeBuilder.RegisterProperty(Owner.FieldProperty, Attribute.ValuePropertyName, Owner.FieldProperty.Name);
            registrator(OnRestrictorMade, owner.RequestType);
        }

        private void OnRestrictorMade(object sender, NewRestrictorEventArgs args)
        {
            var newDefaultRestrictor = args.NewRestrictor as DefaultFieldSourceRestrictor;
            // если ограничитель не того типа или в списке зависимостей его поля отсутствует название поля (field) текущего ограничителя
            if (newDefaultRestrictor == null || newDefaultRestrictor == this)
            {
                return;
            }
            if (newDefaultRestrictor.Attribute.DependsOnFields != null && newDefaultRestrictor.Attribute.DependsOnFields.Any(d => d.Equals(Owner.FieldProperty.Name)))
            {
                newDefaultRestrictor._dependenceRestrictors.Add(this);
                Owner.ConcernedFieldBatlers.Add(newDefaultRestrictor.Owner);
                newDefaultRestrictor.Owner.IsIndependent = false;
            }
            else if (Attribute.DependsOnFields != null && Attribute.DependsOnFields.Any(d => d.Equals(newDefaultRestrictor.Owner.FieldProperty.Name)))
            {
                _dependenceRestrictors.Add(newDefaultRestrictor); // запоминаем одного из родителей
                newDefaultRestrictor.Owner.ConcernedFieldBatlers.Add(Owner);
                Owner.IsIndependent = false;
            }
        }

        public IEnumerable<SimpleEntity> GetRestrictedSource(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            Expression filterExpression = FilterExpression(GetDependenceFieldBatlers(), request);

            Expression<Func<SimpleEntity, bool>> lambda = Expression.Lambda<Func<SimpleEntity, bool>>(filterExpression, _paramExpr);

            Expression<Func<SimpleEntity, object>> groupSelector =
                Expression.Lambda<Func<SimpleEntity, object>>(Expression.Convert(Expression.Property(Expression.TypeAs(_paramExpr, Attribute.SourceType),
                                                                                  Attribute.ValuePropertyName), typeof(object)), _paramExpr);

            var restrictedColl = sourceCollection.AsQueryable().Where(lambda);
            if (!Attribute.DoNotRequireGroup)
            {
                restrictedColl = restrictedColl.GroupBy(groupSelector, (i, g) => g.First());
            }
            return restrictedColl;
        }

        private IEnumerable<IRequestFieldBatler> GetDependenceFieldBatlers()
        {
            return _dependenceRestrictors.Select(d => d.Owner);
        }

        protected Expression FilterExpression(IEnumerable<IRequestFieldBatler> parentFieldBatlers, IRequest request)
        {
            Expression filterExpression = null;
            if (!parentFieldBatlers.Any())
            {
                filterExpression = Expression.Constant(true);
            }
            else
            {
                foreach (var fieldBatler in parentFieldBatlers)
                {
                    var parentValue = fieldBatler.FieldProperty.GetValue(request);
                    if (parentValue == null)
                    {
                        filterExpression = Expression.Constant(false);
                        break;
                    }

                    Func<Expression, Expression, Expression> compareDelegate = parentValue is string
                        ? (Func<Expression, Expression, Expression>)StringComparisonExpression
                        : (Func<Expression, Expression, Expression>)ExplicitComparisonExpression;

                    Expression filter =
                        compareDelegate(
                            Expression.Property(Expression.TypeAs(_paramExpr, Owner.SourceType),
                                fieldBatler.FieldProperty.Name), Expression.Constant(parentValue));
                    filterExpression = filterExpression == null
                                           ? filter
                                           : Expression.AndAlso(filterExpression, filter);
                }
            }
            return filterExpression;
        }

        private static Expression StringComparisonExpression(Expression leftOperand, Expression rightOperand)
        {
            return Expression.Call(leftOperand, StringComparisonMethod, rightOperand,
                Expression.Constant(StringComparison.InvariantCultureIgnoreCase));
        }

        private static Expression ExplicitComparisonExpression(Expression leftOperand, Expression rightOperand)
        {
            return Expression.Equal(leftOperand, rightOperand);
        }

        private static readonly MethodInfo StringComparisonMethod = typeof(string).GetMethod("Equals", new[] { typeof(string), typeof(StringComparison) });
    }
}