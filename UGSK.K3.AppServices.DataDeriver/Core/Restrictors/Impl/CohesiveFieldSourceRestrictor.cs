﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Impl
{
    public class CohesiveFieldSourceRestrictor : IFieldSourceRestrictor<CohesiveFieldAttribute>
    {
        public CohesiveFieldAttribute Attribute { get; private set; }

        public IRequestFieldBatler Owner { get; private set; }

        private readonly IList<CohesiveFieldSourceRestrictor> _aheadRestrictors;

        private readonly ParameterExpression _paramExpr = Expression.Parameter(typeof(IHDBK));

        private readonly IEnumerable<IHDBK> _restrictorCollection;

        private readonly SortedList<int, CohesiveFieldSourceRestrictor> _groupedRestrictors;

        public CohesiveFieldSourceRestrictor(CohesiveFieldAttribute attribute, IRequestFieldBatler owner,
            Func<Type, IEnumerable<IHDBK>> restrictorCollFactory,
            Action<EventHandler<NewRestrictorEventArgs>, Type> registrator)
        {
            Attribute = attribute;
            _aheadRestrictors = new List<CohesiveFieldSourceRestrictor>();
            _restrictorCollection = restrictorCollFactory(Attribute.CohesiveSourceType);
            _groupedRestrictors = new SortedList<int, CohesiveFieldSourceRestrictor>() { { Attribute.Order, this } };
            registrator(OnRestrictorMade, owner.RequestType);
            Owner = owner;
            if (!string.IsNullOrEmpty(Attribute.GrouperPropertyName))
            {
                Owner.Engine.TypeBuilder.RegisterType(owner.FieldProperty, Owner.SourceType,
                    Attribute.CohesiveSourceType, OnConverter);
                Owner.Engine.TypeBuilder.RegisterProperty(owner.FieldProperty, Owner.SourceType.Name,
                    Owner.FieldProperty.Name);
                Owner.Engine.TypeBuilder.RegisterProperty(owner.FieldProperty, Attribute.GrouperPropertyName,
                    Attribute.GrouperPropertyName);
            }
        }

        // todo Ремезов: объединить c GetRestrictedSource
        private IEnumerable<KeyValuePair<object, bool>> OnConverter(IDictionary<SimpleEntity, bool> original, IRequest request)
        {
            var filterExpr = FilterExpression(request);
            filterExpr = filterExpr ?? Expression.Constant(true);

            Expression<Func<IHDBK, bool>> lambdaFilter = Expression.Lambda<Func<IHDBK, bool>>(filterExpr, _paramExpr);

            var kvParam = Expression.Parameter(typeof(KeyValuePair<IHDBK, bool>));

            Expression<Func<IHDBK, string>> restrictSelector = CreateSelector(Owner.SourceType.Name);// todo Ремезов: отборка идёт только по string-полям, что исключает выборку по полям другого типа

            var muttableRestrictorCollection = _restrictorCollection.AsQueryable().Where(lambdaFilter);

            // сортировка по группирующему элементу, как правило для HDBK это номер, если конечно он присутствует
            if (!string.IsNullOrEmpty(Attribute.GrouperPropertyName) && !Attribute.DoNotSort)
            {
                var orderSelector = CreateSelector(Attribute.GrouperPropertyName);
                muttableRestrictorCollection = muttableRestrictorCollection.OrderBy(orderSelector);
            }

            var groupSelector = Expression.Lambda<Func<KeyValuePair<IHDBK, bool>, string>>(
                Expression.Property(
                    Expression.TypeAs(Expression.Property(kvParam, "Key"), Attribute.CohesiveSourceType),
                    Attribute.GrouperPropertyName), kvParam);

            var restrictedCollection = muttableRestrictorCollection.Where(lambdaFilter)
                .Join(original.AsQueryable(), restrictSelector, si => si.Key.Name,
                    (ri, si) => new KeyValuePair<IHDBK, bool>(ri, si.Value), StringComparer.InvariantCultureIgnoreCase)
                .GroupBy(groupSelector, (k, g) => g.First())
                .Select(i => new KeyValuePair<object, bool>(i.Key, i.Value));

            //var restrictedCollection = original.AsQueryable()
            //    .Join(_restrictorCollection.AsQueryable().Where(lambdaFilter), si => si.Key.Name, restrictSelector,
            //        (si, ri) => new KeyValuePair<IHDBK, bool>(ri, si.Value), StringComparer.InvariantCultureIgnoreCase)
            //    .GroupBy(groupSelector, (k, g) => g.First())
            //    .Select(i => new KeyValuePair<object, bool>(i.Key, i.Value));

            return restrictedCollection;
        }

        private Expression<Func<IHDBK, string>> CreateSelector(string propertyName)
        {
            return Expression.Lambda<Func<IHDBK, string>>(
                    Expression.Property(Expression.TypeAs(_paramExpr, Attribute.CohesiveSourceType),
                                        propertyName), _paramExpr);
        }

        private void OnRestrictorMade(object sender, NewRestrictorEventArgs args)
        {
            var newCohesiveRestrictor = args.NewRestrictor as CohesiveFieldSourceRestrictor;
            // если ограничитель не того типа или принадлежат разным ограничителям или это тот же самый
            if (newCohesiveRestrictor == null || newCohesiveRestrictor.Attribute.CohesiveSourceType != this.Attribute.CohesiveSourceType || newCohesiveRestrictor == this)
            {
                return;
            }

            // 1
            if (newCohesiveRestrictor.Attribute.Order > this.Attribute.Order)
            {
                OnGreaterCurrent(newCohesiveRestrictor);
            }
            // 2
            else if (newCohesiveRestrictor.Attribute.Order < this.Attribute.Order)
            {
                OnLessCurrent(newCohesiveRestrictor);
            }

            // A
            if (!string.IsNullOrEmpty(newCohesiveRestrictor.Attribute.GrouperPropertyName) &&
                newCohesiveRestrictor.Attribute.GrouperPropertyName.Equals(this.Attribute.GrouperPropertyName))
            {
                newCohesiveRestrictor._groupedRestrictors.Add(this.Attribute.Order, this);
                this._groupedRestrictors.Add(newCohesiveRestrictor.Attribute.Order, newCohesiveRestrictor);

                // non-A
                // обмениваемся свойствами между полями, входящими в одну группу, чтобы возвращаемый тип включал поля выборку по соседствующему полю в группе (неведущее поле хоть и получит свойство, никогда им не воспользуются, т.к. по нему выборка не пройдет)
                Owner.Engine.TypeBuilder.RegisterProperty(Owner.FieldProperty, newCohesiveRestrictor.Owner.SourceType.Name, newCohesiveRestrictor.Owner.FieldProperty.Name);
                newCohesiveRestrictor.Owner.Engine.TypeBuilder.RegisterProperty(newCohesiveRestrictor.Owner.FieldProperty, this.Owner.SourceType.Name, this.Owner.FieldProperty.Name);
            }
        }

        private void OnGreaterCurrent(CohesiveFieldSourceRestrictor newCohesiveRestrictor)
        {
            newCohesiveRestrictor.Owner.IsIndependent = false;
            newCohesiveRestrictor._aheadRestrictors.Add(this);

            // 1.1
            if (newCohesiveRestrictor.Attribute.Order - this.Attribute.Order == 1)
            {
                // 1.1.1
                if (string.IsNullOrEmpty(newCohesiveRestrictor.Attribute.GrouperPropertyName) &&
                    !string.IsNullOrEmpty(this.Attribute.GrouperPropertyName))
                {
                    // первый элемент в группе, с минимальным Order, является минимальным
                    this._groupedRestrictors.First().Value.Owner.ConcernedFieldBatlers.Add(newCohesiveRestrictor.Owner);
                }
                else if (!string.IsNullOrEmpty(newCohesiveRestrictor.Attribute.GrouperPropertyName) &&
                         !string.IsNullOrEmpty(this.Attribute.GrouperPropertyName) &&
                    // todo Ремезов: этот кейс не включен. необходимо включить     
                    !newCohesiveRestrictor.Attribute.GrouperPropertyName.Equals(this.Attribute.GrouperPropertyName))
                {
                    var firstOnGroupOfNew = newCohesiveRestrictor._groupedRestrictors.FirstOrDefault().Value ?? newCohesiveRestrictor;
                    this._groupedRestrictors.First().Value.Owner.ConcernedFieldBatlers.Add(firstOnGroupOfNew.Owner);
                }
                // 1.1.2
                else if (string.IsNullOrEmpty(this.Attribute.GrouperPropertyName))
                {
                    this.Owner.ConcernedFieldBatlers.Add(newCohesiveRestrictor.Owner);
                }
            }
        }

        private void OnLessCurrent(CohesiveFieldSourceRestrictor newCohesiveRestrictor)
        {
            this.Owner.IsIndependent = false;
            this._aheadRestrictors.Add(newCohesiveRestrictor);

            // 2.1
            if (!string.IsNullOrEmpty(newCohesiveRestrictor.Attribute.GrouperPropertyName) &&
                newCohesiveRestrictor.Attribute.GrouperPropertyName.Equals(this.Attribute.GrouperPropertyName))
            {
                foreach (var fieldBatler in this.Owner.ConcernedFieldBatlers)
                {
                    newCohesiveRestrictor.Owner.ConcernedFieldBatlers.Add(fieldBatler);
                }
                //this.Owner.ConcernedFieldBatlers.Clear();
            }
            // 2.2
            else if (this.Attribute.Order - newCohesiveRestrictor.Attribute.Order == 1)
            {
                newCohesiveRestrictor.Owner.ConcernedFieldBatlers.Add(this.Owner);
            }
        }

        public IEnumerable<SimpleEntity> GetRestrictedSource(IEnumerable<SimpleEntity> sourceCollection, IRequest request)
        {
            var filterExpression = FilterExpression(request);

            filterExpression = filterExpression ?? Expression.Constant(true);

            Expression<Func<IHDBK, bool>> lambdaFilter = Expression.Lambda<Func<IHDBK, bool>>(filterExpression, _paramExpr);

            Expression<Func<IHDBK, string>> restrictSelector = CreateSelector(Owner.SourceType.Name); // todo Ремезов: отборка идёт только по string-полям, что исключает выборку по полям другого типа

            var muttableRestrictorCollection = _restrictorCollection.AsQueryable().Where(lambdaFilter);

            // сортировка по группирующему элементу, как правило для HDBK это номер, если конечно он присутствует
            if (!string.IsNullOrEmpty(Attribute.GrouperPropertyName))
            {
                Expression<Func<IHDBK, string>> orderSelector = CreateSelector(Attribute.GrouperPropertyName);
                muttableRestrictorCollection = muttableRestrictorCollection.OrderBy(orderSelector);
            }

            var restrictedCollection = muttableRestrictorCollection
                .Join(sourceCollection.AsQueryable(), restrictSelector, si => si.Name,
                    (ri, si) => si, StringComparer.InvariantCultureIgnoreCase)
                .GroupBy(k => k.Name, (k, g) => g.First());
            //.Join(_restrictorCollection.AsQueryable().Where(lambdaFilter), si => si.Name, restrictSelector,
            //      (si, ri) => si, StringComparer.InvariantCultureIgnoreCase)
            //.GroupBy(k => k.Name, (k, g) => g.First());
            return restrictedCollection;
        }

        private Expression FilterExpression(IRequest request)
        {
            // todo Ремезов: вызвать ошибку, если производится отбор в поле, которое не является ведущим в связке
            Expression filterExpression = null;
            if (_aheadRestrictors.Any())
            {
                // todo Ремезов: проверить возможность объединения с DefaultFieldSourceRestrictor.FilterExpression
                foreach (var adjacentField in _aheadRestrictors)
                {
                    var adjacentValue = adjacentField.Owner.FieldProperty.GetValue(request);
                    var filter =
                        Expression.Equal(
                            Expression.Property(Expression.TypeAs(_paramExpr, Attribute.CohesiveSourceType), adjacentField.Owner.SourceType.Name),
                            Expression.Constant(adjacentValue));
                    filterExpression = filterExpression == null
                                           ? filter
                                           : Expression.AndAlso(filterExpression, filter);
                }
            }
            return filterExpression;
        }
    }
}