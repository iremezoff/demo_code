﻿using System.Collections.Generic;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Core.Restrictors
{
    public interface IFieldSourceRestrictor<out T> where T : FieldAttributeBase
    {
        T Attribute { get; }
        IRequestFieldBatler Owner { get; }
        IEnumerable<SimpleEntity> GetRestrictedSource(IEnumerable<SimpleEntity> sourceCollection, IRequest request);
    }
}