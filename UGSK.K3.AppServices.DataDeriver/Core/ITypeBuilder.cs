﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Core
{
    public interface ITypeBuilder
    {
        void RegisterType(PropertyInfo fieldName, Type entityType, Type returnType, Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>> converter);
        void RegisterProperty(PropertyInfo entityProperty, string originalProperty, string expectedProperty);
    }
}