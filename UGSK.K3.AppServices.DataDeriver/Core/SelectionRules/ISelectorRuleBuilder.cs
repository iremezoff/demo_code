﻿using System;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Core.SelectionRules
{
    public interface ISelectorRuleBuilder
    {
        ISelectionRule<IRequest> BuildRule(Type ruleType, int priority);
    }
}