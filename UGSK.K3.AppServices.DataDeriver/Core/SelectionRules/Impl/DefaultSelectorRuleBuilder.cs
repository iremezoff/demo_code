﻿using System;
using System.Linq;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Core.SelectionRules.Impl
{
    public class DefaultSelectorRuleBuilder : ISelectorRuleBuilder
    {
        private readonly Func<Type, int, ISelectionRule<IRequest>> _funcFactory;

        public DefaultSelectorRuleBuilder(Func<Type, int, ISelectionRule<IRequest>> funcFactory)
        {
            _funcFactory = funcFactory;
        }

        public ISelectionRule<IRequest> BuildRule(Type ruleType, int priority)
        {
            var ruleInterface =
                ruleType.GetInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(ISelectionRule<>));
            if (ruleInterface == null)
            {
                throw new ArgumentException(string.Format("Тип правила отбора {0} не соответствует общеустановленному типу {1}", ruleType, typeof(ISelectionRule<IRequest>)), "ruleType");
            }
            var rule= _funcFactory(ruleType, priority);
            rule.Priority = priority;
            return rule;
        }
    }
}