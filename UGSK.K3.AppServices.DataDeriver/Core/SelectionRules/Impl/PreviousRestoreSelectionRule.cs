﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UGSK.K3.Infrastructure;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Core.SelectionRules.Impl
{
    // todo Ремезов: придумать, как сделать его синглтоном
    public class PreviousRestoreSelectionRule<T> : ISelectionRule<T> where T : IRequest
    {
        private readonly ParameterExpression _paramExpr = Expression.Parameter(typeof(SimpleEntity));
        private readonly Type _conversionType;

        public IRequestFieldBatler Owner
        {
            get;
            private set;
        }

        private int _priority = 5;

        public int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        public bool NeedStateChanging
        {
            get { return false; }
        }

        public PreviousRestoreSelectionRule(IRequestFieldBatler owner)
        {
            Owner = owner;
            //var attr = Owner.Restrictors.FirstOrDefault(r => r.Attribute != null && r.Attribute.GetType() == typeof(RequestFieldAttribute));
            //_selectedProperty = attr != null ? ((RequestFieldAttribute)attr.Attribute).ValuePropertyName : "Name";
            var sourceProperty = Owner.SourceType.GetProperty(owner.SourceTypeProperty);
            _conversionType = sourceProperty == null ? typeof(string) : sourceProperty.PropertyType;
            //_attribute = attribute;
        }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, T request)
        {
            return sourceCollection.ToDictionary(k => k, v => true);
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedValues)
        {
            return false;
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, T request)
        {
            var value = Enumerable.Empty<SimpleEntity>();

            if (Owner.SourceType != typeof(SimpleEntity) && !Owner.FieldProperty.PropertyType.IsArray) // todo Ремезов: не обрабратываются многозначимые значения
            {
                var fieldValue = Owner.FieldProperty.GetValue(request) != null
                    ? typeof(Entity<int>).IsAssignableFrom(Owner.FieldProperty.PropertyType)
                        ? Owner.FieldProperty.GetValue(request) as Entity<int>
                        : Convert.ChangeType(Owner.FieldProperty.GetValue(request), _conversionType)
                    : _conversionType.IsValueType
                        ? Activator.CreateInstance(_conversionType)
                        : null;

                // экспериментальный код для примтивных типов. Был написан после того, как по RM#9417 неправильно учитывалось значение "Новое ТС", формирующееся FieldRepository. В итоге выбор предыдущего значения происходит в NewVehicleSelectionRule
                //if (fieldValue != null && fieldValue.GetType().IsPrimitive)
                //{
                //    value = new[]
                //    {
                //        Activator.CreateInstance(typeof (GenericSimpleEntity<>).MakeGenericType(fieldValue.GetType()), fieldValue) as SimpleEntity
                //    };
                //}
                //else
                //{
                Func<Expression, Expression, Expression> compareDelegate = fieldValue is string
                    ? StringComparisonExpression
                    : fieldValue is DictionaryEntity
                        ? PlainComparisonExpression
                        : (Func<Expression, Expression, Expression>)ExplicitComparisonExpression;

                Expression filterExpression =
                    compareDelegate(
                        Expression.Property(Expression.TypeAs(_paramExpr, Owner.SourceType),
                            Owner.SourceType.GetProperty(Owner.SourceTypeProperty).Name),
                        Expression.Constant(fieldValue));

                //Expression filterExpression = Expression.Equal(Expression.Property(Expression.TypeAs(_paramExpr, Owner.SourceType),
                //                                                                   Owner.SourceType.GetProperty(Owner.SourceTypeProperty).Name),
                //                                               Expression.Constant(fieldValue));
                var oldValue = sourceCollection.AsQueryable()
                    .FirstOrDefault(Expression.Lambda<Func<SimpleEntity, bool>>(filterExpression, _paramExpr));
                if (oldValue != null)
                {
                    value = new[] { oldValue };
                }
                //}
            }
            //else if (Owner.SourceType != typeof(SimpleEntity) && Owner.FieldProperty.PropertyType.IsArray)
            //{
            //    var propertValue = Owner.FieldProperty.GetValue(request);
            //    var fieldValue = propertValue != null
            //        ? (propertValue as Array)
            //        : _conversionType.GetElementType().IsValueType
            //            ? Activator.CreateInstance(_conversionType)
            //            : null;

            //    if (fieldValue != null)
            //    {
            //        Expression filterExpression =
            //            Expression.Call(
            //                (typeof(Enumerable).GetMember("Contains", MemberTypes.Method,
            //                    BindingFlags.Static | BindingFlags.Public)
            //                    .First(m => (m as MethodInfo).GetParameters().Count() == 2) as MethodInfo)
            //                    .MakeGenericMethod(typeof(string)),
            //                Expression.Call(
            //                    typeof(Enumerable).GetMethod("OfType", BindingFlags.Static | BindingFlags.Public)
            //                        .MakeGenericMethod(typeof(string)),
            //                    Expression.TypeAs(Expression.Constant(fieldValue), typeof(IEnumerable<object>))),
            //                Expression.Property(Expression.TypeAs(_paramExpr, Owner.SourceType),
            //                    Owner.SourceType.GetProperty(Owner.SourceTypeProperty).Name));

            //        var oldValue = sourceCollection.AsQueryable()
            //            .Where(Expression.Lambda<Func<SimpleEntity, bool>>(filterExpression, _paramExpr));

            //        if (oldValue.Any())
            //            return oldValue;
            //        value = new[] { new ATS() { } }; // хак, говорящий о том, что если ПУСов выбрано не было, значит таков выбор пользователя, последующие SelectionRule отрабатывать не должны
            //    }
            //}
            return value;
        }

        private static Expression StringComparisonExpression(Expression leftOperand, Expression rightOperand)
        {
            return Expression.Call(leftOperand, _stringComparisonMethod, rightOperand,
                Expression.Constant(StringComparison.InvariantCultureIgnoreCase));
        }

        private static Expression PlainComparisonExpression(Expression leftOperand, Expression rightOperand)
        {
            return Expression.Call(leftOperand, _plainComparisonMethod, rightOperand);
        }

        private static Expression ExplicitComparisonExpression(Expression leftOperand, Expression rightOperand)
        {
            return
                Expression.Equal(
                    leftOperand,
                    rightOperand);
        }

        private static MethodInfo _stringComparisonMethod = typeof(string).GetMethod("Equals", new[] { typeof(string), typeof(StringComparison) });

        private static MethodInfo _plainComparisonMethod = typeof(object).GetMethod("Equals", new[] { typeof(object) });
    }
}