﻿using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Core.SelectionRules.Impl
{
    public class SingleHavingSelectionRule<T> : ISelectionRule<T> where T : IRequest
    {
        public int Priority
        {
            get { return 0; }
            set { }
        }

        public bool NeedStateChanging
        {
            get { return false; }
        }

        public IEnumerable<KeyValuePair<SimpleEntity, bool>> GetCollectionSourceWithAllowable(IEnumerable<SimpleEntity> sourceCollection, T request)
        {
            return sourceCollection.ToDictionary(k => k, v => true); // по идее, вообще не должен быть вызван
        }

        public bool GetGeneralDenyState(IRequest request, IEnumerable<SimpleEntity> selectedValues)
        {
            return false; // потому что это правило ничего не может заблокировать
        }

        public IEnumerable<SimpleEntity> GetAssignedValue(IEnumerable<SimpleEntity> sourceCollection, T request)
        {
            if (sourceCollection.Count() == 1)
            {
                return new[] { sourceCollection.First() };
            }
            return Enumerable.Empty<SimpleEntity>();
        }
    }
}