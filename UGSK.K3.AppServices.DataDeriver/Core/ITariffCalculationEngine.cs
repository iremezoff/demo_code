﻿using System;
using System.Collections.Generic;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Core
{
    public interface ITariffCalculationEngine<out T> where T : IRequest
    {
        Type RequestType { get; }
        //ICalculator Calculator { get; }
        IRequestFieldBatler this[string fieldName] { get; }
        IRequestFieldBatler this[Type entityType] { get; }
        IEnumerable<IRequestFieldBatler> GetAllFields();
        ITypeBuilder TypeBuilder { get; }
        IEnumerable<IRequestFieldBatler> GetDependences(IRequestFieldBatler batler);
    }
}