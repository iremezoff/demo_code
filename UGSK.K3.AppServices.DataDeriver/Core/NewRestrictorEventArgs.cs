﻿using System;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Core
{
    public class NewRestrictorEventArgs : EventArgs
    {
        public IFieldSourceRestrictor<FieldAttributeBase> NewRestrictor { get; private set; }

        public NewRestrictorEventArgs(IFieldSourceRestrictor<FieldAttributeBase> newRestrictor)
        {
            NewRestrictor = newRestrictor;
        }
    }
}