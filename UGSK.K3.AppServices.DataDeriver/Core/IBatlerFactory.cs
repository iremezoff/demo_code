﻿using System.Reflection;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.AppServices.DataDeriver.Core
{
    public interface IBatlerFactory
    {
        IRequestFieldBatler MakeFieldBatler<TRequest>(PropertyInfo property, ITariffCalculationEngine<TRequest> engine) where TRequest:IRequest;
    }
}