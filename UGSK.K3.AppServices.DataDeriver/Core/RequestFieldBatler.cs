﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;
using UGSK.K3.AppServices.DataDeriver.Product.Field;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;

namespace UGSK.K3.AppServices.DataDeriver.Core
{
    [DebuggerDisplay("Type={GetType()}, Field = {FieldProperty.Name}")]
    public class RequestFieldBatler<T> : IRequestFieldBatler where T : IRequest
    {
        private readonly Func<FieldRepository> _repositoryCreateStrategy;


        protected readonly IEnumerable<ISelectionRule<IRequest>> Rules;


        public IList<IFieldSourceRestrictor<FieldAttributeBase>> Restrictors { get; private set; }

        public ITariffCalculationEngine<IRequest> Engine { get; private set; }

        public PropertyInfo FieldProperty { get; internal set; }

        public Type SourceType { get; internal set; }

        public string SourceTypeProperty { get; internal set; }

        public bool IsIndependent { get; set; }

        public Type RequestType
        {
            get { return typeof(T); }
        }

        public ObservableCollection<IRequestFieldBatler> ConcernedFieldBatlers { get; private set; }

        public IEnumerable<SimpleEntity> CollectionSource { get; private set; }

        public RequestFieldBatler(PropertyInfo fieldProperty, IEnumerable<SimpleEntity> collection,
                                  IEnumerable<IFieldSourceRestrictor<FieldAttributeBase>> sourceRestrictors, IEnumerable<ISelectionRule<IRequest>> rules,
                                  ITariffCalculationEngine<T> engine, Func<FieldRepository> repositoryCreateStrategy)
        {
            // todo Ремезов: перенести инварианты в фабрику
            if (collection == null)
            {
                throw new ArgumentNullException("collection", "Источник данных не может быть не проинициализирован");
            }
            if (fieldProperty == null)
            {
                throw new ArgumentNullException("fieldProperty", "Отсутствует информация о поле запроса");
            }
            if (rules == null)
            {
                throw new ArgumentNullException("rules", "Отсутствует набор правил");
            }
            IsIndependent = true;
            _repositoryCreateStrategy = repositoryCreateStrategy;
            Restrictors = sourceRestrictors.ToList();
            Rules = rules;
            SourceType = collection.GetType().GenericTypeArguments[0]; // возьмётся оригинальный тип-наследник SimpleEntity
            // попытка определить более точный тип: если все элементы коллекции имеют один тип, то берем именно его
            if (SourceType == typeof(SimpleEntity) && collection.Any() && collection.GroupBy(i => i.GetType()).Count() == 1)
            {
                SourceType = collection.First().GetType();
            }
            FieldProperty = fieldProperty;
            CollectionSource = collection;
            ConcernedFieldBatlers = new ObservableCollection<IRequestFieldBatler>();
            ConcernedFieldBatlers.CollectionChanged += ConcernedFieldBatlers_CollectionChanged;
            Engine = (ITariffCalculationEngine<IRequest>)engine;
        }

        void ConcernedFieldBatlers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add)
                return;

            foreach (var batler in e.NewItems.OfType<IRequestFieldBatler>())
            {
                if (ConcernedFieldBatlers.Count(f => f == batler) <= 1)
                    return;
                //while (ConcernedFieldBatlers.Contains(batler))
                    ConcernedFieldBatlers.Remove(batler);
                //ConcernedFieldBatlers.Add(batler);
            }
        }

        protected IEnumerable<SimpleEntity> GetSourceCollection(IRequest request)
        {
            IEnumerable<SimpleEntity> originalSource = CollectionSource;
            if (Restrictors.Any())
            {
                foreach (var restrictor in Restrictors)
                {
                    originalSource = restrictor.GetRestrictedSource(originalSource, request);
                }
            }
            return originalSource;
        }

        public FieldRepository CreateFieldRepository(IRequest request)
        {
            var fieldRepository = _repositoryCreateStrategy();

            fieldRepository.Request = request;
            fieldRepository.Creator = this;
            fieldRepository.FieldName = FieldProperty.Name;

            var source = GetSourceCollection(request).ToList();
            var sourceWithStates = source.ToDictionary(k => k, v => true);

            bool isDenyFlag = false;
            IEnumerable<SimpleEntity> selectedValue = null;

            foreach (var rule in Rules)
            {
                if (rule.NeedStateChanging)
                {
                    var iter = rule.GetCollectionSourceWithAllowable(source, request);
                    // первый вариант
                    sourceWithStates = iter.Join(sourceWithStates, f => f.Key, s => s.Key,
                        (f, s) => new KeyValuePair<SimpleEntity, bool>(f.Key, f.Value & s.Value))
                        .ToDictionary(k => k.Key, v => v.Value); // todo Ремезов: может есть более подходящий способ мерджинга
                    
                }

                isDenyFlag |= rule.GetGeneralDenyState(request, selectedValue); // достаточно только одного условия, чтобы заблочить состояние

                if (selectedValue == null)
                {
                    var value = rule.GetAssignedValue(source, request);
                    if (value.Any())
                    {
                        selectedValue = value.ToArray();
                    }
                }
            }

            fieldRepository.CollectionSource = sourceWithStates;

            fieldRepository.IsDeny = isDenyFlag;

            fieldRepository.SetValue(selectedValue != null
                                         ? selectedValue.Select(v => v.GetType().GetProperty(SourceTypeProperty ?? "Name").GetValue(v))
                                         : new object[0]); // Ремезов todo нужны делегаты вместо рефлексии

            return fieldRepository;
        }
    }
}