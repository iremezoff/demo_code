﻿using System;

namespace UGSK.K3.Service.DTO.Shared
{
    [Flags]
    public enum FieldState
    {
        None = 0,
        Disabled = 0x1,
        Mutiple = 0x8,
        ListMutiple = 0xF
    }
}