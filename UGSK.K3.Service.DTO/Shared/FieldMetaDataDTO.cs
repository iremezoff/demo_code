﻿namespace UGSK.K3.Service.DTO.Shared
{
    public class FieldMetaDataDTO
    {
        public string FieldName { get; set; }

        public string FieldText { get; set; }

        public string Type { get; set; }

        public string Repository { get; set; }

        public string OutputFunction { get; set; }

        public DigestMetaDataDTO DigestMetaData { get; set; }
    }

    public class DigestMetaDataDTO
    {
        public string GroupName { get; set; }

        public int Order { get; set; }

        public bool Show { get; set; }
    }
}