﻿namespace UGSK.K3.Service.DTO.Shared
{
    public class PhoneNumberDTO
    {
        /// <summary>
        /// Телефонный номер
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Статус верификации
        /// <seealso cref="PhoneNumberStatus"/>
        /// 0 - не верифицирован
        /// 1 - верифицирован (#V)
        /// 2 - верифицирован, но закреплён за другим страхователем (#X)
        /// </summary>
        public int Status { get; set; }
    }
}