using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UGSK.K3.Service.DTO.Attributes;

namespace UGSK.K3.Service.DTO.Shared
{
    // Концентратор
    public class VehicleDocumentDTO
    {
        public int Id { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Вид документа ТС", GroupName = "Транспортное средство (ТС)")]
        public string VehicleDocumentType { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Серия", GroupName = "Транспортное средство (ТС)")]
        public string VehicleDocSerial { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Номер", GroupName = "Транспортное средство (ТС)")]
        public string VehicleDocNumber { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Дата выдачи", GroupName = "Транспортное средство (ТС)")]
        public DateTimeOffset? VehicleDocIssuredDate { get; set; }
    }
}
