﻿namespace UGSK.K3.Service.DTO.Shared
{
    public class FieldValue
    {
        public ValueItem[] CollectionSource { get; set; }
        public FieldState State { get; set; }

        public FieldValue()
        {

        }
        public FieldValue(ValueItem[] items, FieldState state)
        {
            CollectionSource = items;
            State = state;
        }
    }
}