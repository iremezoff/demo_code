﻿using System.Diagnostics;

namespace UGSK.K3.Service.DTO.Shared
{
    [DebuggerDisplay("{FieldName} - {Value[0].Value}")]
    public class FieldRepository
    {
        public string FieldName { get; set; }
        public FieldValue[] CollectionSource { get; set; }
        public ValueItem[] Value { get; set; }
        public FieldState State { get; set; }
    }
}