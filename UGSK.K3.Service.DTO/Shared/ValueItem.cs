﻿namespace UGSK.K3.Service.DTO.Shared
{
    public class ValueItem
    {
        public string FieldName { get; set; }
        public object Value { get; set; }

        public ValueItem()
        {

        }
        public ValueItem(string name, object value)
        {
            FieldName = name;
            Value = value;
        }
    }
}