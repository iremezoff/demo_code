using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UGSK.K3.Service.DTO.Auth
{
    [Serializable]
    public class UGSKUserContext
    {
        public UGSKUserContext()
        {
            AvailableFunctions = new List<string>();
        }

        public string Login { get; set; }
        public string AccessToken { get; set; }
        public string Name { get; set; }
        public string Filial { get; set; }
        public string Role { get; set; }
        public decimal KB { get; set; }
        public string SaleChannel { get; set; }
        public PermissionItem[] Permissions { get; set; }

        public bool IsImpersonated
        {
            get
            {
                return FakeUserContext != null;
            }
        }

        public bool CanBeImpersonated()
        {
            return Role == "Андеррайтер" || Role == "Куратор" || Role == "Куратор ГО";
        }

        public UGSKUserContext FakeUserContext
        {
            get;
            set;
        }

        public decimal MinUnderwriterFactor
        {
            get;
            set;
        }

        public decimal MaxUnderwriterFactor
        {
            get;
            set;
        }

        public Guid LocationFiasGuid { get; set; }


        public List<string> AvailableFunctions
        {
            get; 
            set;
        }
    }
}
