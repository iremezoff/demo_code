﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UGSK.K3.Service.DTO.Auth
{
    [Serializable]
    public class PermissionItem
    {
        public string Product { get; set; }
        public string Permission { get; set; }
    }
}
