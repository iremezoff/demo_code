﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using UGSK.K3.Service.DTO.Attributes;
using UGSK.K3.Service.DTO.Shared;

namespace UGSK.K3.Service.DTO
{
    public class ContractDTO // Осаго надо
    {
        [DisableAJAXCall]
        public int? Id { get; set; }

        [DisableAJAXCall]
        public List<CalculationRepository> Calculation { get; set; }

        [DisableAJAXCall]
        public decimal InsurancePremium { get; set; }

        // ------------------------------статус договора ---------------------------
        [DisableAJAXCall]
        [Display(Name = "Статус")]
        public int StatusId { get; set; }

        [DisableAJAXCall]
        [StringBehaviorInGroup("Статус договора: {0}", GroupName = "Параметры договора", Shown = true, Order = 0)]
        public string ContractStatusName { get; set; }

        // --------------------------- Параметры ТС ------------------------------
        [Display(Name = "Цель использования", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 0)]
        public string UsagePurpose { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Марка/Модель ТС", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 1)]
        public string VehicleModelSpelt { get; set; } // стринговое значение выбранное пользователем (выбранное из автокомплита)

        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = false, Order = 2)]
        public int VehicleModel { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Транспортное средство может быть использовано с прицепом", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Транспортное средство (ТС)", Shown = false, Order = 4)]
        public bool WithTrailer { get; set; }

        //[DisableAJAXCall]
        [Display(Name = "Сезонное использование", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Транспортное средство (ТС)", Shown = false, Order = 4)]
        public bool IsSeason { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Марка/Модель ТС на полисе", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 5)]
        public string UserDefinedMarkModel { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Государственный регистрационный знак", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 6)]
        public string VehicleLicensePlate { get; set; }

        [DisableAJAXCall]
        [Display(Name = "VIN", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 7)]
        public string VehicleVIN { get; set; }

        

        [DisableAJAXCall]
        [Display(Name = "Шасси (рама)№", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 12)]
        public string VehicleDocChassisNumber { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Кузов №", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 13)]
        public string VehicleDocBodyNumber { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Мощность двигателя, л.с.", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup("{0} л.с.", GroupName = "Параметры ТС", Shown = true, Order = 14)]
        public int? VehicleDocEnginePowerHP { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Мощность двигателя, КВт", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup("{0} КВт", GroupName = "Параметры ТС", Shown = true, Order = 15)]
        public int? VehicleDocEnginePowerKW { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Год изготовления", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup("{0} г.", GroupName = "Параметры ТС", Shown = true, Order = 16)]
        public int? VehicleYearMade { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Разрешённая максимальная масса", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup("{0} кг.", GroupName = "Параметры ТС", Shown = true, Order = 17)]
        public int? VehicleDocGrossVehicleWeight { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Количество пассажирских мест", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 18)]
        public int? VehicleDocCountPassengerSeat { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Дата очередного ТО", GroupName = "Транспортное средство (ТС)")]
        [DatetimeBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 19)]
        public DateTimeOffset? VehicleCheckupDate { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Документ, оформленный по результатам проведения технического осмотра", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 20)]
        public string VehicleCheckupDocumentType { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Тех. осмотр не требуется", GroupName = "Транспортное средство (ТС)")]
        [BooleanBehaviorInGroup("тех. осмотр не требуется", "требуется тех. осмотр", GroupName = "Параметры ТС", Shown = true, Order = 21)]
        public bool VehicleCheckupIsNotRequired { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Серия", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 22)]
        public string VehicleCheckupDocumentSerial { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Номер", GroupName = "Транспортное средство (ТС)")]
        [StringBehaviorInGroup(GroupName = "Параметры ТС", Shown = true, Order = 23)]
        public string VehicleCheckupDocumentNumber { get; set; }

        //----------------------------------------------------------------------------------------

        // ------------------------------   начало блока [Параметры договора]  ------------------
        [DisableAJAXCall]
        [Display(Name = "Префикс полиса", GroupName = "Период использования")]
        public string PolicyPrefix { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Серия полиса", GroupName = "Период использования")]
        public string PolicySerial { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Номер полиса", GroupName = "Период использования")]
        public string PolicyNumber { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Начало договора", GroupName = "Период использования")]
        [DatetimeBehaviorInGroup(GroupName = "Параметры договора", Shown = true, Order = 3)]
        public DateTimeOffset? ContractFrom { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Окончание договора", GroupName = "Период использования")]
        [DatetimeBehaviorInGroup(GroupName = "Параметры договора", Shown = true, Order = 4)]
        public DateTimeOffset? ContractTo { get; set; }

        [Display(Name = "Регистрация ТС", GroupName = "Период использования")]
        [StringBehaviorInGroup(GroupName = "Параметры договора", Shown = true, Order = 5)]
        public string VehicleRegistration { get; set; }

        [Display(Name = "Срок страхования")]
        [StringBehaviorInGroup(GroupName = "Параметры договора", Shown = true, Order = 6)]
        public string InsuranceTerm { get; set; }

        [Display(Name = "Период использования")]
        [StringBehaviorInGroup(GroupName = "Параметры договора", Shown = false, Order = 7)]
        public string UsagePeriod { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Наименование страховой компании", GroupName = "Данные о предыдущем договоре страхования")]
        [StringBehaviorInGroup(GroupName = "Параметры договора", Shown = true, Order = 8)]
        public string PreviousInsurerName { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Cерия полиса", GroupName = "Данные о предыдущем договоре страхования")]
        [StringBehaviorInGroup(GroupName = "Параметры договора", Shown = true, Order = 9)]
        public string PreviousPolicySerial { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Номер полиса", GroupName = "Данные о предыдущем договоре страхования")]
        [StringBehaviorInGroup(GroupName = "Параметры договора", Shown = true, Order = 10)]
        public string PreviousPolicyNumber { get; set; }

        [DisableAJAXCall]
        public int? PreviousContractId { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Особые отметки", GroupName = "Страховая премия")]
        [StringBehaviorInGroup(GroupName = "Параметры договора", Shown = true, Order = 20)]
        public string SpecialNote { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Документ №")]//[Display(Name = "Платёжный документ")] 
        [AdditionalMetadata("HideOnDisable", true)]
        [StringBehaviorInGroup("Документ №: {0}", GroupName = "Параметры договора", Shown = true, Order = 21)]
        public string PaymentDocument { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Квитанция А7 серия")]
        [StringBehaviorInGroup("Квитанция А7 серия: {0}", GroupName = "Параметры договора", Shown = true, Order = 22)]
        public string PaymentDocumentSerial { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Квитанция А7 номер")]
        [StringBehaviorInGroup("Квитанция А7 номер: {0}", GroupName = "Параметры договора", Shown = true, Order = 23)]
        public string PaymentDocumentNumber { get; set; }

        [Display(Name = "Вид оплаты")]
        [StringBehaviorInGroup("Вид оплаты: {0}", GroupName = "Параметры договора", Shown = true, Order = 20)]
        public string PaymentKind { get; set; }

        [DisableAJAXCall]
        public bool IsCash { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Дата платежа")]
        [DatetimeBehaviorInGroup("Дата платежа: {0}", GroupName = "Особые отметки и оплата договора", Shown = true, Order = 24)]
        public DateTimeOffset? DatePay { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Имеются грубые нарушения условий страхования", GroupName = "Страховая премия")]
        [StringBehaviorInGroup("Грубое нарушение страхования: {0}", GroupName = "Параметры договора", Shown = true, Order = 25)]
        public string AreViolationsExist { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Дата заключения договора", GroupName = "Страховая премия")]
        [DatetimeBehaviorInGroup(GroupName = "Параметры договора", Shown = true, Order = 26)]
        public DateTimeOffset? SigningDate { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Комментарий продавца")]
        [StringBehaviorInGroup(GroupName = "Параметры договора", Shown = true, Order = 27)]
        public string SellerComment { get; set; }
        //-------------------------------------------------------------------------------------
        [Display(Name = "Тип страхователя", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 1)]
        public string InsuredContractorType { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Тип документа", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 2)]
        public string InsuredDocumentType { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Серия", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 3)]
        public string InsuredPersonDocSerial { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Номер", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 4)]
        public string InsuredPersonDocNumber { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Дата выдачи документа, удостоверяющего личность", GroupName = "Страхователь")]
        [DatetimeBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 5)]
        public DateTimeOffset? InsuredPersonDocDateGiven { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Кем выдан документ, удостоверяющий личность", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 6)]
        public string InsuredPersonDocWhomGiven { get; set; }        

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Фамилия", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 7)]
        public string InsuredPersonLastName { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Имя", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 8)]
        public string InsuredPersonFirstName { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Отчество", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 9)]
        public string InsuredPersonMiddleName { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Наименование организации", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 10)]
        public string InsuredOrgName { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Дата рождения", GroupName = "Страхователь")]
        [DatetimeBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 11)]
        public DateTimeOffset? InsuredBirthdate { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Телефон", GroupName = "Страхователь")]
        [PhoneNumberBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 12)]
        public string InsuredCommonPhone { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "ИНН", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 13)]
        public string InsuredOrgINN { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Нерезидент", GroupName = "Страхователь")]
        [BooleanBehaviorInGroup("Нерезидент", "резидент", GroupName = "Участники договора", Shown = false, Order = 14)]
        public bool InsuredIsNotResident { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Адрес", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 15)]
        public string InsuredCommonRealAddress { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Страна", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 31)]
        public string InsuredCountry { get; set; }

        [Display(Name = "Код ФИАС", GroupName = "Страхователь")]
        [DisableAJAXCall]
        public string InsuredCommonRealAddressFiasId { get; set; }

        [Display(Name = "Код КЛАДР", GroupName = "Страхователь")]
        [DisableAJAXCall]
        public string InsuredCommonRealAddressKladrId { get; set; }

        [DisableAJAXCall]
        public Guid? InsuredGuid { get; set; }

        [Display(Name = "Тип собственника", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 16)]
        public string OwnerContractorType { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Тип документа", GroupName = "Собственник")]
        [AdditionalMetadata("HideOnDisable", true)]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 17)]
        public string OwnerDocumentType { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Серия", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 18)]
        public string OwnerPersonDocSerial { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Номер", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 19)]
        public string OwnerPersonDocNumber { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Дата выдачи документа, удостоверяющего личность", GroupName = "Собственник")]
        [DatetimeBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 20)]
        public DateTimeOffset? OwnerPersonDocDateGiven { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Кем выдан документ, удостоверяющий личность", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 21)]
        public string OwnerPersonDocWhomGiven { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Фамилия", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 22)]
        public string OwnerPersonLastName { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Имя", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 23)]
        public string OwnerPersonFirstName { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Отчество", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 24)]
        public string OwnerPersonMiddleName { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Наименование организации", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 25)]
        public string OwnerOrgName { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Дата рождения", GroupName = "Собственник")]
        [DatetimeBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 26)]
        public DateTimeOffset? OwnerBirthdate { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Телефон", GroupName = "Собственник")]
        [PhoneNumberBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 27)]
        public string OwnerCommonPhone { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "ИНН", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 28)]
        public string OwnerOrgINN { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Нерезидент", GroupName = "Собственник")]
        [BooleanBehaviorInGroup("Нерезидент", "резидент", GroupName = "Участники договора", Shown = false, Order = 29)]
        public bool OwnerIsNotResident { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Адрес", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 30)]
        public string OwnerCommonRealAddress { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Страна", GroupName = "Собственник")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = true, Order = 31)]
        public string OwnerCountry { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Код ФИАС", GroupName = "Собственник")]
        public string OwnerCommonRealAddressFiasId { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Код ФИАС", GroupName = "Собственник")]
        public string OwnerCommonRealAddressKladrId { get; set; }

        [DisableAJAXCall]
        public Guid? OwnerGuid { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Ограниченный список лиц допущенных к управлению")]
        [StringBehaviorInGroup("Список водителей ограничен: {0}", GroupName = "Участники договора", Shown = true, Order = 27)]
        public string DriverRestriction { get; set; }

        [DisableAJAXCall]
        [AdditionalMetadata("HideOnDisable", true)]
        [Display(Name = "Стаж", GroupName = "Страхователь")]
        [StringBehaviorInGroup(GroupName = "Участники договора", Shown = false, Order = 28)]
        public string AgeExperience { get; set; }


        //Количество страховых выплат для неограниченного списка
        [DisableAJAXCall]
        [Display(Name = "Количество страховых выплат")]
        [StringBehaviorInGroup("Количество страховых выплат: {0},", GroupName = "Участники договора", Shown = true, Order = -1)]
        public int RSAPayoutCount { get; set; }

        [DisableAJAXCall]
        [Display(Name = "Количество страховых выплат")]
        public int PayoutCount { get; set; }

        //-------РСА ответ---------------------------------------------------------------------
        // Свойство проставляемое js по успешному получению ответа от RSA
        [DisableAJAXCall]
        public bool HasBeenRSACalculated { get; set; }

        //ID ответа RSA
        [DisableAJAXCall]
        public string RSAID { get; set; }

        [DisableAJAXCall]
        public int InsuredId { get; set; }

        [DisableAJAXCall]
        public int OwnerId { get; set; }

        [DisableAJAXCall]
        public int VehicleId { get; set; }
        
        [DisableAJAXCall]
        public Guid Guid { get; set; }

        [DisableAJAXCall]
        public Guid? InsuredVID { get; set; }
    }
}
