﻿using System;

namespace UGSK.K3.Service.DTO.Attributes
{
    public class PropertyMetaDataAttribute : Attribute
    {
        public string Name { get; set; }
        public string Repository { get; set; }
        public string OutputFunction { get; set; }
    }
}