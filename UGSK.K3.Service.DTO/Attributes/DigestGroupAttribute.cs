﻿using System;

namespace UGSK.K3.Service.DTO.Attributes
{
    public class DigestGroupAttribute : Attribute
    {
        public string GroupName { get; set; }
        public bool Show { get; set; }
        public int Order { get; set; }
        public string RepositoryParamName { get; set; }

        public DigestGroupAttribute()
        {
            Show = true;
            GroupName = "Основные параметры";
        }
    }
}