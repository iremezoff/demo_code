﻿using UGSK.K3.Service.DTO.Shared;


namespace UGSK.K3.Service.DTO.Attributes
{
    // : class has some problem! resolve them!
    public class CompositeBehaviorInGroupAttribute : InGroupAttribute
    {
        private static string CSFormat(object data)
        {
            if (data == null)
                return string.Empty;
            return data.ToString();
        }

        public CompositeBehaviorInGroupAttribute()
            : base("function(v){return v;}", CSFormat)
        {
        }
    }
}