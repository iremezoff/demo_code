﻿using System;
using UGSK.K3.Service.DTO.Extentions;
using UGSK.K3.Service.DTO.Shared;


namespace UGSK.K3.Service.DTO.Attributes
{
    public class DatetimeBehaviorInGroupAttribute : InGroupAttribute
    {
        private static string CSFormat(object data)
        {
            if (data == null)
                return string.Empty;
            var dateTime = ConverterExtentions.ToDateTime(data);
            return dateTime.ToString("dd.MM.yyyy");
        }

        public DatetimeBehaviorInGroupAttribute(string format = null)
            : base(string.IsNullOrWhiteSpace(format) ? @"function(v){{ if( !v) return """"; return toShortDate(v); }}" : string.Format(@"function(v){{ if( !v) return """"; return '{0}'.format( toShortDate(v)); }}", format), data => CSFormat(data))
        {
        }
    }
}