﻿using System;
using UGSK.K3.Service.DTO.Shared;


namespace UGSK.K3.Service.DTO.Attributes
{
    public class BooleanBehaviorInGroupAttribute : InGroupAttribute
    {
        private static string CSFormat(object data, string trueStringRepresentation, string falseStringRepresentation)
        {
            bool value = false;
            if (data != null)
            {
                value = Convert.ToBoolean(data);
                if (value)
                    return trueStringRepresentation ?? string.Empty;
                else
                    return falseStringRepresentation ?? string.Empty;
            }
            return string.Empty;
        }

        public BooleanBehaviorInGroupAttribute(string trueStringRepresentation, string falseStringRepresentation)
            : base(
            string.Format(
                @"function(v){{ if( v === false) return ""{1}""; if(!v) return """"; return ""{0}"";}}",
                trueStringRepresentation ?? string.Empty,
                falseStringRepresentation ?? string.Empty),
                v => CSFormat(v, trueStringRepresentation, falseStringRepresentation))
        {
        }
    }
}