﻿using System;
using UGSK.K3.Service.DTO.Shared;

namespace UGSK.K3.Service.DTO.Attributes
{
    public class PercentBehaviorInGroupAttribute : InGroupAttribute
    {
        private static string CSFormat(object data)
        {
            if (data != null)
                return Convert.ToDecimal(data).ToString();
            return string.Empty;
        }

        public PercentBehaviorInGroupAttribute()
            : base(@"function(v){ return v == null ? """" : 100 * v + '%'; }", CSFormat)
        {
        }
    }
}