﻿using UGSK.K3.Service.DTO.Shared;

namespace UGSK.K3.Service.DTO.Attributes
{
    public class StringBehaviorInGroupAttribute : InGroupAttribute
    {
        private static string CSFormat(object data, string format)
        {
            if (data == null)
                return string.Empty;
            if (string.IsNullOrWhiteSpace(format))
                return data.ToString();
            if (data is bool)
            {
                data = (bool) data ? "Да" : "Нет";
            }
            return string.Format(format, data);
        }

        public StringBehaviorInGroupAttribute(string format = null)
            : base((string.IsNullOrWhiteSpace(format)) ? "function(v){return v;}" : @"function(v){return v == null ? """" : """ + format + @""".format(v) ;}", v => CSFormat(v, format))
        {
        }
    }
}