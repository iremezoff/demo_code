﻿using System;
using UGSK.K3.Service.DTO.Shared;


namespace UGSK.K3.Service.DTO.Attributes
{
    public abstract class InGroupAttribute : Attribute 
    {
        public InGroupAttribute(string jsFormatFunction, Func<object, string> csharpFormatFunction)
        {
            JSFormatFunction = jsFormatFunction;
            CSharpFormatFunction = csharpFormatFunction;
        }
        public string GroupName { get; set; }
        public bool Shown { get; set; }
        public int Order { get; set; }
        public readonly string JSFormatFunction;
        public readonly Func<object, string> CSharpFormatFunction;
    }
}