﻿using UGSK.K3.Service.Model.Extensions;

namespace UGSK.K3.Service.DTO.Attributes
{
    public class PhoneNumberBehaviorInGroupAttribute : InGroupAttribute
    {
        private static string CSFormat(object data)
        {
            if (data == null)
                return string.Empty;
            return data.ToString()
                .Replace(PhoneExtensions.PhoneVerificationMarker, "")
                .Replace(PhoneExtensions.PhoneExceptionMarker, "");
        }

        public PhoneNumberBehaviorInGroupAttribute()
            : base(string.Format(@"function(v){0}return v == null ? """" : v.replace(/{1}/g, '').replace(/{2}/g, '');{3}",
                    "{", PhoneExtensions.PhoneVerificationMarker, PhoneExtensions.PhoneExceptionMarker, "}"),
                v => CSFormat(v))
        {
        }
    }
}
