﻿using System.Linq;
using UGSK.K3.Service.DTO.Shared;


namespace UGSK.K3.Service.DTO.Attributes
{
    public class ArrayBehaviorInGroupAttribute : InGroupAttribute
    {
        private static string CSFormat(object data, string delimeter)
        {
            if (data == null)
                return string.Empty;
            return string.Join(delimeter, (data as object[]).Where( v => v != null).Select( v => v));
        }

        public ArrayBehaviorInGroupAttribute(string delimeter = ", ")
            : base(string.Format(@"function(v){{ return v.join('{0}'); }}", delimeter), data => CSFormat(data, delimeter))
        {
        }
    }
}