﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UGSK.K3.Service.DTO.Extentions
{
    public static class ConverterExtentions
    {
        public static DateTime ConvertFromDateTimeOffset(DateTimeOffset dateTime)
        {
            return DateTime.SpecifyKind(dateTime.DateTime, DateTimeKind.Unspecified);
        }

        public static DateTime ToDateTime(object value)
        {
            if (value == null)
            {
                throw new NotImplementedException("Contract is violated!");
            }
            if (value.GetType() == typeof(DateTimeOffset))
            {
                return ConvertFromDateTimeOffset((DateTimeOffset)value);
            }
            return Convert.ToDateTime(value);
        }

        public static string ReplaceUnicodeCodesByChars(string source)
        {
            return System.Text.RegularExpressions.Regex.Replace(source, @"\\u(?<Value>[a-zA-Z0-9]{4})", new System.Text.RegularExpressions.MatchEvaluator((s) => 
                {
                    try
                    {
                        return ((char)int.Parse(s.Groups["Value"].Value, System.Globalization.NumberStyles.HexNumber)).ToString();
                    }
                    catch
                    {
                        return s.Value;
                    }
                })
            );
        }
    }
}
