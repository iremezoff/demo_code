﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using UGSK.K3.Infrastructure;
using UGSK.K3.Service.Model;
using UGSK.K3.Service.WebAPI.Environment;

namespace UGSK.K3.Service.WebAPI.Security
{
    public interface IImpersonaliztionEmployeeGetter : IEmployeeGetter
    {
        Employee GetOriginal();
    }
    public class EmployeeGetter : IImpersonaliztionEmployeeGetter
    {
        public Employee GetEmployee()
        {
            throw new NotImplementedException();
        }

        public Employee GetOriginal()
        {
            throw new NotImplementedException();
        }
    }

    public class ImpersonateForbiddenException : ApplicationException
    {
        private readonly string _impLogin;
        private readonly string _employeeName;

        public ImpersonateForbiddenException()
        {
            
        }

        public ImpersonateForbiddenException(string message, string impLogin, string employeeName)
            : base(message)
        {
            _impLogin = impLogin;
            _employeeName = employeeName;
        }

        public string ImpLogin
        {
            get { return _impLogin; }
        }

        public string EmployeeName
        {
            get { return _employeeName; }
        }
    }
}