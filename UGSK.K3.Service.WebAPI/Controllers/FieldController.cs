﻿using System.Net.Http;
using System.Web.Http;
using UGSK.K3.AppServices.DataDeriver.Strategies;
using UGSK.K3.Product.Contract;
using UGSK.K3.Service.WebAPI.Helpers;

namespace UGSK.K3.Service.WebAPI.Controllers
{
    [Authorize]
    public class FieldController : ApiController
    {
        private readonly IDeriveStrategyFactory _strategyFactory;

        public FieldController(IDeriveStrategyFactory strategyFactory)
        {
            _strategyFactory = strategyFactory; 
        }

        [Route("Field/{productName}/{fieldName}")]
        public HttpResponseMessage Post([FromBody] IRequest request, string productName, string fieldName)
        {

            var strategy = _strategyFactory.GetStrategy(request, fieldName);

            return RepositoryForming.Extract(Request, strategy);
        }

        [Route("Field/{productName}")]
        public HttpResponseMessage Post([FromBody] IRequest request, string productName)
        {

            var strategy = _strategyFactory.GetStrategy(request);

            return RepositoryForming.Extract(Request, strategy);
        }
    }
}