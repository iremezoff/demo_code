namespace UGSK.K3.Service.WebAPI.Environment
{
    public static class ServiceEnvironmentConstants
    {
        public static class ExtendedHeaders
        {
            public const string Impersonalization = "Impersonalization";
        }
    }
}
