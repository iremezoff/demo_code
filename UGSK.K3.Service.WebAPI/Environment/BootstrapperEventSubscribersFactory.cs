﻿using System;
using System.Collections.Generic;
using System.Linq;
using UGSK.K3.Infrastructure.Events;

namespace UGSK.K3.Service.WebAPI.Environment
{
    public class BootstrapperEventSubscribersFactory : IEventSubscribersFactory
    {
        private readonly Func<Event, IEnumerable<IEventHandler<Event>>> _factory;

        public BootstrapperEventSubscribersFactory(Func<Event, IEnumerable<IEventHandler<Event>>> factory)
        {
            _factory = factory;
        }

        public IList<IEventHandler<Event>> GetSubscribers(Event @event)
        {
            if (@event == null)
            {
                throw new ArgumentNullException("event");
            }

            var subscribers = _factory(@event);

            return subscribers.ToList();
        }
    }
}