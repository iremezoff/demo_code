﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UGSK.K3.Infrastructure;
using UGSK.K3.Infrastructure.Events;

namespace UGSK.K3.Service.WebAPI.Environment
{
    public class BootstrapperCommandHandlersFactory : ICommandHandlersFactory
    {
        public IList<Action<IMessage>> GetHandlers<TCommand>() where TCommand : Command
        {
            IEnumerable<ICommandHandler<TCommand>> commandHandlers = new List<ICommandHandler<TCommand>>();

            try
            {
                commandHandlers = Bootstrapper.Container.GetAllInstances<ICommandHandler<TCommand>>();
            }
            catch (Exception ex)
            {
                throw;
            }

            var result = new List<Action<IMessage>>();
            foreach (var commandHandler in commandHandlers)
            {
                result.Add(DelegateAdjuster.CastArgument<IMessage, TCommand>(x => commandHandler.Handle(x)));
            }

            if (result.Count < 1)
                throw new InvalidOperationException("Command handlers not registered");

            return result;
        }

        public IList<Func<IMessage, Task>> GetAsyncHandlers<TCommand>() where TCommand : Command
        {
            IEnumerable<IAsyncCommandHandler<TCommand>> asyncCommandHandlers = new List<IAsyncCommandHandler<TCommand>>();

            try
            {
                asyncCommandHandlers = Bootstrapper.Container.GetAllInstances<IAsyncCommandHandler<TCommand>>();
            }
            catch (Exception ex)
            {
                throw;
            }

            var result = new List<Func<IMessage, Task>>();
            foreach (var asyncCommandHandler in asyncCommandHandlers)
            {
                result.Add(DelegateAdjuster.CastArgument<IMessage, TCommand>(x => asyncCommandHandler.HandleAsync(x)));
            }

            if (result.Count < 1)
                throw new InvalidOperationException("Async command handlers not registered");

            return result;
        }
    }
}
