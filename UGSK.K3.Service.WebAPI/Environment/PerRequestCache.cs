using System;
using System.Net.Http;

namespace UGSK.K3.Service.WebAPI.Environment
{
    public class PerRequestCache : IPerRequestCache
    {
        public T GetOrAdd<T>(string name, T item) where T : class
        {
            var request = Bootstrapper.Resolve<Func<HttpRequestMessage>>()();

            object obj = null;
            if (request.Properties.TryGetValue(name, out obj) && obj != null)
            {
                return obj as T;
            }
            request.Properties[name] = item;
            return item;
        }

        public void Remove(string name)
        {
            var request = Bootstrapper.Resolve<Func<HttpRequestMessage>>()();
            request.Properties.Remove(name);
        }
    }
}