﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.Http;
using System.Web.OData;
using LightInject;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.AppServices.DataDeriver.Core.Impl;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Flexible;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Flexible.Impl;
using UGSK.K3.AppServices.DataDeriver.Core.Restrictors.Impl;
using UGSK.K3.AppServices.DataDeriver.Core.SelectionRules;
using UGSK.K3.AppServices.DataDeriver.Core.SelectionRules.Impl;
using UGSK.K3.AppServices.DataDeriver.Strategies;
using UGSK.K3.AppServices.DataDeriver.Strategies.Impl;
using UGSK.K3.Infrastructure;
using UGSK.K3.Infrastructure.Events;
using UGSK.K3.Infrastructure.Events.Impl;
using UGSK.K3.Product.Contract;
using UGSK.K3.Product.Contract.Annotations;
using UGSK.K3.Product.Contract.Verification;
using UGSK.K3.Service.Model;
using UGSK.K3.Service.WebAPI.ConfigFile;
using UGSK.K3.Service.WebAPI.ConfigFile.ProductConfig;
using UGSK.K3.Service.WebAPI.Converting;
using UGSK.K3.Service.WebAPI.Helpers;
using UGSK.K3.Service.WebAPI.Helpers.Tools;
using UGSK.K3.Service.WebAPI.Security;
using UGSK.K3.Service.WebAPI.Validation;

[assembly: InternalsVisibleTo("UGSK.K3.Service.WebAPI.Test")]

namespace UGSK.K3.Service.WebAPI.Environment
{
    public static class Bootstrapper
    {
        private static ServiceContainer _container;
        
        internal static ServiceContainer Container
        {
            get { return _container; }
        }

        public static T Resolve<T>()
        {
            return Container.TryGetInstance<T>();
        }

        public static object Resolve(Type t)
        {
            return Container.TryGetInstance(t);
        }

        public static void Register<T, T1>() where T1 : T
        {
            Container.Register<T, T1>(new PerContainerLifetime());
        }

        public static void Register<T>(T instance)
        {
            Container.RegisterInstance(typeof(T), instance);
        }

        public static void Run(Assembly webAssembly, HttpConfiguration configuration)
        {
            _container = new ServiceContainer();


            Container.RegisterInstance<IServiceProvider>(new ContextServiceProvider(configuration));

            var handler = new HttpRequestMessageHandler();
            configuration.MessageHandlers.Insert(0, handler);

            Container.Register<Func<HttpRequestMessage>>(factory => () => handler.GetCurrentMessage());
            Container.Register<IPerRequestCache, PerRequestCache>(new PerContainerLifetime());

            Container.RegisterInstance(typeof(IServiceContainer), Container);
            Container.Register<IEmployeeGetter, EmployeeGetter>(new PerScopeLifetime());

            Container.Register<IImpersonaliztionEmployeeGetter, EmployeeGetter>(new PerScopeLifetime());
            
            
            Container.RegisterInstance(AutoMapper.Mapper.Engine);
            
            Container.Register<ICommandSender, CommandSender>(new PerScopeLifetime());
            Container.Register<ICommandHandlersFactory, BootstrapperCommandHandlersFactory>(new PerScopeLifetime());
            Container.Register<IEventSubscribersFactory, BootstrapperEventSubscribersFactory>(new PerScopeLifetime());
            Container.Register<IEventPublisher, EventPublisher>(new PerScopeLifetime());

            Container.Register<MetadataController, MetadataController>(new PerScopeLifetime());
            
            Container.Register(typeof(ISimpleRequestVerifier<>), typeof(CalculationFieldsVerifier<>), new PerScopeLifetime());


            CommonRegistering();

            ProductsRegistering();
            
            Container.ScopeManagerProvider = new PerLogicalCallContextScopeManagerProvider();

            Container.RegisterApiControllers(webAssembly);
            Container.EnableWebApi(configuration);
        }

        private static bool IsBasedOn(Type typeForCheck, Type baseType)
        {
            if (baseType.IsGenericType)
            {
                return typeForCheck.IsGenericType && baseType.IsGenericTypeDefinition && baseType.IsAssignableFrom(typeForCheck.GetGenericTypeDefinition());
            }
            return baseType.IsAssignableFrom(typeForCheck);
        }

        private static void ProductsRegistering()
        {
            foreach (var type in typeof(IRequest).Assembly.GetExportedTypes())
            {
                if (!type.IsInterface && !type.IsAbstract && type.GetInterfaces().Any(i => IsBasedOn(i, typeof(ISelectionRule<>))))
                {
                    Container.Register(type, type, new PerContainerLifetime());
                    continue;
                }

                if (!type.IsInterface && !type.IsAbstract && typeof(IFieldValueConverter).IsAssignableFrom(type))
                {
                    Container.Register(type, type, new PerContainerLifetime());
                }
            }

            var k3FrontConfig = (AppConfigConfiguration)ConfigurationManager.GetSection("k3Front");

            foreach (var productConfig in k3FrontConfig.Products.OfType<K3Product>())
            {
                Assembly assembly = Assembly.Load(productConfig.AssemblyName);

                K3ModelDependencyEngineRegistering(productConfig, assembly);
                ConvertersRegistering(productConfig, assembly);
            }

            Assembly contractAssembly = Assembly.Load("UGSK.K3.Product.Contract");
            RegisterAssemblyComponents(contractAssembly);

            Container.Register<string, IContractConverter>(
                (factory, productName) => factory.TryGetInstance<IContractConverter>(productName));
        }

        private static void ConvertersRegistering(K3Product productConfig, Assembly assembly)
        {
            if (string.IsNullOrEmpty(productConfig.ConverterTypeName))
            {
                var requestType = assembly.GetType(productConfig.RequestTypeName);
                Container.RegisterInstance(typeof(IContractConverter),
                                            new DefaultContractConverter(requestType),
                                            productConfig.Name);
            }
            else
            {
                var converterType = Type.GetType(productConfig.ConverterTypeName);
                Container.Register(typeof(IContractConverter), converterType, productConfig.Name);
            }
        }

        private static void K3ModelDependencyEngineRegistering(K3Product productConfig, Assembly assembly)
        {
            Type requestType = assembly.GetType(productConfig.RequestTypeName);

            Container.Register<Type, IEnumerable<IHDBK>>((factory, type) => RestrictCollectionFactory(requestType, type, factory), requestType.FullName);

            Container.Register(typeof(ITariffCalculationEngine<>).MakeGenericType(requestType), typeof(TariffCalculationEngine<>).MakeGenericType(requestType), new PerContainerLifetime());

            Container.Register(typeof(IStrategyBuilder), typeof(StrategyBuilder<>).MakeGenericType(requestType), requestType.FullName, new PerRequestLifeTime());

            foreach (var component in productConfig.Components.OfType<SingularComponent>())
            {
                string serviceName = string.Format("{0}_{1}", productConfig.Name, component.Name);
                Container.Register(assembly.GetType(component.ServiceType) ??
                                    Assembly.GetExecutingAssembly().GetType(component.ServiceType) ??
                                    Type.GetType(component.ServiceType),
                                    assembly.GetType(component.ComponentType) ??
                                    Assembly.GetExecutingAssembly().GetType(component.ComponentType) ??
                                    Type.GetType(component.ComponentType)
                , serviceName);
            }

            foreach (var type in assembly.GetExportedTypes())
            {
                if (typeof(ICalculator<>).MakeGenericType(requestType).IsAssignableFrom(type))
                {
                    Container.Register(typeof(ICalculator<>).MakeGenericType(requestType), type, new PerContainerLifetime());
                    Container.Register(typeof(ICalculator), type, productConfig.Name, new PerContainerLifetime());
                }
            }

            RegisterAssemblyComponents(assembly);

        }

        private static IStrategyBuilder GetStrategyBuilder(Type requestType, IServiceFactory factory)
        {
            return factory.GetInstance<IStrategyBuilder>(requestType.FullName);
        }

        // развилка похожих фабрик
        private static object FactoryCrotch(Type type, IServiceFactory factory)
        {
            if (type.GetInterfaces().Any(i => IsBasedOn(i, typeof(IFlexCondition<,>))))
            {
                return factory.GetInstance(type);
            }
            if (IsBasedOn(type, typeof(IRepositoryWithTypedId<,>)))
            {
                var httpRequest = factory.GetInstance<Func<HttpRequestMessage>>()();
                
                return factory.GetInstance(type);
            }

            throw new InvalidOperationException("нет такого пути");
        }

        private static IEnumerable<ICalculator> GetAllCalculators()
        {
            return
                Container.AvailableServices.Where(
                    sr => IsBasedOn(sr.ServiceType, typeof(ICalculator<>)))
                    .Select(r => (ICalculator)Container.GetInstance(r.ServiceType));
        }

        private static void CommonRegistering()
        {
            Container.Register<IBatlerFactory, BatlerFactory>(new PerContainerLifetime());
            Container.Register<ICalculatorDispatcher, CalculatorDispatcher>(new PerContainerLifetime());
            Container.Register<Type, object>((factory, type) => FactoryCrotch(type, factory)); // развилка для generic-сервисов (абстракций)
            Container.Register<Type, ICalculator>((factory, type) => factory.GetInstance(typeof(ICalculator<>).MakeGenericType(type)) as ICalculator);
            Container.Register<Type, IFieldValueConverter>((factory, type) => factory.GetInstance(type) as IFieldValueConverter);
            Container.Register<ISelectorRuleBuilder, DefaultSelectorRuleBuilder>();
            Container.Register<EverTrueCondition>();
            Container.Register<IFlexConditionKitFactory, FlexConditionKitFactory>();
            Container.Register<Type, int, ISelectionRule<IRequest>>(
                (factory, type, order) => (ISelectionRule<IRequest>)factory.GetInstance(type, new object[] { order }));
            Container.Register<IDeriveStrategyFactory, DeriveStrategyFactory>(new PerContainerLifetime());

            Container.Register<Type, IStrategyBuilder>((factory, requestType) => GetStrategyBuilder(requestType, factory));

            Container.RegisterInstance<IEnumerable<ISelectionRule<IRequest>>>(new List<ISelectionRule<IRequest>>()
                {
                    new SingleHavingSelectionRule<IRequest>()
                });

            Container.Register(factory => GetAllCalculators());

            Container.Register<int, FirstHavingSelectionRule>((factory, order) => new FirstHavingSelectionRule() { Priority = order });

            // общая регистрация всех ограничителей
            Container.Register<FieldAttributeBase, IRequestFieldBatler, Action<EventHandler<NewRestrictorEventArgs>, Type>, IFieldSourceRestrictor<FieldAttributeBase>>(
                    (factory, attr, batler, registrator) =>
                    (IFieldSourceRestrictor<FieldAttributeBase>)
                    factory.GetInstance(typeof(IFieldSourceRestrictor<>).MakeGenericType(attr.GetType()), new object[] { attr, batler, registrator }));

            // регистрация базового ограничителя, по явным зависимостям, например Территория->ТипТС->Марка->Модель
            Container.Register<FieldAttributeBase, IRequestFieldBatler, Action<EventHandler<NewRestrictorEventArgs>, Type>, IFieldSourceRestrictor<RequestFieldAttribute>>(
                        (factory, attr, batler, reg) =>
                        new DefaultFieldSourceRestrictor((RequestFieldAttribute)attr, batler, reg, factory.GetInstance<Func<Type, IFieldValueConverter>>()));

            // регистрация ограничителя по внешним связям, т.е. HDBK
            Container.Register<FieldAttributeBase, IRequestFieldBatler, Action<EventHandler<NewRestrictorEventArgs>, Type>, IFieldSourceRestrictor<CohesiveFieldAttribute>>(
                        (factory, attr, batler, reg) =>
                            new CohesiveFieldSourceRestrictor((CohesiveFieldAttribute)attr, batler,
                                factory.GetInstance<Func<Type, IEnumerable<IHDBK>>>(batler.RequestType.FullName), reg)
                );

            // регистарция гибких ограничителей (по спецификациям)
            Container.Register<FieldAttributeBase, IRequestFieldBatler, Action<EventHandler<NewRestrictorEventArgs>, Type>, IFieldSourceRestrictor<FlexibleFieldAttribute>>(
                        (factory, attr, batler, reg) =>
                        new FlexibleFieldSourceRestrictor((FlexibleFieldAttribute)attr,
                            batler, factory.GetInstance<IFlexConditionKitFactory>(), reg));
        }

        private static IEnumerable<IHDBK> RestrictCollectionFactory(Type requestType, Type hdbkType, IServiceFactory factory)
        {
            var calculator = Container.GetInstance(typeof(ICalculator<>).MakeGenericType(requestType)) as ICalculator;

            var dbSession = calculator.DbSession;

            var collectionProperty = dbSession.GetType()
                                                   .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                                                   .FirstOrDefault(
                                                       p =>
                                                       typeof(IEnumerable<IHDBK>).IsAssignableFrom(p.PropertyType) &&
                                                       p.PropertyType.GenericTypeArguments[0] == hdbkType);

            if (collectionProperty != null)
            {
                return collectionProperty.GetValue(dbSession) as IEnumerable<IHDBK>;
            }
            throw new InvalidDataException("Данный тип HDBK не зарегистрирован");
        }

        private static void RegisterAssemblyComponents(Assembly assembly)
        {
            foreach (var type in assembly.GetExportedTypes())
            {
                if (!type.IsInterface && !type.IsAbstract && type.GetInterfaces().Any(i => IsBasedOn(i, typeof(ISelectionRule<>))))
                {
                    Container.Register(type, type, new PerContainerLifetime());
                    continue;
                }

                if (!type.IsInterface && !type.IsAbstract && typeof(IFieldValueConverter).IsAssignableFrom(type))
                {
                    Container.Register(type, type, new PerContainerLifetime());
                    continue;
                }

                if (!type.IsInterface && !type.IsAbstract && type.GetInterfaces().Any(i => IsBasedOn(i, typeof(ISpecification<>))))
                {
                    Container.Register(type, type, new PerContainerLifetime());
                    continue;
                }

                if (!type.IsInterface && !type.IsAbstract && type.GetInterfaces().FirstOrDefault(i => IsBasedOn(i, typeof(IFlexCondition<,>))) != null)
                {
                    Container.Register(type, type, new PerContainerLifetime());
                }
            }
        }

    }
}
