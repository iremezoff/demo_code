namespace UGSK.K3.Service.WebAPI.Environment
{
    public interface IPerRequestCache
    {
        T GetOrAdd<T>(string name, T item) where T : class;
        void Remove(string name);
    }
}