﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using UGSK.K3.AppServices.DataDeriver.Product.Field;
using UGSK.K3.AppServices.DataDeriver.Strategies;

namespace UGSK.K3.Service.WebAPI.Helpers
{
    static class RepositoryForming
    {
        public static HttpResponseMessage Extract(HttpRequestMessage request, ISourceDeriveStrategy strategy)
        {
            IEnumerable<FieldRepository> repos = strategy.ExtractRepositories();

            var dataForResponse = repos.Select(PrepareHelper.Prepare).ToList();

            return !dataForResponse.Any()
                       ? request.CreateResponse(HttpStatusCode.NoContent)
                       : request.CreateResponse(HttpStatusCode.OK, dataForResponse);
        }
    }
}