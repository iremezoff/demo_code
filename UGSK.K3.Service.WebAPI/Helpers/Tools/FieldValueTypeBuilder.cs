﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.Product.Contract;
using UGSK.K3.Service.DTO.Shared;
using FieldRepository = UGSK.K3.AppServices.DataDeriver.Product.Field.FieldRepository;

namespace UGSK.K3.Service.WebAPI.Helpers.Tools
{
    public interface IFormatConverter
    {
        IEnumerable<FieldValue> GetConvertedCollection(PropertyInfo fieldName, IDictionary<SimpleEntity, bool> collection, IRequest request);
        IEnumerable<ValueItem> GetConvertedValues(FieldRepository fieldRepo, params object[] value);
        TypeInfo this[PropertyInfo fieldName] { get; }
    }

    public class FieldValueTypeBuilder : ITypeBuilder, IFormatConverter
    {
        private readonly IDictionary<PropertyInfo, TypeInfo> _registeredTypes = new Dictionary<PropertyInfo, TypeInfo>();

        public void RegisterType(PropertyInfo fieldName, Type entityType, Type returnType, Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>> converter)
        {
            // todo Ремезов: ошибка, если fieldName не является наследником SimpleEntity
            var paramExpr = Expression.Parameter(entityType);
            var kvParam = Expression.Parameter(typeof(KeyValuePair<object, bool>));
            var fieldValueConstructor = typeof(FieldValue).GetConstructor(new[] { typeof(ValueItem[]), typeof(FieldState) });
            var fieldValueInitExpr = Expression.New(fieldValueConstructor, Expression.Constant(new ValueItem[0]), Expression.Constant(FieldState.None));
            var typeInfo = new TypeInfo()
                {
                    SourceType = entityType,
                    ExpectedType = returnType,
                    Parameter = paramExpr,
                    //LambdaSelector = Expression.Lambda(typeof(Func<>).MakeGenericType(fieldName,fieldName), paramExpr, paramExpr),
                    Converter = converter,
                    Properties = new Dictionary<string, string>(),
                    LambdaSelector = Expression.Lambda<Func<KeyValuePair<object, bool>, FieldValue>>(fieldValueInitExpr, kvParam)
                };
            _registeredTypes[fieldName] = typeInfo;
        }

        public TypeInfo this[PropertyInfo fieldName]
        {
            get
            {
                TypeInfo typeInfo = null;
                if (_registeredTypes.TryGetValue(fieldName, out typeInfo))
                {
                    return typeInfo;
                }
                return null;
            }
        }

        public void RegisterProperty(PropertyInfo fieldName, string originalProperty, string expectedProperty)
        {
            // todo Ремезов: выдать Exception ошибка на отсутствие типа
            // todo Ремезов: проверка на наличие поля originalProperty в fieldName
            var typeInfo = _registeredTypes[fieldName];

            typeInfo.Properties[originalProperty] = expectedProperty;

            var defaultConstructor = typeof(ValueItem).GetConstructor(new[] { typeof(string), typeof(object) });

            var kvParam = Expression.Parameter(typeof(KeyValuePair<object, bool>));

            var valueItemNewExpression = typeInfo.Properties
                                               .Select(f =>
                                                   Expression.New(defaultConstructor, Expression.Constant(f.Value),
                                                                  Expression.Convert(Expression.Property(
                                                                      Expression.TypeAs(Expression.Property(kvParam, "Key"),
                                                                                        typeInfo.ExpectedType), f.Key), typeof(object)))).ToList();
            Expression arrayInit = Expression.NewArrayInit(typeof(ValueItem), valueItemNewExpression);

            var fieldValueConstructor = typeof(FieldValue).GetConstructor(new[] { typeof(ValueItem[]), typeof(FieldState) });

            Expression<Func<bool, FieldState>> stateInitLambda = (flag) => flag ? FieldState.None : FieldState.Disabled;

            var defaultConverter = Expression.Lambda<Func<KeyValuePair<object, bool>, FieldValue>>(
                    Expression.New(fieldValueConstructor, arrayInit,
                        Expression.Invoke(stateInitLambda, Expression.Property(kvParam, "Value"))), kvParam);

            var fieldValueNewExpression =
                Expression.Call(
                    this.GetType().GetMethod("GetFieldValue", BindingFlags.Static | BindingFlags.NonPublic), kvParam, Expression.Constant(defaultConverter.Compile()));//  

            typeInfo.LambdaSelector = Expression.Lambda<Func<KeyValuePair<object, bool>, FieldValue>>(fieldValueNewExpression, kvParam);

            if (typeInfo.ValueLambdaSelector != null) return;

            var valueParamExpr = Expression.Parameter(typeof(object));

            Expression valueNewExpr = Expression.New(defaultConstructor, Expression.Constant(expectedProperty), valueParamExpr);
            typeInfo.ValueLambdaSelector = Expression.Lambda<Func<object, ValueItem>>(valueNewExpr, valueParamExpr);
        }

        private static FieldValue GetFieldValue(KeyValuePair<object, bool> pair, Func<KeyValuePair<object, bool>, FieldValue> defaultConverter)
        {
            var collectionItem = pair.Key as IDictionary<string, object>;
            if (collectionItem != null)
            {
                return new FieldValue(collectionItem.Select(i => new ValueItem(i.Key, i.Value)).ToArray(),
                    pair.Value ? FieldState.None : FieldState.Disabled);
            }
            return defaultConverter(pair);
        }

        
        public IEnumerable<FieldValue> GetConvertedCollection(PropertyInfo fieldName, IDictionary<SimpleEntity, bool> collection, IRequest request)
        {
            if (!collection.Any())
            {
                return new List<FieldValue>();
            }
            TypeInfo typeInfo;
            if (!_registeredTypes.TryGetValue(fieldName, out typeInfo))
            {
                return new List<FieldValue>();
                //typeInfo = _registeredTypes[fieldName];
            }
            var convertedSource = typeInfo.Converter(collection, request).AsQueryable().Select(typeInfo.LambdaSelector);
            return convertedSource;
        }

        public IEnumerable<ValueItem> GetConvertedValues(FieldRepository fieldRepo, params object[] value)
        {
            //TypeInfo typeInfo;
            //if (value.Any() && !(value.First() is Entity<int>))
            //{
            if (value != null && value.Length == 1 && value.Single() is Dictionary<string, object>)
            {
                return (value.Single() as Dictionary<string, object>).Select(i => new ValueItem(i.Key, i.Value)).ToArray();
            }

            return value != null
                ? value.Select(v => new ValueItem(fieldRepo.FieldName, v))
                : new[] { new ValueItem(fieldRepo.FieldName, value) };
            //}
            //if (!_registeredTypes.TryGetValue(fieldRepo.Creator.FieldProperty, out typeInfo))
            //{
            //    typeInfo = _registeredTypes[fieldRepo.Creator.FieldProperty];
            //}
            //return typeInfo.Converter(
            //        value.Cast<Entity<int>>()
            //            .Select(i => new GenericSimpleEntity<Entity<int>>(i))
            //            .Cast<SimpleEntity>()
            //            .ToDictionary(k => k, k => true), fieldRepo.Request)
            //        .Select(i => new ValueItem(fieldRepo.FieldName, i)); // todo жееееесть

            //value.Select(v =>
            //new ValueItem(fieldRepo.FieldName,
            //    typeInfo.Converter((v as IEnumerable<Entity<int>>).Cast<SimpleEntity>().Select(i => new GenericSimpleEntity<Entity<int>>(i)).ToDictionary(k => k, k => true), 
            //        fieldRepo.Request)));
            //return new[] { new ValueItem(fieldRepo.FieldName, value) };
        }
    }

    public class TypeInfo
    {
        public Type SourceType { get; set; }
        public Type ExpectedType { get; set; }
        public Expression<Func<KeyValuePair<object, bool>, FieldValue>> LambdaSelector { get; set; }
        public Expression<Func<object, ValueItem>> ValueLambdaSelector { get; set; }
        public ParameterExpression Parameter { get; set; }
        public Func<IDictionary<SimpleEntity, bool>, IRequest, IEnumerable<KeyValuePair<object, bool>>> Converter { get; set; }
        public IDictionary<string, string> Properties { get; set; }
    }
}