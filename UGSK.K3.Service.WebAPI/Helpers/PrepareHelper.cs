﻿using System;
using System.Linq;
using UGSK.K3.AppServices.DataDeriver.Product.Field;
using UGSK.K3.Service.DTO.Shared;
using UGSK.K3.Service.WebAPI.Environment;
using UGSK.K3.Service.WebAPI.Helpers.Tools;
using FieldRepository = UGSK.K3.AppServices.DataDeriver.Product.Field.FieldRepository;

namespace UGSK.K3.Service.WebAPI.Helpers
{
    public static class PrepareHelper
    {
        private static object ToPrepareSingleRepository(FieldRepository repository)
        {
            var singleRepo = repository as SingleFieldRepository;

            var convertedCS = Converter.GetConvertedCollection(repository.Creator.FieldProperty, singleRepo.CollectionSource, singleRepo.Request).ToArray();

            // todo Ремезов: hardcode detected. данный хардкод вызван необходимостью передавать для многозначимых значений (like HDBK) всех значений, входящих в составное значение. Формат превращается в аналогичный с MultiRepositoryю необходимо как можно скорее причесать формат
            // hardcode start
            var typeInfo = Converter[repository.Creator.FieldProperty];
            var isManyValued = typeInfo != null && typeInfo.Properties.Count > 1;
            try
            {
                var value = singleRepo.HasValue
                                ? isManyValued
                                      ? (convertedCS.FirstOrDefault(
                                          i =>
                                          i.CollectionSource.All(
                                              vi =>
                                              {
                                                  var property = repository.Request.GetType().GetProperty(vi.FieldName);
                                                  if (property == null) return true; // для полей номера варианта. их в request'е нет, но нужно, чтобы совпадение было 
                                                  return property
                                                             .GetValue(repository.Request)
                                                             .ToString()
                                                             .ToLower() == vi.Value.ToString().ToLower();
                                              }
                                              )) ?? new FieldValue() { CollectionSource = new ValueItem[0] })
                                                   .CollectionSource.Select(i => new ValueItem(i.FieldName, i.Value)).ToArray()
                                      : Converter.GetConvertedValues(repository, repository.GetLiteValue()).ToArray()
                                : null;
                if (value != null && !value.Any())
                    value = null;
                // todo hardcode end

                return new
                    {
                        singleRepo.FieldName,
                        CollectionSource = convertedCS,
                        State = singleRepo.IsDeny ? FieldState.Disabled : FieldState.None,
                        Value = value
                    };
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static object ToPrepareMultipleRepository(FieldRepository repository)
        {
            var multipleRepo = repository as MultipleFieldRepository;
            return new
                {
                    multipleRepo.FieldName,
                    CollectionSource = Converter.GetConvertedCollection(repository.Creator.FieldProperty, multipleRepo.CollectionSource, multipleRepo.Request).ToArray(),
                    State = FieldState.Mutiple | (multipleRepo.IsDeny ? FieldState.Disabled : FieldState.None),
                    Value = multipleRepo.HasValue ? Converter.GetConvertedValues(repository, repository.GetLiteValue() as object[]).ToArray() : null// multipleRepo.Value.ToDictionary(k => k, v => true), multipleRepo.Request).Select(v=>v.CollectionSource[0]).ToArray() : null //todo проблема, а какой брать первым? 
                };
        }

        private static object ToPrepareBasedOnEntityRepository(FieldRepository repository)
        {
            var basedOnEntityRepository = repository as SingleFieldBasedOnEntityRepository;
            return new
            {
                basedOnEntityRepository.FieldName,
                CollectionSource = Converter.GetConvertedCollection(repository.Creator.FieldProperty, basedOnEntityRepository.CollectionSource, basedOnEntityRepository.Request).ToArray(),
                State = basedOnEntityRepository.IsDeny ? FieldState.Disabled : FieldState.None,
                Value = basedOnEntityRepository.HasValue ? Converter.GetConvertedValues(repository, basedOnEntityRepository.GetUnboxValue()).ToArray() : null// multipleRepo.Value.ToDictionary(k => k, v => true), multipleRepo.Request).Select(v=>v.CollectionSource[0]).ToArray() : null //todo проблема, а какой брать первым? 
            };
        }

        private static readonly IFormatConverter Converter = Bootstrapper.Resolve<IFormatConverter>();

        public static object Prepare(FieldRepository repo)
        {
            var preparator = (repo.GetType() == typeof(SingleFieldBasedOnEntityRepository))
                ? ToPrepareBasedOnEntityRepository
                : (repo.GetType() == typeof(SingleFieldRepository))
                    ? (Func<FieldRepository, object>)ToPrepareSingleRepository
                    : ToPrepareMultipleRepository;
            var serializedRepo = preparator(repo);
            return serializedRepo;
        }
    }
}