﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using UGSK.K3.Product.Contract.Errors;
using UGSK.K3.Service.WebAPI.ConfigFile;
using UGSK.K3.Service.WebAPI.ConfigFile.LogRouterConfig;
using UGSK.K3.Service.WebAPI.Logging.WriteLogFormatters;

namespace UGSK.K3.Service.WebAPI.Logging
{
    public class LoggerRouter
    {
        private const string K3FrontConfigSectionName = "k3Front";
        private const string DefaultLogPointName = "*";
        private const string LogPointsAreNotDefined = "Не определены точки логирования";
        private const string MustBeException = "Для логирования должно быть передано исключение (Exception throw in:{0})";
        private static object _syncRoot = new Object();
        private static IWriteLogFormatter _defaultWriteLogFormatter;

        private Dictionary<Type, IWriteLogFormatter> _exceptionFormatters;
        private Dictionary<Type, string> _routeElements;
        private static LoggerRouter _globalLogRouter;

        public static LoggerRouter Instance
        {
            get
            {
                if (_globalLogRouter == null)
                {
                    lock (_syncRoot)
                    {
                        if (_globalLogRouter == null)
                        {
                            _globalLogRouter = new LoggerRouter();
                        }
                    }
                }
                return _globalLogRouter;
            }
        }

        private LoggerRouter()
        {
            _defaultWriteLogFormatter = new DefaultExceptionWriteLogFormatter();
            _exceptionFormatters = new Dictionary<Type, IWriteLogFormatter>
            {
                {typeof(InvalidCalculationRequestException), new InvalidCalculationRequestExceptionFormatter()},
                {typeof(SimpleRequestVerificationException), new SimpleRequestVerificationExceptionWriteLogFormatter()},
            };

            var k3FrontConfig = (AppConfigConfiguration)ConfigurationManager.GetSection(K3FrontConfigSectionName);
            var routerConfiguration = k3FrontConfig.LogRouterConfiguration;
            if (routerConfiguration != null && routerConfiguration.Routes != null)
            {
                IEnumerable<RouteElement> rawRouteElements = routerConfiguration.Routes.OfType<RouteElement>().ToList();
                if (!rawRouteElements.Any())
                {
                    throw new ConfigurationErrorsException(LogPointsAreNotDefined);
                }
                _routeElements = new Dictionary<Type, string>();
                foreach (var item in rawRouteElements)
                {
                    _routeElements.Add(Type.GetType(item.ExceptionType, false, true), item.LogPointName);
                }
            }
        }

        public void Log(Type source, Exception exception, HttpRequestMessage request,
            HttpRequestContext context, HttpResponseMessage response, string incidentId = null, object additionalInfo = null)
        {
            var logPointName = SelectLogPointName(exception);
            var logger = LogManager.GetLogger(logPointName);
            IWriteLogFormatter exceptionFormatter = SelectWriteLogFormatter(exception);

            string formattedException = exceptionFormatter.Format(source, exception, request, context, response, additionalInfo);

            logger.Error(
                (string.IsNullOrEmpty(incidentId))
                    ? formattedException
                    : string.Format("Incident #{0}. {1}", incidentId, formattedException)
                );
        }

        private string SelectLogPointName(Exception exception)
        {
            if (exception == null)
            {
                exception = new ArgumentNullException(
                    string.Format(MustBeException, GetType().FullName)
                    );
            }

            string result = string.Empty;
            if (_routeElements.TryGetValue(exception.GetType(), out result))
            {
                return result;
            }

            var routeOfInheritedExceptionType = _routeElements.FirstOrDefault(x => x.Key.IsInstanceOfType(exception));

            if (routeOfInheritedExceptionType.Value != null)
            {
                return routeOfInheritedExceptionType.Value;
            }

            return DefaultLogPointName;
        }

        private IWriteLogFormatter SelectWriteLogFormatter(Exception exception)
        {
            if (exception == null)
            {
                return _defaultWriteLogFormatter;
            }

            IWriteLogFormatter result;
            return _exceptionFormatters.TryGetValue(exception.GetType(), out result)
                ? result
                : _defaultWriteLogFormatter;
        }
    }
}