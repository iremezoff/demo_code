﻿using System;
using System.Net.Http;
using System.Text;
using System.Web.Http.Controllers;
using UGSK.K3.Product.Contract.Errors;

namespace UGSK.K3.Service.WebAPI.Logging.WriteLogFormatters
{
    public class InvalidCalculationRequestExceptionFormatter : DefaultExceptionWriteLogFormatter
    {
        public InvalidCalculationRequestExceptionFormatter()
            : base()
        {                
        }

        public override string Format(Type source, Exception exception, System.Net.Http.HttpRequestMessage request, 
            HttpRequestContext context, HttpResponseMessage response, object additionalInfo)
        {
            object additionalInfoAndValidationErrors = null;
            try
            {
                var invalidCalculationRequestException = exception as InvalidCalculationRequestException;

                    var invalidCalculationErrors = new StringBuilder();
                    invalidCalculationErrors.Append("\r\n");
                    foreach (var item in invalidCalculationRequestException.Errors)
                    {
                        var memberNames = string.Format("MemberNames: {{ {0} }}", string.Join(", ", item.MemberNames));

                        invalidCalculationErrors.Append(string.Format("{{ ErrorMessage: {0}, MemberNames: {1} }}\r\n", item.ErrorMessage, memberNames));
                    }
                    additionalInfoAndValidationErrors = new
                    {
                        AdditionalInfo = additionalInfo,
                        InvalidCalculationErrors = invalidCalculationErrors.ToString()
                    };
            }
            catch (Exception ex)
            {
                additionalInfoAndValidationErrors = new { ErrorInWriteLogHandler = this.GetType().FullName, Exception = ex };
            }

            return base.Format(source, exception, request, context, response, additionalInfoAndValidationErrors);
        }
    }
}