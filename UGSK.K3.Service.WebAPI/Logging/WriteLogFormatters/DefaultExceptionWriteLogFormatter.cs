﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http.Controllers;

namespace UGSK.K3.Service.WebAPI.Logging.WriteLogFormatters
{
    public class DefaultExceptionWriteLogFormatter : IWriteLogFormatter
    {
        public virtual string Format(Type source, Exception exception, HttpRequestMessage request, HttpRequestContext context, HttpResponseMessage response, object additionalInfo)
        {
            var principal = context.Principal as ClaimsPrincipal;

            string serializedAdditionalData;
            try
            {
                serializedAdditionalData = additionalInfo != null
                    ? JsonConvert.SerializeObject(additionalInfo, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        MaxDepth = 2
                    })
                    : "no data";
            }
            catch (Exception ex)
            {
                serializedAdditionalData = string.Format("Fail on additional data's handling: {0}", ex.Message);
            }

            string claims = principal != null
                ? string.Join("; ", principal.Claims.Select(c => string.Format("{0}: {1}", c.Type, c.Value)))
                : "non-data";

            string requestContent = request.Content != null ? request.Content.ReadAsStringAsync().Result : null;

            return string.Format(
                "Exception: {0};\r\nRequest: {5}\r\nData:{9}\r\nResponse: {6};\r\nClaims: {7};\r\nMessage: {1};\r\nInner Exception: {2};\r\nSource: {3};\r\nStackTrace: {4};\r\nClassSource: {8};\r\nAdditionalInfo: {10}",
                exception.GetType().Name,
                exception.Message,
                exception.InnerException,
                exception.Source,
                exception.StackTrace, request,
                response,
                claims,
                source.FullName,
                requestContent,
                serializedAdditionalData
                );
        }
    }
}