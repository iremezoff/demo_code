﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http.Controllers;
using UGSK.K3.Product.Contract.Errors;

namespace UGSK.K3.Service.WebAPI.Logging.WriteLogFormatters
{
    public class SimpleRequestVerificationExceptionWriteLogFormatter : DefaultExceptionWriteLogFormatter
    {
        public override string Format(Type source, Exception exception, HttpRequestMessage request, HttpRequestContext context, HttpResponseMessage response, object additionalInfo)
        {
            var typedException = exception as SimpleRequestVerificationException;

            if (typedException == null) 
            {
                throw new InvalidCastException("Wrong exception type for this formatter.");
            }

            var principal = context.Principal as ClaimsPrincipal;
            return string.Format(
                    "Exception: {0};\r\nRequest: {5}\r\nData:{9}\r\nResponse: {6};\r\nClaims: {7};\r\nMessage: {1};\r\nChanged fields: {11};" + 
                    "\r\nInner Exception: {2};\r\nSource: {3};\r\nStackTrace: {4};\r\nClassSource: {8};\r\nAdditionalInfo: {10}",
                    exception.GetType().Name, // 0
                    exception.Message, // 1
                    exception.InnerException, // 2 
                    exception.Source, // 3
                    exception.StackTrace, // 4
                    request, // 5
                    response, // 6
                    principal != null
                        ? string.Join("; ", principal.Claims.Select(c => string.Format("{0}: {1}", c.Type, c.Value)))
                        : "non-data", // 7
                    source.FullName, // 8
                    request.Content != null ? request.Content.ReadAsStringAsync().Result : null, // 9
                    additionalInfo != null ? JsonConvert.SerializeObject(additionalInfo, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        MaxDepth = 2
                    }) : "no data", // 10
                    string.Join("; ", typedException.State.Select(e => e.ToString())) // 11
                    );
        }
    }
}