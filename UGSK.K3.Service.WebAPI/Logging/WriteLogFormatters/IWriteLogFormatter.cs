﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace UGSK.K3.Service.WebAPI.Logging.WriteLogFormatters
{
    public interface IWriteLogFormatter
    {
        string Format(Type source, Exception exception, HttpRequestMessage request, HttpRequestContext context, HttpResponseMessage response, object additionalInfo);
    }
}
