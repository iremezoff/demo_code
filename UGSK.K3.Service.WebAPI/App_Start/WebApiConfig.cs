﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;
using System.Web.Http.Validation;
using System.Web.OData.Extensions;
using System.Web.OData.Formatter;
using LightInject;
using UGSK.K3.AppServices.DataDeriver.Core;
using UGSK.K3.Service.Model;
using UGSK.K3.Service.WebAPI.Converting;
using UGSK.K3.Service.WebAPI.Converting.Annotations;
using UGSK.K3.Service.WebAPI.Converting.Formatters;
using UGSK.K3.Service.WebAPI.Environment;
using UGSK.K3.Service.WebAPI.Helpers.Tools;
using UGSK.K3.Service.WebAPI.Logging;
using UGSK.K3.Service.WebAPI.Validation;
using OSAGOContractDTO = UGSK.K3.Service.DTO.ContractDTO;

namespace UGSK.K3.Service.WebAPI
{
    public static class WebApiConfig
    {
        public static HttpConfiguration Configure()
        {
            var config = new HttpConfiguration();

            //: Разобраться, почему форматтеры OData вызывают Entity Framework: There is already an open DataReader associated with this Command 
            //config.Formatters.AddRange(ODataMediaTypeFormatters.Create());

            Bootstrapper.Run(Assembly.GetExecutingAssembly(), config);

            // : Почему именно этот компонент нужно регистрировать отдельно?
            var converter = new FieldValueTypeBuilder();

            Bootstrapper.Register<ITypeBuilder>(converter);
            Bootstrapper.Register<IFormatConverter>(converter);

            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new TrimmingConverter(new[]{string.Empty}));
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            config.Formatters.Insert(0, new Dto2RequestFormatter(Bootstrapper.Resolve<Func<string, IContractConverter>>(),
                Bootstrapper.Resolve<Func<HttpRequestMessage>>()));
            config.Formatters.InsertRange(0, ODataMediaTypeFormatters.Create(
                new System.Web.OData.Formatter.Serialization.DefaultODataSerializerProvider(),
                new DtoODataDeserializerProvider(Bootstrapper.Resolve<Func<string, IContractConverter>>())));

            config.Services.Replace(typeof(ModelValidatorProvider),
                                    new CustomModelValidatorProvider(Bootstrapper.Resolve<IServiceProvider>()));

            config.Filters.Add(new ExceptionInterceptor());

            // Web API routes
            // warning must be before config.MapODataServiceRoute calling (http://forums.asp.net/t/2009235.aspx?Can+not+access+Web+API+service+with+OData)4
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            
            return config;
        }
    }

    internal class HttpRequestMessageHandler : DelegatingHandler
    {
        private readonly LogicalThreadStorage<HttpRequestMessageStorage> _messageStorage =
            new LogicalThreadStorage<HttpRequestMessageStorage>(() => new HttpRequestMessageStorage());

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            _messageStorage.Value.Message = request;
            return base.SendAsync(request, cancellationToken);
        }

        public HttpRequestMessage GetCurrentMessage()
        {
            return _messageStorage.Value.Message;
        }
    }

    public class HttpRequestMessageStorage
    {
        public HttpRequestMessage Message { get; set; }
    }
}