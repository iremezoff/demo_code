﻿using System.Configuration;

namespace UGSK.K3.Service.WebAPI.ConfigFile.LogRouterConfig
{
    public class LogRouterConfiguration : ConfigurationElement
    {
        [ConfigurationProperty("routes", IsRequired = false)]
        public RoutesCollection Routes
        {
            get
            {
                return this["routes"] as RoutesCollection;
            }
        }
    }
}