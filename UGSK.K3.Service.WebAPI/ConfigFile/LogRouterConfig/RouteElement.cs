﻿using System.Configuration;

namespace UGSK.K3.Service.WebAPI.ConfigFile.LogRouterConfig
{
    public class RouteElement : ConfigurationElement
    {
        [ConfigurationProperty("exceptionType", IsRequired = true, IsKey = true)]
        public string ExceptionType
        {
            get
            {
                return this["exceptionType"] as string;
            }
        }

        [ConfigurationProperty("logPointName", IsRequired = true, IsKey = false)]
        public string LogPointName
        {
            get
            {
                return this["logPointName"] as string;
            }
        }
    }
}