﻿using System.Configuration;

namespace UGSK.K3.Service.WebAPI.ConfigFile.LogRouterConfig
{
    public class RoutesCollection : ConfigurationElementCollection
    {
        private const string RouteKey = "route";

        protected override ConfigurationElement CreateNewElement()
        {
            return new RouteElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RouteElement)element).ExceptionType;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return RouteKey; }
        }
    }
}