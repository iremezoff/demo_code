﻿using System.Configuration;

namespace UGSK.K3.Service.WebAPI.ConfigFile.ProductConfig
{
    public class SingularComponent : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("serviceType", IsRequired = true, IsKey = false)]
        public string ServiceType
        {
            get
            {
                return this["serviceType"] as string;
            }
        }

        [ConfigurationProperty("componentType", IsRequired = true, IsKey = false)]
        public string ComponentType
        {
            get
            {
                return this["componentType"] as string;
            }
        }
    }
}