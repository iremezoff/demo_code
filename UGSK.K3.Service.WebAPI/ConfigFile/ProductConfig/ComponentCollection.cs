﻿using System.Configuration;

namespace UGSK.K3.Service.WebAPI.ConfigFile.ProductConfig
{
    public class ComponentCollection : ConfigurationElementCollection
    {
        private const string ProductKey = "component";

        protected override ConfigurationElement CreateNewElement()
        {
            return new SingularComponent();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SingularComponent)element).Name;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return ProductKey; }
        }
    }
}