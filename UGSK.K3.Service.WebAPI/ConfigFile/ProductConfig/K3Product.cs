﻿using System.Configuration;

namespace UGSK.K3.Service.WebAPI.ConfigFile.ProductConfig
{
    public class K3Product : ConfigurationSection
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("assembly", IsRequired = true, IsKey = false)]
        public string AssemblyName
        {
            get
            {
                return this["assembly"] as string;
            }
        }

        [ConfigurationProperty("requestType", IsRequired = true, IsKey = false)]
        public string RequestTypeName
        {
            get
            {
                return this["requestType"] as string;
            }
        }

        [ConfigurationProperty("converterType", IsRequired = false, IsKey = false)]
        public string ConverterTypeName
        {
            get
            {
                return this["converterType"] as string;
            }
        }

        [ConfigurationProperty("components", IsRequired = false)]
        public ComponentCollection Components
        {
            get
            {
                return this["components"] as ComponentCollection;
            }
        }
    }
}