﻿using System.Configuration;

namespace UGSK.K3.Service.WebAPI.ConfigFile.ProductConfig
{
    public class ProductsCollection : ConfigurationElementCollection
    {
        private const string ProductKey = "product";

        protected override ConfigurationElement CreateNewElement()
        {
            return new K3Product();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((K3Product)element).Name;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return ProductKey; }
        }
    }
}