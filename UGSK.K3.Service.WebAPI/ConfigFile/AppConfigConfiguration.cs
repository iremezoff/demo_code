﻿using System.Configuration;
using UGSK.K3.Service.WebAPI.ConfigFile.LogRouterConfig;
using UGSK.K3.Service.WebAPI.ConfigFile.ProductConfig;

namespace UGSK.K3.Service.WebAPI.ConfigFile
{
    public class AppConfigConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("products")]
        public ProductsCollection Products
        {
            get
            {
                return this["products"] as ProductsCollection;
            }
        }

       

        [ConfigurationProperty("logRouter", IsRequired = false)]
        public LogRouterConfiguration LogRouterConfiguration
        {
            get
            {
                return this["logRouter"] as LogRouterConfiguration;
            }
        }
    }
}