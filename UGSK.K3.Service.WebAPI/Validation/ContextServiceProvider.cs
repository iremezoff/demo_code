﻿using System;
using System.Web.Http;

namespace UGSK.K3.Service.WebAPI.Validation
{
    public class ContextServiceProvider : IServiceProvider
    {
        private readonly HttpConfiguration _httpConfig;

        public ContextServiceProvider(HttpConfiguration httpConfiguration)
        {
            _httpConfig = httpConfiguration;
        }
        public object GetService(Type serviceType)
        {
            return _httpConfig.DependencyResolver.GetService(serviceType);
        }
    }
}
