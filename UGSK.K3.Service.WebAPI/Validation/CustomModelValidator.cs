﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Http.Metadata;
using System.Web.Http.Validation;
using System.Web.Http.Validation.Validators;

namespace UGSK.K3.Service.WebAPI.Validation
{
    class CustomModelValidator : DataAnnotationsModelValidator
    {
        private readonly IServiceProvider _provider;

        public CustomModelValidator(IServiceProvider provider, IEnumerable<ModelValidatorProvider> validatorProviders,
                                    ValidationAttribute attribute)
            : base(validatorProviders, attribute)
        {
            _provider = provider;            
        }

        public override IEnumerable<ModelValidationResult> Validate(ModelMetadata metadata, object container)
        {
            string memberName = metadata.GetDisplayName();
            ValidationContext context = new ValidationContext(container ?? metadata.Model, _provider, null)
                {
                    DisplayName = memberName,
                    MemberName = memberName
                };

            ValidationResult result = Attribute.GetValidationResult(metadata.Model, context);

            if (result != ValidationResult.Success)
            {
                // ModelValidationResult.MemberName is used by invoking validators (such as ModelValidationNode) to 
                // construct the ModelKey for ModelStateDictionary. When validating at type level we want to append the 
                // returned MemberNames if specified (e.g. person.Address.FirstName). For property validation, the 
                // ModelKey can be constructed using the ModelMetadata and we should ignore MemberName (we don't want 
                // (person.Name.Name). However the invoking validator does not have a way to distinguish between these two 
                // cases. Consequently we'll only set MemberName if this validation returns a MemberName that is different
                // from the property being validated.

                string errorMemberName = result.MemberNames.FirstOrDefault();
                if (String.Equals(errorMemberName, memberName, StringComparison.Ordinal))
                {
                    errorMemberName = null;
                }

                var validationResult = new ModelValidationResult
                    {
                        Message = result.ErrorMessage,
                        MemberName = errorMemberName
                    };

                return new[] { validationResult };
            }

            return Enumerable.Empty<ModelValidationResult>();
        }
    }
}