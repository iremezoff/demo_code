﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Http.Metadata;
using System.Web.Http.Validation;
using System.Web.Http.Validation.Providers;

namespace UGSK.K3.Service.WebAPI.Validation
{
    public class CustomModelValidatorProvider : DataAnnotationsModelValidatorProvider
    {
        private IServiceProvider _serviceProvider;

        public CustomModelValidatorProvider(IServiceProvider provider)
        {
            _serviceProvider = provider;
        }

        protected override IEnumerable<ModelValidator> GetValidators(ModelMetadata metadata, IEnumerable<ModelValidatorProvider> validatorProviders, IEnumerable<Attribute> attributes)
        {
            var results = new List<ModelValidator>();

            // Produce a validator for each validation attribute we find
            foreach (ValidationAttribute attribute in attributes.OfType<ValidationAttribute>())
            {
                results.Add(new CustomModelValidator(_serviceProvider, validatorProviders, attribute));
            }

            return results;
        }
    }
}