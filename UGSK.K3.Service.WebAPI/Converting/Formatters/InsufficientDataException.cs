using System;

namespace UGSK.K3.Service.WebAPI.Converting.Formatters
{
    public class InsufficientDataException : ApplicationException
    {
        public InsufficientDataException(string message)
            : base(message)
        { }
    }
}