﻿using Microsoft.OData.Core;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.OData;
using System.Web.OData.Formatter.Deserialization;

namespace UGSK.K3.Service.WebAPI.Converting.Formatters
{
    public class DtoODataDeserializerProvider : ODataDeserializerProvider
    {
        private readonly Func<string, IContractConverter> _converterSource;

        public DtoODataDeserializerProvider(Func<string, IContractConverter> converterSource) 
        {
            _converterSource = converterSource;
        }

        public override ODataEdmTypeDeserializer GetEdmTypeDeserializer(Microsoft.OData.Edm.IEdmTypeReference edmType)
        {
            return new DefaultODataDeserializerProvider().GetEdmTypeDeserializer(edmType);
        }

        public override ODataDeserializer GetODataDeserializer(Microsoft.OData.Edm.IEdmModel model, Type type, HttpRequestMessage request)
        {
            //Используем специальный десериализатор только для контроллера PVS
            if (type.IsAssignableFrom(typeof(ODataActionParameters)) &&
                request.RequestUri.Segments.Any(s => s.Contains("pvs")))
            {
                return new PVSODataDeserializer(this, _converterSource);
            }
            else
            {
                return new DefaultODataDeserializerProvider().GetODataDeserializer(model, type, request);
            }
        }
    }

    public class PVSODataDeserializer : ODataActionPayloadDeserializer
    {
        private readonly Func<string, IContractConverter> _converterSource;

        public PVSODataDeserializer(ODataDeserializerProvider deserializerProvider, Func<string, IContractConverter> converterSource)
            : base(deserializerProvider)
        {
            _converterSource = converterSource;
        }

        public override object Read(ODataMessageReader messageReader, Type type, ODataDeserializerContext readContext)
        {
            var actionParameters = readContext.Request.Content.ReadAsAsync<ODataActionParameters>().Result;
            
            var productName = actionParameters.ContainsKey("productName") ? actionParameters["productName"] : null;
            if (productName == null)
            {
                throw new InsufficientDataException("Недостаточно данных для десериализации договора");
            }

            var converter = _converterSource(productName.ToString());
            if (converter == null)
            {
                return null;
            }

            var contract = actionParameters.ContainsKey("contract") ? actionParameters["contract"] : null;
            if (contract == null)
            {
                throw new InsufficientDataException("Недостаточно данных для десериализации договора");
            }

            var obj = JsonConvert.DeserializeObject(contract.ToString(), converter.DtoType);
            actionParameters["contract"] = obj;
            return actionParameters;
        }
    }
}