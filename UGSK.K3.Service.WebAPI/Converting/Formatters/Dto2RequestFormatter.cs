using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.Service.WebAPI.Converting.Formatters
{
    public class Dto2RequestFormatter : JsonMediaTypeFormatter
    {
        private readonly Func<string, IContractConverter> _converterSource;
        private readonly Func<HttpRequestMessage> _requestSource;

        public Dto2RequestFormatter(Func<string, IContractConverter> converterSource, Func<HttpRequestMessage> requestSource)
        {
            _converterSource = converterSource;
            _requestSource = requestSource;
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            MediaTypeMappings.Add(new UriPathExtensionMapping("json", "application/json"));
        }

        public override bool CanReadType(Type type)
        {
            return typeof(IRequest) == type;
        }

        public override bool CanWriteType(Type type)
        {
            return false;
        }

        public override async Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
// ��� ��������� ����������� ������ �� ������
#if DEBUG 
            var e = new StreamReader(readStream).ReadToEnd();

            readStream = new MemoryStream(Encoding.UTF8.GetBytes(e));
#endif

            var requestMessage = _requestSource(); // HttpContext.Current.Items[ServiceEnvironmentConstants.RequestMessage] as HttpRequestMessage;
            var productName = requestMessage.GetRouteData().Values["productName"];
            if (productName == null)
            {
                throw new InsufficientDataException("������������ ������ ��� �������������� ��������");
            }
            var converter = _converterSource(productName.ToString());
            if (converter == null)
            {
                return null; // ���� ������� �� ���������������
            }

            var obj = await base.ReadFromStreamAsync(converter.DtoType, readStream, content, formatterLogger);
            return converter.GetFormedDto(obj);
        }
    }
}