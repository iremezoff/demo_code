﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UGSK.K3.Service.WebAPI.Converting
{
    public class TrimmingConverter : JsonConverter
    {
        private IEnumerable<string> _excludedField;

        public TrimmingConverter(IEnumerable<string> excludedField)
        {
            _excludedField = excludedField;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }

        public override bool CanRead
        {
            get
            {
                return true;
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
                return null;

            return reader.ValueType != typeof(string) || _excludedField.Contains(reader.Path) ? reader.Value.ToString() : ((string)reader.Value).Trim();
        }

        public override bool CanWrite
        {
            get
            {
                return false;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}