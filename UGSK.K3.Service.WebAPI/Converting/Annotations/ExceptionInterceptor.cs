﻿﻿using System;
﻿using System.Collections.Generic;
﻿using System.Linq;
﻿using System.Net;
﻿using System.Net.Http;
﻿using System.Reflection;
﻿using System.Text;
﻿using System.Threading;
﻿using System.Threading.Tasks;
﻿using System.Web.Http.Filters;
﻿using AutoMapper;
﻿using FluentValidation.Results;
﻿using Microsoft.OData.Core;
﻿using Newtonsoft.Json;
﻿using UGSK.K3.Product.Contract;
﻿using UGSK.K3.Product.Contract.Errors;
﻿using UGSK.K3.Service.WebAPI.Converting.Formatters;
﻿using UGSK.K3.Service.WebAPI.Logging;
﻿using UGSK.K3.Service.WebAPI.Security;
﻿using ValidationException = FluentValidation.ValidationException;
﻿using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace UGSK.K3.Service.WebAPI.Converting.Annotations
{
    public abstract class BaseExceptionInterceptor : ActionFilterAttribute, IExceptionFilter
    {
        private static readonly LoggerRouter LogRouter;

        private static readonly Dictionary<Type, Action<ExceptionVisitor, Exception>> VisitHandlers;

        /// <summary>
        /// Фильтры как правило живут на протяжении всей жизни приложения, в отличие от контроллеров. 
        /// Но при этом на контроллер всё равно создается индивидуальный экземпляр, в итоге сбор информации об обработчиках происходит всякий раз, что не требется
        /// </summary>
        static BaseExceptionInterceptor()
        {
            LogRouter = LoggerRouter.Instance;

            var delegateFactory = ((Func<MethodInfo, Action<ExceptionVisitor, Exception>>)CreateDelegate<Exception>).GetMethodInfo().GetGenericMethodDefinition();
            VisitHandlers = typeof(ExceptionVisitor).GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(
                    m =>
                        m.Name.StartsWith("Visit") && m.GetParameters().Count() == 1 &&
                        typeof(Exception).IsAssignableFrom(m.GetParameters().Single().ParameterType))
                .ToDictionary(k => k.GetParameters().Single().ParameterType,
                    v =>
                        delegateFactory.MakeGenericMethod(v.GetParameters().Single().ParameterType)
                            .Invoke(null, new object[] { v }) as
                            Action<ExceptionVisitor, Exception>);
        }


        private static Action<ExceptionVisitor, Exception> CreateDelegate<T>(MethodInfo methodInfo) where T : Exception
        {
            var nestedDelegate = methodInfo.CreateDelegate(typeof(Action<,>).MakeGenericType(methodInfo.DeclaringType, typeof(T))) as Action<ExceptionVisitor, T>;

            return (v, e) => nestedDelegate(v, (T)e);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception != null)
            {
                HandleException(actionExecutedContext);
            }

            base.OnActionExecuted(actionExecutedContext);
        }

        public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            return Task.Run(() => HandleException(actionExecutedContext), cancellationToken);
        }

        private void HandleException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception == null)
                return;

            string incidentId = string.Format("{0}", DateTime.Now.ToString("ddMMyyHHmmss"));



            var visitorList = new List<ExceptionVisitor>();

            var httpVisitor = CreateHttpExceptionVisitor(actionExecutedContext, incidentId);
            visitorList.Add(httpVisitor);

            //var bugsnagVisitor = new BugsnagInformationCollectorExceptionVisitor(employee, incidentId);
            //visitorList.Add(bugsnagVisitor);

#if DEMO || PRODUCTION

            AppInsightInformationCollectorExceptionVisitor aiVisitor = null;
            if (EnabledTelemetry == null)
            {
                object disabledAppInsight;
                actionExecutedContext.ActionContext.RequestContext.Configuration.Properties.TryGetValue(
                    "EnabledAppInsights", out disabledAppInsight);
                EnabledTelemetry = (bool?)disabledAppInsight ?? false;
            }

            if (EnabledTelemetry.Value)
            {
                aiVisitor = new AppInsightInformationCollectorExceptionVisitor(employee, incidentId);
                visitorList.Add(aiVisitor);
            }

#endif

            var objectDumpVisitor = new ObjectDumpCatcherExceptionVisitor();
            visitorList.Add(objectDumpVisitor);

            var obsoleteVisitor = new ObsoleteExceptionInterCeptor();
            visitorList.Add(obsoleteVisitor);

            var vistor = new AggregateExceptionVisitor(visitorList);

            if (!HandleByVisitor(actionExecutedContext, vistor))
            {
                vistor.VisitUnhandledException(actionExecutedContext.Exception);
            }

            LogRouter.Log(GetType(), actionExecutedContext.Exception, actionExecutedContext.Request,
                actionExecutedContext.ActionContext.RequestContext, actionExecutedContext.Response,
                incidentId,
                new
                {
                    AdditionalData = "Dump",
                    actionExecutedContext.ActionContext.ActionArguments, obsoleteVisitor.ErrorData, objectDumpVisitor.ObjectDump
                });

#if DEMO || PRODUCTION

            if (aiVisitor != null)
                aiVisitor.Client.TrackException(aiVisitor.Telemetry);

            //WebAPIClient.Notify(actionExecutedContext.Exception, bugsnagVisitor.Severity, bugsnagVisitor.Metadata);
#endif

        }

        private bool HandleByVisitor(HttpActionExecutedContext actionExecutedContext, AggregateExceptionVisitor vistor)
        {
            Action<ExceptionVisitor, Exception> handler;

            if (VisitHandlers.TryGetValue(actionExecutedContext.Exception.GetType(), out handler))
            {
                handler(vistor, actionExecutedContext.Exception);
                return true;
            }
            // todo Ремезов: с внутренними исключениями пока не придуман метод обработки. Может просто в этом случае постараться выбрасывать оригинальное исключение?
            if (actionExecutedContext.Exception.InnerException is ImpersonateForbiddenException &&
                VisitHandlers.TryGetValue(actionExecutedContext.Exception.InnerException.GetType(), out handler))
            {
                handler(vistor, actionExecutedContext.Exception.InnerException);
                return true;
            }
            return false;
        }

        
        protected abstract HttpResponseBuilderExceptionVisitor CreateHttpExceptionVisitor(HttpActionExecutedContext actionExecutedContext, string incidentid);
    }

    public class ExceptionInterceptor : BaseExceptionInterceptor
    {
        protected override HttpResponseBuilderExceptionVisitor CreateHttpExceptionVisitor(HttpActionExecutedContext actionExecutedContext, string incidentid)
        {
            return new HttpResponseBuilderExceptionVisitor(actionExecutedContext, incidentid);
        }
    }

    public abstract class ExceptionVisitor
    {
        //public virtual void VisitRequestFieldNotFoundException(RequestFieldNotFoundException exception) {}

        public virtual void VisitInvalidCalculationRequestException(InvalidCalculationRequestException exception)
        {
            foreach (var error in exception.Errors)
            {
                VisitValidationResult(error);
            }
        }

        
        public virtual void VisitEnrichException(ApplicationException exception) { }
        public virtual void VisitInsufficientDataException(InsufficientDataException exception) { }
        public virtual void VisitRestrictionException(RestrictionException exception) { }
        public virtual void VisitImpersonateForbiddenException(ImpersonateForbiddenException exception) { }
        public virtual void VisitInvalidOperationException(InvalidOperationException exception) { }
        public virtual void VisitSignContractException(ApplicationException exception) { }
        public virtual void VisitInvalidStatusTransitionException(ApplicationException exception) { }
        public virtual void VisitInvalidStageTransitionException(ApplicationException exception) { }
        public virtual void VisitSimpleRequestVerificationException(SimpleRequestVerificationException exception) { }
        public virtual void VisitApplyingArtificialCalculationException(ApplicationException exception) { }
        public virtual void VisitContractAndCalculationResponseMismatchException(ApplicationException exception) { }

        public virtual void VisitValidationException(ValidationException exception)
        {
            foreach (var error in exception.Errors)
            {
                VisitValidationFailure(error);
            }
        }

        protected virtual void VisitValidationResult(ValidationResult result) { }
        protected virtual void VisitValidationFailure(ValidationFailure result) { }

        

        public virtual void VisitCreateDublicateContractException(CreateDublicateContractException exception) { }
        public virtual void VisitGetKbmException(ApplicationException exception) { }
        public virtual void VisitUnhandledException(Exception exception) { }
    }

    public class ObjectDumpCatcherExceptionVisitor : ExceptionVisitor
    {
        public object ObjectDump { get; private set; }

        public override void VisitInvalidCalculationRequestException(InvalidCalculationRequestException exception)
        {
            base.VisitInvalidCalculationRequestException(exception);

            ObjectDump = exception.Request;
        }
    }

    public class HttpResponseBuilderExceptionVisitor : ExceptionVisitor
    {
        private readonly HttpActionExecutedContext _context;
        private readonly string _incidentId;

        public HttpResponseBuilderExceptionVisitor(HttpActionExecutedContext context, string incidentId)
        {
            _context = context;
            _incidentId = incidentId;
        }

        public HttpActionExecutedContext Context
        {
            get { return _context; }
        }

        


        public override void VisitInvalidCalculationRequestException(InvalidCalculationRequestException exception)
        {
            base.VisitInvalidCalculationRequestException(exception);

            BadRequest(exception);
        }

        protected override void VisitValidationResult(ValidationResult result)
        {
            Context.ActionContext.ModelState.AddModelError(result.MemberNames.FirstOrDefault() ?? string.Empty,
                result.ErrorMessage);
        }

        public override void VisitInsufficientDataException(InsufficientDataException exception)
        {
            Context.ActionContext.ModelState.AddModelError("Common", exception.Message);
            BadRequest(exception);
        }

        public override void VisitRestrictionException(RestrictionException exception)
        {
            Context.ActionContext.ModelState.AddModelError("Discount", exception.Message);
            BadRequest(exception);
        }

        

        public override void VisitImpersonateForbiddenException(ImpersonateForbiddenException exception)
        {
            Context.Response = Context.Request.CreateErrorResponse(HttpStatusCode.Forbidden,
                    Context.Exception);
            Forbidden(exception);
        }

        
        public override void VisitInvalidOperationException(InvalidOperationException exception)
        {
            Forbidden(exception);
        }

        public override void VisitSimpleRequestVerificationException(SimpleRequestVerificationException exception)
        {
            Forbidden(exception);
        }

        public override void VisitValidationException(ValidationException exception)
        {
            base.VisitValidationException(exception);
            BadRequest(exception);
        }

        
        public override void VisitSignContractException(ApplicationException exception)
        {
            base.VisitSignContractException(exception);
            Context.Response = Context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
        }

        public override void VisitInvalidStatusTransitionException(ApplicationException exception)
        {
            base.VisitInvalidStatusTransitionException(exception);
            Context.Response = Context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
        }

        public override void VisitInvalidStageTransitionException(ApplicationException exception)
        {
            base.VisitInvalidStageTransitionException(exception);
            Context.Response = Context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
        }

        public override void VisitCreateDublicateContractException(CreateDublicateContractException exception)
        {
            Context.Response = Context.Request.CreateResponse(HttpStatusCode.BadRequest, exception.Message);
            var baseUri = new Uri(Context.Request.RequestUri.GetComponents(UriComponents.SchemeAndServer, UriFormat.SafeUnescaped));
            Uri newLocation;
            Uri.TryCreate(baseUri, exception.RedirectLocation, out newLocation);
            Context.Response.Headers.Location = newLocation;
        }

        public override void VisitGetKbmException(ApplicationException exception)
        {
            base.VisitGetKbmException(exception);
            Context.Response = Context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
        }

        public override void VisitApplyingArtificialCalculationException(ApplicationException exception)
        {
            Forbidden(exception);
        }

        public override void VisitContractAndCalculationResponseMismatchException(
            ApplicationException exception)
        {
            Context.ActionContext.ModelState.AddModelError("Calculations", exception.Message);
            BadRequest(exception);
        }

        private void Forbidden(Exception exc)
        {
            Context.Response = Context.Request.CreateErrorResponse(HttpStatusCode.Forbidden, exc.Message, exc);
        }

        protected virtual void BadRequest(Exception exc)
        {
            Context.Response = Context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, Context.ActionContext.ModelState);
        }

        public override void VisitUnhandledException(Exception exception)
        {
            // обработичков ПОКА нет либо неожиданная ошибка
            Context.Response =
                Context.Request.CreateResponse(HttpStatusCode.InternalServerError,
                    string.Format("Что-то пошло не так. Обратитесь к разработчикам. Incident #{0}", _incidentId));
        }
    }

    public class ODataHttpResponseBuilderExceptionVisitor : HttpResponseBuilderExceptionVisitor
    {
        private List<Tuple<string, string>> _odataErrors;

        public ODataHttpResponseBuilderExceptionVisitor(HttpActionExecutedContext context, string incidentId)
            : base(context, incidentId)
        {
            _odataErrors = new List<Tuple<string, string>>();
        }

        protected override void VisitValidationFailure(ValidationFailure result)
        {
            _odataErrors.Add(Tuple.Create(result.PropertyName, result.ErrorMessage));
        }

        protected override void VisitValidationResult(ValidationResult result)
        {
            _odataErrors.Add(Tuple.Create(result.MemberNames.FirstOrDefault(), result.ErrorMessage));
        }

       protected override void BadRequest(Exception exc)
        {
            var odataError = CreateODataErrorWithErrorMessages(_odataErrors, exc);
            Context.Response = Context.Request.CreateResponse(HttpStatusCode.BadRequest, odataError);
        }

        /// <summary>
        /// FluentValidation.ValidationFailure and DbValidationError are not implements one interface. 
        /// For except code duplication this method operates with Tuples
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        private ODataError CreateODataErrorWithErrorMessages(IEnumerable<Tuple<string, string>> collection, Exception exception)
        {
            var odataValidationErrors = new Dictionary<string, List<string>>();

            foreach (var error in collection)
            {
                var key = error.Item1.Replace(".", string.Empty);
                var message = error.Item2;
                if (!odataValidationErrors.ContainsKey(key))
                {
                    odataValidationErrors.Add(key, new List<string> { message });
                }
                else
                {
                    odataValidationErrors[key].Add(message);
                }
            }

            var odataError = new ODataError
            {
                ErrorCode = "Validation error",
                Message = JsonConvert.SerializeObject(odataValidationErrors),
                InnerError = new ODataInnerError(exception)
            };
            return odataError;
        }
    }

    public class ObsoleteExceptionInterCeptor : ExceptionVisitor
    {
        private StringBuilder _builder;

        public string ErrorData
        {
            get { return _builder.ToString(); }
        }

        public ObsoleteExceptionInterCeptor()
        {
            _builder = new StringBuilder();
        }

        public override void VisitEnrichException(ApplicationException exception)
        {
            _builder.AppendLine(exception.Message);
        }

        protected override void VisitValidationResult(ValidationResult result)
        {
            _builder.AppendLine(result.ErrorMessage);
        }

        public override void VisitInsufficientDataException(InsufficientDataException exception)
        {
            _builder.AppendLine(exception.Message);
        }

        public override void VisitRestrictionException(RestrictionException exception)
        {
            _builder.AppendLine(exception.Message);
        }

        public override void VisitSignContractException(ApplicationException exception)
        {
            _builder.AppendLine(exception.Message);
        }

        public override void VisitInvalidStatusTransitionException(ApplicationException exception)
        {
            _builder.AppendLine(exception.Message);
        }
        public override void VisitInvalidStageTransitionException(ApplicationException exception)
        {
            _builder.AppendLine(exception.Message);
        }
    }

    public class AggregateExceptionVisitor : ExceptionVisitor
    {
        private readonly List<ExceptionVisitor> _innerExceptionVisitors;


        public AggregateExceptionVisitor(IEnumerable<ExceptionVisitor> innerExceptionVisitors)
        {
            _innerExceptionVisitors = innerExceptionVisitors.ToList();
        }

        public override void VisitInvalidCalculationRequestException(InvalidCalculationRequestException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitInvalidCalculationRequestException(exception));
        }

        public override void VisitEnrichException(ApplicationException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitEnrichException(exception));
        }

        public override void VisitInsufficientDataException(InsufficientDataException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitInsufficientDataException(exception));
        }

        public override void VisitRestrictionException(RestrictionException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitRestrictionException(exception));
        }

        

        public override void VisitImpersonateForbiddenException(ImpersonateForbiddenException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitImpersonateForbiddenException(exception));
        }

        
        public override void VisitInvalidOperationException(InvalidOperationException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitInvalidOperationException(exception));
        }

        public override void VisitSimpleRequestVerificationException(SimpleRequestVerificationException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitSimpleRequestVerificationException(exception));
        }

        public override void VisitValidationException(ValidationException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitValidationException(exception));
        }

        

        public override void VisitSignContractException(ApplicationException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitSignContractException(exception));
        }

        public override void VisitInvalidStatusTransitionException(ApplicationException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitInvalidStatusTransitionException(exception));
        }

        public override void VisitInvalidStageTransitionException(ApplicationException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitInvalidStageTransitionException(exception));
        }
        
        public override void VisitCreateDublicateContractException(CreateDublicateContractException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitCreateDublicateContractException(exception));
        }

        public override void VisitGetKbmException(ApplicationException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitGetKbmException(exception));
        }

        public override void VisitApplyingArtificialCalculationException(ApplicationException exception)
        {
            _innerExceptionVisitors.ForEach(v => v.VisitApplyingArtificialCalculationException(exception));
        }

        public override void VisitContractAndCalculationResponseMismatchException(
            ApplicationException exception)
        {
            _innerExceptionVisitors.ForEach(v=>v.VisitContractAndCalculationResponseMismatchException(exception));
        }
    }
}