using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Filters;
using UGSK.K3.Infrastructure;

namespace UGSK.K3.Service.WebAPI.Converting.Annotations
{
    //: � ����� ������ ����� �������� ��� [AttributeUsage(AttributeTargets.Class]. 
    // � ���� ����� ��� �� ������� ������������ ��� �������� �����������
    /// <summary>
    /// ���� ������ ������������ ������ ��� ������������, ����� ������������� ��� ����� ������ ����� ���������� �������� �������� ��� ��������
    /// </summary>
    public class ResultTransformerAttribute : ActionFilterAttribute
    {
        private const string _jsonMediaType = "application/json";
        public Type SourceType { get; private set; }
        public Type DestinationType { get; private set; }
        public bool TransformCollections { get; set; }

        public Func<Type, Type, ICommonMapper> MapperFactory { get; set; }

        public ResultTransformerAttribute(Type sourceType, Type destinationType)
        {
            SourceType = sourceType;
            DestinationType = destinationType;
            TransformCollections = true;
        }
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);

            var result = actionExecutedContext.Response != null ? (actionExecutedContext.Response.Content as ObjectContent) : null;

            if (result == null || result.Value == null || typeof(HttpError).Equals(result.ObjectType))
                return;

            if (!IsSourceIsEnumerable(result.Value) && !SourceType.IsInstanceOfType(result.Value))
                return;


            Type destinationTypeWrapper = DestinationType;
            var mapper = MapperFactory(SourceType, DestinationType);

            if (IsSourceIsEnumerable(result.Value) && TransformCollections)
            {
                mapper = MapperFactory(result.Value.GetType(), typeof(IList<>).MakeGenericType(new[] { DestinationType }));
                destinationTypeWrapper = typeof(IList<>).MakeGenericType(DestinationType);
            }

            var mappedObj = mapper.Map(result.Value);
            actionExecutedContext.Response.Content = new ObjectContent(destinationTypeWrapper, mappedObj,
                result.Formatter,
                actionExecutedContext.Request.Headers.Accept.FirstOrDefault() ?? new MediaTypeHeaderValue(_jsonMediaType));
        }

        private bool IsSourceIsEnumerable(object value)
        {
            return value.GetType().GetInterfaces().Contains(typeof(IEnumerable<>).MakeGenericType(SourceType));
        }
    }
}