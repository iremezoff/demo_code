﻿using System;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.Service.WebAPI.Converting
{
    public interface IContractConverter
    {
        string ProductName { get; }
        Type DtoType { get; }
        Type RequestType { get; }
        object GetFormedDto(object deserializedObject);
        IRequest GetFormedRequest(object dtoObject);
    }
}
