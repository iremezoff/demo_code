﻿using System;
using UGSK.K3.Product.Contract;

namespace UGSK.K3.Service.WebAPI.Converting
{
    // ReSharper disable UnusedMember.Global
    public class DefaultContractConverter : IContractConverter
    // ReSharper restore UnusedMember.Global
    {
        private readonly Type _requestType;
        public string ProductName { get { return "Anyone"; } }
        public Type DtoType
        {
            get { return _requestType; }
        }
        public Type RequestType
        {
            get { return _requestType; }
        }

        public DefaultContractConverter(Type requestType)
        {
            _requestType = requestType;
        }

        public object GetFormedDto(object deserializedObject)
        {
            return deserializedObject;
        }

        public IRequest GetFormedRequest(object dtoObject)
        {
            return (IRequest)dtoObject;
        }
    }
}