﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.SelfHost;
using Moq;
using Moq.Language.Flow;
using Moq.Protected;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace UGSK.K3.Infrastructure.Search.Test
{
    [TestFixture]
    public class ElasticProxyTest
    {
        private ElasticProxy<TestSearchResult> _target;
        private Mock<ISearchConfig> _mockConfig;
        private HttpSelfHostServer _httpServer;
        private string _serverUri = "http://localhost:65100";
        //private InterceptorDelegateHandler _handler;
        private Mock<DelegatingHandler> _mockHandler;
        private IReturnsResult<DelegatingHandler> _result;
        private string _content;
        private TypeConfig _typeConfig;

        [TestFixtureSetUp]
        public void InitFixture()
        {
            _typeConfig = new TypeConfig();
            _typeConfig.IndexName = "index";
            _typeConfig.IndexTypeName = "type";
            _typeConfig.KeyName = "uuid";
            _typeConfig.ResultType = typeof(TestSearchResult);

            _mockConfig = new Mock<ISearchConfig>();
            _mockConfig.Setup(m => m[typeof(TestSearchResult)]).Returns(_typeConfig);

            _mockHandler = new Mock<DelegatingHandler>();

            int attempts = 5;

            while (attempts-- > 0)
            {
                try
                {
                    var uri = new Uri(
                        _serverUri.Substring(0,
                            _serverUri.IndexOf(":", _serverUri.IndexOf("localhost", StringComparison.Ordinal),
                                StringComparison.CurrentCulture)) + ":" + new Random().Next(60000, 65000) + "/");

                    _mockConfig.Setup(p => p.Host).Returns(uri.Host);
                    _mockConfig.Setup(p => p.Port).Returns(uri.Port);

                    var configuration = new HttpSelfHostConfiguration(uri);
                    configuration.MessageHandlers.Insert(0, _mockHandler.Object);
                    _httpServer = new HttpSelfHostServer(configuration);

                    _httpServer.OpenAsync().Wait();
                    break;
                }
                catch (Exception ex)
                {
                    if (attempts == 0)
                        throw new Exception("Попытки закончились", ex);
                }
            }
        }

        [TestFixtureTearDown]
        public void CleanFixture()
        {

            _httpServer.CloseAsync().Wait();
        }

        [SetUp]
        public void Init()
        {
            _target = new ElasticProxy<TestSearchResult>(_mockConfig.Object);

            // WebAPI Fake

            _content = "{took: 102, timed_out: false, " +
                       "_shards: {total: 1, successful: 1, failed: 0}, " +
                       "hits: {total: 0, max_score: null, hits: []}}";

            _result = _mockHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .Returns((HttpRequestMessage request, CancellationToken token) =>
                {
                    var httpResponse = new HttpResponseMessage
                    {
                        RequestMessage = request,
                        Content = new StringContent(_content)
                    };
                    httpResponse.Content.Headers.ContentType =
                        new MediaTypeWithQualityHeaderValue("application/json");
                    return Task.FromResult(httpResponse);
                });
        }

        [TearDown]
        public void Clean()
        {
            _mockConfig.ResetCalls();
        }

        [Test]
        [ExpectedException(typeof(InvalidDataException))]
        public void Ctor_Exception_InvalidDataExceptionMustBeThrownWhereAreNoConfigurationForParameterOfProxy()
        {
            // without any configuration
            var mockConfig = new Mock<ISearchConfig>();
            _target = new ElasticProxy<TestSearchResult>(mockConfig.Object);

            mockConfig.Verify(m => m[typeof(TestSearchResult)], Times.Once);
        }

        [Test]
        public void Search_Request_WithoutFilterElasticRequestMustBeSent()
        {
            var input = "test input";

            string actualRequest = null;

            _result.Callback((HttpRequestMessage request, CancellationToken token) =>
                    {
                        actualRequest = request.Content.ReadAsStringAsync().Result;
                    });

            var answer = _target.Search(input);

            Assert.IsNotNull(actualRequest);

            var response = JsonConvert.DeserializeObject(actualRequest) as dynamic;
            var actual = response.query.@bool.must[0].query_string as JObject;

            Assert.AreEqual("_all", actual["default_field"].Value<string>());
            Assert.AreEqual(input, actual["query"].Value<string>());
        }

        [Test]
        public void SearchByAnd_Request_WithFilterPredicateElasticRequestMustBeSentWithStars()
        {
            string[] input = { "test", "input" };

            string actualRequest = null;

            _result.Callback((HttpRequestMessage request, CancellationToken token) =>
                    {
                        actualRequest = request.Content.ReadAsStringAsync().Result;
                    });


            var answer = _target.SearchByAnd(input);

            Assert.IsNotNull(actualRequest);
            CollectionAssert.IsEmpty(answer);

            var response = JsonConvert.DeserializeObject(actualRequest) as dynamic;
            var actual = response.query.@bool.must[0].query_string as JObject;

            Assert.AreEqual("_all", actual["default_field"].Value<string>());
            var actualQueryString = actual["query"].Value<string>();
            StringAssert.Contains("AND", actualQueryString); // префиксами искать не получится, т.к. префикс распространяется на одно поле, у нас же их м.б. несколько
            // каждый элемент массива запроса должен быть представлен в query_String
            CollectionAssert.AreEquivalent(Enumerable.Repeat(true, input.Length),
                input.Select(actualQueryString.Contains));
        }

        /// <summary>
        /// Вообще бессмысленный тест, т.к. тупо показывает
        /// </summary>
        [Test]
        public void Search_ListOfResult_CorrectResultOnJSON()
        {
            int expected = 2;
            _content = @"{took: 12, timed_out: false, " +
                       "_shards: {total: 1, successful: 1, failed: 0}, " +
                       "hits: {total: " + expected + ", max_score: 2, hits: [{" +
                       @"_index: """ + _typeConfig.IndexName + @""", " +
                       @"type: """ + _typeConfig.IndexName + @""", " +
                       @"_id: ""FHJlufusRWKF9veAlXMnyA""," +
                       "_score: 2," +
                       "_source: {}}, {" +
                       @"_index: """ + _typeConfig.IndexName + @""", " +
                       @"type: """ + _typeConfig.IndexName + @""", " +
                       @"_id: ""FHJlufusRWKF9veAlXMnyB"", " +
                       "_score: 1.5, " +
                       "_source: {}}" +
                       "]}}";

            string[] input = { "test", "input" };

            string actualRequest = null;

            _result.Callback((HttpRequestMessage request, CancellationToken token) =>
            {
                actualRequest = request.Content.ReadAsStringAsync().Result;
            });

            var actual = _target.SearchByAnd(input);

            Assert.IsNotNull(actualRequest);
            Assert.AreEqual(expected, actual.Count());
        }


        [Test]
        [Ignore] // непонятна цель теста, к тому же он не выполняется.
        public void Search_ListOfResult_QueryWithFilter()
        {
            string[] input = { "Камаз" };

            string actualRequest = null;

            _result.Callback((HttpRequestMessage request, CancellationToken token) =>
            {
                actualRequest = request.Content.ReadAsStringAsync().Result;
            });

            //var expectedFilters = new List<FullTextFilter>()
            //{
            //    new FullTextFilter() {FieldName = "hasModifications", Value = true},
            //    new FullTextFilter() {FieldName = "Type", Value = "Легковые"}
            //};
            var answer = _target.SearchByAnd(input);

            Assert.IsNotNull(actualRequest);
            CollectionAssert.IsEmpty(answer);

            var response = JsonConvert.DeserializeObject(actualRequest) as dynamic;
            var actual = response.filter;

            //Assert.AreEqual(expectedFilters.Count, Enumerable.Count(actual.and));

            Assert.AreEqual(true.ToString().ToLower(), actual.and[0].term["hasModifications"].Value);
            Assert.AreEqual("Легковые", actual.and[1].term["Type"].Value);
        }
    }

    public class TestSearchResult
    {
    }
}
